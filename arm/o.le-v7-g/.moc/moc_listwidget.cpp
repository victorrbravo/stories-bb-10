/****************************************************************************
** Meta object code from reading C++ file 'listwidget.h'
**
** Created: Sun 20. Jan 22:52:04 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/inflow/listwidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'listwidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ListWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      23,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      20,   11,   12,   11, 0x0a,
      35,   27,   11,   11, 0x0a,
      52,   11,   11,   11, 0x0a,
      75,   66,   11,   11, 0x0a,
     106,   11,   11,   11, 0x2a,
     133,   66,   11,   11, 0x0a,
     166,   11,   11,   11, 0x2a,
     195,   66,   11,   11, 0x0a,
     233,   11,   11,   11, 0x2a,
     267,   66,   11,   11, 0x0a,
     294,   11,   11,   11, 0x2a,
     317,   11,   11,   11, 0x0a,
     348,  342,   11,   11, 0x0a,
     373,   66,   11,   11, 0x0a,
     395,   11,   11,   11, 0x2a,
     413,   66,   11,   11, 0x0a,
     444,   11,   11,   11, 0x2a,
     471,   11,   11,   11, 0x0a,
     481,   11,   11,   11, 0x0a,
     500,  498,   12,   11, 0x0a,
     522,  498,   12,   11, 0x0a,
     543,   11,   11,   11, 0x0a,
     560,   11,   11,   11, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_ListWidget[] = {
    "ListWidget\0\0QString\0text()\0newText\0"
    "setText(QString)\0updateCombo()\0inwidget\0"
    "updateComboVariableSafet(bool)\0"
    "updateComboVariableSafet()\0"
    "updateComboAutofilterSafet(bool)\0"
    "updateComboAutofilterSafet()\0"
    "updateComboRecursivefilterSafet(bool)\0"
    "updateComboRecursivefilterSafet()\0"
    "updateComboListTable(bool)\0"
    "updateComboListTable()\0updateComboListLiteral()\0"
    "value\0updateVarGlobal(QString)\0"
    "updateComboFlow(bool)\0updateComboFlow()\0"
    "updateComboConffileSafet(bool)\0"
    "updateComboConffileSafet()\0viewdoc()\0"
    "insertAndClose()\0s\0getRealValue(QString)\0"
    "getVarValue(QString)\0itemsValueList()\0"
    "itemsRealValueList()\0"
};

void ListWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ListWidget *_t = static_cast<ListWidget *>(_o);
        switch (_id) {
        case 0: { QString _r = _t->text();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 1: _t->setText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->updateCombo(); break;
        case 3: _t->updateComboVariableSafet((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->updateComboVariableSafet(); break;
        case 5: _t->updateComboAutofilterSafet((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: _t->updateComboAutofilterSafet(); break;
        case 7: _t->updateComboRecursivefilterSafet((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->updateComboRecursivefilterSafet(); break;
        case 9: _t->updateComboListTable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: _t->updateComboListTable(); break;
        case 11: _t->updateComboListLiteral(); break;
        case 12: _t->updateVarGlobal((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 13: _t->updateComboFlow((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: _t->updateComboFlow(); break;
        case 15: _t->updateComboConffileSafet((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: _t->updateComboConffileSafet(); break;
        case 17: _t->viewdoc(); break;
        case 18: _t->insertAndClose(); break;
        case 19: { QString _r = _t->getRealValue((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 20: { QString _r = _t->getVarValue((*reinterpret_cast< const QString(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 21: _t->itemsValueList(); break;
        case 22: _t->itemsRealValueList(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ListWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ListWidget::staticMetaObject = {
    { &CmdWidget::staticMetaObject, qt_meta_stringdata_ListWidget,
      qt_meta_data_ListWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ListWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ListWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ListWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ListWidget))
        return static_cast<void*>(const_cast< ListWidget*>(this));
    return CmdWidget::qt_metacast(_clname);
}

int ListWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CmdWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 23)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 23;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
