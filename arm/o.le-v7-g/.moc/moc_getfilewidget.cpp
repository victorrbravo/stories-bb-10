/****************************************************************************
** Meta object code from reading C++ file 'getfilewidget.h'
**
** Created: Sun 20. Jan 22:51:46 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/inflow/getfilewidget.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'getfilewidget.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_GetFileWidget[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   15,   14,   14, 0x0a,
      40,   14,   14,   14, 0x0a,
      53,   14,   14,   14, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_GetFileWidget[] = {
    "GetFileWidget\0\0newText\0setText(QString)\0"
    "showDialog()\0insertAndClose()\0"
};

void GetFileWidget::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        GetFileWidget *_t = static_cast<GetFileWidget *>(_o);
        switch (_id) {
        case 0: _t->setText((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->showDialog(); break;
        case 2: _t->insertAndClose(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData GetFileWidget::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject GetFileWidget::staticMetaObject = {
    { &CmdWidget::staticMetaObject, qt_meta_stringdata_GetFileWidget,
      qt_meta_data_GetFileWidget, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &GetFileWidget::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *GetFileWidget::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *GetFileWidget::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_GetFileWidget))
        return static_cast<void*>(const_cast< GetFileWidget*>(this));
    return CmdWidget::qt_metacast(_clname);
}

int GetFileWidget::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = CmdWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 3)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
