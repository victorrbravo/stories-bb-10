/****************************************************************************
** Meta object code from reading C++ file 'ftpwindow.h'
**
** Created: Sun 20. Jan 22:52:10 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/inflow/ftpwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'ftpwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_FtpWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x08,
      33,   10,   10,   10, 0x08,
      48,   10,   10,   10, 0x08,
      61,   10,   10,   10, 0x08,
      94,   78,   10,   10, 0x08,
     123,   10,   10,   10, 0x08,
     157,  136,   10,   10, 0x08,
     199,   10,   10,   10, 0x08,
     222,   10,   10,   10, 0x08,
     243,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_FtpWindow[] = {
    "FtpWindow\0\0connectOrDisconnect()\0"
    "downloadFile()\0uploadFile()\0"
    "cancelDownload()\0commandId,error\0"
    "ftpCommandFinished(int,bool)\0cdToParent()\0"
    "readBytes,totalBytes\0"
    "updateDataTransferProgress(qint64,qint64)\0"
    "enableDownloadButton()\0enableUploadButton()\0"
    "enableConnectButton()\0"
};

void FtpWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        FtpWindow *_t = static_cast<FtpWindow *>(_o);
        switch (_id) {
        case 0: _t->connectOrDisconnect(); break;
        case 1: _t->downloadFile(); break;
        case 2: _t->uploadFile(); break;
        case 3: _t->cancelDownload(); break;
        case 4: _t->ftpCommandFinished((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 5: _t->cdToParent(); break;
        case 6: _t->updateDataTransferProgress((*reinterpret_cast< qint64(*)>(_a[1])),(*reinterpret_cast< qint64(*)>(_a[2]))); break;
        case 7: _t->enableDownloadButton(); break;
        case 8: _t->enableUploadButton(); break;
        case 9: _t->enableConnectButton(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData FtpWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject FtpWindow::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_FtpWindow,
      qt_meta_data_FtpWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &FtpWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *FtpWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *FtpWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_FtpWindow))
        return static_cast<void*>(const_cast< FtpWindow*>(this));
    return QObject::qt_metacast(_clname);
}

int FtpWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
