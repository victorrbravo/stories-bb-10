// Navigation pane project template
import bb.cascades 1.0
import com.rim.example.custom 1.0
 
 
 
  
NavigationPane {
    id: navigationPane
  
   
    
    Page {
        // page with a picture thumbnail
        // property myWindow mymain
        id: firstpage
        Container {
            background: Color.White
//            layout: DockLayout {
//            }
                 ListView {
                        id: listView
                        objectName: "listView"
                        horizontalAlignment: HorizontalAlignment.Center
                        dataModel: MyListModel {
                            id: myListModel
                        }
                        property int activeItem: -1
                        contextActions: [
                        ActionSet {
                            // put context menu actions here
                            title: qsTr("Picture actions")
                            subtitle: qsTr("Set of the useful things to do ...")
                            ActionItem {
                                title: qsTr("Break")
                                onTriggered: {
                                    // define action handler here
                                    console.log ("action triggered: " + title + " active item: " + listView.activeItem)
                                    var flags = myListModel.value(listView.activeItem, "flags");
                                    if(!flags) flags = {};
                                    flags.broken = !flags.broken;
                                    title = flags.broken? qsTr("Unbreak"): qsTr("Break");
                                    myListModel.setValue(listView.activeItem, "flags", flags)
                                    myListModel.setValue(listView.activeItem, "image", assetForFlags(flags));
                                    myListModel.setValue(listView.activeItem, "status", statusForFlags(flags));
                                }
                            }
                            ActionItem {
                                title: qsTr("Hide")
                                onTriggered: {
                                    console.log ("action triggered: " + title)
                                    var flags = myListModel.value(listView.activeItem, "flags");
                                    if(!flags) flags = {};
                                    flags.hidden = !flags.hidden;
                                    title = flags.hidden? qsTr("Show"): qsTr("Hide");
                                    myListModel.setValue(listView.activeItem, "flags", flags)
                                    myListModel.setValue(listView.activeItem, "image", assetForFlags(flags));
                                    myListModel.setValue(listView.activeItem, "status", statusForFlags(flags));
                                }
                            }
                        }
                        ]
                        // Override default GroupDataModel::itemType() behaviour, which is to return item type "header"
                        listItemComponents: [
     
                            ListItemComponent {
                                type: "item"
                                StandardListItem {
                                    title: ListItemData.text
                                    description: ListItemData.description
                                }
                            }
                        ]
                        
                        onSelectionChanged: {
                            // slot called when ListView emits selectionChanged signal
                            // A slot naming convention is used for automatic connection of list view signals to slots
                            console.log ("onSelectionChanged, selected: " + selected)
                        }
                        onActivationChanged: {
                            console.log ("onActivationChanged, active: " + active)
                            if(active) listView.activeItem = indexPath[0]
                        }
                    }
               
            Button {
                id: mybutton
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                text: qsTr("Show detail")
                imageSource: "asset:///images/picture1thumb.png"
                onClicked: {
                 
                    var page = getSecondPage();
                    navigationPane.push(page);

                        console.log("Prueba log");
                        console.debug("Prueba log");
                }
                
                property Page secondPage
                
                function getSecondPage() {
                    if (!secondPage) {
                        secondPage = secondPageDefinition.createObject();
                    }
                    return secondPage;
                }
                attachedObjects: [
                             
                             MainWindow {
                                 id: myWindow
                             },
                             ComponentDefinition {
                                 id: secondPageDefinition
                                 source: "DetailsPage.qml"
                             }                                 
                            ]
            }
            
        }
    }
    onCreationCompleted: {
        // this slot is called when declarative scene is created
        // write post creation initialization here
        console.log("NavigationPane - onCreationCompleted()");

        // enable layout to adapt to the device rotation
        // don't forget to enable screen rotation in bar-bescriptor.xml (Application->Orientation->Auto-orient)
        OrientationSupport.supportedDisplayOrientation = SupportedDisplayOrientation.All;
              
         //        var result = myWindow.clearAllInfoStories();
            mybutton.text = "Press";
           
           
            if ( !myWindow.checkLoadSamples() ) { 
               myWindow.loadSamples();
        }
                 
//                 var result =  myWindow.checkLoadSamples();
          //               mybutton.text = "checkLoadSamples: " + result;
//                                    
         var  mymenu = "[ {\"text\":\"Tiramisu/Nov2010\",\"description\":\"Tiramisu/Nov2010\",\"status\":\"\",\"image\":\"asset:///images/picture1.png\"},{\"text\":\"Tulum/family12\",\"description\":\"Tulum/family12\",\"status\":\"\",\"image\":\"asset:///images/picture1.png\"} ]";
         
         myListModel.load(mymenu);
    }
    
 
}


 
