// Navigation pane project template
import bb.cascades 1.0 


Page {
    // page with a picture detail
    id: grDetail
    
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            onTriggered: {
                // define what happens when back button is pressed here
                // in this case is closed the detail page
                navigationPane.pop()
            }
        }
    }
    Container {
        background: Color.DarkYellow
        Label {
            text: qsTr("Page 3")
            horizontalAlignment: HorizontalAlignment.Center
            textStyle {
                base: SystemDefaults.TextStyles.TitleText
                color: Color.Yellow
            }
        }
        ImageView {
            id: imgView
            imageSource: "asset:///images/picture1.png"
            horizontalAlignment: HorizontalAlignment.Center
        }
        Label {
            text: qsTr("Picture description")
            horizontalAlignment: HorizontalAlignment.Center
        }
    }
}
