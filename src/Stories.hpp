// Navigation pane project template
#ifndef Stories_HPP_
#define Stories_HPP_

#include <QObject>

namespace bb { namespace cascades { class Application; }}

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class Stories : public QObject
{
    Q_OBJECT
public:
    Stories(bb::cascades::Application *app);
    virtual ~Stories() {}

};

#endif /* Stories_HPP_ */
