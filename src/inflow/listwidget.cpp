/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/

#include "listwidget.h"
#include "mainwindow.h"
#include "inflowfirmadoc.h"
//#include "ui_inflowfirmadoc.h"

ListWidget::ListWidget(const QString& s, ListWidget::Type t, QObject *parent,bool istextparent)
    : CmdWidget(s, parent, istextparent)
{

     _type = t;
     //configurator = new SafetYAWL();
}

ListWidget::~ListWidget() {

     //if (configurator) delete configurator;
}

void ListWidget::setText(const QString &newText) {
    qDebug("...ListWidget...newText:|%s|", qPrintable(newText));

}

void ListWidget::updateCombo() {

     switch (type() ) {

          case VariableSafet:
               updateComboVariableSafet();
               break;
         case AutofilterSafet:
              updateComboAutofilterSafet();
              break;
         case RecursivefilterSafet:
             updateComboRecursivefilterSafet();
           break;
         case ListTable:
               updateComboListTable();
               break;
          case ListLiteral:
               updateComboListLiteral();
               break;
          case Flow:
               updateComboFlow();
               break;
     case ConffileSafet:
               updateComboConffileSafet();
               break;

          default:;
     }

}

void ListWidget::updateComboConffileSafet(bool inwidget) {
    QStringList options = conf()["options"].toString().split(":");
         if (options.count() < 1 ) {
         SafetYAWL::streamlog
                 << SafetLog::Warning
                 << tr("IMPORTANTE, No se defini� correctamente el campo \"options\" --> \"%1\"").arg(conf()["options"].toString());
         return;
    }

    QString field = options.at(0);
    if (!field.endsWith(".*")) {
        SafetYAWL::streamlog
                << SafetLog::Warning
                << tr("IMPORTANTE, Debe finalizar en \".*\". No se defini� "
                      "correctamente el campo \"options\" --> \"%1\"").arg(conf()["options"].toString());

    }
    int count = 1;
    QString pattern = field.mid(0,field.length()-2)+QString(".%1");
    while (true) {
        QString newfield = pattern.arg(count);
        if ( !SafetYAWL::getAuthConf().contains(newfield)) {
            break;
        }
        _itemvaluelist.append(SafetYAWL::getAuthConf()[newfield]);

        count++;
    }

}

bool ListWidget::isValid(QString& value) {

    //if ( !CmdWidget::isValid(value)) return false;


    switch(type()) {
    case ListLiteral:
        if ( !conf().contains("options")) {
            SafetYAWL::streamlog
                    << SafetLog::Error
                    << tr("Falla la validacion del widget combo: \"%1\"")
                    .arg(caption());
            return false;
        }
        generateLiterals(conf()["options"].toString(),false);
                break;
    case ListTable:
        updateComboListTable(false);

        break;
    case VariableSafet:
        updateComboVariableSafet(false);

        break;
    case AutofilterSafet:
        updateComboAutofilterSafet(false);
        break;
   case RecursivefilterSafet:
        updateComboRecursivefilterSafet(false);
        break;
        break;
   case Flow:
        updateComboFlow(false);
        break;
     case ConffileSafet:
        updateComboConffileSafet(false);
        break;
    default:;
    }

    if ( type() == ListLiteral) {

        qDebug("...ListLiteral...itemsRealValueList().count():%d",
               itemsRealValueList().count());
        if ( !itemsRealValueList().contains(value)) {
            return false;
        }
    }
    else if(type() == ListTable) {
        qDebug("value: |%s|",qPrintable(value));
        qDebug("...ListTable...itemsValueList().count():%d",
               itemsValueList().count());

        foreach(QString s, itemsValueList()) {
            qDebug("...s: |%s|",qPrintable(s));
        }

        QStringList myvalues = value.split(";",QString::SkipEmptyParts);
        foreach(QString v, myvalues) {
            QString myrealv = getRealValue(v);
            qDebug("myrealv: |%s|",qPrintable(myrealv));
            if ( !itemsRealValueList().contains(myrealv)) {
                return false;
            }
        }

    }
    else {
        qDebug("...validating:...|%s|",qPrintable(value));
        QStringList myvalues = value.split(";");
        foreach(QString myvalue, myvalues) {
            if ( !itemsValueList().contains(myvalue)) {
                return false;
            }
        }
    }
    return true;
}
void ListWidget::updateVarGlobal(const QString& value) {
    if ( varvalues.keys().contains(value)) {
        qDebug("               *****(1)value: |%s|combovarglobal SafetYAWL::combovarglobal0: |%s|",
               qPrintable(value),qPrintable(SafetYAWL::combovarglobal0));
        SafetYAWL::combovarglobal0 = getVarValue(value);
        qDebug("               *****(2)...value: |%s| combovarglobal SafetYAWL::combovarglobal0: |%s|",
               qPrintable(value),qPrintable(SafetYAWL::combovarglobal0));
    }

}


void ListWidget::updateComboFlow(bool inwidget) {
     infos.clear();

     if ( conf().contains("path") ) {
          if (MainWindow::configurator != NULL ) delete  MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( MainWindow::configurator );

          SafetYAWL::streamlog
                  << SafetLog::Debug
                  << tr("DOMMODEL ComboFlow: path: |%1|").arg(conf()["path"].toString());

          MainWindow::configurator->openXML(conf()["path"].toString());
          MainWindow::configurator->convertXMLtoObjects();
          MainWindow::configurator->openDataSources();

          if ( MainWindow::configurator->getWorkflows().count() > 0 ) {             
               infos = MainWindow::configurator->getWorkflows().at(0)->listNextStates(conf()["keyvalue"].toString());
               if ( infos.count() > 0 ) {
                    if ( infos.at( infos.count()-1) == "<SIGN>" ) {
                         buildButtonWidget(infos);
                         return;
                    }

               }
          }

          _itemvaluelist = infos;

     }
     foreach(QString s, _itemvaluelist) {
         SafetYAWL::streamlog
                 << SafetLog::Debug
                 << tr("DOMMODEL inwidget: %2 ComboFlow: _itemvaluelist: |%1|").arg(s).arg(inwidget);

     }



}

void ListWidget::buildButtonWidget(const QStringList& l) {
     qDebug("...buildButtonWidget...");


}

void ListWidget::insertAndClose() {
/*
    QString str;
    if ( _texteditparent ) {
        qDebug("...insertAndClose...");
        if ( varboxlist ) {

            for(int i=0; i < varboxlist->count();i++) {
                QListWidgetItem *myitem = varboxlist->item(i);
                if ( myitem ) {
                    if (myitem->checkState() == Qt::Checked ) {
                        str += myitem->text();
                        str += ";";
                    }
                }
            }
            if (!str.isEmpty()) {
                str.chop(1);
                _texteditparent->insertPlainText(str.toLatin1());
                _texteditparent->insertPlainText("\n");
            }

        }
    }
    close();*/
}


void ListWidget::viewdoc() {

}


void ListWidget::buildWidget() {

         updateCombo();

}

void ListWidget::updateComboListLiteral() {
    if ( !conf().contains("options")) {
          return;
      }
      generateLiterals(conf()["options"].toString());



}

void ListWidget::generateLiterals(const QString& s, bool inwidget) {


}

QString ListWidget::getRealValue(const QString& s) {

    if ( !realvalues.keys().contains(s)) {
        return s;
    }
    return realvalues[ s];
}

QString ListWidget::getVarValue(const QString& s) {

    if ( !varvalues.keys().contains(s)) {
        return s;
    }
    return varvalues[ s];
}

void ListWidget::updateComboListTable(bool inwidget) {

     QSqlQuery query(SafetYAWL::currentDb ); // <-- puntero a db actual
     QString field, table, where, value;
     if ( !conf().contains("options")) return;

     QStringList options = conf()["options"].toString().split(":");
          if (options.count() < 2 ) {
          SafetYAWL::streamlog
                  << SafetLog::Warning
                  << tr("IMPORTANTE, No se definio correctamente el campo \"options\" --> \"%1\"").arg(conf()["options"].toString());
          return;
     }
     field = options.at(0);

     QString realvalue;
     _itemrealvaluelist.clear();
     _itemvaluelist.clear();
     table = options.at(1);
     QString command;
     if ( options.count() > 2 ) {
          where = options.at(2);
          if ( options.count() > 3 ) {
              QString titleField = options.at(3);
              if ( options.count() >  4 ) {
                  QString varglobal = options.at(4);
                  if ( !titleField.isEmpty()) {
                command = QString("SELECT %1,%4,%5 from %2 %3;")
                          .arg(field).arg(table)
                          .arg(where)
                          .arg(titleField)
                          .arg(varglobal);
                }
                  else {
                      command = QString("SELECT %1,'n/a' as safetnotapplicable,%4 from %2 %3;")
                                .arg(field).arg(table)
                                .arg(where)
                                .arg(varglobal);
                  }

              }
              else {
                  if ( !titleField.isEmpty()) {
                command = QString("SELECT %1,%4 from %2 %3;")
                          .arg(field).arg(table)
                          .arg(where)
                          .arg(titleField);
                }
                  else {
                      command = QString("SELECT %1 from %2 %3;")
                                .arg(field).arg(table)
                                .arg(where);
                  }
            }

          }
          else {
            command = QString("SELECT %1 from %2 %3;").arg(field).arg(table).arg(where);
          }
     }
     else {
          command = QString("SELECT %1 from %2;").arg(field).arg(table);
     }

     SafetYAWL::streamlog << SafetLog::Debug << tr("Actualizando lista del combo: \"%1\"").arg( command );
     query.prepare(  command );
     bool executed = query.exec();
     if (!executed ) {
          SafetYAWL::streamlog << SafetLog::Warning << tr("IMPORTANTE, NO se ejecut� correctamente la consulta de opciones del combo: \"%1\"").arg(command);
          return;
     }
     bool istitlefield  = query.record().count() > 1 && query.record().fieldName(1) != "safetnotapplicable";

     bool isvarglobal = query.record().count() > 2;

//     qDebug(".....istitlefield: %d", istitlefield);
//     qDebug(".....isvarglobal: %d", isvarglobal);
//     qDebug(".....command: %s", qPrintable(command));
//     qDebug(".....query.record().count(): %d", query.record().count());
//     qDebug();
     while( query.next() ) {
           realvalue = query.value(0).toString().trimmed();
          if (!_itemrealvaluelist.contains(realvalue) ) {
              _itemrealvaluelist.append(realvalue);
              if ( istitlefield ) {
                  value = query.value(1).toString().trimmed();
              }
              else {
                  value = realvalue;
              }
              _itemvaluelist.append(value);
              realvalues[ value ] = realvalue;
              if ( isvarglobal ) {
//                  qDebug("........isvarglobal:....:value [ %s ] --> %s",
//                         qPrintable(value), qPrintable(query.value(2).toString().trimmed()));
                  varvalues[ value ] = query.value(2).toString().trimmed();
              }
          }

     }
     if ( inwidget ) {

         foreach(QString newitem, _itemvaluelist) {
             if (newitem.trimmed().isEmpty()) {
                 continue;
             }

             //varboxlist->addItem(listitem);
         }

     }
 }



void ListWidget::updateComboVariableSafet(bool inwidget) {
    if (MainWindow::configurator == NULL ) {
        return;
    }
     if ( conf().contains("path") ) {
          delete  MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( MainWindow::configurator );

          qDebug("...path: %s", qPrintable(conf()["path"].toString()));
          QString myautofilter;
          QString myrecursivefilter;
          MainWindow::configurator->openXML(conf()["path"].toString());
          if ( conf().contains("autofilter")) {
              myautofilter = conf()["autofilter"].toString().trimmed();
              qDebug("        ****ComboWidget::updateComboVariableSafet autofilter: |%s|",
                     qPrintable(myautofilter));
              qDebug("        ****ComboWidget::updateComboVariableSafet conf()[\"path\"].toString(): |%s|",
                     qPrintable(conf()["path"].toString()));
              MainWindow::configurator->setAutofilters(myautofilter);
          }

          if ( conf().contains("recursivefilter")) {
              myrecursivefilter = conf()["recursivefilter"].toString().trimmed();
              qDebug("        ****ComboWidget::updateComboVariableSafet autofilter: |%s|",
                     qPrintable(myrecursivefilter));
              MainWindow::configurator->setRecursivefilters(myrecursivefilter);
          }

          MainWindow::configurator->openDataSources();
          MainWindow::configurator->convertXMLtoObjects();


          if ( MainWindow::configurator->getWorkflows().count() > 0 ) {
               _itemvaluelist = MainWindow::configurator->getWorkflows().at(0)->variablesId().toList();
          }

     }
     qDebug("...(4)....***...updateComboVariableSafet....updateComboVar()....");
}



void ListWidget::updateComboRecursivefilterSafet(bool inwidget) {
    if (MainWindow::configurator == NULL ) return;
    if ( conf().contains("path") ) {
         delete  MainWindow::configurator;
         MainWindow::configurator = new SafetYAWL();
         Q_CHECK_PTR( MainWindow::configurator );

         qDebug("...path: %s", qPrintable(conf()["path"].toString()));
         MainWindow::configurator->openXML(conf()["path"].toString());

         MainWindow::configurator->convertXMLtoObjects();

         MainWindow::configurator->openDataSources();

         if ( MainWindow::configurator->getWorkflows().count() > 0 ) {
             _itemvaluelist = MainWindow::configurator->getWorkflow()->recursivefiltersId().toList();
         }


    }

}


void ListWidget::updateComboAutofilterSafet(bool inwidget) {
     if (MainWindow::configurator == NULL ) return;
     qDebug("...***....(1)....updateComboAutofilterSafet....updateCombo()....");
     if ( conf().contains("path") ) {
          delete  MainWindow::configurator;
          MainWindow::configurator = new SafetYAWL();
          Q_CHECK_PTR( MainWindow::configurator );

          qDebug("...path: %s", qPrintable(conf()["path"].toString()));
          MainWindow::configurator->openXML(conf()["path"].toString());

          MainWindow::configurator->convertXMLtoObjects();
          MainWindow::configurator->openDataSources();

          if ( MainWindow::configurator->getWorkflows().count() > 0 ) {
               _itemvaluelist = MainWindow::configurator->getWorkflow()->autofiltersId().toList();
          }



     }
     qDebug("...(4)....***...updateComboVariableSafet....updateComboVar()....");
}


QString ListWidget::text() const {


     return QString("");
}

void ListWidget::getGeoParams() const {
}



