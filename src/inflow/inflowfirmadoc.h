#ifdef INFLOWFIRMADOC_H
#define INFLOWFIRMADOC_H

#include <QtGui/QDialog>
77#include <QPrinter>
#include "combowidget.h"
#include "SafetWorkflow.h"

class ComboWidget;

namespace Ui {
    class InflowFirmaDoc;
}

class InflowFirmaDoc : public QDialog {
    Q_OBJECT
    Q_DISABLE_COPY(InflowFirmaDoc)
    bool jscriptload;
    QString currentDocuments;
    QString _currentSigners;
    QString _entityToSign;
    QString jscriptcolumns;
    QString _listprincipaltitle;
    int _listprincipalcount;
    QList<QSqlField> currentFields;
    QString evalJS(const QString &js);
    QString getScriptLen(const QSqlField& f); // Calcular la longitud del campo para la tabla generada en Listar_Datos
public:
    explicit InflowFirmaDoc(QWidget *parent = 0);
    virtual ~InflowFirmaDoc();
    void generateJScriptreports(const QString& documents,
                                                const QList<QSqlField>& fields,
                                                int ncounts,
                                                const QString& entitytosign,
                                                QList<SafetWorkflow::InfoSigner>& signers);

    void loadSignersInfo(const QList<SafetWorkflow::InfoSigner>& l);
    void loadReportTemplate();
    friend class ComboWidget;
protected:
    virtual void changeEvent(QEvent *e);

private:
    Ui::InflowFirmaDoc *m_ui;
private slots:
    void executeJSCodeAfterLoad(bool ok );
    void doPrint();
    void savepdf();
    void viewpdf();
};

#endif // INFLOWFIRMADOC_H
