/*
* SAFET Sistema Automatizado para la Firma ElectrÃ³nica y Estampado de Tiempo
* Copyright (C) 2008 VÃ­ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e InvestigaciÃ³n en TecnologÃ­as Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los tÃ©rminos de la licencia de 
* software GPL versiÃ³n 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea Ãºtil, pero SI NINGUNA GARANTÃ�A;
* tampoco las implÃ­citas garantÃ­as de MERCANTILIDAD o ADECUACIÃ“N A UN PROPÃ“SITO PARTICULAR.
* Consulte la licencia GPL para mÃ¡s detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5Âº Piso, Boston, MA 02110-1301, USA.
*
*/

#ifdef DIALOGFLOWPARAMETERS_H
#define DIALOGFLOWPARAMETERS_H



#include <QDialog>
#include <QGridLayout>
#include "SafetParameter.h"
#include "cmdwidget.h"


namespace Ui {
    class DialogFlowParameters;
}

class DialogFlowParameters : public QDialog
{
    Q_OBJECT

    QList<SafetParameter*> _parameters;

    QList<CmdWidget*> _widgets;

    QGridLayout *boxlayout;


public:
    explicit DialogFlowParameters(QWidget *parent = 0);
    ~DialogFlowParameters();


    QList<SafetParameter*>& parameters() { return _parameters;}
    void layoutParameters();
    CmdWidget* getWidget(SafetParameter* p);

    QStringList getValues();
private:
    Ui::DialogFlowParameters *ui;
};

#endif // DIALOGFLOWPARAMETERS_H
