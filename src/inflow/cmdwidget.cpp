/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/

#include "cmdwidget.h"
#include "SafetYAWL.h"
#include <QCryptographicHash>


CmdWidget::CmdWidget(const QString& t, QObject *parent,bool istextparent)
 : QObject(parent),
 _conf()
{
    _istextparent = istextparent;
    if ( isTextParent()) {
//        _texteditparent = qobject_cast<QTextEdit*>(parent);
//        if ( _texteditparent == NULL ) {
//             qDebug(".......ERROR...ERROR...CmdWidget::CmdWidget...._texteditparent = NULL");
//        }
//        texteditpos = _texteditparent->textCursor().position();
    }
    _caption = t;    
   // principalWidget = NULL;

    _ispassword = false;
    _typeinput = " type=\"text\" ";
    setFocus(Qt::ActiveWindowFocusReason);
    _options.append(QLatin1String(""));
}


QString CmdWidget::html() {
    QString result;
    QStringList options = conf()["options"].toString().split(",");
    foreach(QString option, options) {
        if (option.startsWith("type::")) {
            _typeinput = option.replace("::","=");
        }
    }
    result = QString("<input %2 name=\"%1\" alt=\"Esto es un campo tipo texto\" id=\"%1\""
                     "size=\"55\"></input>")
            .arg(_caption)
            .arg(_typeinput);

    return result;
}

QString CmdWidget::qml() {
    QString result;
    QStringList options = conf()["options"].toString().split(",");
    QString properties;
    foreach(QString option, options) {
        if (option.startsWith("property::")) {
            properties += option.mid(QString("property::").length());
            properties += "\n";
        }
    }

    result = QString(
                "Row {\n"
                 "id:row%2\n"
                 "height: 90\n"
                 "width: parent.width\n"
                 "spacing: 30\n"
                "TextBox {\n"
                 "       selecttitle: \"%1\"\n"
                "%3"
                 "       }\n"
                "}\n"
                "\n"
                )
            .arg(_caption)
            .arg(_caption.left(2)+_caption.simplified().right(1))
            .arg(properties);

    return result;
}

QString CmdWidget::qmlForKey(const QString& k) {
    QString result;
    if ( conf().contains(QLatin1String("keyvalue"))) {
        return qml();
    }
    return result;
}

QString CmdWidget::htmlForKey(const QString& k) {

    QString result;
    if ( conf().contains(QLatin1String("keyvalue"))) {
        return html();
    }
    return result;

}

void CmdWidget::getGeoParams() const {
//     QRect result;
//     if ( lineedit == NULL) return QRect(0,0,280,36);
//
//     result.setHeight( lineedit->height() );
//     result.setWidth( lineedit->width()+lblvalidator->width()+quitbutton->width()+okbutton->width() );
//
//     return result;
}


void CmdWidget::setText(const QString &newText)  {

}

void CmdWidget::setChangefor(const QString& c) {
    if (c.isEmpty()){
        SYE << tr("Se introdujo un campo vac�o: \"%1\" para el widget")
               .arg(c);
        return;
    }
    _changefor = c.split(",",QString::SkipEmptyParts);


}

void CmdWidget::buildWidget() {
//     lineedit = new QLineEdit;
    _usertooltip = tr("Check field length (from 2 to 18 char) and characters allowed (Alphanumeric)");
}


void CmdWidget::doReturn() {

}

CmdWidget::~CmdWidget()
{
//    if ( lineedit != NULL ) {
//        delete lineedit;
//    }
}

bool CmdWidget::isValid(QString& value) {
    if (value.isEmpty()) {
        SYW << tr("No se lleno el valor para un widget");
        return false;
    }

    if ( !conf().contains("validation")) {
        return true;
    }
    QString mypattern = conf()["validation"].toString().trimmed();

    if (mypattern.isEmpty()) {
        return true;
    }

    QRegExp rx;
    rx.setPattern(mypattern);
    QString myvalue = value.trimmed();

    SYD << tr("...CmdWidget::isValid...mypattern:|%1|....myvalue=:|%2|")
           .arg(mypattern)
           .arg(myvalue);

    bool result = rx.exactMatch(myvalue);
    qDebug("...CmdWidget::isValid...myvalue:|%s|",qPrintable(myvalue));
    qDebug("...CmdWidget::isValid...mypattern:|%s|",qPrintable(mypattern));
    return result;
}

void CmdWidget::insertAndClose() {

}

void CmdWidget::setOptionsProperties(const QStringList ps) {
//    if (principalWidget == NULL ) {
//        qDebug("...principalWidget == NULL...");
//        return;
//    }
//    foreach(QString p, ps){
//        QStringList  fieldproperty = p.split("::");
//        if ( fieldproperty.count() > 1 ) {
//            QString localproperty = fieldproperty.at(0);
//            QVariant value = fieldproperty.at(1);
//            bool result = principalWidget->setProperty(qPrintable(localproperty),value);
//            qDebug("      CmdWidget::setOptionsProperties...localproperty: %s", qPrintable(localproperty));
//            qDebug("      CmdWidget::setOptionsProperties...value: %s", qPrintable(value.toString()));
//            if (!result) {
//                qDebug("La propiedad de nombre \"%s\" no existe",qPrintable(localproperty));
//            }
//            else {
//                if (QString("echoMode") == localproperty &&
//                    QString("Password") == value.toString() ) {
//                    qDebug("isPassword");
//                    _ispassword = true;
//                }
//            }
//
//        }
//    }

}

void CmdWidget::cancelAndClose() {

}

QString CmdWidget::text() const {
//    if (lineedit != NULL ) {
//        return lineedit->text();
//    }
    return QString("");
}

void CmdWidget::setFocus ( Qt::FocusReason reason ) {


/*     QWidget::setFocus ( reason );
     if ( lineedit != NULL ) {
          lineedit->setFocus( reason);
     }
  */   
}



void CmdWidget::changeLblValidator(const QString& text) {

//    QString mytext = text;
//    if ( validator() == NULL ) {
//        if ( !text.isEmpty()) {
//            lblvalidator->setText( "<b><font color=\"Green\">+</font></b>" );
//        }
//        else {
//            lblvalidator->setText( "<b><font color=\"Red\">-</font></b>" );
//        }
//    }
//    else  {
//     int pos = 0;
//     if ( validator()->validate(mytext, pos ) == QValidator::Acceptable ) {
//	lblvalidator->setText( "<b><font color=\"Green\">+</font></b>" );
//     }
//     else {
//	 lblvalidator->setText( "<b><font color=\"Red\">-</font></b>" );
//       }
//    }

	
}
QStringList CmdWidget::options() const {

    return _options;
}


