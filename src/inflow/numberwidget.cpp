/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 V�ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/
#include "numberwidget.h"
#include "SafetYAWL.h"

NumberWidget::NumberWidget(const QString& t, QObject *parent, bool istextparent )
    :CmdWidget(t,parent, istextparent) {



}

void NumberWidget::setText(const QString &newText) {


}


void NumberWidget::getGeoParams() const {

}

void NumberWidget::buildWidget() {
     qDebug("...NumberWidget...buildWidget...");
     QString mytip = tr("Campo Num�rico. Escriba un n�mero entero o decimal");
     _usertooltip = mytip;

/*     spinboxedit = NULL;
     if ( conf().contains("options"))  {

         setOptionsProperties(conf()["options"].toString().split(","));
         if ( conf()["options"].toString().split(",").contains("decimal") ) {
             spinboxedit = new QDoubleSpinBox();
         }
         else {
            spinboxedit = new QSpinBox();
            (qobject_cast<QSpinBox*>(spinboxedit))->setRange(0,9999);
         }
     }
     else {
         spinboxedit = new QSpinBox();
         (qobject_cast<QSpinBox*>(spinboxedit))->setRange(0,9999);
     }

     principalWidget = spinboxedit;
     if ( conf().contains("validation")) {
         QStringList mylist = conf()["validation"].toString().split("::");
         if (mylist.count() > 1 ) {
             QString usertip = mylist.at(1);
             mytip = usertip;
         }
     }

     spinboxedit->setToolTip(mytip);

     setOptionsProperties(conf()["options"].toString().split(",")); // Colocar las propiedades
     spinboxedit->setGeometry(0,0,350,30);



     if (isTextParent()) {
         okbutton = new QToolButton;
         okbutton->setGeometry(0,0,25,30);
         okbutton->setIcon(QIcon(":/yes.png"));
         quitbutton = new QToolButton;
         quitbutton->setGeometry(0,0,25,30);
         quitbutton->setText( "X");
     }
     mainLayout = new QHBoxLayout;
     mainLayout->addWidget(spinboxedit);
     //mainLayout->addWidget(lblvalidator);
     mainLayout->setSpacing( 1 );

     if ( isTextParent()) {
        mainLayout->addWidget(quitbutton);
        mainLayout->addWidget(okbutton);
        connect(okbutton, SIGNAL(clicked()), _texteditparent, SLOT(insertAndClose()) );
        connect(quitbutton, SIGNAL(clicked()), _texteditparent, SLOT(cancelAndClose()) );
    }
//     setLayout(mainLayout);
*/
}

QString NumberWidget::qml() {
    QString result;
     QStringList options = conf()["options"].toString().split(",");
    QString properties ;
    foreach(QString option, options) {
        if (option.startsWith("property::")) {
            properties += option.mid(QString("property::").length());
            properties += "\n";
        }
    }
    result = QString(
                "Row {\n"
                "id:row%2\n"
                "height: 65\n"
                "width: parent.width\n"
                "spacing: 30\n"
                "NumberBox {\n"
                 "        selectdesc : \"%1\"\n"
                 "       selecttitle : \"%1\"\n"
                 "%3"
                 "         }\n"
                "     }\n"
                "\n"
                )
            .arg(caption())
            .arg(caption().left(2)+caption().simplified().right(1))
            .arg(properties);

    return result;
}

void NumberWidget::setFocus ( Qt::FocusReason reason ) {
/*     qDebug("...NumberWidget::setFocus....(text)");
     QWidget::setFocus ( reason );
     spinboxedit->setFocus( reason);*/

}
bool NumberWidget::isValid(QString& value) {
    bool okint, okfloat;
    int vi = value.toInt(&okint);
    float vf = value.toDouble(&okfloat);

    return okint || okfloat;

}


QString NumberWidget::text() const {
    QString result;
       return result;
 }
