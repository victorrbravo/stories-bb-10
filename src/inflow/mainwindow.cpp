/****************************************************************************
**
** Copyright (C) 2006-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
/*
* SAFET Sistema Automatizado para la Firma Electr?nica y Estampado de Tiempo
* Copyright (C) 2008 V?ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci?n en Tecnolog�as Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t?rminos de la licencia de
* software GPL versi?n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea ?til, pero SI NINGUNA GARANT?A;
* tampoco las impl??citas garant??as de MERCANTILIDAD o ADECUACI�N A UN PROP?SITO PARTICULAR.
* Consulte la licencia GPL para m?s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5? Piso, Boston, MA 02110-1301, USA.
*
*/

//#include <QtGui>
#include <QtXml>
//#include <QCloseEvent>
//#include <QGraphicsSvgItem>
//#include <QToolBar>
#include <QLatin1String>
//#include <QWebFrame>
//#include <QDesktopServices>


#include <getopt.h>
#include "mainwindow.h"
#include "textedit.h"
#include "qcmdcompleter.h"
#include "SafetTextParser.h"
#include "assistant.h"
#include "threadconsole.h"
#ifdef SAFET_OPENSSL
#include <openssl/rand.h>
#include "SafetCipherFile.h"
#endif
#include "dialogflowparameters.h"
#include "combowidget.h"
#include "getfilewidget.h"
#include "listwidget.h"
#include "texteditwidget.h"
#include "datetimewidget.h"
#include "numberwidget.h"
#include "suggestwidget.h"
#include "SafetBinaryRepo.h"
#include "ftpwindow.h"

// Variables Est�ticas


SafetYAWL* MainWindow::configurator = NULL;
//QStringList  MainWindow::sbmenulist;
MainWindow *MainWindow::mymainwindow = NULL;
QString MainWindow::currentaccount = "";
QString MainWindow::currentrole = "";
QString MainWindow::_lastchangekey = "";
QString MainWindow::showString = "";
QString MainWindow::_lasttitle = "";
QString MainWindow::_lastoperation = "";
QString MainWindow::_currconfpath = "";
bool MainWindow::_issigning = false;
QStringList MainWindow::_qmlpages;
QString MainWindow::_currentCommonName = "";
QMap<QString,QPair<QString,QString> > MainWindow::_graphs;

//ThreadConsole MainWindow::myThreadConsole(NULL);

QMap<QString,QStringList> MainWindow::permises; // Estructura para guardar los permisos

int MainWindow::_lastnumbersteps = 0;
QTimer MainWindow::_panelinfotimer;
int MainWindow::_seconstohide = 0;

//ThreadConsole * MainWindow::myThreadConsole = NULL;

MainWindow::MainWindow(const QString& path)
    : streamText( &outputText )
{

    _hastories = false;

    _wfnumpars  = 0; // Contador temporal utilizado para calcular el n�mero de par�metro en la funci�n GetWidget
    MainWindow::_lastnumbersteps = 0;
     //** Inicializacion de variables
     //** ...

     _isloadeditactions = false;  // No se han cargado las acciones
     SafetYAWL::listDynWidget = NULL;

     parsed = false;
     MainWindow::mymainwindow = this; // Colocando direcci?n de MainWindow
     _listprincipalcount = 0; // Contador para la variable actual

     _checkgoprincipal = false;
     _issmartmenuvisible = false;
    // ** Colocar los manejadores para Los servicioes del Escritorio (QDesktopServices)


     // ** Configurando el Menu de la pantalla principal

     _currentjson = QLatin1String("");

     // Colocar estilo principal


     _dirInput = QDir::homePath()+"/.safet/input";
     if ( !_dirInput.endsWith("/") ) _dirInput.append("/");

     _pathdoc = "deftrac.xml";

     filecompleters.append( _dirInput+_pathdoc);

     filecompleters.append( _dirInput + SAFETCONSOLEFILE );
     filecompleters.append( _dirInput + SAFETCONFIGFILE );
     filecompleters.append( _dirInput + SAFETMANAGEMENTSIGNFILE );
     filecompleters.append( _dirInput + SAFETUSERSFILE );




     loadSettings();


     SafetYAWL::fileout.close();
     SafetYAWL::filelog.close();
     QString logpath = QDir::homePath()+"/.safet/log";


     SafetYAWL::fileout.open(stdout, QIODevice::WriteOnly);
     SafetYAWL::streamout.setDevice(&SafetYAWL::fileout);



     SafetYAWL::filelog.setFileName(logpath + "/"+ "safet.log");
     qDebug("....MainWindow::MainWindow....asignado archivo log: |%s|",
            qPrintable(logpath + /*"/safet.log"*/ SafetYAWL::FILENAMELOG));
     SafetYAWL::filelog.open(QIODevice::Append);

#ifdef SAFETLOG_STDOUT
     SafetYAWL::streamlog.setDevice(&SafetYAWL::fileout);
#else
     SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);
#endif

     SafetYAWL::streamlog.turnOnAll();

     runningConsole = false;

     jscriptload = false; // No cargar el script




    initApp = false;

    // *** Conexiones

    // para mostrar el assistant
    // Colocar la Configuracion Local
    QLocale::setDefault(QLocale::C);
//    setlocale(LC_ALL, "C");




    if (path.isEmpty()) {
        //_currconfpath = QDir::homePath();
        _currconfpath = QDir::homePath()+"";
        SafetYAWL::pathconf = _currconfpath + "/" + Safet::datadir;
    }
    else {
        _currconfpath = path;
        SafetYAWL::pathconf = path + "/" + Safet::datadir;
    }

    QDir mydir(SafetYAWL::pathconf);
    if (!mydir.exists()) {
        clearAllInfoStories();
        qDebug("clearing....");
    }

    qDebug("...MainWindow::MainWindow....SafetYAWL::pathconf:|%s|",
           qPrintable(SafetYAWL::pathconf));

    qDebug("...MainWindow::MainWindow...._currconfpath:|%s|",
           qPrintable(_currconfpath));

    QString authconfbin = _currconfpath+ "/.safet/auth.conf.bin";
    if (!QFile::exists(authconfbin)) // no existe el auth.conf.bin asi que se hace todo el proceso
    {
     

    }

    MainWindow::configurator = new SafetYAWL(_currconfpath + "/" + Safet::datadir);
    //if ( countplug > 0 ) {
        SafetYAWL::loadAllConfFiles(3 /* AuthConf */ );
    //}


    if( MainWindow::configurator ) {
        MainWindow::configurator->openDataSources();
    }

    SYD << tr("....MainWindow::MainWindow...SafetYAWL::pathconf...:|%1|")
           .arg(SafetYAWL::pathconf);
    
    loadAuthorization();
    loadInfo();


    SYD << tr("....MainWindow::MainWindow.....loadinfo");
    bool islogin = login("vbravo","vbravo");

    SYD << tr("....MainWindow::MainWindow.....login");

    QString myinput  = _currconfpath + "/" + Safet::datadir + "/input/deftrac.xml";
    setInputPath(myinput);

    SYD << tr("....MainWindow::MainWindow.....myinput:%1").arg(myinput);
}


QStringList MainWindow::testQML() {
    QStringList mylist;
    mylist.append("page 1");
    mylist.append("page 2");
    return mylist;
}


void MainWindow::changeStoriesParams(const QString& usesensor, const QString& infodialog, const QString& typeinfog,
                                     const QString& usecamera) {

    QString myaction = QString("operacion:Values_to_store UseSensor:%1\n ShowInfoDialog:%2\n TypeInfographic:%3 UseCamera: %4")
            .arg(usesensor)
            .arg(infodialog)
            .arg(typeinfog)
            .arg(usecamera);


    qDebug("\n\n.....***MainWindow::changeStoriesParams myaction:|%s|", qPrintable(myaction));

    QString result = toInputConfigure(myaction);


    qDebug(".....***MainWindow::changeStoriesParams result:|%s|", qPrintable(result));







}

bool MainWindow::clearAllInfoStories() {

    bool result = false;
    QString mydir = QString(_currconfpath + "/" + Safet::datadir);
    QDir myhome(_currconfpath);

    if (QFile::exists(mydir)) {
        SYD << tr("\"%1\" directory exists")
               .arg(mydir);
       doDelAllFilesOnDirectory(mydir + "/" + "input");
       qDebug("Deleting:|%1|", qPrintable(mydir + "/" + "input"));
       myhome.rmdir(Safet::datadir + "/" + "input"); //1
       doDelAllFilesOnDirectory(mydir + "/" + "flowfiles"); //2
       myhome.rmdir(Safet::datadir + "/" + "flowfiles");
       doDelAllFilesOnDirectory(mydir + "/" + "log"); //3
       myhome.rmdir(Safet::datadir + "/" + "log");
       doDelAllFilesOnDirectory(mydir + "/" + "dtd"); //4
       myhome.rmdir(Safet::datadir + "/" + "dtd");
       doDelAllFilesOnDirectory(mydir + "/" + "reports"); //5
       myhome.rmdir(Safet::datadir + "/" + "reports");
       doDelAllFilesOnDirectory(mydir + "/" + "stats"); //6
       myhome.rmdir(Safet::datadir + "/" + "stats");
       doDelAllFilesOnDirectory(mydir + "/" + "graphs"); //7
       myhome.rmdir(Safet::datadir + "/" + "graphs");
       doDelAllFilesOnDirectory(mydir + "/" + "images"); //8
       myhome.rmdir(Safet::datadir + "/" + "images");
       doDelAllFilesOnDirectory(mydir + "/" + "certs"); //9
       myhome.rmdir(Safet::datadir + "/" + "certs");
       doDelAllFilesOnDirectory(mydir + "/" + "sql"); //10
       myhome.rmdir(Safet::datadir + "/" + "sql");
       doDelAllFilesOnDirectory(mydir + "/" + "xmlrepository"); //11
       myhome.rmdir(Safet::datadir + "/" + "xmlrepository");
       doDelAllFilesOnDirectory(mydir);
        result = myhome.rmdir(Safet::datadir);
        if ( !result ) {
            SYE << tr("Ocurred an error. It's not possible remove \".safet\" directory."
                      "Probably directory no is empty");
            qDebug("No fue posible eliminar directorio .safet");
             return false;
        }
    }

    myhome.mkdir(Safet::datadir);
    myhome.mkdir(Safet::datadir +"/" + "input"); //1
    myhome.mkdir(Safet::datadir +"/" + "flowfiles"); //2
    myhome.mkdir(Safet::datadir +"/" + "log"); //3
    myhome.mkdir(Safet::datadir +"/" + "reports"); //4
    myhome.mkdir(Safet::datadir +"/" + "dtd"); //5
    myhome.mkdir(Safet::datadir +"/" + "sql"); //6
    myhome.mkdir(Safet::datadir +"/" + "certs"); //7
    myhome.mkdir(Safet::datadir +"/" + "images"); //8
    myhome.mkdir(Safet::datadir +"/" + "graphs"); //9
    myhome.mkdir(Safet::datadir +"/" + "stats"); //10
    myhome.mkdir(Safet::datadir +"/" + "xmlrepository"); //11

    QString ns;
    ns = Safet::SAFETCONF;
    MainWindow::writeDefaultFile(mydir + "/" + "safet.conf",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::AUTHCONF;
    MainWindow::writeDefaultFile(mydir + "/" + "auth.conf",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::YAWLINPUTDTD;
    MainWindow::writeDefaultFile(mydir + "/" + "dtd" +"/"+  "yawlinput.dtd",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::YAWLWORKFLOWDTD;
    MainWindow::writeDefaultFile(mydir + "/" + "dtd" +"/"+  "yawlworkflow.dtd",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::DEFTRACXML;
    MainWindow::writeDefaultFile(mydir + "/" + "input" +"/"+  "deftrac.xml",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::DEFCONSOLEXML;
    MainWindow::writeDefaultFile(mydir + "/" + "input" +"/"+  "defconsole.xml",ns.replace("/home/user",QDir::homePath()));
    ns =Safet::DEFCONFIGUREXML;
    MainWindow::writeDefaultFile(mydir + "/" + "input" +"/"+  "defconfigure.xml",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::DEFFLOWXML;
    MainWindow::writeDefaultFile(mydir + "/" + "input" +"/"+  "defflow.xml",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::TEMPLATEQUERYFLOW;
    MainWindow::writeDefaultFile(mydir + "/" + "flowfiles" +"/"+  "safetQueries.xml",ns.replace("/home/user",QDir::homePath()));




    // Creando la base de datos

    SafetBinaryRepo myrepo("mydb.db");

    if (!myrepo.open("")) {
        SYE << tr("Can not open database file (mydb.db)");
        qDebug("No logro abrir el repositorio");
        return false;
    }
    if (! myrepo.createTable("trayecto",
                             "nombre TEXT PRIMARY KEY  NOT NULL, fechacreacion INTEGER, status TEXT, usuario TEXT, ultimafecha INTEGER, flujo TEXT, lastpicture TEXT, lasttextinfo TEXT") ) {
        SYE << tr("Can not create TRAYECTO table  in database file (mydb.db)");
        qDebug("No creo trayecto");
        return false;
    }
    if (! myrepo.createTable("trayecto_registro",
                             "name_live_story char(18) NOT NULL ,fechaaccion INTEGER, propietario TEXT, id_trayecto INTEGER, status TEXT, picture TEXT, textinfo TEXT,story_name TEXT NOT NULL" ) ) {
        SYE << tr("Can not create TRAYECTO_REGISTRO table  in database file (mydb.db)");
        qDebug("No creo trayector");
        return false;
    }
    if (! myrepo.createTable("trayecto_consulta",
                            "id INTEGER PRIMARY KEY  NOT NULL, live_story TEXT  NOT NULL UNIQUE, Story_name TEXT  NOT NULL, currdate INTEGER, title TEXT  NOT NULL UNIQUE, brand TEXT, textquery TEXT,icon TEXT" ) ) {
        SYE << tr("Can not create TRAYECTO_CONSULTA table  in database file (mydb.db)");
        qDebug("No creo trayector");
        return false;
    }


    SYD << tr("clearAllInfoStories...ok!");
    return true;
}


bool MainWindow::checkLoadSamples() {
    //*** chequear si ya se cargaron los ejemplos

        QString myquery = QString("operacion:Listar_datos Cargar_archivo_flujo:%1/.safet/flowfiles/safetQueries.xml "
                                  "Variable: vConsultas")
                                  .arg(QDir::homePath());

        _hastories = true;

        QString laststories = toInputConsole(myquery);

        SYD <<  tr("laststories:|%1|").arg(laststories);
        if (laststories.indexOf(tr("tiramisu/Weekend")) >= 0) {
             SYD << tr("Se cargaron los ejemplos satisfactoriamente.");
            return true;
        }


        //*** chequear si ya se cargaron los ejemplos

        return false;


}

bool MainWindow::loadSamples() {




    QString mydir = QString(_currconfpath + "/" + Safet::datadir);
    SYD << tr(".............MainWindow::loadSamples...mydir:|%1|").arg(mydir);
    QString ns;
    ns = Safet::TEMPLATEFLOWTIRAMISU;
    MainWindow::writeDefaultFile(mydir + "/" + "flowfiles" +"/"+  "tiramisu.xml",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::TEMPLATEFLOWSUGARLOAF;
    MainWindow::writeDefaultFile(mydir + "/" + "flowfiles" +"/"+  "sugarloaf_mountain.xml",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::TEMPLATETULUM;
    MainWindow::writeDefaultFile(mydir + "/" + "flowfiles" +"/"+  "tulum.xml",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::TEMPLATEPIZZA;
    MainWindow::writeDefaultFile(mydir + "/" + "flowfiles" +"/"+  "pizza.xml",ns.replace("/home/user",QDir::homePath()));
    ns  = Safet::TEMPLATETOLEDO;
    MainWindow::writeDefaultFile(mydir + "/" + "flowfiles" +"/"+  "toledo.xml",ns.replace("/home/user",QDir::homePath()));
    ns = Safet::TEMPLATEGET_LOAN;
    MainWindow::writeDefaultFile(mydir + "/" + "flowfiles" +"/"+  "get_loan.xml",ns.replace("/home/user",QDir::homePath()));


    QString myinsert;
    SafetBinaryRepo myrepo("mydb.db");

    if (!myrepo.open("")) {
        SYE << tr("Can not open database file (mydb.db)");
        qDebug("No logro abrir el repositorio");
    }
    bool result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(1)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto  "
    "(nombre, fechacreacion,  status,usuario, ultimafecha, flujo, lastpicture, lasttextinfo)  "
    "VALUES  ('Weekend',1332382116,'_Livenote','victor',1332382116,'/sdcard/.safet/flowfiles/tiramisu.xml','http://www.tastyretreat.com/wp-content/uploads/2011/01/Tiramisu-1024x768.jpg','Ingredients: 6 egg yolks##SAFETCOMMA## 3 tablespoons sugar##SAFETCOMMA## 1 pound mascarpone cheese##SAFETCOMMA## 1 1/2 cups strong espresso##SAFETCOMMA## cooled##SAFETCOMMA## 2 teaspoons dark rum##SAFETCOMMA## 24 packaged ladyfingers##SAFETCOMMA## 1/2 cup bittersweet chocolate shavings##SAFETCOMMA## for garnish');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(2)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro  (name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES  "
            "('Weekend',1331305272,'victor','','Coffee','http://www.victorbravo.info/media/archivos/coffee.jpg','Prepare two pots of strong coffee','tiramisu');";
    result = myrepo.execSQL(myinsert);

    qDebug("........MainWindow::loadSamples....(3)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro "
    "(name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES   "
    "('Weekend','1331305812','victor','','Egg_whites','http://www.victorbravo.info/media/archivos/eggwhites.jpg','In a bowl##SAFETCOMMA## whisk the egg whites until thick','tiramisu');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(4)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro "
    "(name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES   "
    "('Weekend',1331306610,'victor','','Cream','http://www.victorbravo.info/media/archivos/cream.jpg','In a large bowl##SAFETCOMMA## using an electric mixer with whisk attachment##SAFETCOMMA## beat egg yolks and sugar until thick and pale##SAFETCOMMA## about 5 minutes. Add mascarpone cheese and beat until smooth. Then##SAFETCOMMA## add egg whites.','tiramisu')";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(5)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro "
    "(name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES   "
    "('Weekend',1331306910,'victor','','Biscuits','http://www.victorbravo.info/media/archivos/biscuits.jpg','In a small shallow dish##SAFETCOMMA## add remaining espresso and rum. Dip each ladyfinger into espresso. Place the soaked ladyfinger on the bottom of a 13 by 9 inch baking dish','tiramisu');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(6)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro "
    "(name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES   "
    "('Weekend',1331307050,'victor','','Spread','http://www.victorbravo.info/media/archivos/spread.jpg','Spread evenly 1/2 of the mascarpone mixture over the ladyfingers. Arrange another layer of soaked ladyfingers and top with remaining mascarpone mixture.','tiramisu');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(7)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro "
    "(name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES   "
    "('Weekend',1331307150,'victor','','Refrigerator','http://www.victorbravo.info/media/archivos/tiramisu.jpg','Cover tiramisu with plastic wrap and refrigerate for at least 2 hours##SAFETCOMMA## up to 8 hours.','tiramisu');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(8)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro "
    "(name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES   "
    "('Weekend',1332365938,'victor','','_Livenote','','Ingredients: 6 egg yolks##SAFETCOMMA## 3 tablespoons sugar##SAFETCOMMA## 1 pound mascarpone cheese##SAFETCOMMA## 1 1/2 cups strong espresso##SAFETCOMMA## cooled##SAFETCOMMA## 2 teaspoons dark rum##SAFETCOMMA## 24 packaged ladyfingers##SAFETCOMMA## 1/2 cup bittersweet chocolate shavings##SAFETCOMMA## for garnish','tiramisu');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(9)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_consulta"
    "(id, live_story, Story_name, currdate, title, brand, textquery,icon)"
    "VALUES"
    "(100001,'Weekend','/sdcard/.safet/flowfiles/tiramisu.xml',1332382116,'tiramisu/Weekend','Graph','operacion:Show_live_story_flow Name_story:/sdcard/.safet/flowfiles/tiramisu.xml Live_story:Weekend','qrc:/lasticon.png');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(10)..result:|%d|", result);


    // ************************************************************************
    // SugarLoaf

    myinsert = "INSERT INTO trayecto  "
    "(nombre, fechacreacion,  status,usuario, ultimafecha, flujo, lastpicture, lasttextinfo)  "
    "VALUES  ('Nov_2010',1332548586,'_Livenote','victor',1332548586,'/sdcard/.safet/flowfiles/sugarloaf_mountain.xml','http://www.victorbravo.info/media/archivos/pandeazucar.jpg','Incredible##SAFETCOMMA## magic and amazing experience.');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(11)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro  (name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES  "
    "('Nov_2010',1288882335,'victor','','Bondhino','http://www.victorbravo.info/media/archivos/bodhino.jpg','Take a Bondhino (glass-walled cable car) in Ticket Office. Trips every 20 minutes.','sugarloaf_mountain');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(12)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro  (name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES  "
    "('Nov_2010',1288883400,'victor','','Vermelha_beach','http://www.victorbravo.info/media/archivos/playavermelha.jpg','You can see a Vermelha beach from bondhino. It is spectacular','sugarloaf_mountain');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(13)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro  (name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES  "
            "('Nov_2010',1288884500,'victor','','Urca','http://www.victorbravo.info/media/archivos/urka.jpg','When you arrive at the first station##SAFETCOMMA## you are in Urca##SAFETCOMMA## you can take photos and eat something there.','sugarloaf_mountain');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(14)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro  (name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES  "
            "('Nov_2010',1288886340,'victor','','Botafogo','http://www.victorbravo.info/media/archivos/botafogo.jpg','Exploring Urca##SAFETCOMMA## you will find Botafogo view.','sugarloaf_mountain');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(15)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro  (name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES  "
    "('Nov_2010',1288888630,'victor','','Sugarloaf','http://www.victorbravo.info/media/archivos/pandeazucar.jpg','Then##SAFETCOMMA## take second cable car to Sugarloaf Mountain. In Sugarloaf Mountain you can see Copacabana beach.','sugarloaf_mountain');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(16)..result:|%d|", result);

    myinsert = "INSERT INTO trayecto_registro  (name_live_story,fechaaccion, propietario,id_trayecto, status, picture, textinfo,story_name) "
    "VALUES  "
            "('Nov_2010',1332532398,'victor','','_Livenote','','Incredible##SAFETCOMMA## magic and amazing experience.','sugarloaf_mountain');";

    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(17)..result:|%d|", result);


    myinsert = "INSERT INTO trayecto_consulta "
    "(id, live_story, Story_name, currdate, title, brand, textquery,icon)"
    "VALUES "
    "(100002,'Nov_2010','/sdcard/.safet/flowfiles/sugarloaf_mountain.xml',1332548586,'sugarloaf_mountain/Nov_2010','Graph','operacion:Show_live_story_flow Name_story:/sdcard/.safet/flowfiles/sugarloaf_mountain.xml   Live_story:Nov_2010','qrc:/lasticon.png');";


    result = myrepo.execSQL(myinsert);
    qDebug("........MainWindow::loadSamples....(18)..result:|%d|", result);


    // ************************************************************************
    // TULUM

    myinsert = "INSERT INTO trayecto VALUES ('Family12','1331307100','Playa','victor','1345145562','/sdcard/.safet/flowfiles/tulum.xml','http://www.victorbravo.info/media/archivos/tulum5.jpg','Despu�s del recorrido puede ba�arse en la hermosa playa');";
    result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_consulta VALUES ('100003','Family12','/sdcard/.safet/flowfiles/tulum.xml','1345145562','tulum/Family12','Graph','operacion:Show_live_story_flow Name_story:/sdcard/.safet/flowfiles/tulum.xml Live_story:Family12','qrc:/lasticon.png');";

    result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_registro VALUES ('Family12','1331303500','victor',null,'Muralla','http://www.victorbravo.info/media/archivos/tulum1.jpg','El primer punto de las ruinas es la muralla que protega a la ciudad antigua','tulum');";
    result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_registro VALUES ('Family12','1331307272','victor',null,'Primera_Casa','http://www.victorbravo.info/media/archivos/tulum2.jpg','Es una muestra de las casas de la ciudad','tulum');";
        result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_registro VALUES ('Family12','1331310700','victor',null,'Casa_Real','http://www.victorbravo.info/media/archivos/tulum3.jpg','Las casas reales eran de una construcci�n compleja. Algunas ten�an pintura hecha con insectos de la regi�n','tulum');";
        result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_registro VALUES ('Family12','1331315900','victor',null,'Faro','http://www.victorbravo.info/media/archivos/tulum4.jpg','Las ventanas de esta construcci�n eran utilizadas como faro para entrar por el arrecife','tulum');";
   result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_registro VALUES ('Family12','1331318100','victor',null,'Playa','http://www.victorbravo.info/media/archivos/tulum5.jpg','Despu�s del recorrido puede ba�arse en la hermosa playa','tulum');";
    result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_registro VALUES ('Family12','1331320100','victor',null,'_Livenote',null,'Asombrosas ruinas en la riviera maya##SAFETCOMMA## bell�sima playa y lugar de muchas historias','tulum');";
    result = myrepo.execSQL(myinsert);


    // ************************************************************************
    // PIZZA

    myinsert = "INSERT INTO trayecto VALUES ('Viernes_noche','1331309100','Compartir','victor','1345148804','/sdcard/.safet/flowfiles/pizza.xml','http://www.victorbravo.info/media/archivos/pizza7.jpg','Sirva y comparte su pizza hecha en casa con sus invitados');";
    result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_consulta VALUES ('100004','Viernes_noche','/sdcard/.safet/flowfiles/pizza.xml','1345148804','pizza/Viernes_noche','Graph','operacion:Show_live_story_flow Name_story:/sdcard/.safet/flowfiles/pizza.xml Live_story:Viernes_noche','qrc:/lasticon.png');";

    result = myrepo.execSQL(myinsert);

    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331303500','victor',null,'_Livenote',null,'Ingredientes: 20 gr. de Levadura Fresca 300 gr.de Harina 200 ml. de Agua 4 cucharadas de aceite Sal Az�car. Para el relleno Peperonni Queso Champi�ones Salchichas Or�gano Ajo en Polvo Salsa de Tomate','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331302500','victor',null,'Ingredientes','http://www.victorbravo.info/media/archivos/pizza1.jpg','Coloque todos los ingredientes en una mesa para empezar la preparaci�n','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331303500','victor',null,'Revolver','http://www.victorbravo.info/media/archivos/pizza2.jpg','En 200 ml de agua tibia no caliente##SAFETCOMMA## tibia##SAFETCOMMA## esto es importante disuelvan los 20 g de Levadura Fresca Tambi�n probamos con levadura instant�nea##SAFETCOMMA## pero el resultado no fue tan bueno como con levadura fresca)##SAFETCOMMA## una cucharada colmada de harina##SAFETCOMMA## un poco##SAFETCOMMA## muy poco de az�car y m�s o menos el doble de sal (en relaci�n a lo que agregaron de az�car)','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331305272','victor',null,'Reposar','http://www.victorbravo.info/media/archivos/pizza3.jpg','Revuelven hasta que se deshagan los grumos de harina##SAFETCOMMA## luego de esto dejan la mezcla reposar 10 minutos##SAFETCOMMA## tratando que no se mueva. Se recomieda batir a mano##SAFETCOMMA## pueden quedarles algunos grumos##SAFETCOMMA## pero deben ser los menos. Un tip es aplastar los grumos contra el borde del recipiente con una cuchara y luego seguir revolviendo. Durante los 10 minutos de reposo de la mezcla pueden preparar el choclo y los champignones (si usan champignones en conserva no es necesario preparalos##SAFETCOMMA## si son de bandeja##SAFETCOMMA## deben saltearlos##SAFETCOMMA## botar�n bastante jugo##SAFETCOMMA## este jugo no va en la pizza) Despues de los 10 minutos ver�n como la mezcla crece##SAFETCOMMA## sale como una espuma','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331305700','victor',null,'Amasar','http://www.victorbravo.info/media/archivos/pizza4.jpg','Ahora##SAFETCOMMA## en los 300 gr de harina##SAFETCOMMA## pongan cuatro cucharadas de aceite##SAFETCOMMA## el ajo en polvo (aunque esto es opcional y se puede reemplazar con lo que crean que le de un buen sabor) y agreguen la mezcla que ten�an reposando##SAFETCOMMA## ahora amasen y amasen y amasen##SAFETCOMMA## si ven que la masa se les pega a las manos##SAFETCOMMA## necesita m�s harina (pero no abusen tampoco)','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331306200','victor',null,'Tapar','http://www.victorbravo.info/media/archivos/pizza5.jpg','Amasen durante 10 minutos y cuando ya este lista la masa##SAFETCOMMA## la dejan en un lugar c�lido##SAFETCOMMA## si la hacen en verano la pueden dejar a la intemperie##SAFETCOMMA## un d�a fr�o##SAFETCOMMA## dentro del microondas es un buen lugar y recomiendo que la tapen con un pa�o de cocina. Lo que tiene que pasar aqu�##SAFETCOMMA## es que la masa empieza a crecer##SAFETCOMMA## le aparecen ojos y dientes y se come a toda su familia##SAFETCOMMA## tienen que ver cuando alcance el doble de su tama�o##SAFETCOMMA## eso##SAFETCOMMA## ni m�s ni menos (tampoco se trata de algo nuclearmente exacto##SAFETCOMMA## pero lo m�s que se pueda) esto pasar� aproximadamente en 30 minutos.','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331307100','victor',null,'Aceite','http://www.victorbravo.info/media/archivos/pizza8.jpg','Cuando la masa haya crecido##SAFETCOMMA## la sacan. En su molde##SAFETCOMMA## con un pincel##SAFETCOMMA## con el dedo o con una servilleta esparzan aceite##SAFETCOMMA## debe quedar cubierto todo el molde##SAFETCOMMA## pero no deben haber excesos##SAFETCOMMA## luego de esto##SAFETCOMMA## estiran la masa sobre su molde. Yo ocupe uno rectangular que por lo general viene con el horno##SAFETCOMMA## la masa debe quedar m�s o menos delgada. (M�s o menos a esta altura##SAFETCOMMA## prendan el horno para que este caliente)','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331307800','victor',null,'Salsa','http://www.victorbravo.info/media/archivos/pizza6.jpg','Una vez estirada##SAFETCOMMA## con un tenedor##SAFETCOMMA## pinchen la pizza en distintos lugares##SAFETCOMMA## varias veces. Esparzan la salsa de tomate y el or�gano##SAFETCOMMA## la ponen en el horno entre 5 y 10 minutos. (de vez en cuando miren la base de la masa para ver si no se esta quemando)','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331308500','victor',null,'Horno','http://www.victorbravo.info/media/archivos/pizza9.jpg','Coloque el relleno y vuelva a meter al horno##SAFETCOMMA## espere de 10 a 15 minutos m�s','pizza');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Viernes_noche','1331309100','victor',null,'Compartir','http://www.victorbravo.info/media/archivos/pizza7.jpg','Sirva y comparte su pizza hecha en casa con sus invitados','pizza');";

    result = myrepo.execSQL(myinsert);



    // ************************************************************************
    // TOLEDO


    myinsert = "INSERT INTO trayecto VALUES ('Verano10','1331307100','Plaza','victor','1345164938','/sdcard/.safet/flowfiles/toledo.xml','http://www.victorbravo.info/media/archivos/toledo6.png','De regreso a la plaza puede degustar la comida local##SAFETCOMMA## es una maravilla.');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_consulta VALUES ('100005','Verano10','/sdcard/.safet/flowfiles/toledo.xml','1345164938','toledo/Verano10','Graph','operacion:Show_live_story_flow Name_story:/sdcard/.safet/flowfiles/toledo.xml Live_story:Verano10','qrc:/lasticon.png');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Verano10','1331320100','victor',null,'_Livenote',null,'La ciudad de Toledo##SAFETCOMMA## declarada Patrimonio de la Humanidad por la UNESCO en 1986##SAFETCOMMA## est� enclavada en un promontorio rocoso rodeado por el r�o Tajo en pleno centro de Espa�a##SAFETCOMMA## a escasos 70 kil�metros de la capital##SAFETCOMMA## Madrid. Considerada por muchos como una Segunda Roma##SAFETCOMMA## Toledo no deja de ser una ciudad �nica. Unica en sus detalles##SAFETCOMMA## su historia##SAFETCOMMA## su cultura y su encanto. Las tres culturas de las tres religiones monote�stas -la musulmana##SAFETCOMMA## la hebrea y la cristiana- dejaron su huella en esta pe�ascosa pesadumbre y cuna de civilizaciones -como dec�a Cervantes- abrazada el Tajo##SAFETCOMMA## testigo de la impronta de todos los pueblos de la Pen�nsula Ib�rica.','toledo');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Verano10','1331325500','victor',null,'Renfe','http://www.victorbravo.info/media/archivos/toledo1.png','Tome el tren en la estaci�n de Atocha o Chamartin en Madrid. Compre sus tickets por internet o en la boleter�a','toledo');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Verano10','1331336272','victor',null,'Castillo','http://www.victorbravo.info/media/archivos/toledo2.png','Al llegar a la estaci�n de tren de Toledo##SAFETCOMMA## obtendr� esta vista##SAFETCOMMA## siga el camino de subida hasta llegar a la Plaza Principal','toledo');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Verano10','1331340700','victor',null,'Tren','http://www.victorbravo.info/media/archivos/toledo3.png','Al llegar a la plaza puede tomar el tren que sale cada media hora y que la dar� un paseo alrededor de Toledo##SAFETCOMMA## no se puede perder este paseo','toledo');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Verano10','1331346200','victor',null,'Vuelta','http://www.victorbravo.info/media/archivos/toledo4.png','Desde el tren puede observar una vista espectacular de Toledo##SAFETCOMMA## que est� rodeado por el r�o','toledo');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Verano10','1331348100','victor',null,'Catedral','http://www.victorbravo.info/media/archivos/toledo5.png','Al regresar puede ir al monumento m�s importante de Toledo su Catedral','toledo');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('Verano10','1331352100','victor',null,'Plaza','http://www.victorbravo.info/media/archivos/toledo6.png','De regreso a la plaza puede degustar la comida local##SAFETCOMMA## es una maravilla.','toledo');";

    result = myrepo.execSQL(myinsert);


    // ************************************************************************
    // GET_LOAN


    myinsert = "INSERT INTO trayecto VALUES ('New_house','1331710100','Planning','victor','1345235590','/sdcard/.safet/flowfiles/get_loan.xml','http://www.victorbravo.info/media/archivos/get_loan5.jpg','Make sure you have the money you need when you need it. Here are tools and strategies to help you understand the finances and cash flow of your business.');";

    result = myrepo.execSQL(myinsert);



    myinsert = "INSERT INTO trayecto_consulta VALUES ('100006','New_house','/sdcard/.safet/flowfiles/get_loan.xml','1345235590','get_loan/New_house','Graph','operacion:Show_live_story_flow Name_story:/sdcard/.safet/flowfiles/get_loan.xml Live_story:New_house','qrc:/lasticon.png');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('New_house','1331304100','victor',null,'_Livenote',null,'Whether you need money for home improvements or to buy a new home##SAFETCOMMA## banks and credit unions are available to lend you money','get_loan');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('New_house','1331307100','victor',null,'Learn_about','http://www.victorbravo.info/media/archivos/get_loan1.jpg','Before preparing and submitting a loan application##SAFETCOMMA## it is important to understand how lenders will evaluate your application. The following documents provide information on key factors in lending decisions','get_loan');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('New_house','1331408400','victor',null,'Financial','http://www.victorbravo.info/media/archivos/get_loan2.jpg','Developing Your Financial Statement. Financial statements are reports that let you know how your business is doing financially. They re important because they help you manage your business finances##SAFETCOMMA## and they reoften required by lenders when applying for a loan.','get_loan');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('New_house','1331507100','victor',null,'Needs','http://www.victorbravo.info/media/archivos/get_loan3.jpg','Before submitting a loan application##SAFETCOMMA## it is important to determine how much money you need and how you will use your loan. Understanding the cash flow of your business is an important step to determining your loan amount and whether you will be able to repay it successfully. Here are some resources and tools that will help you navigate this process.','get_loan');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('New_house','1331608100','victor',null,'Prepare','http://www.victorbravo.info/media/archivos/get_loan4.jpg','Below you ll find techniques to help you prepare a strong loan application package as well as tips to help you communicate to a lender why you need a loan','get_loan');";

    result = myrepo.execSQL(myinsert);


    myinsert = "INSERT INTO trayecto_registro VALUES ('New_house','1331710100','victor',null,'Planning','http://www.victorbravo.info/media/archivos/get_loan5.jpg','Make sure you have the money you need when you need it. Here are tools and strategies to help you understand the finances and cash flow of your business.','get_loan');";

    result = myrepo.execSQL(myinsert);




    return true;


}


bool MainWindow::writeDefaultFile(const QString& filename, const QString& tpl) {
    QFile file(filename);

    bool open = file.open(QIODevice::WriteOnly | QIODevice::Text);
    if (!open ) {
        SYE << tr("Cannot create file (write) \"%1\"")
               .arg(filename);
        qDebug("Error al crear:|%s|", qPrintable(filename));
        return false;
    }

    QTextStream out(&file);
    out.setCodec("ISO 8859-1");
    QString mystring;
    out << tpl;
    file.close();

    return true;

}

QString MainWindow::loadReportTemplate(const QString& json,const QString &filename,
                                       const QString& nameoperation) {

    QString html;

    QStringList mylist = nameoperation.split(":",QString::SkipEmptyParts);

    QString shortnameop = mylist.at(mylist.count()-1);


        QString fullnamePage;
        if ( filename.isEmpty()) {
            fullnamePage = SafetYAWL::getConf()["Reports/general.template"];
        }
        else {
            fullnamePage = filename;
        }
//       QString namePage = fullnamePage.section("/",-1);


       QFile infile(fullnamePage);
       if ( !infile.open(QIODevice::ReadOnly) ) {

               SYE << tr("No es posible leer el archivo de plantilla: \"%1\" ")
                      .arg(fullnamePage);
           return html;
       }

       QTextStream in(&infile);
       in.setCodec("ISO 8859-1");
       bool isreplacing = false;
       while(!in.atEnd()) {
           QString line = in.readLine();
           if (line.indexOf("<SAFET/>") != -1 && !isreplacing) {
                line.replace("<SAFET/>",json);
                isreplacing = true;
           }

           if (line.indexOf("<SAFETCURRENTACCOUNT/>") != -1 && !isreplacing) {
               QString myformat = SafetYAWL::getConf()["GeneralOptions/account.format"].trimmed();
               if ( myformat.isEmpty()) {
                   myformat = "%1 - %2/%3";
               }
               else {
                   myformat.replace("%realname","%1");
                   myformat.replace("%accountname","%2");
                   myformat.replace("%rolname","%3");
               }

               QString curr;

               if ( !MainWindow::currentaccount.isEmpty()) {
               curr = QString(myformat)
                       .arg("user")
                       .arg(MainWindow::currentaccount)
                       .arg(MainWindow::currentrole);
               }


               line.replace("<SAFETCURRENTACCOUNT/>",curr);
           }

           if ( line.indexOf("<TEMPLATEPATH/>")!= -1 )  {
               line.replace("<TEMPLATEPATH/>",templatePath());
           }
           if ( line.indexOf("<MODULESMENU/>")!= -1 )  {
               line.replace("<MODULESMENU/>",generateModulesMenu());
           }

           if ( line.indexOf("<SAFETRETURNCONSOLE/>")!= -1 )  {
               line.replace("<SAFETRETURNCONSOLE/>",returnFormLink(shortnameop));
           }

           html += line;
           html += "\n";

       }

       infile.close();

       return html;


}



bool MainWindow::delXmlMenuGroupForm(const QString& newff, const QString& ncmd) {
    qDebug("delXmlMenuGroupForm....(1)...");
//    QString myformatfile = newff;
//    myformatfile = myformatfile.replace(" ","_").toLower();

//    QString mypathfile = _currconfpath+ "/" + Safet::datadir + "/flowfiles/" + myformatfile +".xml";

    QString mypathfile = newff;

    QString myformatfile = newff.section("/",-1).replace(".xml","");

    qDebug("....MainWindow::delXmlMenuGroupForm...myformatfile:|%s|", qPrintable(myformatfile));
    bool result = QFile::remove(mypathfile);

    if (!result) {
        qDebug("no es posible eliminar:|%s|",qPrintable(mypathfile));
        SYE << tr("it's no possible remove story \"%1\"")
               .arg(newff);
        return false;
    }

    qDebug("....MainWindow::delXmlMenuGroupForm...removed:|%s|", qPrintable(mypathfile));

    QString fname = filecompleters.at(0);

    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de de entrado \"%1\" no existe en el sistema de archivos."
                  "No es posible agregar un grupo al menu")
               .arg(fname);

        return false;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible agregar un grupo al menu")
               .arg(fname);

        qDebug("...no se lee");
        return false;
    }


    bool iscontent = doc.setContent(&file);

    if (!iscontent) {
        SYE << tr("El formato del archivo XML de men� de formularios \"%1\" no es v�lido")
               .arg(fname);
        qDebug("...no ta bien");
        return false;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();


    QDomNode n = root;


    QString mysearchitem = ncmd+ QString("_%1").arg(myformatfile);


    QDomElement beforeelement;

    while( !n.isNull() ) {

        if ( n.isElement() ) {
            beforeelement = n.toElement();
            QString myname = beforeelement.attribute("name");
            qDebug(".....delXmlMenuGroupForm....n.name:|%s|",qPrintable(myname));
            if (myname.compare(mysearchitem,Qt::CaseInsensitive) ==0 ) {
                  qDebug("...delXmlMenuGroupForm....break New step");
                break;
            }

        }

        n = n.nextSibling();
    }



    if (!beforeelement.isNull()) {

        docElem.removeChild(beforeelement);
        qDebug("...delXmlMenuGroupForm....se removio un elemento del archivo XML...");

    }

    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
        SYE << tr("No es posible escribir el archivo \"%1\" con el nodo \"%2\" eliminado")
               .arg(fname)
               .arg(mysearchitem);
        return false;
    }

    QTextStream out(&myfile);

    out << doc.toString();

    qDebug("...delXmlMenuGroupForm...escribiendo...doc.toString():\n|%s|",qPrintable(doc.toString()));

    myfile.close();
    return true;

}

bool MainWindow::addXmlMenuGroupForm(const QString& newff, const QString& ncmd) {

    QString myformatfile = newff;
//    myformatfile = myformatfile.replace(" ","_").toLower();

    QString mypathfile = _currconfpath+ "/" + Safet::datadir + "/flowfiles/" + myformatfile +".xml";

    if (QFile::exists(mypathfile)) {
        SYE << tr("The story \"%1\" was created. Choose other name").arg(myformatfile);
        return false;
    }
    if (filecompleters.count() == 0) {
        SYE <<  tr("error filecompleters.count() == 0");
        qDebug("error addXmlMenuGroup...");
        return false;
    }
    QString mytempfile = SafetYAWL::getTempNameFiles(1).at(0);
    QFile tmpfile(mytempfile);
    bool isopen = tmpfile.open(QIODevice::WriteOnly);
    if (!isopen ) {
        SYE << tr("El archivo temporal \"%1\" no se puede escribir"
                  ".No es posible agregar un grupo al menu")
               .arg(mytempfile);

        qDebug("...no se lee:|%s|", qPrintable(mytempfile));
        return false;
    }

    QTextStream outtemp(&tmpfile);

    outtemp << Safet::TEMPLATEINPUTFILE;

    tmpfile.close();



    QString nodename;
    QDomDocument doc;
    QFile file(mytempfile);
    if (!QFile::exists(mytempfile) ) {
        SYE << tr("El archivo de de entrada (temporal) \"%1\" no existe en el sistema de archivos"
                  ".No es posible agregar un grupo al menu")
               .arg(mytempfile);

        qDebug("...no existe temporal:|%s|",qPrintable(mytempfile));
        return false;
    }

    isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de entrada \"%1\" no se puede leer"
                  ".No es posible agregar un grupo al menu")
               .arg(mytempfile);

        qDebug("...no se lee entrada");
        return false;
    }


    bool iscontent = doc.setContent(&file);

    if (!iscontent) {
        SYE << tr("El formato del archivo XML de men� de formularios \"%1\" no es v�lido")
               .arg(mytempfile);
        qDebug("...no ta bien formato");
        return false;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();

    qDebug("...MainWindow::addXmlMenuGroupForm.....(1)...");

        qDebug("...MainWindow::addXmlMenuGroupForm.....(1)...root:|%s|",qPrintable(root.nodeName()));

    QDomNode n = root;

    qDebug("...MainWindow::addXmlMenuGroupForm.....(1)...n:|%s|",qPrintable(n.nodeName()));


    QDomElement mynewelement, beforeelement;

    while( !n.isNull() ) {

        if ( n.isElement() ) {
            beforeelement = n.toElement();
            QString myname = beforeelement.attribute("name");
            qDebug(".....addXmlMenuGroupForm....n.name:|%s|",qPrintable(myname));
            if (myname.startsWith(ncmd)) {
                qDebug("...break New step");
                break;
            }

        }

        n = n.nextSibling();
    }



    mynewelement = beforeelement.cloneNode().toElement();



    QDomDocument docdeftrac;


    QString fname = filecompleters.at(0);

    qDebug("...MainWindow::addXmlMenuGroupForm.....(1)...fname:|%s|",qPrintable(fname));
    file.setFileName(fname);

    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de de entrada  \"%1\" no existe en el sistema de archivos"
                  ".No es posible agregar un grupo al menu")
               .arg(fname);

        qDebug("...no existe :|%s|",qPrintable(mytempfile));
        return false;
    }

    isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de entrada \"%1\" no se puede leer"
                  ".No es posible agregar un grupo al menu")
               .arg(fname);

        qDebug("...no se lee entrada**");
        return false;
    }


    iscontent = docdeftrac.setContent(&file);

    qDebug("...abriendo archivo...");
    if (!iscontent) {
        SYE << tr("El formato del archivo XML de men� de formularios \"%1\" no es v�lido")
               .arg(fname);
        qDebug("...no ta bien formato");
        return false;
    }

    file.close();


    QDomElement defDocElem = docdeftrac.documentElement();
    QDomNode defroot = defDocElem.firstChild();

    qDebug("...MainWindow::addXmlMenuGroupForm.....(1)...defroot:|%s|",qPrintable(defroot.nodeName()));

    QDomNode defbeforeelement = defroot;



    QString newnodename = ncmd+ "_" +  myformatfile;
    mynewelement.setAttribute("name",newnodename);

    qDebug("...MainWindow::addXmlMenuGroupForm.....(1)...newnodename:|%s|",qPrintable(newnodename));


    QDomElement mycommand = mynewelement.firstChild().toElement();
    QDomElement oldcommand = mycommand;
    qDebug("...MainWindow::addXmlMenuGroupForm.....(2)...myfield1:|%s|",
           qPrintable(mycommand.nodeName()));
    QDomElement myfieldlist = mycommand.firstChild().toElement();
    QDomElement oldfieldlist = myfieldlist;
    qDebug("...MainWindow::addXmlMenuGroupForm.....(2)...myfield2:|%s|",
           qPrintable(myfieldlist.nodeName()));

    QDomNode mycurrfield = myfieldlist.firstChild();

    while (!mycurrfield.isNull()) {


        QDomElement myfield  = mycurrfield.toElement();
        QDomElement oldmyfield = myfield;
        QString mytitle = myfield.attribute("title");
        qDebug("...myfield...title:|%s|",qPrintable(mytitle));

        if ( mytitle== "id") {
            myfield.setAttribute("options",QString("nombre:trayecto:where flujo='%1'::nombre").arg(mypathfile));
            myfieldlist.replaceChild(myfield,oldmyfield);
        }
//        else if ( mytitle.compare("story_name", Qt::CaseInsensitive) == 0 ) {
//            myfield.setAttribute("literal",myformatfile);
//            myfieldlist.replaceChild(myfield,oldmyfield);

//        }
        else if ( mytitle.compare("Next_status", Qt::CaseInsensitive) == 0 ) {
            myfield.setAttribute("path",QString("%1").arg(mypathfile));
            myfieldlist.replaceChild(myfield,oldmyfield);
        }
        mycurrfield = myfield.nextSibling();
    }

    mycommand.replaceChild(myfieldlist,oldfieldlist);
    mynewelement.replaceChild(mycommand,oldcommand);

// Cambiar para el literal  del story_name

    mycommand = mycommand.nextSibling().toElement();
    oldcommand = mycommand;
    myfieldlist = mycommand.firstChild().toElement();
    oldfieldlist = myfieldlist;

    mycurrfield = myfieldlist.firstChild();

    while (!mycurrfield.isNull()) {


        QDomElement myfield  = mycurrfield.toElement();
        QDomElement oldmyfield = myfield;
        QString mytitle = myfield.attribute("title");
        qDebug("...myfield...title:|%s|",qPrintable(mytitle));
        if ( mytitle.compare("story_name", Qt::CaseInsensitive) == 0 ) {
            myfield.setAttribute("literal",myformatfile);
            myfieldlist.replaceChild(myfield,oldmyfield);
            break;
        }
        mycurrfield = myfield.nextSibling();
    }

    mycommand.replaceChild(myfieldlist,oldfieldlist);
    mynewelement.replaceChild(mycommand,oldcommand);

    // Cambiar para el literal  del story_name

    if ( defDocElem.insertAfter(mynewelement,defbeforeelement).isNull() ) {

        SYE << tr("No pudo insertarse el elemento en el menu");
        qDebug("falla insercion menu");
        return false;
    }

    qDebug("...MainWindow::addXmlMenuGroupForm.....(2)...myformatfile:|%s|",
           qPrintable(myformatfile));
    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
        SYE << tr("No es posible escribir el archivo \"%1\" con el nodo \"%2\" agregado")
               .arg(fname)
               .arg(nodename);
        return false;
    }


    QTextStream out(&myfile);

    out << docdeftrac.toString();

    qDebug("...addXmlMenuGroupForm...escribiendo...docdeftrac.toString():\n|%s|",qPrintable(docdeftrac.toString()));

    myfile.close();
    QFile myflowfile(mypathfile);
    if ( !myflowfile.open(QIODevice::WriteOnly))  {
        SYE << tr("It's no possible write \"%1\"  file")
               .arg(mypathfile);
        return false;
    }
    QTextStream outflow(&myflowfile);
    outflow << Safet::TEMPLATEFLOWFILE;

    myflowfile.close();
    return true;
}

QString MainWindow::returnFormLink(const QString& nameoperation) {

}

QString MainWindow::generateModulesMenu() {
    QString result;

    return result;
}

QString MainWindow::menuCommands(const QString& currfile) {

    QString result = "import QtQuick 1.1; import com.nokia.meego 1.0\n";    

    result +=  "ListModel {\n id: pagesModel\n";

    QString TEMPL  = QString("ListElement {\n title: \"%1\"\n brand: \"%2\" \n icon: \"%3\"\n }\n");
    QString TEMPLONLY  = QString("ListElement {\n title: \"%1\"\n brand: \"%2\" \n icon: \"%3\"\n  }\n");

    DomModel* mymodel = NULL;

    QString myinput = QDir::homePath()+"/.safet/input/"+currfile;
    if ( !currfile.isEmpty() && QFile::exists(myinput)) {
        setInputPath(myinput);
    }

    mymodel =  new DomModel(inputPath(),NULL);



    QMap<QString,bool> myperms;

    QStringList commands = mymodel->getCommands(true,true);
    QString currtitle;
    QString curroperations;
    QString curricon;

    foreach(QString c, commands) {
        curricon = "qrc:/storiesicon.png";
        QStringList mylist = c.split(SafetYAWL::LISTSEPARATORCHARACTER);
        Q_ASSERT(mylist.count() > 0 );
        QString newc = mylist.at(0);
        if (mylist.count() > 2) {
            if (!mylist.at(2).isEmpty() ) {
                curricon = mylist.at(2);
            }
        }
        QString title = newc;
        if (title.startsWith(QLatin1String("operacion:titulo::"))) {
            if (!curroperations.isEmpty()) {
                result += curroperations;
                curroperations = "";
            }
            title = newc.mid(QString("operacion:titulo::").length());
            QString converttitle = MainWindow::convertOpToTitle(title);
            currtitle =  converttitle;
            continue;
        }


        if (c.startsWith("operacion:safet")) {
            continue;
        }
        if (c.startsWith("operacion:Show_live_story_flow")) {
            continue;
        }

        myperms= MainWindow::doPermiseExecOperationAction(c);

        if (myperms.isEmpty()) {
/** Quitar para habilitar permisos
            continue;
 **/
        }



        QString converttitle = MainWindow::convertOpToTitle(title);
        if (myperms.contains("execute") && myperms["execute"] || true /** Quitar '|| true' para habilitar permisos */) {

            if (converttitle.startsWith("New step")) {
                continue;
//                curroperations += TEMPL
//                        .arg(converttitle)
//                        .arg(tr("User stories"))
//                        .arg(curricon);

            }
            else {
                curroperations += TEMPL
                        .arg(converttitle)
                        .arg(currtitle)
                        .arg(curricon);
            }

        }
        else {
            if (myperms.contains("read") && myperms["read"]) {
                curroperations += TEMPLONLY
                        .arg(converttitle)
                        .arg(currtitle)
                        .arg(curricon);
            }
        }

    }

    result += curroperations;

    result += "}\n";


    qDebug(".................menuCommands...results:\n:|%s|\n",qPrintable(result));

    return result;


}



QString MainWindow::tsFromFile(const QString& path) {
    QString result;
    if (!QFile::exists(path)) {
        SYE << tr("Filename no exists: %1").arg(path);
        qDebug("file no exist: %s",qPrintable(path));
        return result;
    }

    QFileInfo myfile(path);

    result = myfile.lastModified().toString("dd/MM/yyyy  HH:mm:ss");

    return result;

}

QString MainWindow::photoGallery() {

    QString result;
    result += ""
            "import QtQuick 1.1\n"
            "import com.nokia.meego 1.0\n"
            "import com.nokia.extras 1.0\n"
            "import QtMobility.gallery 1.1\n"

            "Page {\n"
            "property string currvalue: \"\"\n"
            "id: mySelectPhoto\n"
            "orientationLock: PageOrientation.LockPortrait\n"
            "tools: ToolBarLayout {\n"
            "id: myTool\n"
            "          ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
            "selectid.text = \"...\";\n"
            "pageStack.pop();\n"
            "                                             }\n"
            "}\n"
            "}\n"
            "Item {\n"
            "id: root\n"

            "anchors.fill: parent\n"
            "GridView {\n"
            "anchors.fill:  parent\n"

            "maximumFlickVelocity: 3000\n"

            "model: model\n"

            "cellHeight: 160\n"
            "cellWidth: 160\n"

            "delegate:   Column {\n"
            "id: myCol\n"
            "Image {\n"
            "   id: myImage\n"
            "asynchronous: true\n"
            "smooth: true\n"
            "source:  \"file:///sdcard/.thumbnails/grid/\" + Qt.md5(url) + \".jpeg\"\n"
            "    }\n"
            "                    MouseArea {\n"
            "anchors.fill: myImage\n"
            "onClicked: {\n"
            "console.log(\"url:\" + url);\n"
            "selectid.text = url.substr(url.lastIndexOf(\"/\")+1);\n"
            "containerbox.currvalue = url.substr(7);\n"
            "pageStack.pop();\n"

            "  console.log(\"containerbox.currvalue:\"+containerbox.currvalue);\n"

            "dateBoxDae.currtext = mymain.tsFromFile(containerbox.currvalue);\n"
            "}\n"
            "     }\n"
            "}\n"

            "}\n"

            "DocumentGalleryModel {\n"
            "id: model\n"
            "rootType: DocumentGallery.Image\n"
            "properties: [\"url\"]\n"
            "filter: GalleryWildcardFilter {\n"
            "property: \"fileName\";\n"
            "value: \"*.jpg\";\n"
            "}\n"
            "}\n"
            "}\n"
            "}\n";


    //qDebug("photo:\n%s",qPrintable(result));

    return result;


}

QString MainWindow::generatePrincipalMenu() {


            QString result;
            result = "import QtQuick 1.1; import com.nokia.meego 1.0\n";
                    result += QString("ListModel {\n");
            result += QString(""
               "           ListElement { title: \"Graph\"; brand: \"Principal menu\"; textquery: \"\" ; icon:\"qrc:/lasticon.png\" }\n"
               "           ListElement { title: \"Create new story\"; brand: \"Principal menu\"; textquery: \"\"  ; icon:\"qrc:/liveicon.png\"}\n"
               "          ListElement { title: \"Delete story\"; brand: \"Principal menu\"; textquery: \"\"  ; icon:\"qrc:/storyicon.png\"}\n"
               "         ListElement { title: \"Clear all info\"; brand: \"Utilities\"; textquery: \"\"  ; icon:\"qrc:/delete.png\"}\n"
               "         ListElement { title: \"Load samples\"; brand: \"Utilities\"; textquery: \"\"  ; icon:\"qrc:/storiesicon.png\"}\n"
//               "         ListElement { title: \"Batch\"; brand: \"Utilities\"; textquery: \"\"  ; icon:\"qrc:/storiesicon.png\"}\n"
//               "         ListElement { title: \"Load Configuration\"; brand: \"Principal menu\"; textquery: \"\"  ; icon:\"qrc:/storiesicon.png\"}\n"
//               "        ListElement { title: \"Upload Database\"; brand: \"Principal menu\"; textquery: \"\"  ; icon:\"qrc:/storiesicon.png\"}\n"
               "         }    \n");


            qDebug("...generatePrincipalMenu....result:\n|%s|",qPrintable(result));
            return result;
}



QString MainWindow::generateLastStoriesMenu() {

    QString myquery = QString("operacion:Listar_datos Cargar_archivo_flujo:/sdcard/.safet/flowfiles/safetQueries.xml "
                              "Variable: vConsultas");

    _hastories = true;

    QString laststories = toInputConsole(myquery);


    QString result = "import QtQuick 1.1; import com.nokia.meego 1.0\n";
            result += QString("ListModel {\n");

//            qDebug("....MainWindow::generateLastStoriesMenu...laststories:|%s|", qPrintable(laststories));
    if (!laststories.isEmpty()) {
        laststories=  laststories.replace("},", "}\n ListElement ");
        laststories=  laststories.replace(",", ";");
        laststories = laststories.replace(QRegExp("id:\\s*\"[0-9]+\"\\s*;")," ");
        result +=  "ListElement " + laststories +"\n";
    }
    else {
        _hastories = false;
    }

     qDebug("....MainWindow::generateLastStoriesMenu..._hastories:|%d|", _hastories);
    result += QString("         }    \n");


    qDebug("...saliendo...generateLastStoriesMenu");
    return result;

}

void MainWindow::listQmlCommandList() {

    int i = 0;
    foreach(QString s, _qmlCommandList) {
        qDebug("%d....MainWindow::listQmlCommandList():|%s|",++i,qPrintable(s));
    }
}

QString MainWindow::unconvertTitle(const QString& t) {
            QString result;
            if (t.length() < 2 ) {
                return result.toUpper();
            }
            result = t;
            result.trimmed();
            result.replace(QRegExp("\\s+")," ");
            result.replace(" ","_");
        //    result = result.at(0).toUpper()+ result.mid(1).toLower();
            return result;

        }

QString MainWindow::convertOpToTitle(const QString& op) {
    QString result;
    if ( op.isEmpty()) {
        return op;
    }

    result = op;
    result.replace("operacion:","");
    result.replace("_"," ");
    return result;
}

void MainWindow::registerLogin(const QString& user) {
    SYA << tr("LOGIN:El usuario \"%1\" ha ingresado al sistema SAFET")
           .arg(user);
}

void MainWindow::registerLogout(const QString& user) {

    MainWindow::currentaccount = "";
    MainWindow::currentrole = "";

    SYA << tr("LOGOUT:El usuario \"%1\" ha salido del sistema SAFET")
           .arg(user);

}

void MainWindow::log(const QString& message) {

    SYA << message;
}


QString MainWindow::normPicture(const QString& p, const QString fieldname) {

    QString newp = p;
     if (newp.startsWith("http://") || newp.startsWith("qrc://")
             || newp.startsWith("file://")) {
         newp.replace(Safet::COLONMARK,":");
         return QString("%2: \"%1\"")
                 .arg(newp)
                 .arg(fieldname);
     }

     return QString("%2: \"file://%1\"")
             .arg(p)
             .arg(fieldname);

 }




QString MainWindow::generateStepInfo(const QString& ref, const QString& nodename, int typepage,
                                             const QString& title,int posinit,  bool lastpage ) {





            QString result;


            if ( typepage == 0  ) {
                result = QString("import QtQuick 1.1\n"
                                     "import com.nokia.meego 1.0\n"
                                     "import com.nokia.extras 1.0\n"
                            "   Page {\n"
                            "     id: myStepInfo\n"
                            "     objectName: \"myPageImage\"\n"
                            "    tools: ToolBarLayout {\n"
                            "              id: myConsolePageToolbar\n"
                            "              ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
                                 "                     var mypop = mymain.dequeueQmlCommand();   \n"
                                 "                     console.log(\"dequeue:\"+mypop);\n"
                                 "                          pageStack.pop(); \n"
                                                 "         }\n"
                                "                 }\n"
                            "          }\n"
                                 "Text {\n"
                                 "     anchors.topMargin: 5\n"
                                 "    text: \"%1\" \n"
                                 "    font.pointSize: 18\n"
                                 "    anchors.horizontalCenter: parent.horizontalCenter \n"
                                 "    horizontalAlignment: \"AlignHCenter\" \n"
                                 " }\n"
                                "TextArea {\n"
                                 "     anchors.fill: parent\n"
                                 "     anchors.topMargin: 50\n"
                                 "     text: \"%2\" \n"
                                 "     enabled: false \n"
                                 "    font.pointSize: 16\n"
                                 " }\n"
                            " }\n")
                        .arg(nodename)
                        .arg(ref);


            }
            else if ( typepage == 1) {
                QString currpicture;




                currpicture = MainWindow::normPicture(ref.trimmed());

                result = QString("import QtQuick 1.1\n"
                                 "import com.nokia.meego 1.0\n"
                                 "import com.nokia.extras 1.0\n"
                        "   Page {\n"
                        "     id: myStepInfo\n"
                            "     objectName: \"myPageImage\"\n"
                        "    tools: ToolBarLayout {\n"
                        "                        id: myConsolePageToolbar\n"
                                 "            ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
                                      "                var mypop = mymain.dequeueQmlCommand();   \n"
                                        "                     console.log(\"dequeue:\"+mypop);\n"
                                      "                pageStack.pop(); \n"
                                        "                }\n"
                                 "                     }\n"

                        "           }\n"

                             "Text {\n"
                             "     anchors.topMargin: 5\n"
                             "    text: \"%2\" \n"
                             "    font.pointSize: 18\n"
                             "    anchors.horizontalCenter: parent.horizontalCenter \n"
                             "    horizontalAlignment: \"AlignHCenter\" \n"
                             " }\n"
                            "Image {\n"
                             "     anchors.fill: parent\n"
                             "     anchors.topMargin: 50\n"
                              "    fillMode: Image.PreserveAspectFit\n"
                             "    smooth: true\n"
                             "    %1\n"
                             " }\n"
                        " }\n")
                    .arg(currpicture)
                    .arg(nodename);
            }
            else if ( typepage == 2) {
                QStringList mynodes = nodename.split(SafetYAWL::LISTSEPARATOR,QString::SkipEmptyParts);
                QStringList refs = ref.split(Safet::SEPARATOR);
                qDebug("......generateStepInfo...ref:|%s|", qPrintable(ref));



                QStringList mypictures;
                QStringList mytimes;
                QStringList myagos;
                QStringList mydates;
                QString mysafetnote;
                QString mytimelapse;


                if (refs.count() > 0 ) {
                    mypictures = refs.at(0).split(SafetYAWL::LISTSEPARATOR);
                }
                QStringList mytexts;
                if (refs.count() > 1 ) {
                    mytexts = refs.at(1).split(SafetYAWL::LISTSEPARATOR);
                }
                if (refs.count() > 2 ) {
                    mytimes = refs.at(2).split(SafetYAWL::LISTSEPARATOR);
                }
                if (refs.count() > 3 ) {
                    mydates = refs.at(3).split(SafetYAWL::LISTSEPARATOR);
                }

                if (refs.count() > 4 ) {
                   myagos = refs.at(4).split(SafetYAWL::LISTSEPARATOR);
                }

                if (refs.count() > 5 ) {
                    mysafetnote = refs.at(5);
                }

                if (refs.count() > 6 ) {
                    mytimelapse = refs.at(6);
                }

                Q_ASSERT( mynodes.count()== mypictures.count());

                Q_ASSERT( mynodes.count() == mytexts.count());

                        int i = 0;


                        foreach(QString mynode, mynodes) {
                            QString currpicture;

                            if (mypictures.at(i).isEmpty()) {

                                if (mytexts.at(i).isEmpty()) {

                                    continue;
                                }
                                currpicture = "source: \"qrc:/noimage.png\"";
                            }
                            else {
                                //currpicture = MainWindow::normPicture(mypictures.at(i));
                                qDebug("...(typage 2) ...mypictures.at(i):|%s|", qPrintable(mypictures.at(i)));
                                if (mypictures.at(i).startsWith(QDir::homePath()+"") ) {
                                    currpicture = "source: \"file:///sdcard/.thumbnails/grid/\"+Qt.md5(\"file://" + mypictures.at(i) + "\")+\".jpeg\"";
                                    qDebug("...md5...");
                                }
                                else {
                                    currpicture = MainWindow::normPicture(mypictures.at(i));
                                    qDebug("...nomd5");
                                }

                            }
                            QString myago;
                            if (i < myagos.count()) {
                                myago = myagos.at(i);
                            }


                            result +=
                                    QString(""
                                            "        Row {\n"
                                            "               id: rowControl%2\n"
                                            "               anchors.topMargin: 32 \n"
                                            "               anchors.leftMargin: 5 \n"
                                            "               %3 \n"
                                            "               height: 78\n"
                                            "               spacing: 2\n"
                                            "        Image {\n"
                                            "          id: imageNumber%2\n"
                                            "        width :  25 \n"
                                            "        height :  25 \n"
                                            "        source: \"qrc:/nro%7.png\" \n"
                                            "        }\n"
                                            "        Text {\n"
                                            "               id: infoControl%2\n"
                                            "               text: \"%4\" \n"
                                            "               width: 138\n"
                                            "               wrapMode: Text.WordWrap\n"
                                            "               font.pointSize: 9\n"
                                            "               font.family: \"Nokia Pure Text Light\" \n"
                                            "               verticalAlignment: \"AlignVCenter\" \n"
                                            "       }\n"
                                            "        Text {\n"
                                            "               id: textControl%2\n"
                                            "               text: \"%1\" \n"
                                            "               width: 165\n"
                                            "               height: parent.height\n"
                                            "               font.pointSize: 14\n"
                                            "               font.bold: true\n"
                                            "               wrapMode: Text.WordWrap\n"
                                            "               font.family: \"Helvetica\" \n"
                                            "               horizontalAlignment: \"AlignHCenter\" \n"
                                            "               verticalAlignment: \"AlignVCenter\" \n"
                                            "       }\n"
                                            "        Image {\n"
                                            "               id: imageControl%2\n"
                                            "               width :  123 \n"
                                            "               %5\n"
                                            "               %6\n"
                                            "        }\n"
                                            "\n"
                                            "     }\n"
                                            "\n")
                                    .arg(mynode.replace("_"," ")+QString("<br/><font color=#808080 size=1>%1<br/>%2</font>").arg(mydates.at(i)).arg(myago))
                                    .arg(i)
                                    .arg(i==0?"y: 40":QString("anchors.top: rowControl%1").arg(i-1)+QString(".bottom"))
                                    .arg(i< mytexts.count()?SafetYAWL::cutSentence(mytexts.at(i),100):"")
                                    .arg(currpicture) //5
                                    .arg(i<mypictures.count()?"height: parent.height":"height: 40") //6
                                    .arg(i+1); //7




                                if (i+1< mynodes.count()) {
                                    if (!mypictures.at(i+1).isEmpty() && !mytexts.at(i+1).isEmpty()) {
                                        QString mywaiting = mytimes.at(i+1);


                                        mywaiting.replace("hour","hr");
                                        mywaiting.replace("minute","min");
                                        mywaiting.replace("second","sec");
                                        mywaiting.replace("waiting","");
                                        mywaiting.replace("and","");
                                        myago.replace("hour","hr");
                                        myago.replace("minute","min");
                                        myago.replace("second","sec");
                                        myago.replace("ago","");
                                        myago.replace("and","");

                                        result += QString("\n"
                                                      "        Image {\n"
                                                      "               id: arrowImage%1\n"
                                                      "               anchors.top: rowControl%1.bottom \n"
                                                      "               anchors.horizontalCenter: parent.horizontalCenter\n"
                                                      "               source: \"qrc:/flechaabajo.png\"\n"
                                                      "               width :  24 \n"
                                                      "               height: 24 \n"
                                                      "        }\n"
                                                      "        Image {\n"
                                                      "               id: waitingImage%1\n"
                                                      "               anchors.top: rowControl%1.bottom \n"
                                                      "               anchors.left: parent.left \n"
                                                      "               anchors.leftMargin: 25 \n"
                                                      "               source: \"qrc:/waiting.svg\"\n"
                                                      "               width :  112 \n"
                                                      "               height: 26 "
                                                      "        }\n"
                                                      "        Text {\n"
                                                      "               id: waitingTitle%1\n"
                                                      "               anchors.topMargin: 1 \n"
                                                      "               anchors.top: rowControl%1.bottom\n"
                                                      "               anchors.left: parent.left \n"
                                                      "               anchors.leftMargin: 64 \n"
                                                      "               font.pixelSize: 10\n"
                                                      "               color: \"white\"\n"
                                                      "               verticalAlignment: \"AlignVCenter\" \n"
                                                      "               text: \"%3\" \n"
                                                      "        }\n"
                                                      "        Text {\n"
                                                      "               id: timeControl%1\n"
                                                      "               anchors.topMargin: 12 \n"
                                                      "               anchors.top: rowControl%1.bottom\n"
                                                      "               anchors.left: parent.left \n"
                                                      "               anchors.leftMargin: 30 \n"
                                                      "               font.bold: false\n"
                                                      "               color: \"black\"\n"
                                                      "               verticalAlignment: \"AlignVCenter\" \n"
                                                      "               text: \"%2\" \n"
                                                      "        }\n"

                                                      "\n"
                                                      )
                                                .arg(i)
                                                .arg(mywaiting)
                                                .arg(tr("waiting"));

                                        }

                                }



                                i++;

                        }

                        if (!mysafetnote.isEmpty()) {

                            result +=  QString(


                                   "        Text {\n"
                                   "               id: textNote\n"
                                   "               color: \"black\"\n"
                                   "               anchors.top: rowControl%1.bottom\n"
                                   "               anchors.topMargin: 7 \n"
                                   "               anchors.left: parent.left\n"
                                   "               anchors.leftMargin: 4\n"
                                   "               anchors.right: parent.right\n"
                                   "               anchors.rightMargin: 4\n"
                                   "               text: \"%2\" \n"
                                   "               wrapMode: Text.WordWrap\n"
                                   "        }\n"

                                               )
                                    .arg(i-1)
                                    .arg(tr("%1 %2").arg(mytimelapse).arg(SafetYAWL::cutSentence(mysafetnote,280)));

                            result +=  QString("        Image {\n"
                                        "          id: poweredStories\n"
                                    "        width :  24 \n"
                                    "        height :  24 \n"
                                    "        anchors.top: textNote.bottom\n"
                                    "        anchors.topMargin: 8 \n"
                                    "        x: 340\n"
                                    "        source: \"qrc:/storiesicon24.png\" \n"
                                    "        }\n"
                                   "        Text {\n"
                                   "               id: poweredText\n"
                                   "               color: \"#808080\"\n"
                                   "               anchors.top: textNote.bottom\n"
                                   "               anchors.topMargin: 10 \n"
                                   "               anchors.left: poweredStories.right\n"
                                   "               anchors.leftMargin: 4\n"
                                   "               text: \"<i>powered by </i><font color=black>Stories</font>\" \n"
                                   "        }\n"

                                               );


                        }
                        else {
                            result +=  QString("        Image {\n"
                                        "          id: poweredStories\n"
                                    "        width :  24 \n"
                                    "        height :  24 \n"
                                    "        anchors.top: rowControl%1.bottom\n"
                                    "        anchors.topMargin: 8 \n"
                                    "        x: 340\n"
                                    "        source: \"qrc:/storiesicon24.png\" \n"
                                    "        }\n"
                                   "        Text {\n"
                                   "               id: poweredText\n"
                                   "               color: \"#808080\"\n"
                                   "               anchors.top: rowControl%1.bottom\n"
                                   "               anchors.topMargin: 10 \n"
                                   "               anchors.left: poweredStories.right\n"
                                   "               anchors.leftMargin: 4\n"
                                   "               text: \"<i>powered by </i><font color=black>Stories</font>\" \n"
                                   "        }\n"

                                               )
                                    .arg(i-1);

                        }
                        int cheight = i*130;
                        if ( cheight < 854-70) {
                            cheight = 854-70;
                        }


                        QString startresult = QString("import QtQuick 1.1\n"
                                         "import com.nokia.meego 1.0\n"
                                         "import com.nokia.extras 1.0\n"

                                         "   Page {\n"
                                "     id: myStepInfo\n"
                                "     objectName: \"myPageImage\"\n"
                                "orientationLock: PageOrientation.LockPortrait\n"
                                "    tools: ToolBarLayout {\n"
                                         "            id: mytlbar\n"
                                         "            ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
                                         "                   pageStack.pop(); \n"
                                         "                     }\n"
                                         "            }\n"
                                         "       ToolIcon { iconId: \"toolbar-image-edit\"; onClicked: {\n"
                                         "            mytlbar.visible = false;\n"
                                         "           saveinfographic.message = principalViewer.takeSnapshot()+\" saved\";\n"
                                                     "principalViewer.saveItemImage(myFlicInfo,QDir::homePath()+\"/MyDocs/Pictures/storytoshare.png\");\n"
                                         "            mytlbar.visible = true;\n"
                                         "           saveinfographic.open();\n"
                                         "                         }\n"
                                         "           }\n"
                                "           }\n"

                                         "Flickable {\n"
                                          "id: myFlicInfo \n"
                                          "width: 480; height: 854\n"
                                          "contentWidth: 480; contentHeight: %2\n"
                                         "Rectangle {\n"
                                             "id: background\n"
                                             "anchors.fill: parent\n"
                                             "color:  \"#ffffff\"\n"
                                           "}\n"


                                         "  QueryDialog {\n"
                                         "       id: saveinfographic\n"
                                         "      icon: \"qrc:/storiesicon.png\"\n"
                                         "      titleText: \"Infographic\"\n"
                                         "      message: \"\"\n"
                                         "      acceptButtonText: \"OK\"\n"
                                         "  }\n"

                                     "Text {\n"
                                     "     anchors.topMargin: 5\n"
                                    "      id: titleControl\n"
                                     "     text: \"%1\" \n"
                                     "    font.pointSize: 14\n"
                                     "    font.bold: true\n"
                                     "    color:  \"#303030\"\n"
                                     "    anchors.horizontalCenter: parent.horizontalCenter \n"
                                     "    horizontalAlignment: \"AlignHCenter\" \n"
                                     " }\n")
                            .arg(title)
                            .arg(cheight);


                        result = startresult + result;

                        result += "      }\n";
                        result += " }\n";



                        //********************************


                } else if ( typepage == 3) { // Vista por notas
                QStringList mynodes = nodename.split(SafetYAWL::LISTSEPARATOR,QString::SkipEmptyParts);
                QStringList firstnodes;
                QStringList refs = ref.split(Safet::SEPARATOR);
                QStringList mypictures;
                QStringList mytimes;
                QStringList myagos;
                QStringList mydates;
                QString mysafetnote;
                QString mytimelapse;


                if (refs.count() > 0 ) {
                    mypictures = refs.at(0).split(SafetYAWL::LISTSEPARATOR);
                }
                QStringList mytexts;
                if (refs.count() > 1 ) {
                    mytexts = refs.at(1).split(SafetYAWL::LISTSEPARATOR);
                }
                if (refs.count() > 2 ) {
                    mytimes = refs.at(2).split(SafetYAWL::LISTSEPARATOR);
                }
                if (refs.count() > 3 ) {
                    mydates = refs.at(3).split(SafetYAWL::LISTSEPARATOR);
                }

                if (refs.count() > 4 ) {
                   myagos = refs.at(4).split(SafetYAWL::LISTSEPARATOR);
                }

                if (refs.count() > 5 ) {
                    mysafetnote = refs.at(5);
                }

                if (refs.count() > 6 ) {
                    mytimelapse = refs.at(6);
                }



                Q_ASSERT( mynodes.count()== mypictures.count());

                Q_ASSERT( mynodes.count() == mytexts.count());

                     firstnodes = mynodes;

                        int i = 0;




                        for(int enode = 0; enode < posinit;enode++) {
                            mynodes.pop_front();
                        }

                         int nodescount = mynodes.count();

                     foreach(QString mynode, mynodes) {
                            QString currpicture;

                            if (mypictures.at(posinit+i).isEmpty()) {

                                if (mytexts.at(posinit+i).isEmpty()) {

                                    continue;
                                }
                                currpicture = "source: \"qrc:/noimage.png\"";
                            }
                            else {
                                //currpicture = MainWindow::normPicture(mypictures.at(i));
                                qDebug("...(typepage 3)... mypictures.at(i):|%s|", qPrintable(mypictures.at(posinit+i)));
                                if (mypictures.at(posinit+i).startsWith(QDir::homePath()+"") ) {
                                    currpicture = "source: \"file:///sdcard/.thumbnails/grid/\"+Qt.md5(\"file://" + mypictures.at(posinit+i) + "\")+\".jpeg\"";
                                    qDebug("...md5...");
                                }
                                else {
                                    currpicture = MainWindow::normPicture(mypictures.at(posinit+i));
                                    qDebug("...nomd5");
                                }

                            }
                            QString myago;
                            if (posinit+i < myagos.count()) {
                                myago = myagos.at(posinit+i);
                            }


                            QString mywaiting;
                            if (posinit+i+1< mynodes.count()) {
                                if (!mypictures.at(posinit+i+1).isEmpty() && !mytexts.at(posinit+i+1).isEmpty()) {
                                    mywaiting = mytimes.at(posinit+i+1);


                                    mywaiting.replace("hour","hr");
                                    mywaiting.replace("minute","min");
                                    mywaiting.replace("second","sec");
                                    mywaiting.replace("waiting","for next step");
                                    mywaiting.replace("and","");
                                }

                            }

                            result +=
                                    QString(""
                                            "Note {\n"
                                            "   markerId: \"note1\" \n"
                                            "  anchors.leftMargin: 10\n"
                                            "anchors.left: %5 \n"
                                            "%2\n"                                            
                                            "   id: note%1\n"
                                            "   allText: \"%9\"\n"
                                            "   noteId: %1\n"
                                            "  noteText: \"%3\"\n"
                                            "titleText: \"%4\"\n"
                                            "sourceNumber: \"%7\"\n"
                                            "   %6\n"
                                            " bottomtext: \"%8\"\n"
                                           " }\n"

                                            "\n")

                                    .arg(i)
                                     .arg(i==0?"y: 40":(i%2==0?QString("anchors.top: note%1").arg(i-1)+QString(".bottom"):QString("anchors.top: note%1").arg(i-1)+QString(".top")))
                                    .arg(posinit+i< mytexts.count()?SafetYAWL::cutSentence(mytexts.at(posinit+i),135):"") // 3
                                    .arg(mynode.replace("_"," ")) // 4
                                    .arg(((i)%2 ==0 )?"parent.left":QString("note%1").arg(i-1)+QString(".right")) // 5
                                    .arg(currpicture)
                                    .arg(QString("qrc:/nro%1.png").arg(posinit+i+1)) // 7
                                    .arg(mywaiting) // 8
                                    .arg(posinit+i< mytexts.count()?SafetYAWL::cutSentence(mytexts.at(posinit+i),450):"");




                                i++;
                                if (i > 5) {
                                    break;
                                }

                        }

                        if (!mysafetnote.isEmpty() && (i >= nodescount)) {


                            result +=  QString(
                                   "        Note {\n"
                                   "               id: textNote\n"
                                   "               anchors.top: note%1.bottom\n"
                                   "               anchors.topMargin: 7 \n"
                                   "               anchors.left: parent.left\n"
                                   "               anchors.leftMargin: 4\n"
                                   "               titleText: \"%2\" \n"
                                   "               noteText: \"%3\" \n"
                                   "               allText: \"%4\" \n"
                                   "               normal: false \n"

                                   "        }\n"

                                               )
                                    .arg(i-1)
                                    .arg(mytimelapse)
                                    .arg(SafetYAWL::cutSentence(mysafetnote,250))
                                    .arg(mysafetnote);



                              result +=  QString("        Image {\n"
                                          "          id: poweredStories\n"
                                      "        width :  24 \n"
                                      "        height :  24 \n"
                                      "        anchors.top: textNote.bottom\n"
                                      "        anchors.topMargin: 8 \n"
                                      "        x: 340\n"
                                      "        source: \"qrc:/storiesicon24.png\" \n"
                                      "        }\n"
                                     "        Text {\n"
                                     "               id: poweredText\n"
                                     "               color: \"#ffffff\"\n"
                                     "               anchors.top: textNote.bottom\n"
                                     "               anchors.topMargin: 10 \n"
                                     "               anchors.left: poweredStories.right\n"
                                     "               anchors.leftMargin: 4\n"
                                                 ""
                                     "               text: \"<i>powered by </i><font color=black>Stories</font>\" \n"
                                     "        }\n"

                                                 );




                        }
                        else {
                                        result +=  QString(
                                                    "Text {\n"
                                                    "   anchors.top:note%1.bottom\n"
                                                    "    anchors.topMargin: 10\n"
                                                    "    anchors.left: parent.left\n"
                                                    "    anchors.right: parent.right\n"
                                                    "    anchors.rightMargin: 10\n"
                                                         "id: toolNext\n"
                                                    "     height: 40\n"
                                                    "     color: \"black\"\n"
                                                    "     font.bold: true\n"
                                                    "     font.pointSize: 16 \n"
                                                    "     text: \"Continue...\"\n"
                                                    "     horizontalAlignment: Text.AlignRight\n"
                                                "}\n"
                                                    )
                                                .arg(i-1);
                                        result +=  QString("        Image {\n"
                                                    "          id: poweredStories\n"
                                                "        width :  24 \n"
                                                "        height :  24 \n"
                                                "        anchors.top: toolNext.bottom\n"
                                                "        anchors.topMargin: 8 \n"
                                                "        x: 340\n"
                                                "        source: \"qrc:/storiesicon24.png\" \n"
                                                "        }\n"
                                               "        Text {\n"
                                               "               id: poweredText\n"
                                               "               color: \"#ffffff\"\n"
                                               "               anchors.top: toolNext.bottom\n"
                                               "               anchors.topMargin: 10 \n"
                                               "               anchors.left: poweredStories.right\n"
                                               "               anchors.leftMargin: 4\n"
                                               "               text: \"<i>powered by </i><font color=black>Stories</font>\" \n"
                                               "        }\n"

                                                           )
                                                .arg(i-1);



                        }
                        int cheight = i*130;
                        if (typepage == 3 ) {
                            cheight = i*146;
                        }
                        if ( cheight < 854-70) {
                            cheight = 854-70;
                        }
                        QString buttoned;

                        if (  i < nodescount ) {
                            buttoned =
                                    QString("\n"
                                    "            ToolIcon { iconId: \"toolbar-tab-next\"; onClicked: {\n"
                                            "                     var result = mymain.generateStepInfo(\"%1\",\"%2\",%3,\"%6\",%4,%5);\n"
                                    "                       pageStack.push(Qt.createQmlObject(result,parent)); \n"
                                    "            }\n"
                                    "   }\n")
                                    .arg(ref)
                                    .arg(nodename)
                                    .arg(typepage)
                                    .arg(i)
                                    .arg(i == nodescount)
                                    .arg(tr("Page 2-") + title);



                        }
                        QString startresult = QString("import QtQuick 1.1\n"
                                         "import com.nokia.meego 1.0\n"
                                         "import com.nokia.extras 1.0\n"

                                         "   Page {\n"
                                "             id: myStepInfo\n"
                                "orientationLock: PageOrientation.LockPortrait\n"
                                "    tools: ToolBarLayout {\n"
                                         "            id: mytlbar\n"
                                         "            ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
                                         "                   pageStack.pop(); \n"
                                         "          }\n"
                                         "}\n"
                                                      "%3\n"
                                         "       ToolIcon { iconId: \"toolbar-image-edit\"; onClicked: {\n"
                                         "            mytlbar.visible = false;\n"
                                         "           saveinfographic.message = principalViewer.takeSnapshot()+\" saved!\";\n"
                                                      "principalViewer.saveItemImage(myFlicInfo,QDir::homePath()+\"/MyDocs/Pictures/storytoshare.png\");\n"
                                         "            mytlbar.visible = true;\n"
                                         "           saveinfographic.open();\n"
                                         "                         }\n"
                                         "           }\n"
                                "           }\n"
                                         "Flickable {\n"
                                          "id: myFlicInfo \n"
                                          "width: 480; height: 854\n"
                                          "contentWidth: 480; contentHeight: %2\n"
                                         " BackgroundCork {}\n"

                                         "  QueryDialog {\n"
                                         "       id: saveinfographic\n"
                                         "      icon: \"qrc:/storiesicon.png\"\n"
                                         "      titleText: \"Infographic\"\n"
                                         "      message: \"\"\n"
                                         "      acceptButtonText: \"OK\"\n"
                                         "  }\n"
                                      "ToolNote {\n"
                                      "id: toolSafetNote\n"
                                      "        height: 40\n"
                                      "        titleText: \"%1\"\n"
                                  "}\n"

                                                      )
                            .arg(title)
                            .arg(cheight)
                                .arg(buttoned);



                        result = startresult + result;

                        result += "      }\n";
                        result += " }\n";





            }



      //     qDebug("\n\n....MainWindow::generateStepInfo....result:|%s|",qPrintable(result));

            return result;

}


QString MainWindow::toTime_t(const QString &v, const QString& exp ) {
    QString result = v;
    QRegExp reg(exp);

      if ( !reg.exactMatch(v)) {

          qDebug("NO .... MainWindow::toTime_t:|%s|",qPrintable(v));
          return result;

      }

      result.replace("'","");
      qDebug("Yes...MainWindow::toTime_t...result:|%s|",qPrintable(result));

      QDateTime mydate = QDateTime::fromString(result,"dd/MM/yyyy hh:mm:ss");
      qDebug("......MainWindow::toTime_t...year:%d", mydate.date().year());
      qDebug("......MainWindow::toTime_t...month:%d", mydate.date().month());
      qDebug("......MainWindow::toTime_t...day:%d", mydate.date().day());
     qDebug("......MainWindow::toTime_t...hour:%d", mydate.time().hour());
     qDebug("......MainWindow::toTime_t...min:%d", mydate.time().minute());
     qDebug("......MainWindow::toTime_t...min:%d", mydate.time().second());
     result = QString("%1").arg(mydate.toTime_t());

        qDebug("Yes...MainWindow::toTime_t...result (2):|%s|",qPrintable(result));


    return result;

}

QString MainWindow::generateFormHead(const QString& o, const QString& mymodule, const QString& mydefault) {
            QString result;

            QString myInputModule;
            int modulenumber = 0;

            MainWindow::_lastchangekey = ""; // reset for getUpdateString

            if (mymodule=="deftrac.xml")  {

                if ( o.startsWith("Show ")) {
                    modulenumber = 1;
                    //myInputModule = "mymain.toInputConsole(myOperation);\nmymain.addQmlCommandList(myuVar + \"<SAFETSEPARATOR/>\" +myOperation);\n";
                    myInputModule = "mymain.toInputConsole(myOperation);\n";
                }
                else {
                    // myInputModule = "mymain.toInputForm(myOperation)\nmymain.addQmlCommandList(myuVar + \"<SAFETSEPARATOR/>\" +myOperation);\n";
                    myInputModule = "mymain.toInputForm(myOperation)\n";
                }
            } else if (mymodule=="defconsole.xml" ) {
                // myInputModule = "mymain.toInputConsole(myOperation);\nmymain.addQmlCommandList(myuVar + \"<SAFETSEPARATOR/>\" +myOperation);\n";
                myInputModule = "mymain.toInputConsole(myOperation);\n";
            } else if (mymodule=="defflow.xml" ) {
                modulenumber = 1;
                myInputModule = "mymain.toInputFlow(myOperation)";
            }
            if (myInputModule.isEmpty()) {
                SYE << tr("Error generating Form Head, Input Module is Empty");
                return result;
            }

            QStringList  mylist = mydefault.split("<SAFETSEPARATOR/>",QString::SkipEmptyParts);
            QString mydefs;
            foreach(QString def, mylist) {
                mydefs += "\"" + def + "\",";
            }
            if (!mydefs.isEmpty()) {
                mydefs.chop(1);
            }


            QString myDefautArray = QString("var defaults =[ %1 ]").arg(mydefs);
            qDebug("\n..........................generateFormHead...entrando..................myDefautArray:|%s|",qPrintable(myDefautArray));
            result = QString("import QtQuick 1.1\n"
                    "import com.nokia.meego 1.0\n"
                    "import com.nokia.extras 1.0\n"
                    "import \"arrayControls.js\" as CurrControls\n"
                    "import \"parser.js\" as CurrData\n"
                           "Component {\n"
                            "id: gateView\n"
                            "Page {\n"
                                "id: myFormPageOne\n"
                                "objectName: \"myFormPageOne\"\n"
                                "tools: ToolBarLayout {\n"
                                 "   id: staticToolbar1\n"
                             "            ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
                                  //"                          var mypop = mymain.dequeueQmlCommand();   \n"
                             //"                     console.log(\"(Head Form)...dequeue....:\"+mypop);\n"
                                  "                           pageStack.pop(); \n"
                                    "                    }\n"
                                    "              }\n"
//                             "  ToolIcon { iconId: \"toolbar-list\"; onClicked: {\n"
//                             " "
//                             "sheetCalendar.open();\n"
//                             "}\n"
//                             "}\n"
                                 "  ToolIcon { iconId: \"toolbar-done\"; onClicked: handleInputForm(); }\n"
                                "}\n"
                             "orientationLock: PageOrientation.LockPortrait\n"
                             "Sheet {\n"
                             "     id: sheetCalendar\n"

                             "     acceptButtonText: \"Insert\"\n"
                             "     rejectButtonText: \"Cancel\"\n"

//                             "     content: Flickable {\n"
//                             "         anchors.fill: parent\n"
//                             "         anchors.leftMargin: 10\n"
//                             "         anchors.topMargin: 5\n"
//                             "         contentWidth: myOrg.width\n"
//                             "         contentHeight: myOrg.height\n"
//                             "         flickableDirection: Flickable.VerticalFlick\n"
//                             "     }\n"
//                             "     onAccepted: {firstTextArea.currtext = firstTextArea.currtext+myOrg.day+\"/\"+myOrg.month+\"/\"+myOrg.year;}\n"
                             "     onAccepted: {}\n"
                             "     onRejected: {}\n"
                             " }\n"
                                "Menu {\n"
                                  "   id: myPageMenu\n"
                                  "   visualParent: myFormPageOne\n"
                                  "   MenuLayout {\n"
                                  "      MenuItem { text: \"Last query\"; onClicked: {\n"
                                  "             if ( mymain.countPages() == 0 ) {\n"
                                  "                banner0.text = \"No saved query\";\n "
                                  "                banner0.show();\n "
                                  "             } else {\n"
                                  "                    var myresult = mymain.qmlPage(mymain.countPages()-1);    \n"
                                  "             }\n"
                                  "          if ( myresult.indexOf(\"import\") == -1 ) {\n"
                                  "                CurrData.load(myresult,listConsoleModel);\n  "
                                  "               pageStack.push(myConsoleComponent);\n"
                                  "          } else { \n"
                                  "                   pageStack.push(Qt.createQmlObject(myresult,parent) );\n"
                                  "                 } \n"
                                  "            } \n"
                                  "      } \n"
                                 "      MenuItem { text: \"before the last query\"; onClicked: {\n"
                                 "             if ( mymain.countPages() < 2 ) {\n"
                                 "                banner0.text = \"No saved query\";\n "
                                 "                banner0.show();\n "
                                 "             } else {\n"
                                 "                    var myresult = mymain.qmlPage(mymain.countPages()-2);    \n"
                                 "             }\n"
                                 "          if ( myresult.indexOf(\"import\") == -1 ) {\n"
                                 "                CurrData.load(myresult,listConsoleModel);\n  "
                                 "               pageStack.push(myConsoleComponent);\n"
                                 "          } else { \n"
                                 "                   pageStack.push(Qt.createQmlObject(myresult,parent) );\n"
                                 "                 } \n"
                                 "            } \n"
                                 "      } \n"

                             "   }\n"
                                 "}\n"
                             "function selchangefor(thefield,thekey) {\n"
                                  "  if (thekey.length == 0 ) {\n"
                                  "          return;\n"
                                  "     }\n"
                                  " console.log(\"...function.selchangefor...thefield:\"+thefield+\",...thekey:\"+thekey);\n"
                                  " mymain.setInputPath(\"%2%1\");\n"
                                  "console.log(\"the..field:|\"+thefield +\"|...thekey:\|\"+thekey+\"|\");\n"
                                  " var mypair = mymain.generateModifyQML(myVar,thefield,thekey).split(\"<PAIRSEPARATOR/>\");\n"
                                  "if  (mypair.length > 1 ) {\n"
                                  "    var mylist = mypair[1].split(\"<SAFETSEPARATOR/>\");"
                                  "    for(var i=0; i< mylist.length;i++) {\n"
                                  "      var myfield = \"\";\n"
                                  "      var mymodel = \"\";\n"
                                  "      myfield = mylist[i].substr(0,mylist[i].indexOf(\":\"));\n"
                                  "      mymodel = mylist[i].substr(mylist[i].indexOf(\":\")+1);\n"
                                  "      console.log(\"myfield:\"+myfield);\n"
                                  "      console.log(\"mymodel:\"+mymodel);\n"
                                  "      for(var j=2; j< CurrControls.getList().length;j++) {\n"
                                  "         if ( CurrControls.getList()[j].children[0].currtitle == myfield) {\n"
                                  "                var myeles = mymodel.split(\"<LISTSEPARATOR/>\");\n                  "
                                  "                console.log(\"...field:\"+CurrControls.getList()[j].children[0].currtitle);"
                                  "                CurrControls.getList()[j].children[0].currmodel.clear();\n"
                                  "                for(var k=0; k<myeles.length;k++) {\n"
                                  "                    CurrControls.getList()[j].children[0].currmodel.append({\"name\": myeles[k].split(\"</SAFETMODELSEPARATOR>\")[0], \"value\": myeles[k].split(\"</SAFETMODELSEPARATOR>\")[1]});\n"
                                  "                }\n"
                                  "                break;"
                                  "         }\n"
                                  "      }\n"
                                  "    }\n"
                                  "}\n"
                                  "var myvalues = mypair[0];\n"
                                  "var mylist = myvalues.split(\"<SAFETSEPARATOR/>\");"
                                  "for(var i=0; i< mylist.length;i++) {\n"
                                  "  var myname = \"\";\n"
                                  "  var myvalue = \"\";\n"
                                  "  myname = mylist[i].substr(0,mylist[i].indexOf(\":\"));\n"
                                  "     if (myname.length == 0 ) {\n"
                                  "        continue;\n"
                                  "     }\n"
                                  "     if (myname.charCodeAt(0) == 10 ) {\n"
                                  "        myname = myname.substr(1);\n"
                                  "     }\n"
                                  "  myvalue = mylist[i].substr(mylist[i].indexOf(\":\")+1);\n"
                                  "   for(var j=2; j< CurrControls.getList().length;j++) {\n"
                                  "       if ( CurrControls.getList()[j].children[0].currtitle == myname) {\n"
                                  "              CurrControls.getList()[j].children[0].currtext = myvalue;\n      "
                                  "               break;"
                                  "       }\n"
                                  "   }\n"
                                  "}\n"
                                  "    busyIndicator.visible = false; \n"
                              "}\n"
                             "function handleInputForm() {\n"
                             "HIT:console.log(\"function handleInputForm()....CurrControls.getList().length:\"+CurrControls.getList().length);\n"
                             "var myOperation = \"operacion:\" + mymain.unconvertTitle(myVar)+\" \";\n"
                             "var myTitle = \"\"; \n"
                              "var myuVar = mymain.unconvertTitle(myVar);\n"
                              "var liven = \"\";\n   "
                              "var fromn = \"\";\n   "
                             "if (CurrControls.getList().length == 0 ) {\n"
                          "      console.log(\"genOperation()\");  \n"
                              "       }\n"
                          "   console.log(\"CurrControls.getList().length:\"+CurrControls.getList().length);\n"
                             "for(var i=0; i < CurrControls.getList().length;i++) {\n"
                             "    if (i < 2 ) {\n"
     //                        "      console.log(\"handleInputForm ejecutada...CurrControls value of:|\"+CurrControls.getList()[i].objectName+\"|\"); \n"
                             "    }else {\n"
                                 "  if (CurrControls.getList()[i].children[0].currvalue.length > 0 ) {\n"
                                 "       var mytitle = CurrControls.getList()[i].children[0].currtitle;\n"
                                 "       if ( mytitle == \"Live_story_name\") {\n"
                                 "          liven =   mymain.checkIfDateTime(mymain.unconvertTitle(CurrControls.getList()[i].children[0].currvalue));\n"
                                 "          myOperation =  myOperation + mytitle+\":\"+ liven;\n"
                                 "       } else if (mytitle == \"Live_story\"){\n      "
                                 "          liven =   mymain.checkIfDateTime(mymain.unconvertTitle(CurrControls.getList()[i].children[0].currvalue));\n"
                                 "          myOperation =  myOperation + mytitle+\":\"+ mymain.checkIfDateTime(CurrControls.getList()[i].children[0].currvalue);\n"
                                 "       } else if (mytitle == \"From_story_name\"){\n      "
                                 "          fromn =    mymain.checkIfDateTime(CurrControls.getList()[i].children[0].currvalue);\n"
                                 "          myOperation =  myOperation + mytitle+\":\"+ fromn;\n"
                                 "       } else {\n           "
                                 "          myOperation =  myOperation + mytitle+\":\"+ mymain.checkIfDateTime(CurrControls.getList()[i].children[0].currvalue);\n"
                                 "       }\n"
                                 "       myOperation = myOperation + \" \";\n"
                                 "       if ( i < 4) { \n"
                                 "          myTitle = myTitle + mymain.checkIfDateTime(CurrControls.getList()[i].children[0].currtext);\n"
                                 "       if ( i+1 < CurrControls.getList().length) \n"
                                 "                           myTitle = myTitle + \"/\";\n"
                                 "       } \n"
                                 "  }\n"
                                 "  else { \n"
                                    " console.log(\"empty field:|\"+CurrControls.getList()[i].children[0].currtitle+\"|\");\n"
                                 "  }\n"
                               "  }\n"
                             "}\n"
                               //"    mymain.setLasttitle(myTitle);\n"
                             "console.log(\"-------------------------------------->generateFormHead.myOperation:\"+myOperation);\n"
                          "       var result = %3;\n            "
                             "    if (result == \"successfully!\") {\n"
                          "          if ( myuVar == \"Add_new_live_story\") {\n"
                           "                  console.log(\"...........Add_new_live_story\");\n"
                                                "result = mymain.toInputForm(\"operacion:safet_Add_query Live_story:\"+liven+\" Story_name:\"+fromn+\" Description:\" +mymain.extractFileName(fromn)+\"/\"+liven+\" Text_query:\","
                                                "\"operacion:Show_live_story_flow Name_story:\"+fromn+\"  Live_story:\"+liven );\n"

                              "      }\n"
                              "      %5\n"
                              "    console.log(\".............. MainWindow::generateFormHead....mymain.lasttitle():\"+mymain.lasttitle());\n"
                              "      CurrControls.clear(); \n"
                              "console.log(\".............. MainWindow::generateFormHead....myOperation:\"+myOperation);\n"
                              "        var myrepage = pageStack.clear(); \n"
                                "        myrepage = pageStack.push(mainPage,null,true); \n"
                          "              mymain.saveQmlCommandList();\n"
                          "             while( !mymain.emptyQmlCommandList()) {\n"
                          "                    var mycmd = mymain.dequeueQmlCommand();\n"
                          "                    handleCmdStack(mycmd);\n    "
                          "            }\n"
                             "\n"
                          "            mymain.restoreQmlCommandList();\n"
                          "       banner0.text =  \"Action performed successfully!\";\n"
                          "       banner0.show();\n"
                          "          ;\n         "
                          "     } else if (result.indexOf(\"import\")  != -1 ) {\n         "
                             "      CurrControls.clear(); \n"
                             "        var MyCompGraph = Qt.createQmlObject(result,parent);\n"
                             "        pageStack.push(MyCompGraph);\n"
                             "        console.log(\"HIT:inputForm:\"+\"operacion:safet_Add_query Live_story:\"+liven+\" Story_name:\"+fromn+\" Description: \"+myTitle+\"  Text_query: \",myOperation);\n"
                             "        mymain.toInputForm(\"operacion:safet_Add_query Live_story:\"+liven+\" Story_name:\"+mymain.extractFileName(fromn)+\" Description: \"+myTitle+\"  Text_query: \",myOperation);\n            "
                             "        console.log(\"HIT:after input form\");\n"
                              " \n"
                             "    } else {\n         "
                             "       console.log(\".....result.length:|\"+result.length+\"|\" );\n"
                             "       if (result.length == 0 ) {\n"
                             "          banner0.text =  mymain.currentError();\n"
                             "          banner0.show();\n"
                             "          console.log(\"(Head)...mostrando error...\");\n"
                             "       }else {\n"
                             "             console.log(\"...CurrData...(1)..\" );\n"
                             "             CurrData.load(result,listConsoleModel);\n  "
                                     "     pageStack.push(myConsoleComponent);\n"
                              "        console.log(\"agregando componente\");\n"
                             "        mymain.savePage(result);\n"
                             "             //CurrPages.addItem(myConsoleComponent);\n"
                             "       }\n"
                             "    }\n"
                             "}\n"

                             "Flickable {\n"
                              "id: myFlicInfo \n"
                              "width: 480; height: 854\n"
                              "contentWidth: 480; contentHeight: 1290\n"

                                "function getProperties(obj, properties) {\n"
                                 "   properties = properties || {};\n"

                                 "   var prototype = Object.getPrototypeOf(obj);\n"
                                 "   if (prototype !== null) {\n"
                                 "       getProperties(prototype, properties);\n"
                                 "   }\n"

                               "     var names = Object.getOwnPropertyNames(obj);\n"
                               "     for (var i = 0; i < names.length; i++) {\n"
                               "         var name = names[i];\n"
                               "         properties[name] = obj[name];\n"
                               "     }\n"

                               "     return properties;\n"
                               " }\n"
                                 "ListModel {  id:listConsoleModel }\n"
                               " function iterate(obj) {\n"
                               "     obj = Object(obj);\n"

                               "     var properties = getProperties(obj);\n"

                               "     for (var name in properties) {\n"
                               "         if (typeof properties[name] !== \"function\") {\n"
                               "             console.log(\"property: \" + name);\n"
                               "         }\n"
                               "     }\n"
                               "     for (var name in properties) {\n"
                               "         if (typeof properties[name] === \"function\") {\n"
                               "             console.log(\"function: \" + name);\n"
                               "         }\n"
                               "     }\n"
                               " }\n"
                               "\n"
                                "Background {}\n"

                                 "Component {\n"
                                 "   id: myConsoleComponent\n"
                                 "   Page {\n"
                                 "   id: myConsolePage\n"
                                 "   tools: ToolBarLayout {\n"
                                 "       id: myConsolePageToolbar\n"
                             "            ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
                                  "                var mypop = mymain.dequeueQmlCommand();   \n"
                                  "                     console.log(\"dequeue:\"+mypop);\n"
                                  "                pageStack.pop(); \n"
                                    "               }\n"
                                    "       }\n"
                                  " }\n"
                                 "Text {\n"
                                 "    id: titleConsoleView\n"
                                 "    x: 96\n"
                                 "    y: 11\n"
                                 "    color: \"white\"\n"
                                 "    text: \" \"\n"
                                 "    horizontalAlignment: Text.AlignHCenter\n"
                                 "    font.pixelSize: 23\n"
                                 "    font.bold: true\n"
                                 "  }\n"

                                 "ListView {\n"
                                 "    id: listConsoleView\n"
                                 "    anchors.fill: parent\n"
                                 "    model: listConsoleModel\n"
                                 "    delegate: Rectangle {\n"
                                  "        width:parent.parent.width\n"
                                  "        height:80\n"
                                  "        Text {\n"
                                  "        text: \"<b>\"+title+\"</b>\" +\"<br/>\" +subtitle\n"
                                  "        font.pointSize: 14\n"
                                  "        }\n"
                                  "    }\n"
                                 "    }\n"
                                 "   }\n"
                                 "  }\n"
                                    "QueryDialog {\n"
                                        "id: success\n"
                                        "icon: \"qrc:/storiesicon.png\"\n"
                                        "titleText: \"Stories\"\n"
                                        "message: \"Action performed successfully!\"\n"
                                        "acceptButtonText: \"OK\"\n"
                                    "}\n"

                                "Rectangle {\n"
                                "    id: background\n"
                                "    anchors.fill: parent\n"
                                "    color:  \"#3b5998\"\n"
                                "}\n"
                                "Column  {\n"
                                 "   id: inColumn\n"
                                 "   objectName: \"mycolumn1\"\n"
                                 "   anchors.topMargin: 25\n"
                                 "   anchors.bottomMargin: 10\n"
                                 "   anchors.horizontalCenter: parent.horizontalCenter\n"
                                 "   width: parent.width\n"
                                 "   spacing: 15\n"
                                 "   \n"
                                "Component.onCompleted: {\n"
                                 "  var count = children.length;\n"
                                 "  %4;\n"
                                 "  for (var i = 0; i < count; i++) {\n"

                                 "      var item = children[i];\n  "
                                 "      CurrControls.addItem(item);\n"
                                  "     if (i > 1 ) {\n"
                                 "         children[i].children[0].selkey.connect(selchangefor);\n"
                                 "     console.log(\"defaults len: \"+defaults.length);\n"
                                 "     console.log(\"defaults   i: \"+i);\n"
                                 "          if ( (i-2) < defaults.length ) {\n"
                                 "               children[i].children[0].currtext  = defaults[i-2];\n"
                                 "             if ( children[i].children[0].currtype == \"selectbox\" && children[i].children[0].selectmodel.count > 1)  {\n"
                                 "                for(var k=0; k< children[i].children[0].selectmodel.count;k++) {\n"
                                 "                  if ( children[i].children[0].selectmodel.get(k).name == defaults[i-2] )  \n"
                                 "                       children[i].children[0].currvalue  =  children[i].children[0].selectmodel.get(k).value;\n"
                                  "                }\n"
                                 "               }  else {           \n"
                                 "                  children[i].children[0].currvalue  = defaults[i-2];\n"
                                 "               }        \n  "
                                 "          }        \n  "
                                 "        }        \n  "
                                 "  }\n"
                                 "for (var i = 0; (i+2 < count)  && i < defaults.length; i++) {\n"
                    "              if ( children[i+2].children[0].currtype == \"selectbox\" && children[i+2].children[0].selectmodel.count > 1)  {\n"
                                         "     selchangefor(children[i+2].children[0].selecttitle,defaults[i]);\n"
                                 "   }\n"
                                 " }\n"
                                 "}\n"
                                 "   InfoBanner  {\n"
                                 "                 id: banner0\n"
                                 "                 text: \"info of banner yuhu!\"\n"
                                 "}\n"
                                 "   Text {\n"
                                 "       id:gateNameText\n"
                                 "       text: myVar\n"
                                 "       anchors.bottomMargin: 15\n"
                                 "       anchors.horizontalCenter: parent.horizontalCenter\n"
                                 "       style: Text.Raised\n"
                                 "       font.pixelSize: 36\n"
                                 "       color: \"white\"\n"
                                 "   }\n"
                                 "   \n")
                    .arg(mymodule)
                    .arg(QString(QDir::homePath()+"")+ "/" + Safet::datadir +"/input"+"/")
                    .arg(myInputModule)
                    .arg(myDefautArray)
                    .arg(!o.startsWith("Show ")?"mymain.addQmlCommandList(mymain.lasttitle()+\"<SAFETSEPARATOR/>\"+mymain.lastOperation());":"");





            //qDebug("\n\n.......saliendo of generateFormHead...result:|\n%s\n|",qPrintable(result));
            return result;
        }




bool MainWindow::checkBoolOption(const QString& op) {

    bool result = false;
    SYD << tr("MainWindow::checkBoolOption....(1)...");
    QString myoption = SafetYAWL::getConf()[op];
    SYD << tr("MainWindow::checkBoolOption....(2)...");
    if (myoption == "yes" || myoption == "on")  {
        result = true;
    }
    else if (myoption == "off" || myoption == "no")  {
        result = false;
    }
    else {
    }


    return result;

}


QString MainWindow::declarativeCamera(const QString& action, const QString& namelive, const QString& namestep) {


    QString mysheet = ""
            "if (camera.capturedImagePath.length == 0 ) {\n"
            "                         bannerNoPhoto.show();\n"
            "} else {\n"
            "myInfoSheet.open();\n"
            "};\n";

    QString mynosheet = QString(""
                                "var myOperation = \"\";\n  "
                                "if (camera.capturedImagePath.length == 0 ) {\n"
                                "                         bannerNoPhoto.show();\n"
                                "         return;\n"
                                "            } else {\n"

            "            myOperation = \"operacion:%1 id:%2 Next_status:%3  Picture:\"+ camera.capturedImagePath;\n"
                                " }\n"
            "            console.log(\"..........declarativeCamera.....***camera myOperation:\"+ myOperation);\n"
            "            mymain.toInputForm(myOperation);\n"
            "           mymain.addQmlCommandList(mymain.lasttitle()+\"<SAFETSEPARATOR/>\"+mymain.lastOperation());\n"
                "        //         playSound.play();\n"
                                "          var myrepage = pageStack.clear(); \n"
                                  "         myrepage = pageStack.push(mainPage,null,true); \n"
                                "              mymain.saveQmlCommandList();\n"
                                "             while( !mymain.emptyQmlCommandList()) {\n"
                                "                    var mycmd = mymain.dequeueQmlCommand();\n"
                                "                    handleCmdStack(mycmd);\n    "
                                "            }\n"
                                   "\n"
                                "            mymain.restoreQmlCommandList();\n"


                                )
            .arg(action)
            .arg(namelive)
            .arg(namestep);




    QString myresult = QString( ""
    "import Qt 4.7\n"
    "import QtMultimediaKit 1.1\n"
    "import com.nokia.meego 1.0\n"
    "import com.nokia.extras 1.0\n"
    "import QtQuick 1.1\n"
    "Page {\n"
    "    tools: ToolBarLayout {\n"
    "                        id: myTool\n"
    "                     ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
    "                                    pageStack.pop();\n"
    "                                 }\n"
    "                              }\n"
    "                          ToolIcon { iconId: \"toolbar-done\"; onClicked: {\n"
    "                                           %4                     "
    "                                       }\n"
    "                       }\n"
    "            }\n"
    "   InfoBanner{\n"
    "                 id: bannerNoPhoto\n"
    "                 text: qsTr(\"No photo select. Press 'View' button before track step\")\n"
    "}\n"


    "    Sheet {\n"
    "        id: myInfoSheet\n"
    "        anchors.fill: parent\n"
    "        rejectButtonText: \"Cancel\"\n"
    "        acceptButtonText: \"Accept\"\n"

    "        content: TextArea  { id: myInfoArea; anchors.fill: parent}\n"
    "        onAccepted:  {\n"
    "            var myOperation = \"\"; \n"
    "        if (myInfoArea.text.length == 0 ) {\n"
    "            myOperation = \"operacion:%1 id:%2 Next_status:%3 Picture:\"+ camera.capturedImagePath ;\n"
    "        } else {\n"
    "            myOperation = \"operacion:%1 id:%2 Next_status:%3 Picture:\"+ camera.capturedImagePath +\" Info:\"+ myInfoArea.text;\n"
    "        }\n"
    "            console.log(\"camera myOperation:\"+ myOperation);\n"
    "            mymain.toInputForm(myOperation);\n"
    "     mymain.addQmlCommandList(mymain.lasttitle()+\"<SAFETSEPARATOR/>\"+mymain.lastOperation());\n"
        "    //             playSound.play();\n"
                            "        var myrepage = pageStack.clear(); \n"
                            "        myrepage = pageStack.push(mainPage,null,true); \n"
                            "              mymain.saveQmlCommandList();\n"
                            "             while( !mymain.emptyQmlCommandList()) {\n"
                            "                    var mycmd = mymain.dequeueQmlCommand();\n"
                            "                    handleCmdStack(mycmd);\n    "
                            "            }\n"
                                   "\n"
                                "            mymain.restoreQmlCommandList();\n"


    "        }\n"



    "    }\n"
                                "       SoundEffect {\n"
                                "           id: playSound\n"
                                "         source: \"qrc:/beep.wav\"\n"
                                "   }\n"

    "    Rectangle {\n"
    "    id : cameraUI\n"
    "    anchors.fill:  parent\n"
    "    color: \"black\"\n"
    "    state: \"PhotoCapture\"\n"

    "    states: [\n"
    "        State {\n"
    "            name: \"PhotoCapture\"\n"
    "            StateChangeScript {\n"
    "                script: {\n"
    "                    camera.visible = true\n"
    "                    camera.focus = true\n"
    "                    stillControls.visible = true\n"
    "                    photoPreview.visible = false\n"
    "                }\n"
    "            }\n"
    "        },\n"
    "        State {\n"
    "            name: \"PhotoPreview\"\n"
    "            StateChangeScript {\n"
    "                script: {\n"
    "                    camera.visible = false\n"
    "                    stillControls.visible = false\n"
    "                    photoPreview.visible = true\n"
    "                    photoPreview.focus = true\n"
    "                }\n"
    "            }\n"
    "        }\n"
    "    ]\n"

    "    PhotoPreview {\n"
    "        id : photoPreview\n"
    "        anchors.fill : parent\n"
    "        onClosed: cameraUI.state = \"PhotoCapture\"\n"
    "        focus: visible\n"

    "        Keys.onPressed : {\n"
    "            //return to capture mode if the shutter button is touched\n"
    "            if (event.key == Qt.Key_CameraFocus) {\n"
    "                cameraUI.state = \"PhotoCapture\"\n"
    "                event.accepted = true;\n"
    "            }\n"
    "        }\n"
    "    }\n"

    "    Camera {\n"
    "        id: camera\n"
    "//        x : 10\n"
    "//        y : 10\n"
    "        anchors.fill:  parent\n"
    "        focus : visible //to receive focus and capture key events\n"
    "        //captureResolution : \"1024x768\"\n"

    "        flashMode: stillControls.flashMode\n"
    "        whiteBalanceMode: stillControls.whiteBalance\n"
    "        exposureCompensation: stillControls.exposureCompensation\n"

    "        onImageCaptured : {\n"
    "            photoPreview.source = preview\n"
    "            stillControls.previewAvailable = true\n"
    "            cameraUI.state = \"PhotoPreview\"\n"
    "        }\n"
    "    }\n"

    "    CaptureControls {\n"
    "        id: stillControls\n"
    "        anchors.fill: parent\n"
    "//        anchors.left: parent.left\n"
    "//        anchors.leftMargin: 10\n"
    "//        width: 100\n"
    "//        height: 400\n"
    "        camera: camera\n"
    "        onPreviewSelected: cameraUI.state = \"PhotoPreview\"\n"
    "    }\n"
    "    }\n"

    "}\n")
            .arg(action)
            .arg(namelive)
            .arg(namestep)
            .arg((MainWindow::checkBoolOption("StoriesOptions/showinfodialog") && MainWindow::checkBoolOption("StoriesOptions/showinfodialog"))?mysheet:mynosheet);



    qDebug(".........****declarativeCamera...myresult:|\n%s\n|",qPrintable(myresult));


    return myresult;

}

void MainWindow::grabQML(QDeclarativeView& comp ) {

            QPixmap mypic;
            mypic = QPixmap::grabWidget(&comp);

            mypic.save(QDir::homePath()+"/MyDocs/may.png");

}

QString MainWindow::extractFileName(const QString& p) {
    QString result = p;

    result = result.section("/",-1);
    result.replace("."+result.section(".",-1),"");


    return result;

}

QString MainWindow::checkIfDateTime(const QString& d) {
            QString result = d;
            QDateTime mydate;            
            mydate = QDateTime::fromString(d,"dd/MM/yyyy hh:mm:ss");
            if (mydate.isValid()) {
                 qDebug("....MainWindow::checkIfDateTime...isvalid!\n");
                return QString("%1").arg(mydate.toTime_t());
            }
            result.replace(":",Safet::COLONMARK);

            return result;
}

void MainWindow::savePage(const QString& value) {

            while ( MainWindow::_qmlpages.count() > 2) {
                MainWindow::_qmlpages.removeFirst();
            }
            MainWindow::_qmlpages.append(value);

}

void MainWindow::clearPages() {
            MainWindow::_qmlpages.clear();
}

QString MainWindow::qmlPage(int pos) {

            qDebug("...MainWindow::_qmlpages...pos: %d", pos);
            qDebug("...MainWindow::_qmlpages...count: %d", MainWindow::_qmlpages.count());
            if ( pos < MainWindow::_qmlpages.count()) {
                return MainWindow::_qmlpages.at(pos);
            }
            qDebug("...posicion no ok...");
            SYE << tr("Posici�n de pagina (%1) QML superior al limite (%2")
                   .arg(pos)
                   .arg(MainWindow::_qmlpages.count());

            return QString();
        }

QString MainWindow::generateModifyQML(const QString& operation, const QString& fieldname,
                                               const QString& key) {

            QString result;
            QString cmd = unconvertTitle(operation);
            DomModel* mymodel = new DomModel(inputPath(),NULL);
            QStringList mylist;


            result = mymodel->getUpdateString(cmd,fieldname,key,mylist);


            result.chop(SafetYAWL::LISTSEPARATORCHARACTER.length());
            qDebug("...MainWindow::generateModifyQML...result..(after)..: |%s|", qPrintable(result));
            // **** Agregar modelos (para los selectBox)
            // *********************************************************************************

            QString second;
            second += SafetYAWL::PAIRSEPARATOR;

            QStringList fields = mymodel->getFields(cmd);

            foreach(QString f, fields) {
                QString newfield = f;
                QString trimfield = f;
                bool mandatory = false;

                if (newfield.endsWith("*")) {
                    newfield.chop(1);
                    trimfield = newfield.trimmed();
                    mandatory = true;
                }
                if ( fieldname == trimfield) {
                    continue;
                }
                CmdWidget* mywidget = mymodel->selWidgetFromField(cmd,trimfield,key);

                QString newitem;
                if (mywidget == NULL ) {
                    newitem = tr("No hay widget para \"%1\"\n").arg(trimfield);
                }
                else {
                    if (!mywidget->conf().contains("changefor")) {
                        continue;
                    }
                    if ( !mywidget->conf()["changefor"].toString().split(",",QString::SkipEmptyParts).
                            contains(fieldname)) {
                        continue;
                    }
                    newitem = trimfield + ":";
                    newitem += QString("%1%2")
                          .arg(mywidget->qmlForKey(key))
                           .arg(SafetYAWL::LISTSEPARATORCHARACTER);
                }
                  second += newitem;


            }
            if (second.length() > SafetYAWL::PAIRSEPARATOR.length() ) {
                second.chop(SafetYAWL::LISTSEPARATORCHARACTER.length());
                result += second;
            }
            return result;

        }



        QString MainWindow::generateModifyHTML(const QString& operation, const QString& fieldname,
                                               const QString& key) {
            QString result;
            SYD << tr("MainWindow::generateModifyHTML....operation:|%1|..."
                      "fieldname:|%2|....key:|%3|")
                   .arg(operation)
                   .arg(fieldname)
                   .arg(key);
            DomModel* mymodel = new DomModel(inputPath(),NULL);
            Q_CHECK_PTR( mymodel );
            QStringList mylist;
            SafetYAWL::streamlog
                    << SafetLog::Debug
                       << tr("MainWindow::generateModifyHTML...operacion: |%1|..fieldname:|%2|...key:|%3|")
                          .arg(operation)
                          .arg(fieldname)
                          .arg(key);

            if (operation.startsWith(QLatin1String("Listar_"))
                    || operation.endsWith(QLatin1String("Siguiente_estado"),Qt::CaseInsensitive)
                    || operation.startsWith(QLatin1String("Siguiente_estado"),Qt::CaseInsensitive)
                     ||   operation.endsWith(QLatin1String("Generar_gr�fico_coloreado"),Qt::CaseInsensitive)
                     ||   operation.endsWith(QLatin1String("con_autofiltro"),Qt::CaseInsensitive)
                     ||   operation.endsWith(QLatin1String("con_filtrorecursivo"),Qt::CaseInsensitive)
                ){
                result = QString("%1")
                        .arg(formFieldsForKey(operation,fieldname, key,mymodel));
                SafetYAWL::streamlog
                        << SafetLog::Debug
                           << tr("MainWindow::generateModifyHTML...result:|%1|")
                              .arg(result);
            }
            else if (operation.startsWith(tr("agregar_ticket") ) ) {
                SYD << tr("......MainWindow::generateModifyHTML....agregar_ticket....key:|%1|")
                       .arg(key);
                QString pathflow = key;
                //pathflow = pathflow.section(SafetYAWL::LISTSEPARATORCHARACTER,1,1);
                SYD << tr("......**MainWindow::generateModifyHTML....pathflow:|%1|")
                       .arg(pathflow);
                result = QString("%1")
                        .arg(formFieldsForKey(operation,fieldname, pathflow,mymodel));
                 SYD << tr("MainWindow::generateModifyHTML...result:|%1|")
                              .arg(result);
            }
            else if ( operation.startsWith(tr("Agregar_nodo"))
                      || operation.startsWith(tr("Borrar_nodo"))
                      || operation.startsWith(tr("Cambiar_conexi�n")) ) {
                QString pathflow = key;
                SYD << tr("......MainWindow::generateModifyHTML....pathflow(key,agregar):|%1|")
                       .arg(pathflow);
                result = QString("%1")
                        .arg(formFieldsForKey(operation,fieldname, pathflow,mymodel));
                SafetYAWL::streamlog
                        << SafetLog::Debug
                           << tr("MainWindow::generateModifyHTML...result(agregar):|%1|")
                              .arg(result);
            }
            else if ( operation.startsWith(tr("Cambiar_fecha"))) {

                doRestoreGraph(Safet::PLANNEDGRAPHSFILENAME);
                SYD << tr("......MainWindow::generateModifyHTML....key:|%1|")
                       .arg(key);
                if (!graphs().contains(key)) {
                    SYW << tr("No se encuentra en el repositorio el archivo de grafos planificados");
                    return result;
                }
                QString pathflow = graphs()[key].second;
                pathflow = pathflow.section(SafetYAWL::LISTSEPARATORCHARACTER,1,1);

                SYD << tr("......MainWindow::generateModifyHTML....pathflow:|%1|")
                       .arg(pathflow);

                result = QString("%1")
                        .arg(formFieldsForKey(operation,fieldname, pathflow,mymodel));
                SafetYAWL::streamlog
                        << SafetLog::Debug
                           << tr("MainWindow::generateModifyHTML...result:|%1|")
                              .arg(result);

            }
            else  {
                result = mymodel->getUpdateString(operation,fieldname,key,mylist);
                SafetYAWL::streamlog
                        << SafetLog::Debug
                           << tr("generateModifyHTML: |%1|")
                              .arg(result);
            }
        //    result = "Componente: AppPequenita";

            return result;
        }


QString MainWindow::generateFormFooter(const QString& o) {
        QString result;


        result = QString("            }\n");
                             result += QString("BusyIndicator {\n"
                                       "   id: busyIndicator\n"
                                       "   x: 300\n"
                                       "   y: 500\n"
                                       "   width: 500\n"
                                       "   height: 450\n"
                                       "   running: true\n"
                                       "   visible: false\n"
                                       "   opacity: 1\n"
                                      "}\n");
              result += QString("}\n"); // Flickable
             result += QString("}\n");


             result +=  QString("}\n");

            return result;
        }

QString MainWindow::formFieldsForKey(const QString& o, const QString& fieldname,
                                             const QString& keyvalue,
                                             DomModel* mymodel) {
            QString result;
            QString purl = o.section("/",-1);
            QString cmd;
            if (purl.split(":").count() > 1 ) {
                cmd = purl.split(":").at(1).trimmed();
            }
            else {
                cmd = purl;
            }


            Q_CHECK_PTR( mymodel );


        //    result += QString("<tr><td></td><td><input name=\"safetoperation\""
        //                     " id=\"safetoperation\" type=\"hidden\" value=\"%1\"/></td></tr>\n")
        //            .arg(cmd);

            QStringList fields = mymodel->getFields(cmd);
            SafetYAWL::streamlog
                    << SafetLog::Debug
                       << tr("MainWindow::formFieldsForKey....fields.count():%1")
                          .arg(fields.count());

            foreach(QString f, fields) {
                QString newfield = f;
                QString trimfield = f;
                bool mandatory = false;

                if (newfield.endsWith("*")) {
                    newfield.chop(1);
                    trimfield = newfield.trimmed();
                    mandatory = true;
                }
                if ( fieldname == trimfield) {
                    continue;
                }
                SafetYAWL::streamlog
                        << SafetLog::Debug
                           << tr("MainWindow::formFieldsForKey....trimfield:%1")
                              .arg(trimfield);

                CmdWidget* mywidget = mymodel->selWidgetFromField(cmd,trimfield);

                QString newitem;
                if (mywidget == NULL ) {
                    SYW << tr("No hay widget para \"%1\"").arg(trimfield);
                    newitem = tr("No hay widget para \"%1\"\n").arg(trimfield);
                }
                else {
                    if (!mywidget->conf().contains("changefor")) {
                        continue;
                    }
                    SYD << tr("MainWindow::formFieldsForKey: |%1|")
                           .arg(fieldname);
                    if ( !mywidget->conf()["changefor"].toString().split(",",QString::SkipEmptyParts).
                            contains(fieldname)) {
                        continue;
                    }
                    newitem = QString("%3: %1%2\n")
                          .arg(mywidget->htmlForKey(keyvalue))
                           .arg(SafetYAWL::LISTHTMLSEPARATOR)
                           .arg(trimfield);
                        SYD << tr("MainWindow::formFieldsForKey....newitem:%1")
                                  .arg(newitem);


                }
                  result += newitem;


            }
            if (result.count() > SafetYAWL::LISTSEPARATORCHARACTER.count()) {
                //result.chop(SafetYAWL::LISTSEPARATORCHARACTER.count());
            }
            return result;

        }


QString MainWindow::menuForm(const QString& o) {
    QString result;


    QString cmd = MainWindow::unconvertTitle(o);
        qDebug("...MainWindow::menuForm...(1)...cmd:|%s|",
               qPrintable(cmd));
    result =  "\n";
    DomModel* mymodel = new DomModel(inputPath(),NULL);

    QStringList fields = mymodel->getFields(cmd);

    foreach(QString f, fields) {
        QString newfield = f;
        QString trimfield = f;
        bool mandatory = false;

        if (newfield.endsWith("*")) {
            newfield.chop(1);
            trimfield = newfield.trimmed();
            mandatory = true;
        }


        CmdWidget* mywidget = mymodel->selWidgetFromField(cmd,trimfield);
        if (mywidget == NULL ) {
            continue;
        }
        QString newitem = QString("%1") .arg(mywidget->qml());
        result += newitem;
    }
    result += "\n";

    qDebug("........MainWindow::menuForm...result:\n%s\n",qPrintable(result));
    return result;
}

QString MainWindow::addParametersDialog(const QString& nameoperation) {
            QString result;
            if ( !nameoperation.startsWith("consola:")) {
                return QString();
            }

            result =
                    "<button id=\"parsbutton\" >Par&aacute;metros</button>"
                    "<div class=\"demo\">\n"
                    "<div id=\"parsdialog\" title=\"Par&aacute;metros\"  >\n"
                    "<p class=\"validateTips\">Cargue de nuevo la p&aacute;gina para ver los par&aacute;metros</p>\n"
                    "<form >\n"
                    "<span id=\"safetmarkup\" name=\"safetmarkup\"></span>"
                    "<div id=\"safetfieldset\" name=\"safetfieldset\">\n"
                    "Nombre<br/>\n"
                    "<input type=\"text\" name=\"name\" id=\"name\" class=\"text ui-widget-content ui-corner-all\" />\n"
                    "</div>\n"
                    "</form>\n"
                    "</div>\n"
                    "</div>\n";


            return result;

        }

        QStringList MainWindow::autoComplete(const QString& path) {

            QSet<QString> result;
            setInputPath(path);
            DomModel* mymodel = new DomModel(inputPath(),NULL);
            Q_CHECK_PTR( mymodel );

            QStringList commands = mymodel->getCommands();


            foreach(QString c, commands) {
                if ( !c.startsWith("operacion:")) {
                    continue;
                }
                QString newcmd = QString("%1|").arg(c);
                result.insert(newcmd);
                if (c.split(":").count() < 2) {
                    continue;
                }
                QString cmd = c.split(":").at(1).trimmed();
                QStringList fields = mymodel->getFields(cmd);
                foreach(QString f, fields) {
                    QString newfield = f;
                    QString trimfield = f;
                    bool mandatory = false;

                    if (newfield.endsWith("*")) {
                        newfield.chop(1);
                        trimfield = newfield.trimmed();
                        mandatory = true;
                    }
                    CmdWidget* mywidget = mymodel->selWidgetFromField(cmd,trimfield);
                    if (mywidget == NULL ) {
                        result.insert(QString("No hay widget para \"%1\"").arg(trimfield));
                        continue;
                    }
                    QStringList values = mywidget->options();
                    QString namefield = newfield;
                    foreach(QString v, values) {
                        newfield = namefield + QLatin1String(": ") + v.trimmed() +
                                QString("|%1")
                                .arg(mandatory?QLatin1String("*"):QLatin1String(""));
                        result.insert(newfield);
                    }

                }



            }

            return result.toList();

        }

        MainWindow::~MainWindow() {

            if (SafetYAWL::listPlugins != NULL ) {
                for(int i=0; i < SafetYAWL::listPlugins->count();i++) {
                    QPluginLoader* myplug = SafetYAWL::listPlugins->at(i);
                    if (myplug) {

                        QObject* myobject = myplug->instance();
                        if (myobject) {
                            delete myobject;
                        }
                        myplug->unload();
                    }
                }
                SafetYAWL::listPlugins->clear();
                SafetYAWL::listDynWidget->clear();
                delete SafetYAWL::listPlugins;
                delete SafetYAWL::listDynWidget;
                SafetYAWL::listPlugins = NULL;
                SafetYAWL::listDynWidget = NULL;

            }

        }


        void MainWindow::setToInputManagementSignDocument() {

        }

        void MainWindow::setToInputReports() {

        }



        void MainWindow::setToInputFlowGraph() {

        }


        QString MainWindow::getPrincipalCSS() const {
            return QString::fromUtf8(
                        "* {\n"
                        "color: #355670;\n"
                        "font: 12px \"Arial, Liberation Sans\";\n"
                        "}\n"
                        "\n"
                        "QTabWidget {"
                        "    background-image: url(:/background.png);} \n"
                        "#background\n"
                        "{ background-image: url(\":/background.png\"); }\n"
                        "\n"
                        "/* widgets */\n"
                        "QPushButton\n"
                        "{\n"
                        "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #406A9E, stop: 0.75 #00355F);\n"
                        "border: 0px;\n"
                        "border-radius: 2px;\n"
                        "color: white;\n"
                        "margin-left: 1px;\n"
                        "margin-right: 1px;\n"
                        "min-width: 75px;\n"
                        "padding: 1px 7px;\n"
                        "}\n"
                        "\n"
                        "QDialogButtonBox QPushButton, #home QPushButton\n"
                        "{ padding: 4px 7px; }\n"
                        "\n"
                        "QPushButton:hover, QPushButton:focus\n"
                        "{\n"
                        "background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #F1C157, stop:0.28 #FADF91, stop:1 #EBA927);\n"
                        "color: #00355F;\n"
                        "}\n"
                        "\n"
                        "QPushButton:disabled\n"
                        "{ background-color: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #A3AFC0, stop: 0.75 #80909D); }\n"
                        "\n"
                        "/* header */\n"
                        "#headerLine, #headerLine2\n"
                        "{ background-color: #00355F; }\n"
                        "\n"
                        "#cards, #languages\n"
                        "{\n"
                        "background-color: #00355F;"
                        "\n"
                        "border: 0px;\n"
                        "margin-right: 0px;\n"
                        "color: white;\n"
                        "}\n"
                        "\n"
                        "#cards QListView, #languages QListView\n"
                        "{\n"
                        "background-color: white;\n"
                        "border: 0px;\n"
                        "color: #355670;\n"
                        "}\n"
                        "\n"
                        "#cards::down-arrow, #languages::down-arrow\n"
                        "{ image: url(\":/images/languages_button.png\"); }\n"
                        "\n"
                        "#cards::drop-down, #languages::drop-down\n"
                        "{ border: 0px; }\n"
                        "\n"
                        "#settings, #help\n"
                        "{\n"
                        "background-color: #00355F;\n"
                        "color: #afc6d1;\n"
                        "font: bold;\n"
                        "min-width: 0px;\n"
                        "}\n"
                        "\n"
                        "#settings:hover, #settings:focus\n"
                        "#help:hover, #help:focus\n"
                        "{ text-decoration: underline; }\n"
                        "\n"
                        "#infoFrame\n"
                        "{\n"
                        "background-color: rgba( 255, 255, 255, 153 );\n"
                        "border-radius: 3px;\n"
                        "color: #668696;\n"
                        "padding-left: 10px;\n"
                        "padding-right: 10px;\n"
                        "}\n"
                        "\n"
                        "#infoCard\n"
                        "{ font: bold; }\n"
                        "\n"
                        "/* content */\n"
                        "#homeButtonLabel,\n"
                        "#viewContentLabel, #viewKeysLabel\n"
                        "{\n"
                        "color: #00355F;\n"
                        "font: bold;\n"
                        "}\n"
                        "\n"
                        "#home, #intro, #view,\n"
                        "#viewContentView\n"
                        "{ background-color: tra"
                        "nsparent; }\n"
                        "\n"
                        "#viewContentFrame\n"
                        "{\n"
                        "background-color: rgba( 255, 255, 255, 200 );\n"
                        "border: 1px solid gray;\n"
                        "color: #355670;\n"
                        "padding: 5px;\n"
                        "}\n"
                        "\n"
                        "#viewKeysFrame\n"
                        "{\n"
                        "background-color: rgba( 255, 255, 255, 200 );\n"
                        "border: 1px solid gray;\n"
                        "padding: 5px;\n"
                        "}\n"
                        "\n"
                        "#viewKeys, #viewKeysScroll\n"
                        "{\n"
                        "background-color: transparent;\n"
                        "color: #71889A;\n"
                        "}");
        }

        void MainWindow::setToInputForm() {

        }


        void MainWindow::goPrincipal() {

        }

        void MainWindow::setToInputConsole() {




        }


        void MainWindow::linkClickedSbMenu(const QUrl& url) {
            Q_ASSERT( url.toString().split(":", QString::SkipEmptyParts).count() > 1 );
            QString action = url.toString().split(SafetYAWL::LISTSEPARATORCHARACTER, QString::SkipEmptyParts).at(1);
            Q_ASSERT(!action.isEmpty());
        }

        void MainWindow::linkClickedSbResult(const QUrl& url) {


        }

        void MainWindow::createDockWindow() {

        }
        void MainWindow::createDockShowResultWindow() {

        }




        void MainWindow::buildMenuOnPanelsbMenu(const QString& option) {

        }






        void MainWindow::loadSettings() {

        }

        void MainWindow::writeSettings() {



        }


        void MainWindow::setModelCompleter(int opt) {


        }


        void MainWindow::toDelAllWorkflow() {

        }

        void MainWindow::toDelOneWorkflow() {

        }

        void MainWindow::toClearTextEdit() {
            // limpiar la barra de estado

        }


        QMap<QString,bool> MainWindow::doPermiseExecOperationAction(const QString& op) {

            QMap<QString,bool> result;

            QString nameop = op;
            if (op.startsWith(QLatin1String("operacion:titulo::"))) {
                return result;
            }
            if (op.startsWith(QLatin1String("operacion:"))) {
                nameop = op.mid( QString("operacion:").length() );
            }


//            if (!MainWindow::permises.contains(nameop))  {

//                SYD
//                        << tr("La operaci�n \"\"%1\"\" no existe en el archivo de autorizaci�n<br/>."
//                              "Consulte con el administrador para que asigne el permiso solicitado").arg(op);
//                return result;
//            }

            if ( MainWindow::permises[ nameop ].count() < 3) {
                SYE << tr("La operaci�n \"%1\" solo tiene \"%2\" elementos de informaci�n.<br/>Es "
                          "probable que el archivo de autorizaci�n no est� correctamente formado")
                       .arg(nameop).arg(MainWindow::permises[ nameop ].count());

                return result;
            }

            QStringList mytypes = MainWindow::permises[ nameop ].at(1).split(";");

            QStringList myusers = MainWindow::permises[ nameop ].at(0).split(";");
            QStringList myroles = MainWindow::permises[ nameop ].at(2).split(";");


            foreach(QString t, mytypes) {
                QStringList mylist = t.split(":",QString::SkipEmptyParts);
                if (mylist.count()>1) {
                    if (currentaccount == mylist.at(0)) {
                        result[mylist.at(1)] = true;
                    }
                    else {
                        if (myusers.contains(currentaccount) || myroles.contains(MainWindow::currentrole)) {
                            result[mylist.at(1)] = true;
                        }
                        else {
                            result[mylist.at(1)] = false;
                        }
                    }
                }
                else {
                    if (myusers.contains(currentaccount) || myroles.contains(MainWindow::currentrole)) {
                        result[mylist.at(0)] = true;
                    }
                    else {
                        result[mylist.at(0)] = false;
                    }
                }


            }



            return result;
        }

        QMap<QString,QString> MainWindow::loadPermisesHierarchy() {
            int i = 1;
            QMap<QString,QString> result;
            while(true) {
                QString mykeyconf = QString("Hierarchy/path.%1").arg(i);
                QString mypath = SafetYAWL::getAuthConf()[mykeyconf].trimmed();
                if (mypath.isEmpty() || i>10) {
                    break;
                }

                QStringList mylist = mypath.split(",",QString::SkipEmptyParts);
                if (mylist.size() > 0 ) {
                    if ( !result.contains(mylist.at(mylist.size()-1)) ) {
                        result[ mylist.at(mylist.size()-1)] = "";
                    }
                    else {
                        result[ mylist.at(mylist.size()-1)] += ",";
                    }
                    for(int j=mylist.size()-2;j>=0;j--) {
                        result[ mylist.at(mylist.size()-1)] += mylist.at(j);
                        result[ mylist.at(mylist.size()-1)] += ",";
                    }
                    result[ mylist.at(mylist.size()-1)].chop(1);
                }

                i++;
            }

            return result;
        }


        bool MainWindow::doPermiseExecOperation(const QString& op, QMap<QString,QString>& phierarchy,
                                                const QString& permise) {


            QString nameop = op;
            if (op.startsWith(QLatin1String("operacion:titulo::"))) {
                return true;
            }
            if (op.startsWith(QLatin1String("operacion:"))) {
                nameop = op.mid( QString("operacion:").length() );
            }
            if (!MainWindow::permises.contains(nameop))  {

                SYD
                        << tr("La operaci�n \"\"%1\"\" no existe en el archivo de autorizaci�n<br/>."
                              "Consulte con el administrador para que asigne el permiso solicitado").arg(op);
                return false;
            }

            if ( MainWindow::permises[ nameop ].count() < 3) {
                SYE << tr("La operaci�n \"%1\" solo tiene \"%2\" elementos de informaci�n.<br/>Es "
                          "probable que el archivo de autorizaci�n no est� correctamente formado")
                       .arg(nameop).arg(MainWindow::permises[ nameop ].count());
                return false;
            }
            QStringList myusers = MainWindow::permises[ nameop ].at(0).split(";");
            QStringList myroles = MainWindow::permises[ nameop ].at(2).split(";");
            if (!myusers.contains(currentaccount) && !myroles.contains(MainWindow::currentrole)) {
                SYD  << tr("El usuario <b>%1</b> no est� autorizado para utilizar la operaci�n <b>\"%2\"</b>.<br/>"
                           "Consulte con el administrador para que asigne el permiso solicitado")
                        .arg(currentaccount).arg(nameop);
                return false;
            }
            QStringList mytypes = MainWindow::permises[ nameop ].at(1).split(";");

            QString realtypes;
            foreach(QString t, mytypes) {
                QStringList mylist = t.split(":",QString::SkipEmptyParts);
                if (mylist.count()>1) {
                    if (currentaccount == mylist.at(0) || currentrole == mylist.at(0)) {
                        realtypes += mylist.at(1);
                        realtypes += ",";
                    }
                }
                else {
                    realtypes += mylist.at(0);
                    realtypes += ",";
                }
            }
            realtypes.chop(1);

            if (!realtypes.contains(permise) && !MainWindow::checkPermiseInHierarchy(permise,phierarchy,realtypes)) {
                SYE << tr("El usuario \"%1\" no tiene autorizado  el permiso del tipo \"%3\" "
                          "que le permita utilizar la operaci�n \"%2\"<br/>"
                          "Consulte con el administrador para que asigne el permiso solicitado")
                       .arg(currentaccount).arg(nameop).arg(permise);
                SYD << tr("...MainWindow::doPermiseExecOperation...permise:|%2|...realtypes:|%1|")
                       .arg(realtypes)
                       .arg(permise);
                return false;
            }


            return true;
        }

        bool MainWindow::checkPermiseInHierarchy(const QString& permise,
                                                 const QMap<QString,QString>& myphierarchy,
                                                 const QString& realtypes) {
            SYD << tr("...MainWindow::checkPermiseInHierarchy...myphierarchy.size():|%1|")
                   .arg(myphierarchy.size());
            if (!myphierarchy.contains(permise)) {
                return false;
            }
            QStringList  mylist = myphierarchy[permise].split(",",QString::SkipEmptyParts);

            foreach(QString s, mylist) {
                if (realtypes.contains(s)) {
                    return true;
                }
            }
            return false;

        }

        void MainWindow::setupToolbar() {
        }




        void MainWindow::setEnabledToolBar(bool e) {

        }


        void MainWindow::checkSelInputTab(int opt) {
        }


        void MainWindow::selInputTab(int opt)  {

        }


        void MainWindow::toLoadWeb() {

            QString code;
            code = "SAFETlist = ["
                    "{areacode: \"201\", state: \"Merida\"},"
                    "{areacode: \"203\", state: \"Zulia\"},"
                    "{areacode: \"204\", state: \"Trujillo\"},"
                    "{areacode: \"205\", state: \"Bolivar\"}"
                    "];"
                    "myColumnDefs = ["
                    "{key:\"areacode\",label:\"C?digo\",width:100,resizeable:true,sortable:true},"
                    "{key:\"state\",label:\"Estado\",width:100,resizeable:true,sortable:true}"
                    "];"
                    " safetproccessData()";

            //code = "doListTable()";

            evalJS(code);

        }


QString MainWindow::getScriptLen(const QSqlField& f) {

            //    qDebug("...MainWindow::getScriptLen...f.type():%d",f.type());
            QString result = "40";
            switch ( f.type()) {

            case QVariant::String:
                if (f.value().toString().length() > 50 || f.name().contains("desc")) {
                    result = "300";
                }
                else {
                    result = "130";
                }
                break;
            case QVariant::Date:
            case QVariant::DateTime:
                result = "150";
            default:;

            }
            //    qDebug("...MainWindow::getScriptLen...f.type():%d...result:|%s|...len:%d",f.type(),
            //           qPrintable(result),f.value().toString().length());
            //    qDebug();
            return result;
}

void MainWindow::executeJSCodeAfterLoad(/* bool ok */ ) {


            // ** Reemplazar caracteres especiales, hacen falta pruebas aqui
            currentDocuments.replace("\n","");
            // ** Reemplazar caracteres especiales, hacen falta pruebas aqui
            QString data;
            data =  QString( "safetvariable = '%1';\n").arg(_listprincipalvariable);
            data += QString("safetkey = '%1';\n").arg(_listprincipalkey);
            QStringList mylist = _listprincipaltitle.split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);
            data += QString("safettitle = '%1';\n").arg(mylist.at(0));
            if ( mylist.count() > 1 ) {
                data += QString("safetreport = '%1';\n").arg(mylist.at(1));
            }
            else {
                data += QString("safetreport = '%1';\n").arg("Reporte");
            }

            data += QString("safetlistcount = %1;\n").arg(_listprincipalcount);
            SYD << tr("....MainWindow::listDocuments..._listprincipaltitle:|%1|")
                   .arg(_listprincipaltitle);

            QString jsondata;
            jsondata += "safetlist = [";
            jsondata += currentDocuments;
            jsondata += "];\n";




            jscriptcolumns = "safetcolumns  = [";
            int i = 0;
            foreach(QSqlField f, currentFields ) {

                QString scriptlen = getScriptLen(f);
                jscriptcolumns
                        += QString("{ key: \"%1\",label:\"%1\",width:%2,"
                                   "resizeable:true,sortable:true},\n")
                        .arg(f.name())
                        .arg(scriptlen);

                i++;
            }
            if ( i > 0 ) {
                jscriptcolumns.chop(2);
            }
            jscriptcolumns += "];\n";
            jsondata += jscriptcolumns;

            data += jsondata;
            //    qDebug("...data:\n%s\n",
            //           qPrintable(data));

            //    qDebug("...jscriptcolumns:\n%s\n",
            //           qPrintable(jscriptcolumns));

            QString code;
            code = data;
            // Colocando los datos JSON
            _currentjson = data;
            //    _currentjson = jsondata;


            // Colocando los datos JSON
            code += " ";
            code += jscriptcolumns;
            code += " ";
            //    qDebug();
            //    qDebug("code:\n%s",qPrintable(code));
            //    qDebug();


            code = "safetproccessData();";




}

void MainWindow::generateJScriptreports(const QString& documents,
                                                const QList<QSqlField>& fields) {

            currentDocuments = documents;
            currentFields = fields;
            jscriptload = true;

}


QString MainWindow::evalJS(const QString &js) {
            //     Q_CHECK_PTR( weboutput );
            //     qDebug(".....evalJS...QWebFrame *frame = weboutput->page()->mainFrame();");
            //    QWebFrame *frame = weboutput->page()->mainFrame();
            //    return frame->evaluateJavaScript(js).toString();
            return QString("");
}

void MainWindow::toSend(bool sign) {


}

void MainWindow::toInputUsers() {

            SafetTextParser parser;
            QString texto;
            //    texto = completingTextEdit->toPlainText().toLatin1();
            //qDebug(".. ...(1)..texto: \n|%s|\n", qPrintable(texto));

            parser.setStr( texto.toLatin1() );
            QString str = "agregar,eliminar,actualizar,mostrar";
            QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
            parser.setCommands(str.split(","),commandstr.split(","));
            //qDebug("....xml...(1): \n|%s|\n", qPrintable(parser.str()));
            QString xml = parser.toXml();
            //qDebug("....xml...(2): \n|%s|\n", qPrintable(xml));
            SafetYAWL::streamlog << SafetLog::Debug << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
        //    myoutput->setPlainText(xml);


            parser.processInput( xml.toLatin1() );
            QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
            QString filename = "defusers.xml";

            parser.openXml(filepath + "/" + filename);
            QStringList names = parser.loadNameConfs();

            foreach(QString n, names) {
                //         QMap<QString,QVariant> conf = centraledit()->findConf(n);
                //         parser.addConf(conf, n);
            }

            QStringList results = parser.processXml(false);

            if ( queryForErrors() ) {
                qDebug(".......****..queryforerror....");
                return;
            }

            QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf";
            if (QFile::exists(fileconf)) {
                foreach(QString s, results) {
                    qDebug("(toInputUsers)toInputConfigure.......result: %s", qPrintable(s));
                    proccessConfFile(s,fileconf,true);
                }


                doCipherFile(true);
                SafetYAWL::readAuthConfFile();
                qDebug("     ----toInputUsers...SafetYAWL::readAuthConfFile() ");
            }
            else {
                SafetYAWL::streamlog.initAllStack();
                SafetYAWL::streamlog
                        << SafetLog::Error
                        << tr("No se pueden realizar los cambios. No se encuentra el archivo :\"%1\"")
                           .arg(fileconf);

                if ( queryForErrors() ) {
                    qDebug(".......****..queryforerror....(2)");
                    return;
                }


            }

            showSuccessfulMessage(tr("La operaci�n de <b>configuraci�n</b> fue exitosa"
                                     "....<b>ok!</b>"));

        }

        void MainWindow::showSuccessfulMessage(const QString& m) {
            QString message = QString("<table><tr><td><font color=green>%1</font>"
                                      "</td></tr></table>")
                    .arg(m);

        }

        void MainWindow::configureStatusBar() {


        }



        void MainWindow::showSmartMenu() {


        }


        void MainWindow::timeHideResult() {

        }




bool MainWindow::queryForErrors() {


            QStack<QPair<QDateTime,QString> > mystack = SafetYAWL::streamlog.stopErrorStack();

            qDebug("...hay %d errores en la pila", mystack.count());
            SafetYAWL::streamlog.stopAllStack();

            if (mystack.count() > 0 ) { // Verificacion NO exitosa
                QString message = renderMessageStack(mystack,SafetLog::Error);
                //QString messagew = renderMessageStack(mystack,SafetLog::Warning);
                _currenterror = message;
                qDebug("...queryForErrors...saliendo");
                qDebug("...hay %d errores en la pila...(2)...", mystack.count());
                SafetYAWL::streamlog.cleanAllStack();
                return true;
            }
            return false;

 }

 void MainWindow::checkSafetArguments(const QString &s) {
            SafetYAWL::argsflow.clear();
            SafetYAWL::argsflow.resize(10);
            qDebug("...entrando...MainWindow::checkSafetArguments---");
            QString argument = SafetYAWL::getConf()["Widgets/arguments.*"];
            QStringList arguments  = argument.
                    split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);

            QStringList  input = s.split(":");
            if ( input.count() != 2 ) {
                return;
            }

            qDebug("......procesando lista de argumentos....(1)...arguments.count():%d",arguments.count());
            int i = 0;
            foreach(QString a, arguments) {
                QString inputfield = "_"+a;
                qDebug("...inputfield: |%s|", qPrintable(inputfield));
                qDebug("...input.at(0).trimmed(): |%s|", qPrintable(input.at(0).trimmed()));
                if ( inputfield == input.at(0).trimmed() ) {
                    qDebug("..procesando i:%d...|%s|",i,qPrintable(input.at(1).trimmed()));
                    SafetYAWL::argsflow[i] = input.at(1).trimmed();
                }
                i++;

            }
            qDebug("...i: %d",i);


        }



QString  MainWindow::toInputForm(const QString& action, const QString& textquery) {

    SafetTextParser parser;

    if ( textquery.isEmpty()) {
        SYD << QString("[IOPERATIONFORM] action=%1").arg(action);
    }
    else {
        SYD << QString("[IOPERATIONFORM] textquery=%2 action=%1").arg(action).arg(textquery);
    }

    _isgui = false;

    QString myaction;
    if ( !textquery.isEmpty()) {

        QString myquery = textquery;
        myaction = action + myquery.replace(":",Safet::COLONMARK);
    }
    else {
        myaction = action;
    }

    myaction.replace(",",Safet::COMMA);

    qDebug("\n\n..........*************MainWindow::toInputForm:....myaction:|%s|", qPrintable(myaction));


    SafetYAWL::streamlog.initAllStack();

    if (myaction.indexOf(SafetYAWL::ENDOFFLOW) >= 0 ) {
        QString message =   tr("El estado de un documento no puede cambiar al estado \"%1\"")
                .arg(SafetYAWL::ENDOFFLOW);
        message.replace("<","&lt;");
        message.replace(">","&gt;");
        SYE << message;
        queryForErrors();
        qDebug("..........MainWindow::toInputForm...saliendo por aca");
        return QString();

    }

    _listprincipaltitle = "**SAFET**" + myaction;


    parser.setStr( myaction );
    QString str = "agregar,eliminar,actualizar,mostrar";
    QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
    parser.setCommands(str.split(","),commandstr.split(","));

    QString xml = parser.toXml();
    qDebug("\n\n..........*************MainWindow::toInputForm:....xml:|%s|", qPrintable(xml));
    QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
    QString filename = "deftrac.xml";

    parser.processInput( xml );

    parser.openXml(filepath + "/" + filename);


    QStringList results = parser.processXml(true,true);


    if ( results.isEmpty() ) {
        queryForErrors();
        qDebug("..........MainWindow::toInputForm...saliendo por alla");
        return QString("");

    }
    else {

    }

    qDebug("..........******MainWindow::toInputForm...ok!....successfully!");
    return QString("successfully!");

}

QString MainWindow::getFlowParameters(const QString& flowfilename){
    QString result;
    SYD << tr("MainWindow::getFlowParameters: %1...._currconfpath:%2")
           .arg(flowfilename)
           .arg(_currconfpath);


    if ( configurator != NULL ) {
        delete configurator;
        configurator = new SafetYAWL(_currconfpath+ "/" + Safet::datadir);
    }

    configurator->setAutofilters( commands['a']);
    configurator->setRecursivefilters( commands['r']);
    SafetYAWL::streamlog.initAllStack();
    configurator->openXML(flowfilename);

    if (queryForErrors()) {
        //delete configurator;
        qDebug("...retornando.....executedParsed");
        return QString("Ocurri� un error inesperado. Consulte al administrador");
    }

    configurator->openDataSources();

    configurator->convertXMLtoObjects();
    qDebug(".....configurator->convertXMLtoObjects();....(1)...");



    //           bool showdlgpar = SafetYAWL::getConf()["GeneralOptions/parameters.showdialog"]
    //                             .compare("on",Qt::CaseSensitive) == 0;

    SafetWorkflow* mywf = configurator->getWorkflow();


    if (mywf != NULL ) {
        qDebug();
        int n = mywf->getParameterlist().count();
        _wfnumpars = 0;
        for(int i=0; i < n; i++) {
            SafetParameter *p = mywf->getParameterlist().at(i);
            Q_CHECK_PTR(p);
            result += QString("%1: ")
                    .arg(p->title());
            result += QString("%1<br/>\n").arg(getWidget(p));

        }
        _wfnumpars  = 0;

    }

    SYD << tr("...MainWindow::getFlowParameters....result:\n\n%1\n\n")
           .arg(result);
     return result;
}


        QString MainWindow::getWidget(SafetParameter* p) {
            QMap<QString,QVariant> myconf;
            CmdWidget* mywidget = NULL;
            Q_CHECK_PTR( p );
            SafetYAWL::streamlog
                    << SafetLog::Debug
                    << tr("getWidget: title:%1")
                       .arg(p->title());

            if ( !p->options().isEmpty()) {
                myconf.insert("options",p->options());
            }
            if ( !p->path().isEmpty()) {
                myconf.insert("keyvalue",p->path());
            }
            if ( !p->filter().isEmpty()) {
                myconf.insert("filter",p->filter());
            }
            if ( !p->literal().isEmpty()) {
                myconf.insert("literal",p->literal());
            }
            if ( !p->format().isEmpty()) {
                myconf.insert("format",p->format());
            }
            if ( !p->order().isEmpty()) {
                myconf.insert("order",p->order());
            }
            if ( !p->validation().isEmpty()) {
                myconf.insert("validation",p->validation());
            }
            if ( !p->input().isEmpty()) {
                myconf.insert("input",p->input());
            }


            QString mytype = p->type();
            DomModel::TypeField enumtype = DomModel::typeFieldFromString(mytype);

            QWidget* parent = NULL;
            QString defaultvalue;
            defaultvalue = p->defaultvalue();
            QString currid;
            if (!p->configurekey().isEmpty()) {
                currid = QString("configurekey.%1")
                        .arg(p->configurekey());
            }
            else {
                currid = QString("parameters.%1")
                        .arg( _wfnumpars );
                _wfnumpars++;
            }

            switch (enumtype) {
            case DomModel::FileName:
                mywidget = new GetFileWidget(currid,parent,false);
                Q_CHECK_PTR( mywidget );
                mywidget->setConf(myconf);
                mywidget->buildWidget();
                // mywidget->setValidator( validator);
                if (!defaultvalue.isEmpty()) {
                    //            mywidget->setText(defaultvalue);
                }
                return mywidget->html();
            case DomModel::DateTime:
                mywidget = new DateTimeWidget(currid, parent,false);
                Q_CHECK_PTR( mywidget );
                mywidget->setConf(myconf);
                if (!defaultvalue.isEmpty()) {
                    //            mywidget->setText(defaultvalue);
                }

                return mywidget->html();
            case DomModel::String:
                mywidget = new CmdWidget(currid, NULL,false);
                mywidget->setConf(myconf);
                Q_CHECK_PTR( mywidget );
                mywidget->buildWidget();
                //  mywidget->setValidator( validator);
                //        if (!defaultvalue.isEmpty()) {
                //            mywidget->setText(defaultvalue);
                //        }
                return mywidget->html();
            case DomModel::Suggest:
                mywidget = new SuggestWidget(currid, NULL,false);
                mywidget->setConf(myconf);
                Q_CHECK_PTR( mywidget );
                //        mywidget->buildWidget();
                // mywidget->setValidator( validator);
                if (!defaultvalue.isEmpty()) {
                    //            mywidget->setText(defaultvalue);
                }

                return mywidget->html();

            case DomModel::Number:
                mywidget = new NumberWidget(currid, NULL, false);
                mywidget->setConf(myconf);
                Q_CHECK_PTR( mywidget );
                //        mywidget->buildWidget();
                // mywidget->setValidator( validator);
                if (!defaultvalue.isEmpty()) {
                    //            mywidget->setText(defaultvalue);
                }

                return mywidget->html();
            case DomModel::StringBox:
//                mywidget = new TextEditWidget(currid, NULL,false);
//                Q_CHECK_PTR( mywidget );
//                mywidget->setConf(myconf);
//                mywidget->buildWidget();
//                // mywidget->setValidator( validator);
//                if (!defaultvalue.isEmpty()) {
//                    //            mywidget->setText(defaultvalue);
//                }
//
//                return mywidget->html();

            case DomModel::ComboListTable:
                qDebug("....combotable.....(1)...");
                mywidget = new ComboWidget(currid,ComboWidget::ListTable, NULL,false);
                qDebug("....combotable.....(2)...");
                mywidget->setConf(myconf);
                mywidget->buildWidget();
                qDebug("....combotable.....(3)...");

                return mywidget->html();
            case DomModel::ComboListLiteral:
                mywidget = new ComboWidget(currid,ComboWidget::ListLiteral, parent);
                mywidget->setConf(myconf);
                mywidget->buildWidget();

                return mywidget->html();
            case DomModel::ComboColor:
                mywidget = new ComboWidget(currid,ComboWidget::ColorSafet, parent,false);
                mywidget->setConf(myconf);
                mywidget->buildWidget();

                return mywidget->html();
                break;
            case DomModel::ComboConffile:
            case DomModel::ListConffile:
                if (   enumtype  == DomModel::ComboConffile ) {
                    mywidget = new ComboWidget(currid,ComboWidget::ConffileSafet, parent,false);
                }
                else {
                    mywidget = new ListWidget(currid,ListWidget::ConffileSafet, parent);

                }

                mywidget->setConf(myconf);
                mywidget->buildWidget();

                return mywidget->html();
            case DomModel::ListTable:
                mywidget = new ListWidget(currid,ListWidget::ListTable, parent,false);
                mywidget->setConf(myconf);
                mywidget->buildWidget();
                qDebug("ListTable...mywidget->setConf(myconf)...(2)...");
                return mywidget->html();
            case DomModel::ListLiteral:
                mywidget = new ListWidget(currid,ListWidget::ListLiteral, parent,false);
                mywidget->setConf(myconf);
                mywidget->buildWidget();
                qDebug("ListLiteral...mywidget->setConf(myconf)...(2)...");
                return mywidget->html();
            default:
                mywidget = NULL;
            }


            if (mywidget != NULL ) {
                return mywidget->html();
            }

        }


 QString MainWindow::replaceMarks(const QString& s) {
            QString result = s;

            result.replace(Safet::COLONMARK,":");
            return result;
  }


 QString MainWindow::extractParameters(const QString& action) {
            QString result = action;

            int pos = 0;
            QRegExp rxpars("(parameters\\.)([a-zA-Z0-9_\\-\\./]+)\\s*:\\s*([a-zA-Z_0-9�������'\\*=\\.\\-\\(\\),;%#\\x3c\\x3e\\x2f\\[\\]/]+)");
            QRegExp rxconf("(configurekey\\.)([a-zA-Z0-9_\\-\\./]+)\\s*:\\s*([a-zA-Z_0-9�������'\\*=\\.\\-\\(\\),;%#\\x3c\\x3e\\x2f\\[\\]/]+)");
            QMap<QString,QString> mypars;
            QMap<QString,QString> myconfigs;

            while (pos < action.length()) {
                pos = action.indexOf(rxpars,pos);
                if (pos==-1) {
                    break;
                }
                mypars[rxpars.cap(2)] = rxpars.cap(3);
                pos += rxpars.cap(0).length()+1;
                result.replace(rxpars.cap(0),"");

            }
            SYD << tr("...MainWindow::extractParameters...mypars.count()):|%1|")
                   .arg(mypars.count());

            // para configuraciones
            pos = 0;
            while (pos < action.length()) {
                pos = action.indexOf(rxconf,pos);
                if (pos==-1) {
                    break;
                }
                myconfigs[rxconf.cap(2)] = rxconf.cap(3);
                pos += rxconf.cap(0).length()+1;
                result.replace(rxconf.cap(0),"");

            }
            if (mypars.count()> 0 ) {
                setParsValues(mypars);
            }
            if (myconfigs.count() > 0 ) {
                setConffileValues(myconfigs);
            }


            return result;
 }




QString  MainWindow::toInputFlow(const QString& action) {

            SYD << QString("[IOPERATIONFLOW] action=%1").arg(action);

            QString newaction = extractParameters(action);

            SafetTextParser parser;



            _listprincipaltitle = SafetYAWL::LISTSEPARATORCHARACTER + newaction;

            _listprincipaltitle.replace("\n"," ");
            _listprincipaltitle.replace("\r"," ");

            parser.setStr( newaction );

            QString str = "agregar,eliminar,actualizar,mostrar";
            QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
            parser.setCommands(str.split(","),commandstr.split(","));

            QString xml = parser.toXml();


            parser.processInput( xml.toLatin1() );
            QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
            QString filename = "defflow.xml";


            SafetYAWL::streamlog.initAllStack();
            parser.openXml(filepath + "/" + filename);
            qDebug("...flow...SafetYAWL::streamlog.initAllStack();....(2)...");
            if ( queryForErrors() ) {
                return QString();
            }

            QStringList results = parser.processXml(false,true);
            if ( results.isEmpty() ) {
                if ( queryForErrors() ) {
                    return QString();
                }
            }

            ParsedSqlToData data;
            if ( results.count() > 0) {
                data = SafetTextParser::parseSql(results.at(0),true);

                if (parser.operationName().compare(tr("Add_step_into_story"),Qt::CaseSensitive) == 0 ) {
                    QString texto = QString("|%1|%2|%3|")
                            .arg(data.map["Story_name"])
                            .arg(data.map["Previous_step"])
                            .arg(MainWindow::unconvertTitle(data.map["Name_step"]));

                    QString myfile = data.map["Story_name"];
                    QString mytask = data.map["Previous_step"];
                    QString mynewtask =  MainWindow::unconvertTitle(data.map["Name_step"]);
                    qDebug("*Modify story.....:|%s|",qPrintable(texto));

                    QString result = addNodeToXMLWorkflow(myfile, mytask, mynewtask);
                    qDebug("....toInputFlow...result:|%s|",qPrintable(result));
                    if (result.isEmpty()) {
                        _currentjson = "";

                    }
                    else {
                        _currentjson = tr("successfully!");
                    }

                }
                else if (parser.operationName().compare(tr("Delete_step_in_story"),Qt::CaseSensitive) == 0  ) {
                    QString myfile = data.map["Story_name"];
                    QString mytask =  data.map["Name_step"];
                    QString result = delNodeToXMLWorkflow(myfile, mytask);
                    qDebug("....toInputFlow...result:|%s|",qPrintable(result));

                    QString myoperation = QString("operacion:safet_delete_step story_name:%1 Name_status:%2")
                            .arg(extractFileName(myfile))
                            .arg(mytask);

                    qDebug("....toInputFlow...toInputForm,,,,,myoperation:|%s|",qPrintable(myoperation));
                    result = toInputForm(myoperation);
                    qDebug("....toInputFlow...toInputForm,,,,,result:|%s|",qPrintable(result));
                    _currentjson = tr("successfully!");

                }
                else if (parser.operationName().compare(tr("Change_name_step"),Qt::CaseSensitive) == 0  ) {
                    QString myfile = data.map["Story_name"];
                    QString mytask = data.map["Previous_step"];
                    QString mynewtask =  data.map["Name_step"];
                    QString result = delNodeToXMLWorkflow(myfile, mytask);
                    result = addNodeToXMLWorkflow(myfile, mytask, mynewtask);

                }
                else if (parser.operationName().compare(tr("Create_new_story"),Qt::CaseInsensitive) == 0  ) {

                    QString myls = MainWindow::unconvertTitle(data.map["Live_story_name"]);
                    QString mystory  = MainWindow::unconvertTitle(data.map["Story_name"]).toLower();
                    if ( addXmlMenuGroupForm(mystory) ) {
                        _currentjson = tr("successfully!");
                        qDebug("...agregando correctamente...addXmlMenuGroupForm....");
                        QString myoperation = QString("operacion:Add_new_live_story Live_story_name:%1 "
                                                      " From_story_name: %2")
                                .arg(myls)
                                .arg( _currconfpath+ "/" + Safet::datadir + "/flowfiles/" + mystory +".xml");
                                qDebug(".......MainWindow::toInputFlow...(for to form)....operation:|%s|",qPrintable(myoperation));
                        _currentjson = toInputForm(myoperation);
                        if ( _currentjson != tr("successfully!")) {
                            queryForErrors();
                            _currentjson = "";
                        }
                        else {
                            QString myoperation = QString("operacion:safet_Add_query Story_name:%3 Live_story:%2  Description: %1/%2  Text_query: ")
                                    .arg(mystory)
                                    .arg(myls)
                                    .arg(_currconfpath+ "/" + Safet::datadir + "/flowfiles/" + mystory +".xml");

                           QString myquery = QString("operacion:Show_live_story_flow Name_story:%1  "
                                                     "Live_story:%2 ")
                                   .arg(_currconfpath+ "/" + Safet::datadir + "/flowfiles/" + mystory +".xml")
                                   .arg(myls);

//                           qDebug("... MainWindow::toInputFlow. Create Story....myoperation:|%s|",qPrintable(myoperation));
//                           qDebug("... MainWindow::toInputFlow. Create Story....myquery:|%s|",qPrintable(myquery));
                           toInputForm(myoperation, myquery);
                        }

                    }
                    else{
                        qDebug("return ...no....");

                        _currentjson = "";

                    }

                }
                else if (parser.operationName().compare(tr("Add_new_live_story"),Qt::CaseInsensitive) == 0  ) {

                    QString mylive = MainWindow::unconvertTitle(data.map["Live_story_name"]).toLower();
                    if ( addXmlMenuGroupForm(mylive) ) {
                        _currentjson = tr("successfully!");
                    }
                    else{
                        _currentjson = "";
                    }

                }

                else if (parser.operationName().compare(tr("Delete_story"),Qt::CaseInsensitive) == 0  ) {

                    QString mystory = extractFileName(data.map["Story_name"]);
                    qDebug("...........MainWindow::toInputFlow....name:|%s|", qPrintable(mystory));
                    QString myoperation = QString("operacion:safet_delete_steps story_name:%1")
                            .arg(mystory);
                    qDebug("...........MainWindow::toInputFlow....Delete_live_step...myoperation:|%s|", qPrintable(myoperation));

                    QString result = toInputForm(myoperation);
                    qDebug("...........MainWindow::toInputFlow....Delete_live_step...result:|%s|", qPrintable(mystory));
                   myoperation = QString("operacion:safet_Delete_live flujo:%1")
                            .arg(data.map["Story_name"]);
                    result = toInputForm(myoperation) ;

                    qDebug("...........MainWindow::toInputFlow....Delete_live_story...result:|%s|", qPrintable(mystory));
                    if ( delXmlMenuGroupForm(data.map["Story_name"]) ) {
                        _currentjson = tr("successfully!");
                    }
                    else{
                        _currentjson = "";
                    }
                }

            }
            if (_currentjson.isEmpty()) {
                  queryForErrors();
            }

            return currentJSON();
}

bool MainWindow::toBatch() {
    QString mypathfile = _currconfpath+ "/" + Safet::datadir + "/" + "batch.txt";

       if (!QFile::exists(mypathfile)) {
        SYE << tr("\"%1\" is not exist in safet configuration directory").arg("batch.txt");
        qDebug("...MainWindow::toBatch... no existe...mypathfile:|%1|",qPrintable(mypathfile));
        return false;

    }

    QFile myfile(mypathfile);

    bool isopen = myfile.open(QIODevice::ReadOnly);

    if (!isopen ) {
        SYE << tr("Can not read batch file \"%1\"")
               .arg(mypathfile);
        qDebug("...no se lee:|%s|", qPrintable(mypathfile));
        return false;

    }

    QTextStream in(&myfile);
    in.setCodec("ISO 8859-1");

    QRegExp rx;
    rx.setPattern("\\[IOPERATION([A-Z]+)\\]\\s+action\\=(.*)");
    QRegExp rxq;
    rxq.setPattern("(textquery\\=(.*))\\s+action\\=");

    while ( !in.atEnd()) {
        QString line = in.readLine();
        QString myquery;
        int pos  = rxq.indexIn(line);
        if (pos != -1 ) {
            QString myextract = rxq.cap(1);
            qDebug("....MainWindow::toBatch....myextract:|%s|",qPrintable(myextract));
            myquery = rxq.cap(2);
            line.replace(myextract,"");

        }
        pos = rx.indexIn(line);
         if  ( pos == -1 ) {
             SYW << tr("line:%1 is bad format").arg(line);
             qDebug("........MainWindow::toBatch....bad format: %1", qPrintable(line));
             continue;
        }
         QString mycmd = rx.cap(1);
         QString myaction = rx.cap(2);
         qDebug("....MainWindow::toBatch....cmd:|%s|",qPrintable(mycmd));
         qDebug("....MainWindow::toBatch....myquery:|%s|",qPrintable(myquery));
         qDebug("....MainWindow::toBatch....myaction:|%s|",qPrintable(myaction));

         if (mycmd == "FORM") {
             _currentjson = toInputForm(myaction,myquery);
         }
         else if (mycmd == "CONSOLE") {
             // no show
             // toInputConsole(myaction);

         }
         else if (mycmd == "FLOW") {
             _currentjson = toInputFlow(myaction);
         }
         else {
             SYW << tr("line:\"%1\" is command not reconognized").arg(line);
         }




    }


    myfile.close();

    return true;





}

QString  MainWindow::toInputConsole(const QString& action) {

    SYD << QString("[IOPERATIONCONSOLE] action=%1").arg(action);
            // icono de progreso en barra de estado


    qDebug("....MainWindow::toInputConsole...(1)...action:|%s|", qPrintable(action));
    if (action.indexOf("operacion:Show_live_story_flow") != -1) {

        setLastOperation(action);
        qDebug("...........MainWindow::toInputConsole....setLastOperation:|%s|",
               qPrintable(action));

    }


            QString newaction = extractParameters(action);

            SafetTextParser parser;

            SYD << tr("...SafetTextParser parser(this);.....(2)");

            //    texto  = completingTextEdit->toPlainText().toLatin1();


            _listprincipaltitle = SafetYAWL::LISTSEPARATORCHARACTER + newaction;

            _listprincipaltitle.replace("\n"," ");
            _listprincipaltitle.replace("\r"," ");

            parser.setStr( newaction );

            QString str = "agregar,eliminar,actualizar,mostrar";
            QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
            parser.setCommands(str.split(","),commandstr.split(","));

            qDebug("....toXml()...1");
            QString xml = parser.toXml();


            parser.processInput( xml.toLatin1() );
            QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
            filepath = QDir::homePath() + "/.safet/input";
            QString filename = "defconsole.xml";


            SafetYAWL::streamlog.initAllStack();
            parser.openXml(filepath + "/" + filename);
            qDebug("...console...SafetYAWL::streamlog.initAllStack();....(queryForErrorsq2)...");
            if ( queryForErrors() ) {

                return QString();
            }

            QStringList results = parser.processXml(false,true);
            if ( results.isEmpty() ) {
                qDebug("...console...SafetYAWL::streamlog.initAllStack();....(queryForErrorsq3)...");
                if ( queryForErrors() ) {
                    qDebug("...console...SafetYAWL::streamlog.initAllStack();....(queryForErrorsq4)...");
                    return QString() ;
                }
            }


            qDebug("....MainWindow::toInputConsole...........(1*).........");
            ParsedSqlToData data;
            showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver gr�fico de flujo de trabajo");
            if ( results.count() > 0) {
                data = SafetTextParser::parseSql(results.at(0),true);


                if ( parser.operationName().startsWith("Listar_",Qt::CaseSensitive)
                     && parser.operationName().indexOf("_autofiltro") == -1
                     && parser.operationName().indexOf("_filtrorecursivo") == -1
                     ) {
                    showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver reporte");
                    QString texto = QString("-f %1 -d -v %2")
                            .arg(data.map["Cargar_archivo_flujo"])
                            .arg(data.map["Variable"]);
                    parseArgs( texto );

                    if (! executeParsed() ) {
                        qDebug("....MainWindow::toInputConsole...noexecuted...");
                        return QString();
                    }

                    QString myjson = currentJSON();
                    qDebug("...**MainWindow::toInputConsole...currentJSON():|%s|",qPrintable(myjson));
                    return myjson;

                }
                else if (parser.operationName().compare("Listar_datos_con_autofiltro",Qt::CaseSensitive) == 0 ) {
                    showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver reporte");
                    QString texto = QString("-f %1 -d -v %2 -a %3")
                            .arg(data.map["Cargar_archivo_flujo"])
                            .arg(data.map["Variable"])
                            .arg(data.map["Autofiltro"]);

                    qDebug("*Listar_datos_con_autofiltro.....:|%s|",qPrintable(texto));
                    parseArgs( texto );

                    //                loadReportTemplate();
                    if (! executeParsed() ) {
                        return QString();
                    }



                }
                else if (parser.operationName().compare("Listar_datos_con_filtrorecursivo",Qt::CaseSensitive) == 0 ) {
                    showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;").arg("Ver reporte");
                    QString texto = QString("-f %1 -d -v %2 -r %3")
                            .arg(data.map["Cargar_archivo_flujo"])
                            .arg(data.map["Variable"])
                            .arg(data.map["Filtro_recursivo"]);

                    qDebug("*Listar_datos_con_filtrorecursivo.....:|%s|",qPrintable(texto));
                    parseArgs( texto );

                    //                loadReportTemplate();
                    if (! executeParsed() ) {
                        return QString();
                    }



                }
                else if (parser.operationName().startsWith("Listar_datos_para_clave") ) {
                    showString = QString("<a href=\"operacion:::%1\" title=\"%1\">%1</a>&nbsp;&nbsp;&nbsp;")
                            .arg("Ver reporte");
                    QString texto = QString("-f %1 -d -k %2")
                            .arg(data.map["Cargar_archivo_flujo"])
                            .arg(data.map["Clave"]);

                    SYD << tr("..............MainWindow::toInputCOnsole...Listar_datos_para_clave.....texto:"
                              "|%1|").arg(texto);
                    parseArgs( texto );

                    if (! executeParsed() ) {
                        return QString();
                    }



                }

                else if ( parser.operationName().compare(QString::fromLatin1("Generar_gr�fico_b�sico"),Qt::CaseSensitive) == 0) {
                    QString texto = QString("-f %1 -p graphviz -g %2")
                            .arg(data.map["Cargar_archivo_flujo"]);

                    parseArgs( texto );
                    if (! executeParsed() ) {
                        return QString();
                    }



                }
                else if ( parser.operationName().compare(QLatin1String("Generar_gr�fico_coloreado"),Qt::CaseSensitive) == 0) {
                    QString texto = QString("-f %1 -p graphviz -g -k coloured")
                            .arg(data.map["Cargar_archivo_flujo"]);

                    parseArgs( texto );
                    if (! executeParsed() ) {
                        qDebug("...no generado grafico coloreado...");
                        return QString();
                    }


                }
                else if ( parser.operationName().compare("Generar_gr�fico_con_autofiltro",Qt::CaseSensitive) == 0) {
                    QString texto = QString("-f %1 -p graphviz -g -k coloured -a %2")
                            .arg(data.map["Cargar_archivo_flujo"])
                            .arg(data.map["Autofiltro"]);

                    qDebug("Listar_datos_con_autofiltro.....:|%s|",qPrintable(texto));
                    parseArgs( texto );
                    if (! executeParsed() ) {
                        return QString();
                    }


                }
                else if ( parser.operationName().compare("Generar_gr�fico_con_filtrorecursivo",Qt::CaseSensitive) == 0) {
                    QString texto = QString("-f %1 -p graphviz -g -k coloured -r %2")
                            .arg(data.map["Cargar_archivo_flujo"])
                            .arg(data.map["Filtro_recursivo"]);
                    qDebug("**** FILTRO RECURSIVO TEXTO: ||%s||", qPrintable(texto));
                    parseArgs( texto );
                    qDebug( "****  commands.count(): %d", commands.count());
                    if (! executeParsed() ) {
                        return QString();
                    }



                }
                else if (parser.operationName().startsWith("Show_live_story_flow")) {
                    QString texto = QString("-f %1 -p graphviz -g -k %2 ")
                            .arg(data.map["Name_story"])
                            .arg(data.map["Live_story"]);
                    qDebug("**** ****** TEXTO: ||%s||", qPrintable(texto));
                    parseArgs( texto );
                    qDebug( "*************  commands.count(): %d", commands.count());
                    if (! executeParsed() ) {
                        return QString();
                    }

                }
                else if (parser.operationName().startsWith("Restaurar_gr�fico_de_flujo")) {

                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    SYD << tr("......**MainWindow::toInputConsole....namegraph: |%1|")
                           .arg(namegraph);
                    if ( configurator == NULL ) {
                        SYE << tr("No se ha cargardo el Objeto para generar flujos de trabajo");
                        return QString();
                    }
                    evalConffileValues();
                    doRestoreGraph();

                    QString code;
                    if ( !graphs().contains(namegraph)) {
                        SYE << tr("No se encuentra el nombre de grafo seleccionado : \"%1\"")
                               .arg(namegraph);
                        return QString();

                    }
                    code = graphs()[namegraph].first;
                    SYD << tr("MainWindow::toInputConsole....code: |%1|")
                           .arg(code);

                    QString datetag = graphs()[namegraph].second;
                    doRenderGraph(code,datetag);


                }
                else if (parser.operationName().startsWith(tr("Agregar_planificaci�n"))) {
                    QString pathgraph = MainWindow::replaceMarks(data.map["Cargar_archivo_flujo"]);
                    SYD << tr("......***MainWindow::toInputConsole....compare...pathgraph: |%1|")
                           .arg(pathgraph);
                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    SYD << tr("......***MainWindow::toInputConsole....compare...namegraph: |%1|")
                           .arg(namegraph);

                    if ( doGeneratePlannedGraph(pathgraph, namegraph) ) {
                        _currentjson = tr("Se guardo el grafo de planificacion \"%1\" con �xito##SAFETMESSAGE##")
                                .arg(namegraph);
                    }
                    else {
                        _currentjson = tr("No se guard�  el grafo de planificacion. Vea el registro para detalles##SAFETMESSAGE##");
                    }


                }
                else if (parser.operationName().startsWith(tr("Ver_listado_estad�sticas"))) {
                    bool ok;
                    uint seconds = data.map["Desde"].toUInt(&ok);
                    QDateTime myfromdate = QDateTime::fromTime_t(seconds);
                    QString fromdate = myfromdate.toString("dd/MM/yyyy hh:mmap");
                    seconds = data.map["Hasta"].toUInt(&ok);
                    QDateTime mytodate = QDateTime::fromTime_t(seconds);
                    QString todate = mytodate.toString("dd/MM/yyyy hh:mmap");



                    SafetBinaryRepo myrepo;

                    if (!myrepo.fileExists()) {

                        _currentjson = "No hay estadisticas guardadas actualmente##SAFETMESSAGE##";

                    }
                    else {
                        QStringList todatelist, tospandatelist;
                        todatelist.append("timeprev");
                        todatelist.append("timecurr");
                        tospandatelist.append("secondswait");
                        myrepo.open();
                        QPair<QString,QString> myfields = myrepo.dataJSON("graphs","",
                                                                          todatelist,
                                                                          tospandatelist);

                        _currentjson += myfields.first;
                        _currentjson += "\n";
                        _currentjson += myfields.second;
                        _currentjson += "\n";

                    }
                    qDebug("..........MainWindow::toInputConsole..._currentjson:\n|%s|\n",qPrintable(_currentjson));

                }
                else if (parser.operationName().startsWith(tr("Agregar_nodo_a_gr�fico_de_flujo"))) {
                    SYD << tr(".....MainWindow::toInputConsole....Agregar_nodo_a_gr�fico_de_flujo...:");
                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    QString beforenode = MainWindow::replaceMarks(data.map["Nodo_anterior"]);
                    QString newnamenode = MainWindow::replaceMarks(data.map["Nombre_nuevo_nodo"]);
                    QString paralell = MainWindow::replaceMarks(data.map["Es_paralelo_al_nodo_anterior"]);
                    QString foptions = MainWindow::replaceMarks(data.map["Campo_options"]);
                    QString fquery = MainWindow::replaceMarks(data.map["Campo_query"]);
                    QString ftitle = MainWindow::replaceMarks(data.map["Campo_title"]);
                    QString fdocumentsource = MainWindow::replaceMarks(data.map["Campo_documentsource"]);


                    bool isparalell = false;

                    if (paralell == "Si") {
                        isparalell = true;
                    }

                    SafetYAWL::streamlog.initAllStack();
                    QString result = addNodeToXMLWorkflow(namegraph,beforenode,newnamenode,isparalell,foptions,
                                                          fquery,ftitle,fdocumentsource);


                    if (!result.isEmpty()) {
                        _currentjson = tr("Agregado nodo \"%1\" a \"%2\" satisfactoriamente!\n##SAFETMESSAGE##")
                                .arg(newnamenode)
                                .arg(result);
                    }
                    else {
                        queryForErrors();
                        return QString();
                    }

                }
                else if (parser.operationName().startsWith(tr("Borrar_nodo_de_gr�fico_de_flujo"))) {
                    SYD << tr(".....MainWindow::toInputConsole....Borrar_nodo_de_gr�fico_de_flujo...:");
                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    QString deletenode = MainWindow::replaceMarks(data.map["Nodo_a_borrar"]);

                    QString result = delNodeToXMLWorkflow(namegraph,deletenode);
                    if (!result.isEmpty()) {
                        _currentjson = tr("Eliminado nodo \"%1\" de \"%2\" satisfactoriamente!\n##SAFETMESSAGE##")
                                .arg(deletenode)
                                .arg(result);
                    }
                    else {
                        queryForErrors();
                        return QString();
                    }

                }
                else if (parser.operationName().startsWith(tr("Cambiar_conexi�n_de_gr�fico_de_flujo"))) {
                    SYD << tr(".....MainWindow::toInputConsole....Cambiar_conexi�n_de_gr�fico_de_flujo...:");
                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    QString nodesource = MainWindow::replaceMarks(data.map["Nodo_origen"]);
                    QString nodetarget = MainWindow::replaceMarks(data.map["Nodo_destino_actual"]);
                    QString newnodetarget = MainWindow::replaceMarks(data.map["Nuevo_nodo_destino"]);


                    SYD << tr("---MainWindow::toInputConsole....(change)namegraph:|%1|").arg(namegraph);
                    SYD << tr("---MainWindow::toInputConsole....(change)nodesource:|%1|").arg(nodesource);

                    SafetYAWL::streamlog.initAllStack();
                    QString result = changeConnXMLWorkflow(namegraph,
                                                           nodesource,
                                                           nodetarget,
                                                           newnodetarget);
                    if (!result.isEmpty()) {
                        _currentjson = tr("Se cambi� la conexi�n en el grafo  \"%1\" de \"%2\""
                                          "a \"%3\" satisfactoriamente!\n##SAFETMESSAGE##")
                                .arg(result)
                                .arg(nodetarget)
                                .arg(newnodetarget);
                    }
                    else {
                        queryForErrors();
                        return QString();
                    }

                }
                else if (parser.operationName().startsWith(tr("Cambiar_fecha_de_planificaci�n"))) {
                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    SYD << tr("......**MainWindow::toInputConsole....Cambiar_fecha_de_planificaci�n...namegraph: |%1|")
                           .arg(namegraph);
                    QString nametask = MainWindow::replaceMarks(data.map["Tarea"]);
                    SYD << tr("......**MainWindow::toInputConsole....Cambiar_fecha_de_planificaci�n...nametask: |%1|")
                           .arg(nametask);

                    bool ok;
                    uint seconds = data.map["fecha_planificada"].toUInt(&ok);
                    QDateTime mydate = QDateTime::fromTime_t(seconds);
                    QString namedate = mydate.toString("dd/MM/yyyy hh:mmap");

                    SYD << tr("\n\n......**MainWindow::toInputConsole....Cambiar_fecha_de_planificaci�n...namedate: |%1|")
                           .arg(namedate);

                    doRestoreGraph(Safet::PLANNEDGRAPHSFILENAME);

                    Q_ASSERT( MainWindow::graphs().contains(namegraph));
                    QString code = MainWindow::graphs()[namegraph].first;

                    code = doModifyDateGraph(nametask,mydate,code);
                    MainWindow::graphs()[namegraph].first = code;
                    doSaveCompleteMapGraphs(Safet::PLANNEDGRAPHSFILENAME);

                    _currentjson = tr("�Del grafo \"%3\" Se cambi� la fecha de la tarea \"%1\" a \"%2\" satisfactoriamente!##SAFETMESSAGE##")
                            .arg(nametask)
                            .arg(namedate)
                            .arg(namegraph);


                }
                else if (parser.operationName().startsWith(tr("Copiar_fechas_de_planificaci�n"))) {
                    QString source = MainWindow::replaceMarks(data.map["Nombre_grafo_origen"]);
                    QString target = MainWindow::replaceMarks(data.map["Nombre_grafo_destino"]);


                    doRestoreGraph(Safet::PLANNEDGRAPHSFILENAME);

                    if ( !MainWindow::graphs().contains(source) ) {
                        SYE << tr("No existe el grafo origen: \"%1\"")
                               .arg(source);
                        return QString();
                    }

                    if ( !MainWindow::graphs().contains(target) ) {
                        SYE << tr("No existe el grafo destino: \"%1\"")
                               .arg(target);
                        return QString();
                    }


                    QString codesource = MainWindow::graphs()[source].first;
                    QString codetarget = MainWindow::graphs()[target].first;

                    QMap<QString,QDateTime> mydates;

                    SYD << tr("....MainWindow...toInputConsole....Copiar_fechas...");
                    mydates = getDatesGraph(codesource);

                    for(int i=0; i < mydates.keys().count(); i++) {
                        QString mytask = mydates.keys().at(i);
                        if (mydates.contains(mytask)) {
                            QDateTime mydate = mydates[ mytask ];
                            SYD << tr("...toInputConsole: %1->%2")
                                   .arg(mytask)
                                   .arg(mydate.toString("dd/MM/yyyy hh:mmap"));
                            codetarget = doModifyDateGraph(mytask,mydate,codetarget);
                        }
                    }
                    SYD << tr("target:%1\n%2\n\n")
                           .arg(target)
                           .arg(codetarget);
                    MainWindow::graphs()[target].first = codetarget;
                    doSaveCompleteMapGraphs(Safet::PLANNEDGRAPHSFILENAME);

                    _currentjson = tr("Se copiaron todas las fechas del grafo \"%1\" "
                                      "al grafo \"%2\" satisfactoriamente!\n##SAFETMESSAGE##")
                            .arg(source)
                            .arg(target);

                }
                else if (parser.operationName().startsWith(tr("Comparar_gr�fico_de_flujo"))) {


                    // Colocando configuraci�n de comparaci�n por defecto
                    SafetYAWL::getConf().getMap()["Plugins.Graphviz/plugins.graphviz.extrainfo.show"]
                            = "InfoText,InfoDate";
                    // Colocando configuraci�n de comparaci�n por defecto

                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    SYD << tr("......**MainWindow::toInputConsole....compare...namegraph: |%1|")
                           .arg(namegraph);
                    QString nameplannedgraph = MainWindow::replaceMarks(data.map["Planificado"]);
                    SYD << tr("......**MainWindow::toInputConsole....compare...nameplannedgraph: |%1|")
                           .arg(nameplannedgraph);

                    if ( configurator == NULL ) {
                        SYE << tr("No se ha cargardo el Objeto para generar flujos de trabajo");
                        return QString();
                    }
                    QString code, codeplanned; // C�digo Real y Planificado
                    evalConffileValues();

                    doRestoreGraph();

                    if ( !graphs().contains(namegraph)) {
                        SYE << tr("No se encuentra el nombre de grafo seleccionado : \"%1\"")
                               .arg(namegraph);
                        return QString();

                    }
                    code = graphs()[namegraph].first;
                    SYD << tr("MainWindow::toInputConsole....Comparar_grafico...code: |%1|")
                           .arg(code);


                    QString datetag = graphs()[namegraph].second;

                    // Para Gr�fico Planificado ********

                    doRestoreGraph(Safet::PLANNEDGRAPHSFILENAME);
                    if ( !graphs().contains(nameplannedgraph)) {
                        SYE << tr("No se encuentra el nombre de grafo seleccionado (Planificado) : \"%1\"")
                               .arg(nameplannedgraph);
                        return QString();

                    }

                    codeplanned = graphs()[nameplannedgraph].first;

                    SYD << tr(".......................MainWindow::toInputConsole....**codeplanned**:\n%1\n-------------\n")
                           .arg(codeplanned);
                    QString compcode = doCompareGraphs(code,codeplanned);



                    if ( !compcode.isEmpty()) {
                        doRenderGraph(compcode,datetag);
                    }
                    SYD << tr("....***MainWindow::toInputConsole....regresando...(1)...");


                }
                else if (parser.operationName().compare("Borrar_estad�stica",Qt::CaseSensitive) == 0 ) {

                    QString namestats = data.map["Clave_completa"];
                    SYD << tr("....MainWindow::toInputConsole...Borrar_estad�stica...(1)...namestats:"
                              "|%1|")
                           .arg(namestats);

                    QString delall = data.map["borrar_todos"];
                    SYD << tr("....MainWindow::toInputConsole...Borrar_estad�stica...(1)...borrar_todos:"
                              "|%1|")
                           .arg(delall);

                    QString whereclause = "1";
                    if (delall.isEmpty()) {
                        whereclause = QString("completekey= '%1'")
                                .arg(namestats);
                    }
                    SafetBinaryRepo myrepo;

                    if (!myrepo.fileExists()) {

                        _currentjson = tr("La base de datos de estad�sticas est� vac�a."
                                          "No se elimin� ning�n registro.##SAFETMESSAGE##");

                        return QString();
                    }
                    myrepo.open();

                    if ( myrepo.deleteRecords("graphs",whereclause) > 0 ) {
                        _currentjson = tr("Se ha(n) eliminado el(los) registro(s) seleccionado(s) "
                                          "de la base de datos de Estad�sticas##SAFETMESSAGE##");
                    }





                }
                else if (parser.operationName().startsWith("Borrar_gr�fico_de_flujo") ||
                         parser.operationName().startsWith("Borrar_planificaci�n") ) {

                    QString mygraphfilename = Safet::GRAPHSFILENAME;
                    QString namegraph = MainWindow::replaceMarks(data.map["Nombre_grafo"]);
                    SYD << tr("......**MainWindow::toInputConsole....namegraph: |%1|")
                           .arg(namegraph);
                    if ( configurator == NULL ) {
                        SYE << tr("No se ha cargardo el Objeto para generar flujos de trabajo");
                        return QString();
                    }
                    if (parser.operationName().startsWith("Borrar_planificaci�n") ) {
                        mygraphfilename = Safet::PLANNEDGRAPHSFILENAME;
                    }
                    if ( deleteGraph(namegraph,mygraphfilename) ) {

                        SYA << tr("Borrado correctamente Grafo \"%1\"")
                               .arg(namegraph);
                        _currentjson = tr("Borrado correctamente Grafo \"%1\"##SAFETMESSAGE##")
                                .arg(namegraph);
                    }


                }
            }
            return currentJSON();


        }


        QString MainWindow::doModifyDateGraph(const QString& nametask,const QDateTime& mydate,
                                              const QString& code) {
            QString result;



            QStringList mytasks = code.split("\n", QString::SkipEmptyParts);


            bool isfound = false;
            SYD << tr("\n");
            foreach(QString mytask, mytasks) {

                SYD << tr(".........MainWindow::doModifyDateGraph.....mytask:|%1|")
                       .arg(mytask.split(",").at(0));
                SYD << tr(".........MainWindow::doModifyDateGraph...nametask:|%1|")
                       .arg(QString("Nodo:%1").arg(nametask));
                if ( !isfound && (mytask.split(",").at(0) == QString("Nodo:%1").arg(nametask))) {
                    QString patterndate = QString(",0...1...%2...%1")
                            .arg(mydate.toString("dd/MM/yyyy hh:mmap"))
                            .arg(MainWindow::currentaccount);
                    mytask = mytask.section(",",0,-2)+patterndate;
                    SYD << tr(".....MainWindow::doModifyDateGraph....mytask (modified):\n|%1|\n")
                           .arg(mytask);
                    isfound = true;
                }
                result += mytask;
                result += "\n";
            }
            SYD << tr("\n");

            return result;
        }


        QMap<QString,QDateTime> MainWindow::getDatesGraph(const QString& code) {

            QMap<QString,QDateTime> result;

            QStringList mytasks = code.split("\n", QString::SkipEmptyParts);


            foreach(QString mytask, mytasks) {
                QString currtask  = mytask.split(",").at(0).mid(tr("Nodo:").length());

                QString myinfo = mytask.section(",",-1);
                QStringList myinfos = myinfo.split("...",QString::SkipEmptyParts);
                if (myinfos.count() == 4 ) {
                    result[currtask ] = QDateTime::fromString(myinfos.at(3),
                                                              "dd/MM/yyyy hh:mmap");
                    SYD << tr("MainWindow::getDatesGraph....currtask:|%1|->|%2|")
                           .arg(currtask)
                           .arg(myinfos.at(3));
                }

            }


            return result;
        }


        bool MainWindow::doGeneratePlannedGraph(const QString &pathflow, const QString& name) {

            QString texto = QString("-f %1 -p graphviz -g -k %2 ")
                    .arg(pathflow)
                    .arg(Safet::NOKEY);
            SYD << tr(".....MainWindow::doGeneratePlannedGraph....Texto: |%s|").arg(texto);

            parseArgs( texto );

            if (! executeParsed() ) {
                SYE << tr("No fue posible ejecutar la accion \"%1\"")
                       .arg(texto);
                return false;
            }

            SYD << tr(".......MainWindow::doGeneratePlannedGraph....SafetYAWL::lastgraph..:\n|%1|\n\n")
                   .arg(SafetYAWL::lastgraph);


            QStringList nodes = SafetYAWL::lastgraph.split("\n", QString::SkipEmptyParts);

            QString newgraph;
            QDateTime now = QDateTime::currentDateTime();
            foreach(QString nd, nodes) {

                QDateTime nowplus = now.addMonths(1);
                QString patterndate = QString(",0...1...%2...%1")
                        .arg(nowplus.toString("dd/MM/yyyy hh:mmap"))
                        .arg(MainWindow::currentaccount);
                QString n = nd.section(",",0,-2)+patterndate;

                newgraph += n +"\n";
            }

            SYD << tr("...MainWindow::doGeneratePlannedGraph....newgraph..:\n|%1|")
                   .arg(newgraph);


            QString pathgraphfile = SafetYAWL::pathconf+ "/graphs" + "/"+
                    Safet::PLANNEDGRAPHSFILENAME +".gph";

            SYD << tr("...MainWindow::doSaveGraph...doGeneratePlannedGraph: |%1|").arg(pathgraphfile);


            QFile myfile(pathgraphfile);
            if( !myfile.open(QIODevice::WriteOnly | QIODevice::Append)) {
                SYE << tr("No es posible escribir en la ruta: \"%1\"")
                       .arg(pathgraphfile);
                return false;
            }
            QString msg = SafetYAWL::getConf()["Graphs/infotext.format"];
            QString date = now.toString("dd MMM yyyy");
            QString time = now.toString("h:mm:ssap");
            QString datetime = now.toString("dd MMM yyyy hh:mm:ssap");
            msg.replace("%time",time);
            msg.replace("%datetime",datetime);
            msg.replace("%date",date);

            QDataStream out(&myfile);   // we will serialize the data into the file
            out << name;
            out <<  newgraph;
            out <<  msg+SafetYAWL::LISTSEPARATORCHARACTER+pathflow;
            myfile.close();

            SYA << tr("Se guard� un gr�fico de flujo de trabajo con el nombre: "
                      "\"%1\"")
                   .arg(name);




            return true;

        }

        bool MainWindow::doSaveCompleteMapGraphs(const QString& gfn) {
            QString pathgraphfile = SafetYAWL::pathconf+ "/graphs" + "/"+
                    gfn +".gph";
            SYD << tr("...MainWindow::deleteGraph.....pathgraphfile: |%1|")
                   .arg(pathgraphfile);
            QFile myfile(pathgraphfile);

            if( !myfile.open(QIODevice::WriteOnly)) {
                SYE << tr("No es posible escribir en la ruta: \"%1\"")
                       .arg(pathgraphfile);
                return false;
            }
            QDataStream out(&myfile);   // we will serialize the data into the file

            for(int i=0; i < graphs().keys().count();i++) {
                QString title = graphs().keys().at(i);
                QPair<QString,QString> mypair = graphs()[title];
                out << title;
                out << mypair;
            }
            myfile.close();

            return true;

        }

        bool MainWindow::deleteGraph(const QString& n, const QString& gfn) {
            SYD << tr("...MainWindow::deleteGraph....n:|%1|")
                   .arg(n);
            QString pathgraphfile = SafetYAWL::pathconf+ "/graphs" + "/"+
                    gfn +".gph";
            SYD << tr("...MainWindow::deleteGraph.....pathgraphfile: |%1|")
                   .arg(pathgraphfile);
            QFile myfile(pathgraphfile);
            if( !myfile.open(QIODevice::ReadOnly)) {
                SYE  << tr("No es posible leer gr�ficos de la ruta: \"%1\"")
                        .arg(pathgraphfile);
                return false;
            }

            QDataStream in(&myfile);


            QString newn = n;
            newn.replace(QRegExp("\\s+")," ");
            graphs().clear();
            int i = 0;
            while( !in.atEnd()) {
                QString title;
                QPair<QString,QString> mypair;
                in >> title;
                in >> mypair;
                title.replace(QRegExp("\\s+")," ");

                if (title.compare(newn,Qt::CaseSensitive) == 0 ) {
                    continue;
                }
                if (!title.isNull()) {
                    title.replace(QRegExp("\\s+")," ");
                    graphs() [ title ] = mypair;
                    i++;
                }

            }


            SYD << tr("Se restauraron \"%1\" graficos de \"%2\"")
                   .arg(i)
                   .arg(pathgraphfile);

            myfile.close();


            if( !myfile.open(QIODevice::WriteOnly)) {
                SYE << tr("No es posible escribir en la ruta: \"%1\"")
                       .arg(pathgraphfile);
                return false;
            }
            QDataStream out(&myfile);   // we will serialize the data into the file

            for(i=0; i < graphs().keys().count();i++) {
                QString title = graphs().keys().at(i);
                QPair<QString,QString> mypair = graphs()[title];
                out << title;
                out << mypair;
            }
            myfile.close();

            return true;

        }

        void MainWindow::threadEndJob()
        {


        }

        QStringList MainWindow::lastInfoGraph() {
            QStringList mylist;
            mylist.append(SafetYAWL::lastgraph);
            mylist.append(SafetYAWL::lastinfodate);
            return mylist;
        }

        void MainWindow::processMainWindowThread(){
            // deshabilitar el boton de enviar consulta hasta que termine el thread
//            completingButtonForm->setEnabled(false);
//            completingButtonCons->setEnabled(false);
//            completingButtonSign->setEnabled(false);
//            completingButtonConf->setEnabled(false);
//            completingButtonUsers->setEnabled(false);
//            // deshabilitar el boton de enviar de la barra de herramientas
//            standardbar->actionAt(60,30)->setEnabled(false);

            //    qDebug("despues de myThreadConsole->start();");
        }

        void MainWindow::toInputSign() {
            // icono de progreso en barra de estado


            SafetTextParser parser;

            QString texto;
            //    texto = completingTextEdit->toPlainText().toLatin1();
            qDebug("toInputSign....texto: \n|%s|\n", qPrintable(texto));

            parser.setStr( texto.toLatin1() );
            QString str = "agregar,eliminar,actualizar,mostrar";
            QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
            parser.setCommands(str.split(","),commandstr.split(","));
            qDebug("....xml...(1): \n|%s|\n", qPrintable(parser.str()));
            QString xml = parser.toXml();
            qDebug("....xml...(2): \n|%s|\n", qPrintable(xml));
            SafetYAWL::streamlog << SafetLog::Debug << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
         //   myoutput->setPlainText(xml);


            qDebug("\n\n....MainWindow::toInputFormxml:\n%s", qPrintable(xml));
            parser.processInput( xml.toLatin1() );
            QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
            QString filename = "defmanagementsignfile.xml";
            qDebug("toInputSign.filepath + \"/\" + filename: %s", qPrintable(filepath + "/" + filename));
            parser.openXml(filepath + "/" + filename);


            QStringList results = parser.processXml(false);
            if ( queryForErrors() ) {
                return;
            }


            ParsedSqlToData data;
            foreach(QString s, results) {
                data = SafetTextParser::parseSql(s);
                if ( parser.operationName().startsWith("Firmar",Qt::CaseInsensitive) ) {
                    qDebug("...signDocumentsFromData...");

                    if ( parser.operationName().endsWith("Tarjeta")) {
                        QString nametowrite;
                        QStringList list;
                        SafetDocument mydoc;
                        Q_CHECK_PTR( configurator );
                        bool result = configurator->signDocumentsFromData(data,nametowrite,list,mydoc);
                        if (result ) {
                            setPathOfSafetDocument(nametowrite);
                            successVerification(list,tr("Firma electr�nica (digidoc) realizada "
                                                        "exitosamente sobre \"%1\"....ok!")
                                                .arg(nametowrite), mydoc);
                            return;
                        }


                        QStack<QPair<QDateTime,QString> > mystack = SafetYAWL::streamlog.stopErrorStack();
                        SafetYAWL::streamlog << SafetLog::Debug << trUtf8("Hay \"%1\" errores guardados en la lista. De inmediato se procede"
                                                                          " a inicializar la lista de errores.")
                                                .arg(mystack.count());
                        SafetYAWL::streamlog.stopAllStack();
                        qDebug("ERRORES DE VERIFICACION: %d", mystack.count());
                        if ( mystack.count() > 0 ) {
                            QString message = renderMessageStack(mystack,SafetLog::Error);
                            QString messagew = renderMessageStack(mystack,SafetLog::Warning);
                            SafetYAWL::streamlog << SafetLog::Action << tr("<b>Firma Electronica</b> sobre"
                                                                           "el archivo INVALIDA");


                        }
                        qDebug("toInputSign()");
                    }
                    else if ( parser.operationName().endsWith("P12")) {
                        qDebug("...signDocumentsFromData..P12.");

                        QString nametowrite;
                        QStringList list;
                        SafetDocument mydoc;
                        Q_CHECK_PTR( configurator );

                        bool exec = configurator->signDocumentsFromData(data,nametowrite,list,mydoc,false);

                        if ( !exec ) {
                            return;
                        }
                        setPathOfSafetDocument(nametowrite);
                        successVerification(list,tr("Firma electr�nica (digidoc) realizada "
                                                    "exitosamente sobre \"%1\"....ok!")
                                            .arg(nametowrite), mydoc);
                        QStack<QPair<QDateTime,QString> > mystack = SafetYAWL::streamlog.stopErrorStack();
                        SafetYAWL::streamlog << SafetLog::Debug << trUtf8("Hay \"%1\" errores guardados en la lista. De inmediato se procede"
                                                                          " a inicializar la lista de errores.")
                                                .arg(mystack.count());
                        SafetYAWL::streamlog.stopAllStack();
                        qDebug("ERRORES DE VERIFICACION: %d", mystack.count());
                        if ( mystack.count() > 0 ) {
                            QString message = renderMessageStack(mystack,SafetLog::Error);
                            QString messagew = renderMessageStack(mystack,SafetLog::Warning);
                            SafetYAWL::streamlog << SafetLog::Action << tr("<b>Firma Electronica</b> sobre"
                                                                           "el archivo INVALIDA");


                        }
                    }

                }
                else if (parser.operationName().startsWith("Verificar",Qt::CaseInsensitive)) {
                    SafetYAWL::streamlog << SafetLog::Action << tr("Se va a realizar la verificacion...ok!");

                    SafetDocument doc;
                    Q_CHECK_PTR( configurator );
                    configurator->verifyDocumentsFromData(data,doc);
                    setPathOfSafetDocument(doc.getPathOfSafetDocument());
                    QStringList list;
                    int nsigns = doc.numberOfSignaturesOnOpenXAdESContainer();

                    list << tr("Ver firmas del documento(%1)").arg(nsigns) << tr("Mostrar carpeta del contenedor") << tr("Enviar contenedor por correo-e");


                    successVerification(list,tr("Verificada correctamente la <b>Firma Electr�nica</b>"
                                                " sobre el archivo \"<b>%1</b>\"...ok!")
                                        //.arg(digidocfilename), safetDocument);
                                        .arg(doc.getPathOfSafetDocument()), doc);

                    QStack<QPair<QDateTime,QString> > mystack = SafetYAWL::streamlog.stopErrorStack();
                    SafetYAWL::streamlog << SafetLog::Debug << trUtf8("Hay \"%1\" errores guardados en la lista. De inmediato se procede"
                                                                      " a inicializar la lista de errores.")
                                            .arg(mystack.count());

                    SafetYAWL::streamlog.stopAllStack();

                    qDebug("**ERRORES DE VERIFICACION: %d", mystack.count());
                    if ( mystack.count() > 0 ) {
                        QString message = renderMessageStack(mystack,SafetLog::Error);
                        QString messagew = renderMessageStack(mystack,SafetLog::Warning);
                        SafetYAWL::streamlog << SafetLog::Action << tr("<b>Firma Electronica</b> sobre el archivo \"<b>%1</b>\""
                                                                       " INVALIDA").arg(doc.getPathOfSafetDocument());

                    }
                }
            }
            if ( false) {
                qDebug("Verificaci�n de Firma(s) INV�LIDA...*NOT NOT NOT*");
            }
            else {

            }

        }

QString MainWindow::toInputConfigure(const QString &action) {

            QString result;
            SafetTextParser parser;
            QString texto  = action;

            //QString customconfigure = "operacion:Values_to_store  savedqueries: ";


            qDebug("...MainWindow::toInputConfigure....action:|%s|", qPrintable(texto));
            //texto = texto.replace("operacion:","");
            //texto =  texto.replace(":",Safet::COLONMARK);


            //texto = "'"+customconfigure + texto+"'";

            qDebug("...MainWindow::toInputConfigure....texto (completo):|%s|", qPrintable(texto));

            parser.setStr( texto.toLatin1() );
            QString str = "agregar,eliminar,actualizar,mostrar";
            QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
            parser.setCommands(str.split(","),commandstr.split(","));
            qDebug("....xml...(1): \n|%s|\n", qPrintable(parser.str()));
            QString xml = parser.toXml();
            qDebug("....xml...(2): \n|%s|\n", qPrintable(xml));


            parser.processInput( xml.toLatin1() );
            QString filepath = SafetYAWL::getConfFile().getValue("Input", "input.path").toString();
            QString filename = "defconfigure.xml";

            parser.openXml(filepath + "/" + filename);
            qDebug("...toInputConfigure....filepath: %s", qPrintable(filepath));

            QStringList names = parser.loadNameConfs();

            foreach(QString n, names) {
                //          QMap<QString,QVariant> conf = centraledit()->findConf(n);
                //          parser.addConf(conf, n);
            }

            QStringList results = parser.processXml(false);

            if ( queryForErrors() ) {
                qDebug(".......****..queryforerror....fail");
                return result;
            }


            foreach(QString s, results) {
                qDebug("toInputConfigure.......result: %s", qPrintable(s));
                proccessConfFile(s);
            }

            if ( MainWindow::configurator  /* && user say yes */ ) {
                configurator->closeDataSources();
                delete MainWindow::configurator;
                MainWindow::configurator = new SafetYAWL();
                Q_CHECK_PTR( configurator);
                configurator->openDataSources();
            }

            qDebug("...MainWindow::toInputConfigure...exiting");
            return result;

        }





        void MainWindow::proccessConfFile(const QString& sql, const QString& filename, bool multiplefields) {

            qDebug("...replaceTextInFile...(1)");
            QString fileconf = filename;
            if ( filename.isEmpty() ) {
                fileconf = SafetYAWL::pathconf+ "/" + "safet.conf";

            }
            qDebug("....proccessConfFile...fileconf: %s", qPrintable(fileconf));

            QRegExp rx;
            QString newsql = sql;
            newsql.replace("'","");
            qDebug("...newsql: %s", qPrintable(newsql));
            QString updatePattern = "UPDATE\\s+([�������a-zA-Z0-9_\\.\\(\\)#%][�������a-zA-Z0-9_,'\\.\\(\\)\\-#%\\x3c\\x3e\\x2f]*)"
                    "\\s+SET\\s+"
                    "([�������a-zA-Z0-9_\\.\\(\\)\\*;#%][�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\s\\*;#%]*"
                    "[�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*;#]\\s+)?WHERE\\s+"
                    "([�������a-zA-Z0-9_\\.\\(\\)\\*;#%][�������a-zA-Z0-9_,'\\=\\.\\(\\)\\s\\-\\*;#%\\x3c\\x3e\\x2f]*"
                    "[�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*;#%\\x3c\\x3e\\x2f])";

            //UPDATE lista SET database.user.1='vbravo' WHERE database.db.1='dbtracrootve'
            qDebug("        (###)......updatePattern: |%s|", qPrintable(updatePattern));

            QString insertPattern = "INSERT INTO\\s+([a-zA-Z0-9_][a-zA-Z0-9_\\.\\-]*)\\s+"
                    "\\(([a-zA-Z0-9_\\.\\(\\)][a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*]*)\\)\\s+"
                    "VALUES\\s+\\(([a-zA-Z0-9_'\\./\\(\\)][a-zA-Z0-9_,'\\=\\.\\-/\\*\\s\\x3c\\x3e\\x2f]*)\\)\\s*";


            QString deletePattern = "DELETE FROM\\s+([�������a-zA-Z0-9_\\.\\(\\)][�������a-zA-Z0-9_,'\\.\\(\\)\\-]*)"
                    "\\s+WHERE\\s+"
                    "([�������a-zA-Z0-9_\\.\\(\\)\\*;][�������a-zA-Z0-9_,'\\=\\.\\(\\)\\s\\-\\*;\\x3c\\x3e\\x2f]*"
                    "[�������a-zA-Z0-9_,'\\=\\.\\(\\)\\-\\*;\\x3c\\x3e\\x2f])";


            bool isdeleting = false;
            rx.setPattern(updatePattern);
            int pos = rx.indexIn(newsql);

            qDebug(".........newsql: %s", qPrintable(newsql));
            if ( pos == -1 ) {

                // qDebug("        (###)......insertPattern: |%s|", qPrintable(insertPattern));
                rx.setPattern(insertPattern);
                pos = rx.indexIn(newsql);
                qDebug("  ...pos: %d", pos);
                if (pos == -1 ) {
                    //qDebug("        (###)......deletePattern: |%s|", qPrintable(deletePattern));
                    rx.setPattern(deletePattern);
                    pos = rx.indexIn(newsql);
                    //   qDebug("  ...deletePattern...pos: %d", pos);
                    if (pos == -1 ) {

                        SYE << tr("Ocurrio un error con la sentencia SQL \"%1\" formada por la entrada"
                                  " Realice una lectura del registro para conocer donde se encuentra"
                                  "el error").arg(newsql);
                        return;
                    }
                    else {
                        isdeleting = true;
                        //     qDebug("...deletepattern: newsql: %s",qPrintable(newsql));
                        //doInsertInAuthConfFile(rx,MainWindow::DELETEPATTERN);

                    }
                }
                else {

                    doInsertInAuthConfFile(rx);


                    return;
                }
            }

            QString keyfield, fields;
            if ( !isdeleting ){
                keyfield = rx.cap(3);
                fields = rx.cap(2);
            }
            else {
                keyfield = rx.cap(2);
            }


            qDebug("...pattern: |%s|",qPrintable(rx.pattern()));
            // Para el keyfield
            if ( keyfield.split("=",QString::SkipEmptyParts).count() != 2 ) {
                SafetYAWL::streamlog << SafetLog::Error << tr("Ocurrio un error con la sentencia SQL \"%1\" formada por la entrada."
                                                              "No se encuentra la asignaci�n de campos con el operador '='."
                                                              " Realice una lectura del registro para conocer donde se encuentra"
                                                              "el error").arg(newsql);
                return;
            }
            QString firstkeyfield = keyfield.split("=",QString::SkipEmptyParts).at(0);
            //qDebug("         proccessConfFile...firstkeyfield...(1):%s", qPrintable(firstkeyfield));
            int numberreg = 0;
            QString prefixkey;
            QString secondkeyfield = keyfield.split("=",QString::SkipEmptyParts).at(1);
            qDebug("         proccessConfFile...secondkeyfield...(1):%s", qPrintable(secondkeyfield));


            if (multiplefields) {
                if (firstkeyfield.endsWith(".*")  ) {

                    prefixkey = firstkeyfield.mid(0,firstkeyfield.length()-2);
                    //qDebug("**prefixkey: %s", qPrintable(prefixkey));
                    numberreg = SafetYAWL::getAuthConf().getNumberRegister(secondkeyfield,prefixkey,true);

                    //qDebug("**numberreg: %d", numberreg);
                    if (numberreg > 0 ) {
                        firstkeyfield = QString("%1.%2").arg(prefixkey)
                                .arg(numberreg);
                        //  qDebug("   (*)proccessConfFile...firstkeyfield...(2):%s", qPrintable(firstkeyfield));
                    }


                }
            }

            //qDebug("   processConffile...(1)...firstkeyfield: %s", qPrintable(firstkeyfield));
            QString searchtext = tr("\\s*(%1)\\s*\\=\\s*([�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\(\\)#%\\x3c\\x3e\\x2f]"
                                    "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\s\\(\\);#%\\x3c\\x3e\\x2f]*)"
                                    "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\(\\);#%\\x3c\\x3e\\x2f]")
                    .arg(firstkeyfield);
            QString replacetext;
            QString currentfirstkeyfield;
            if ( !isdeleting ) {
                replacetext = tr ("%1 = %2").arg(firstkeyfield).arg(secondkeyfield);

            }
            else {
                replacetext = "";
            }
            currentfirstkeyfield = firstkeyfield;


            SafetYAWL::replaceTextInFile(fileconf,
                                         searchtext,
                                         replacetext);


            // Para los otros campos que no son claves

            if (isdeleting) {
                fields = searchFieldsInAuthConf(firstkeyfield);
            }

            //qDebug("   processConffile...(2)...fields.count():%d",fields.count());
            QStringList listfields;
            listfields = fields.split(",",QString::SkipEmptyParts);


            foreach( QString s, listfields ) {
                if ( s.split("=",QString::SkipEmptyParts).count() != 2 && !isdeleting) {
                    continue;
                }
                if (!isdeleting) {
                    firstkeyfield = s.split("=",QString::SkipEmptyParts).at(0);
                }
                else {
                    firstkeyfield = s;
                }
                //qDebug("...firstkeyfield: %s", qPrintable(firstkeyfield));
                if (multiplefields && !isdeleting) {
                    //qDebug("numberreg: %d", numberreg);
                    if (firstkeyfield.endsWith(".*")  && numberreg > 0 ) {
                        firstkeyfield = QString("%1.%2").arg(firstkeyfield.mid(0,firstkeyfield.length()-2))
                                .arg(numberreg);
                        //qDebug("...proccessConfFile...firstkeyfield...(2):%s", qPrintable(firstkeyfield));


                    }
                }
                if (!isdeleting) {
                    secondkeyfield = s.split("=",QString::SkipEmptyParts).at(1).trimmed();
                }
                else {
                    secondkeyfield = "";
                }
                searchtext = tr("\\s*(%1)\\s*\\=\\s*([�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\x3c\\x3e\\x2f]"
                                "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\s,;\\x3c\\x3e\\x2f]*"
                                "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\x3c\\x3e\\x2f])").arg(firstkeyfield);
                if (!isdeleting) {
                    replacetext  = tr("%1 = %2").arg(firstkeyfield).arg(secondkeyfield);

                }
                else {
                    //qDebug("searchtext: %s", qPrintable(searchtext));
                    replacetext ="";
                }

                // qDebug("           ...processConfUsers...replacetext:|%s|",qPrintable(replacetext));
                SafetYAWL::replaceTextInFile(fileconf,
                                             searchtext,
                                             replacetext);

            }

            // Reorganizar los otros campos mayotres a numberreg
            if (isdeleting) {

                numberreg++;
                int result;
                listfields.push_front(currentfirstkeyfield);
                while(true){


                    foreach( QString s, listfields ) {
                        QString currprefkey = s.split(".").at(0)+"."+
                                s.split(".").at(1);
                        QString nextkey = QString("%1.%2").arg(currprefkey)
                                .arg(numberreg);

                        //qDebug("...nextkey: %s",qPrintable(nextkey));
                        searchtext = tr("\\s*(%1)\\s*\\=\\s*([�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\(\\)\\x3c\\x3e\\x2f]"
                                        "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$\\s,;\\(\\)\\x3c\\x3e\\x2f]*"
                                        "[�������a-zA-Z0-9\\$\\#/\\-\\._/:!\\?\\^\\$,;\\(\\)\\x3c\\x3e\\x2f])").arg(nextkey);

                        QString replacekey = QString("%1.%2").arg(currprefkey)
                                .arg(numberreg-1);
                        //qDebug("...replacekey: %s",qPrintable(replacekey));

                        replacetext  = tr("%1 = ||cap||").arg(replacekey);
                        result = SafetYAWL::replaceTextInFile(fileconf,
                                                              searchtext,
                                                              replacetext,
                                                              Qt::CaseSensitive,
                                                              2);


                    }
                    if ( !result ) break;
                    numberreg++;


                }
            }

        }


        QString MainWindow::searchFieldsInAuthConf(const QString& key) {
            qDebug("searchFieldsInAuthConf...Clave...key: %s", qPrintable(key));
            QString retfields;
            QString exp;

            if ( key.split(".").count() < 3 ) {
                return QString("");
            }
            exp = "[a-zA-Z0-9]+/("+key.split(".").at(0)+"\\.[a-zA-Z0-9]+\\."+key.split(".").at(2)+")";

            QRegExp rx(exp);
            //qDebug("searchFieldsInAuthConf...Clave...RegExp: %s", qPrintable(exp));
            for(int i =0; i < SafetYAWL::getAuthConf().keys().count() ; i++){
                QString currentkey = SafetYAWL::getAuthConf().keys().at(i);

                int pos = rx.indexIn(currentkey);
                if ( pos >= 0 ) {
                    if ( rx.cap(1) != key ) {
                        retfields += rx.cap(1) +",";
                        //qDebug("searchFieldsInAuthConf...Clave...CurrentKey: %s", qPrintable(rx.cap(1)));
                    }
                }
            }
            if ( !retfields.isEmpty()){
                retfields.chop(1);
            }

            qDebug("...retfields: %s", qPrintable(retfields));
            return retfields;

        }

        void MainWindow::doInsertInAuthConfFile(QRegExp& rx) {


            //qDebug("...doInsertInAuthConfFile...capturas...1: %s", qPrintable(rx.cap(1)));
            //qDebug("...doInsertInAuthConfFile...capturas...2: %s", qPrintable(rx.cap(2)));
            //qDebug("...doInsertInAuthConfFile...capturas...3: %s", qPrintable(rx.cap(3)));

            QStringList fields, values;

            fields = rx.cap(2).split(",");
            values = rx.cap(3).split(",");

            QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf";
            int countuser = 1;
            QString replacetext,newfield;

            bool isreplacing = true;
            QString sectiontext;
            while (isreplacing ) {

                QString  newtext;

                if (sectiontext.isEmpty() && fields.count() > 0 ) {
                    QString firstfield = fields.at(0);
                    sectiontext = firstfield.mid(0,1).toUpper()+firstfield.mid(1);
                    sectiontext = sectiontext.split(".").at(0);
                    //qDebug("    ..........sectiontext:%s", qPrintable(sectiontext));
                }
                for(int i = 0; i < fields.count(); i++) {
                    newfield = fields.at(i);
                    newfield.chop(2);
                    //qDebug("...fields.at(%d): %s", i, qPrintable(newfield));
                    QString subfield = newfield.mid(0,1).toUpper()+newfield.mid(1);
                    newfield = QString("%1.%2").arg(subfield).arg(countuser);
                    QString firstfield = newfield.split(".").at(0);
                    QString key = firstfield +"/"+newfield.mid(firstfield.length()+1);
                    // qDebug("...key: %s",qPrintable(key));

                    newfield = newfield.toLower();
                    if (!SafetYAWL::getAuthConf().contains(key) ) {
                        newtext = QString("%1").
                                arg(newfield)
                                +" = " + values.at(i) + "\n";
                        replacetext += newtext;
                        // qDebug("! replacing:%s",qPrintable(newtext));
                        isreplacing = false;
                    }
                    else {
                        replacetext += QString("%1").arg(newfield)
                                +" = " + SafetYAWL::getAuthConf()[ key ] + "\n";
                    }
                }

                replacetext += "\n";
                countuser++;


            }



            qDebug();
            qDebug("...**replaceSectionInFile...:\n%s",
                   qPrintable(replacetext));
            qDebug();

            SafetYAWL::replaceSectionInFile(fileconf,sectiontext,replacetext);

        }

        void MainWindow::restoreWindowState()
        {
        }



        void MainWindow::saveWindowState()
        {
        }


        void MainWindow::closeEvent(QCloseEvent *event)
        {
        }


        void MainWindow::toChangeUser() {
            qDebug("...MainWindow::toChangeUser()...");
            checkGoPrincipal();

        }

        void MainWindow::resizeEvent(QResizeEvent *event) {
        }


        void MainWindow::checkGoPrincipal() {

        }

        void MainWindow::moveEvent(QMoveEvent *event)
        {
        }



        void MainWindow::showEvent(QShowEvent *event)
        {
        }


        void MainWindow::doExit() {
        }

        bool MainWindow::maybeSave() {

            return false;
        }


        void MainWindow::setupStackedWebviews(const QIcon& icon, const QString& name, const QString& desc) {



        }

        bool MainWindow::searchInHistoryList(const QString& str) {

        }

        void MainWindow::saveToHistoryList() {
        }

        void MainWindow::addToHistoryList() {


        }

        QMap<QString,QString> MainWindow::loadEditActions() {
            QMap<QString,QString> result;

            QStringList actions  = SafetYAWL::getConf()["GeneralOptions/consoleactions.*"].split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);

            foreach(QString s, actions) {
                QStringList tlist = s.split(";",QString::SkipEmptyParts);
                if ( tlist.count() < 2 ) continue;
                QString mykey = tlist.at(0);
                QString myvalue = tlist.at(1);
                result[mykey] = myvalue;
            }
            return result;
        }


        void MainWindow::editToHistoryList() {

        }

        void MainWindow::delToHistoryList() {
        }

        void MainWindow::setupTabWidget() {

            // Para la consola de salida


        }

        QString MainWindow::drawWorkflow(const QString& filename) {

            QString newfile = filename.section("/",-1)+"."+SafetYAWL::getConf()["Plugins.Graphviz/graphtype"];
            QString myfile = QDir::tempPath()+"/"+newfile;
            qDebug("...drawWorkflow:|%s|", qPrintable(myfile));
            QFile::copy(filename,myfile);
            return myfile;

        }


        void MainWindow::setMediaPath(const QString& m) {
            _mediapath = m;
            if (_mediapath.endsWith("/")) {
                _mediapath.chop(1);
            }

        }
        QString MainWindow::mediaPath() {
            return _mediapath;
        }


        void MainWindow::setKeyPath(const QString& m) {
            _keypath = m;
            if (_keypath.endsWith("/")) {
                _keypath.chop(1);
            }

        }

        QString MainWindow::keyPath() {
            return _keypath;
        }


        void MainWindow::buildModel(QMap<QString,QVariant>& mymap) {



        }


        void MainWindow::refreshListView(const QString& doc) {

        }

        void MainWindow::doQuit() {

            if (maybeSave() ) {
                saveWindowState();
                qApp->quit();
            }
        }







bool MainWindow::loadWidgetPlugins(const QString& f, bool add) {
            if (!configurator->loadWidgetPlugins(f)) {
                SafetYAWL::streamlog
                        << SafetLog::Warning
                        << tr("No se cargo correctamente el plugins: \"%1\"")
                           .arg(f);
                return false;
            }
            QMap<QString,QVariant> conf;
            qDebug("....MainWindow::loadWidgetPlugins...f: %s", qPrintable(f));
            if ( add ) {
                Q_CHECK_PTR( SafetYAWL::listDynWidget );

                Q_ASSERT( SafetYAWL::listDynWidget->count() > 0 );
                WidgetInterface* mywid = SafetYAWL::listDynWidget->at(SafetYAWL::listDynWidget->count()-1);
                Q_CHECK_PTR( mywid );
                conf = mywid->conf();
                qDebug("....loadWidgetPlugins...conf[namewidget]: %s f: %s",
                       qPrintable(conf["namewidget"].toString()), qPrintable(f));
                //Q_ASSERT( conf["namewidget"] == f );
            }
            return true;
        }



        void MainWindow::createMenu() {

        }

bool MainWindow::doSaveGraph(const QStringList& mypars) {

            QString pathgraphfile = SafetYAWL::pathconf+ "/graphs" + "/"+
                    Safet::GRAPHSFILENAME +".gph";
            SYD << tr("...MainWindow::doSaveGraph.....pathgraphfile: |%1|")
                   .arg(pathgraphfile);
            QFile myfile(pathgraphfile);
            if( !myfile.open(QIODevice::ReadOnly)) {
                SYE  << tr("No es posible leer gr�ficos de la ruta: \"%1\"")
                        .arg(pathgraphfile);
                return false;
            }

            QDataStream in(&myfile);

            graphs().clear();
            int i = 0;
            bool graphexists = false;

            QString newtitle = mypars.at(0);
            newtitle.replace(QRegExp("\\s+")," ");

            while( !in.atEnd()) {
                QString title;
                QPair<QString,QString> mypair;
                in >> title;
                in >> mypair;
                title.replace(QRegExp("\\s+")," ");


                if (title.compare(newtitle,Qt::CaseSensitive) == 0 ) {
                    mypair.first = mypars.at(1);
                    mypair.second = mypars.at(2);
                    graphexists = true;
                }

                if (!title.isNull()) {
                    title.replace(QRegExp("\\s+")," ");
                    graphs() [ title ] = mypair;
                    i++;
                }

            }

            if (!graphexists ) {
                QPair<QString,QString> mypair;
                mypair.first = mypars.at(1);
                mypair.second = mypars.at(2);
                graphs()[ newtitle ] = mypair;
            }


            myfile.close();

            if( !myfile.open(QIODevice::WriteOnly)) {
                SYE << tr("No es posible escribir en la ruta: \"%1\"")
                       .arg(pathgraphfile);
                return false;
            }
            QDataStream out(&myfile);   // we will serialize the data into the file

            for(i=0; i < graphs().keys().count();i++) {
                QString title = graphs().keys().at(i);
                QPair<QString,QString> mypair = graphs()[title];
                out << title;
                out << mypair;
            }
            myfile.close();

            return true;


            //    QString pathgraphfile = SafetYAWL::pathconf+ "/graphs" + "/"+
            //            Safet::GRAPHSFILENAME +".gph";

            //    SYD << tr("...MainWindow::doSaveGraph...pathgraphfile: |%1|").arg(pathgraphfile);

            //    QFile myfile(pathgraphfile);
            //    if( !myfile.open(QIODevice::WriteOnly | QIODevice::Append)) {
            //        SYE << tr("No es posible escribir en la ruta: \"%1\"")
            //               .arg(pathgraphfile);
            //        return false;
            //    }
            //    if ( mypars.count() < 3 ) {
            //        SYE << tr("El n�mero de parametros para guardar un grafo es tres(3): "
            //                  "nombre,codigo y etiqueta de fecha...El actual es \"%1\"")
            //               .arg(mypars.count());
            //        return false;

            //    }
            //    SYD << tr("SAFET SAVEGRAPH....Grafo Guardado:-----\n%1\n-------")
            //           .arg(mypars.at(1));
            //    QDataStream out(&myfile);   // we will serialize the data into the file
            //        out << mypars.at(0);
            //        out <<  mypars.at(1);
            //        out <<  mypars.at(2);
            //    myfile.close();

            //    SYA << tr("Se guard� un gr�fico de flujo de trabajo con el nombre: "
            //              "\"%1\"")
            //           .arg(mypars.at(0));

            //    return true;

        }


void MainWindow::doRestoreGraph(const QString& gfn) {
            QString pathgraphfile = SafetYAWL::pathconf+ "/graphs" + "/"+
                    gfn +".gph";
            QFile myfile(pathgraphfile);
            if( !myfile.open(QIODevice::ReadOnly)) {
                SYW  << tr("No es posible leer gr�ficos de la ruta de archivos \"%1\"")
                        .arg(pathgraphfile);
                return;
            }

            QDataStream in(&myfile);
            QString title;

            graphs().clear();
            int i = 0;
            while( !in.atEnd()) {
                QPair<QString,QString> mypair;
                in >> title;
                in >> mypair;
                SYD << tr("...MainWindow::doRestoreGraph....title:|%1|").arg(title);
                title.replace(QRegExp("\\s+")," ");
                if (!title.isNull()) {
                    graphs() [ title ] = mypair;
                    i++;
                }
            }
            SYD << tr("Se restauraron \"%1\" graficos de \"%2\"")
                   .arg(i)
                   .arg(pathgraphfile);


        }

void MainWindow::doRenderGraph(const QString& code, const QString& datetag) {
            Q_CHECK_PTR( configurator );
            if ( configurator == NULL ) {

                SYD << tr("MainWindow::doRenderGraph...configurator is null");
                return;
            }

            if ( SafetYAWL::curOutputInterface == NULL ) {

                SYD << tr("...curOutputInterface is null");
                configurator->loadPlugins("graphviz");
            }

            //QMap<QString,QString> mymap = SafetYAWL::getConfFile().readFile(SafetYAWL::pathconf + "/" + SafetYAWL::fileconf);
            QMap<QString,QString> mymap =  SafetYAWL::getConf().getMap();

            SYD << tr("...MainWindow::doRenderGraph...Plugins.Graphviz/extrainfo.show:|%1|")
                   .arg(SafetYAWL::getConf()["Plugins.Graphviz/extrainfo.show"]);

            SYD << tr("trying curOutputInterface...(1)");
            Q_ASSERT ( SafetYAWL::curOutputInterface == NULL );
            QString parsedCodeGraph = SafetYAWL::curOutputInterface->parseCodeGraph(code, mymap);
            SYD << tr("trying configurator...(2)");

#ifdef SAFETLOG_STDOUT
     SafetYAWL::fileout.close();
     SafetYAWL::fileout.open(stdout, QIODevice::WriteOnly);
     SafetYAWL::streamlog.setDevice(&SafetYAWL::fileout);
#else
    SafetYAWL::filelog.close();
    SafetYAWL::filelog.open(QIODevice::Append);

    SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);
#endif


            char type[4];

            strncpy(type,qPrintable(SafetYAWL::getConf()["Graphviz.Plugins/graphtype"]),3);
            //********* QUITAR *************
            strncpy(type,"svg",3);
            //********* QUITAR *************
            type[3] = 0;


            SYD << tr("...*MainWindow::doRenderGraph.....graphtype:|%1|")
                   .arg(type);
            QString img = SafetYAWL::curOutputInterface->renderGraph(parsedCodeGraph, type, mymap);
            if (img.startsWith("ERROR")) {
                SafetYAWL::streamlog
                        << SafetLog::Error
                        << tr("Ocurri� el siguiente error al generar el gr�fico de flujo \"%1\"")
                           .arg(img);
                queryForErrors();
                return ;
            }
            SafetYAWL::filelog.close();
            SafetYAWL::filelog.open(QIODevice::Append);
            SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);

            SafetYAWL::lastgraph = code;
            SafetYAWL::lastinfodate = datetag;
            _currentjson = drawWorkflow(img); // Dibujar el flujo
            SafetYAWL::streamlog.stopAllStack();


        }

QString MainWindow::doCompareGraphs(const QString& firstgraph, const QString& secondgraph) {

            QString newgraph = generateGraphCompared(firstgraph, secondgraph);

            return newgraph;

}


QString MainWindow::addInfoGraphDateText() {
            QString result;
            QString inc = SafetYAWL::getConf()["Graphs/infotext.include"].trimmed();

            if ( inc.compare("on") != 0 ) {
                SYD << tr("MainWindow::addInfoGraphDateText....inc:|%1|")
                       .arg(inc);
                return result;
            }

            result = SafetYAWL::getConf()["Graphs/infotext.format"];
            QDateTime now = QDateTime::currentDateTime();
            QString date = now.toString("dd MMM yyyy");
            QString time = now.toString("h:mm:ssap");
            QString datetime = now.toString("dd MMM yyyy hh:mm:ssap");
            result.replace("%time",time);
            result.replace("%datetime",datetime);
            result.replace("%date",date);

            SafetYAWL::lastinfodate = result;
            SYD << tr("............*MainWindow::addInfoGraphDateText....SafetYAWL::lastinfodate:|%1|")
                   .arg(result);

            return result;
        }



QString MainWindow::currentGraphTitle() {
            QString curtitle = SafetYAWL::getConf()["GeneralOptions/currentflowtitle"].trimmed();
            return curtitle;
}

QString MainWindow::generateGraphCompared(const QString& first, const QString& second) {
            QString result;
            QStringList myfirstnodelist, mysecondnodelist;
            myfirstnodelist = first.split("\n",QString::SkipEmptyParts);
            mysecondnodelist = second.split("\n",QString::SkipEmptyParts);
            bool ok;
            int prodays = SafetYAWL::getConf()["DefaultValues/graphs.daysproject"].toInt(&ok);
            if (prodays <= 0 ) {
                SYE << tr("El par�metro \"%1\" no tiene un valor v�lido")
                       .arg("DefaultValues/graphs.daysproject");
                return QString();
            }
            //    if ( myfirstnodelist.count() != mysecondnodelist.count()) {
            //                SYE << tr("Los gr�ficos seleccionados son diferentes."
            //                      " No tienen la misma cantidad de nodos."
            //                      " El primer gr�fico tiene \"%1\" nodos. "
            //                      " El segundo gr�fico tiene \"%2\" nodos." )
            //                   .arg(myfirstnodelist.count()-1)
            //                   .arg(mysecondnodelist.count()-1);
            //        return QString();
            //    }



            QString currentnode;
            for(int i=0; i < myfirstnodelist.count(); i++ ) {
                QString myfirst = myfirstnodelist.at(i);
                QStringList myfirstlist = myfirst.split(",",QString::SkipEmptyParts);
                int rlnodes = myfirstlist.count();
                Q_ASSERT(rlnodes > 0 );
                QStringList statsreal = myfirstlist.at(rlnodes-1).split("...",QString::SkipEmptyParts);
                if (statsreal.count() < 2) {
                    continue;
                }

                int ntokens = statsreal.at(0).toInt(&ok);
                if (ntokens > 0 ) {
                    currentnode = myfirstlist.at(0);
                    break;
                }

            }
            SYD << tr(".....MainWindow::generateGraphCompared....currentnode:|%1|")
                   .arg(currentnode);


            if ( currentnode.isEmpty()) {
                SYE << tr("NO se encuentra el nodo donde se encuentra la clave para el gr�fico a comparar");
                return QString();
            }

            bool passnode = false;

            for(int i=0; i < myfirstnodelist.count(); i++ ) {
                int days = 0;
                SYD << tr ("....MainWindow::generateGraphCompared...:|%1|")
                       .arg(i);

                QPair<double,int>  mypair;
                mypair.first = 0;
                mypair.second = 0;
                QString myfirst = myfirstnodelist.at(i);
                QString mysecond;
                QStringList myfirstlist = myfirst.split(",",QString::SkipEmptyParts);

                bool issecondfound = false;
                for(int j=0; j < mysecondnodelist.count(); j++ ) {
                    if ( myfirstlist.at(0) == mysecondnodelist.at(j).section(",",0,0) ) {
                        mysecond =  mysecondnodelist.at(j);
                        issecondfound = true;
                        break;
                    }
                }
                if ( !issecondfound ) {
                    SYE << tr(" Generando comparaci�n de grafos: no se encontr� el \"%1\"")
                           .arg(myfirstlist.at(0));
                    return QString();
                }
                QStringList mysecondlist = mysecond.split(",",QString::SkipEmptyParts);

                if (myfirstlist.count() == 0 ) {
                    SYD << tr(" Error 1");
                    break;
                }
                if (mysecondlist.count() == 0 ) {
                    SYD << tr(" Error 2");
                    break;
                }
                if (myfirstlist.count() != mysecondlist.count()) {
                    SYD << tr(" Error 3");
                    break;
                }

                QString realnode = myfirstlist.at(0).trimmed();
                QString plannode = mysecondlist.at(0).trimmed();
                SYD << tr("....MainWindow::generateGraphCompared....realnode:|%1|...plannode:|%2|")
                       .arg(realnode)
                       .arg(plannode);
                if ( realnode != plannode ) {
                    SafetYAWL::streamlog
                            << SafetLog::Error
                            << tr("Los gr�ficos seleccionados son diferentes."
                                  " No tienen los mismos nodos en la misma posici�n."
                                  " El primer nodo tiene nombre \"%1\". "
                                  " El segundo nodo tiene nombre \"%2\"." )
                               .arg(myfirstlist.at(0))
                               .arg(mysecondlist.at(0));
                    return QString();

                }

                int rlnodes = myfirstlist.count();
                int plnodes = mysecondlist.count();
                QStringList statsreal = myfirstlist.at(rlnodes-1).split("...",QString::SkipEmptyParts);
                QStringList statsplanned = mysecondlist.at(plnodes-1).split("...",QString::SkipEmptyParts);


                int nstatsreal = 0;
                if ( statsreal.count() > 2 ) {
                    nstatsreal = 1;
                }
                double porc = 0;

                QString datestrp;
                QDateTime dateplanned;


                QString statsinfo;

                bool isdelay = false;


                if (statsreal.count() < 4 ) {

                    if ( statsplanned.count() < 4 ) {
                        statsinfo =  tr("0...1...n/a...n/a\n");
                    }
                    else {
                        QDateTime mynow = QDateTime::currentDateTime();
                        datestrp  = statsplanned.at(3);
                        dateplanned = QDateTime::fromString(datestrp,"dd/MM/yyyy hh:mmap");

                        if (mynow < dateplanned) {

                            statsinfo = tr("0...1...n/a...Debe entregar en <br/>%1<br/>(%2)\n")
                                    .arg(SafetWorkflow::humanizeDate(days,mynow
                                                                     .toString("dd/MM/yyyy hh:mmap"),
                                                                     "dd/MM/yyyy hh:mmap",
                                                                     dateplanned),
                                         datestrp);

                        }
                        else {
                            statsinfo = tr("0...1...n/a...Debio entregar hace <br/>%1<br/>(%2)\n")
                                    .arg(SafetWorkflow::humanizeDate(days,dateplanned
                                                                     .toString("dd/MM/yyyy hh:mmap"),
                                                                     "dd/MM/yyyy hh:mmap",
                                                                     mynow),
                                         datestrp);
                            isdelay = true;

                        }
                    }
                    QRegExp rx;
                    rx.setPattern("info\\.task\\.color:\\s*([\\-a-zA-Z0-9_\\.]+)\\s*,?");

                    for(int j=0; j < myfirstlist.count()-nstatsreal; j++) {
                        QString nextfield =  myfirstlist.at(j);
                        if ( rx.indexIn( nextfield ) != -1 ) {
                            if ( days >= prodays ) {
                                porc = 1;
                            }
                            else {
                                porc = double(days)/double(prodays);
                            }

                            porc = 1-porc;

                            if ( isdelay) {
                                porc = porc*-1;
                            }


                            nextfield = QString("info.task.color:%1")
                                    .arg(porc,0,'g',2);

                        }

                        result += nextfield;
                        result += ",";

                    }
                    result += statsinfo;
                    continue;
                }


                if  (statsplanned.count() < 4 ) {
                    SYD << tr(" Error 20");
                    break;
                }


                if  (statsreal.count() < 4 ) {
                    SYD << tr(" Error 5");
                    break;
                }

                datestrp  = statsplanned.at(3);
                dateplanned = QDateTime::fromString(datestrp,"dd/MM/yyyy hh:mmap");



                QString datestr = statsreal.at(3).section("<br/>",0,0);
                QDateTime  datereal = QDateTime::fromString(datestr,"dd/MM/yyyy hh:mmap");




                if ( realnode == currentnode) {
                    passnode = true;
                    if ( datereal < dateplanned) {
                        statsinfo = tr("0...1...(a tiempo) AQUI...%1<br/>entrega:%2<br/>plan.:%3\n")
                                .arg(SafetWorkflow::humanizeDate(days,datestr,"dd/MM/yyyy hh:mmap",
                                                                 dateplanned))
                                .arg(datestr)
                                .arg(dateplanned.toString("dd/MM/yyyy hh:mmap"));
                    }
                    else {
                        statsinfo = tr("0...1...(atraso) AQUI...%1<br/>actual:%3<br/>plan.:%2\n")
                                .arg(SafetWorkflow::humanizeDate(days,datestrp,"dd/MM/yyyy hh:mmap",
                                                                 datereal))
                                .arg(datestrp)
                                .arg(datereal.toString("dd/MM/yyyy hh:mmap"));
                        isdelay = true;

                    }
                }
                else {
                    if (!passnode) {
                        if ( datereal < dateplanned) {
                            statsinfo = tr("0...1...a tiempo...%1(adelanto)<br/>entrega:%2<br/>plan.:%3\n")
                                    .arg(SafetWorkflow::humanizeDate(days,datestr,"dd/MM/yyyy hh:mmap",
                                                                     dateplanned))
                                    .arg(datestr)
                                    .arg(dateplanned.toString("dd/MM/yyyy hh:mmap"));
                        }
                        else {
                            statsinfo = tr("0...1...atraso...%1<br/>actual:%3<br/>plan.:%2\n")
                                    .arg(SafetWorkflow::humanizeDate(days,datestrp,"dd/MM/yyyy hh:mmap",
                                                                     datereal))
                                    .arg(datestrp)
                                    .arg(datereal.toString("dd/MM/yyyy hh:mmap"));

                            isdelay = true;

                        }
                    }
                    else {
                        if ( datereal < dateplanned) {
                            statsinfo = tr("0...1...a tiempo...%1(adelanto)<br/>entrega:%2<br/>plan.:%3\n")
                                    .arg(SafetWorkflow::humanizeDate(days,datestr,"dd/MM/yyyy hh:mmap",
                                                                     dateplanned))
                                    .arg(datestr)
                                    .arg(dateplanned.toString("dd/MM/yyyy hh:mmap"));
                        }
                        else {
                            statsinfo = tr("0...1...atraso...%1<br/>actual:%3<br/>plan.:%2\n")
                                    .arg(SafetWorkflow::humanizeDate(days,datestrp,"dd/MM/yyyy hh:mmap",
                                                                     datereal))
                                    .arg(datestrp)
                                    .arg(datereal.toString("dd/MM/yyyy hh:mmap"));
                            isdelay = true;

                        }

                    }

                }

                QRegExp rx;
                rx.setPattern("info\\.task\\.color:\\s*([a-zA-Z0-9_\\.]+)\\s*,?");

                for(int j=0; j < myfirstlist.count()-nstatsreal; j++) {
                    QString nextfield =  myfirstlist.at(j);

                    if ( rx.indexIn( nextfield ) != -1 ) {
                        if ( days >= prodays ) {
                            porc = 1;
                        }
                        else {
                            porc = double(days)/double(prodays);
                        }

                        porc = 1-porc;
                        if (isdelay ) {
                            porc = porc*-1;
                        }


                        nextfield = QString("info.task.color:%1")
                                .arg(porc,0,'g',2);

                    }

                    result += nextfield;
                    result += ",";

                }

                result += statsinfo;

            }


            return result;
        }


        void MainWindow::evalEventOnExit(SafetLog::Level l) {
            if ( l == SafetLog::ExitApp ) {

            }
        }



        QString MainWindow::evalEventOnInput(SafetYAWL::TypeInput p, const QString& message, bool& ok) {
            QString str;

            return str;
        }

        QAbstractItemModel *MainWindow::modelFromFile(const QString& fileName)
        {
//            QFile file(fileName);
//            if (!file.open(QFile::ReadOnly))
//                return new QStringListModel(completer);
//
//       //     QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
//            QStringList words;
//
//            while (!file.atEnd()) {
//                QByteArray line = file.readLine();
//                if (!line.isEmpty())
//                    words << line.trimmed();
//            }
//
//            QApplication::restoreOverrideCursor();
//            return new QStringListModel(words, completer);
        }



        void MainWindow::about()
        {

            QString alpha, beta, release;
            QRegExp rx;
            rx.setPattern("([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.([0-9]+)");

            int pos = rx.indexIn(SafetYAWL::getNumberVersion());
            qDebug("...version: pos: %d",pos);
            if (pos >= 0 ) {
                alpha = rx.cap(2);
                beta = rx.cap(3);
                release = rx.cap(4);
            }

        }

        // *****************************************************************************************
        // M�todo para la Consola


void MainWindow::doGetSignDocument() {

}


void MainWindow::doSendSignDocument() {
}


bool MainWindow::executeParsed() {



            if  (!commands.contains('f') ) {
                if ( !commands.contains('h') && !commands.contains('V') && !commands.contains('T') ) {
                    streamText << tr("*** No se especifico la ruta del archivo de flujo de trabajo (.xml) *** \n");
                    streamText  <<  tr("Opcion: -f <archivo> o --file <archivo> \n");
                    sendStreamToOutput();
                    return false;
                }
            } else {
                if ( configurator != NULL ) {
                    delete configurator;
                    configurator = new SafetYAWL(_currconfpath+ "/" + Safet::datadir);
                }

                configurator->setAutofilters( commands['a']);
                configurator->setRecursivefilters( commands['r']);
                SafetYAWL::streamlog.initAllStack();
                configurator->openXML(commands['f']);

                if (queryForErrors()) {
                    //delete configurator;
                    qDebug("...retornando.....executedParsed");
                    return false;
                }

                configurator->openDataSources();

                configurator->convertXMLtoObjects();
                evalConffileValues(); // Evaluar el mapa de configuraci�n actual
                Q_CHECK_PTR(configurator);
                SafetWorkflow* mywf = configurator->getWorkflow();
                Q_CHECK_PTR(mywf);
                evalParsValues(mywf);

            }



            if ( commands.contains('c') ) {
                streamText << tr("Chequeado...!") << endl;
            }

            QMap<int, QString>::const_iterator i;
            for (i = commands.constBegin(); i != commands.constEnd(); ++i) {
                if ( i.key() != 'f' ) {
                    bool result = processCommand(i.key() );
                    if ( !result ) {
                        return false;
                    }
                }
            }

            sendStreamToOutput();

            return true;



        }


        bool MainWindow::parseArgs(const QString& a) {

            commands.clear();
            QStringList args = a.split(QRegExp("\\s+"), QString::SkipEmptyParts);

            int currentOption = 0;
            foreach( QString par, args) {
                QRegExp rx;
                rx.setPattern("\\-[a-zA-Z]");
                int pos = rx.indexIn(par);
                if ( pos > -1 )  {
                    Q_ASSERT( par.length() == 2 );
                    currentOption = (int)(par.at(1).toAscii());
                    qDebug("currentOption: %c", currentOption);
                    commands.insert(currentOption,"");
                }
                else {
                    Q_ASSERT( commands.contains(currentOption) );
                    qDebug("commands['%c'] = %s", currentOption, qPrintable(par));
                    commands[currentOption] = par;
                }

            }

        }

        bool MainWindow::parse(int &argc, char **argv) {
            int c;
            int verbose_flag = 0;
            qDebug("argv[0]: |%s|", argv[0]);
            qDebug("argv[1]: |%s|", argv[1]);
            parsed = true;
            while (1)
            {
                struct option long_options[] =
                {
                    /* These options set a flag. */
                {"verbose", no_argument,       &verbose_flag , 1},
                {"brief",   no_argument,       &verbose_flag, 0},
                /* These options don't set a flag.
                          We distinguish them by their indices. */
                {"listtasks",   no_argument,       0, 'l'},
                {"listconditions",   no_argument,       0, 'C'},
                {"check",    no_argument,       0, 'c'},
                {"listdocuments",    no_argument,       0, 'd'},
                {"data",    no_argument,       0, 'D'},
                {"file",  required_argument, 0, 'f'},
                {"autofilter",  required_argument, 0, 'a'},
                {"recursivefilter",  required_argument, 0, 'r'},
                {"task",  required_argument, 0, 't'},
                {"template",  no_argument, 0, 'T'},
                {"variable",  required_argument, 0, 'v'},
                {"json",  required_argument, 0, 'j'},
                {"graph",  required_argument, 0, 'g'},
                {"sign",  no_argument, 0, 's'},
                {"keyvalue",  required_argument, 0, 'k'},
                {"plugins",  required_argument, 0, 'p'},
                {"stat",  required_argument, 0, 'S'},
                {"help",  no_argument, 0, 'h'},
                {"version",  no_argument, 0, 'V'},
                {0, 0, 0, 0}
            };
                /* getopt_long stores the option index here. */
                int option_index = 0;

                c = getopt_long (argc, argv, "lCcf:a:t:Tj:dv:g:k:sS:hVD:p:",
                                 long_options, &option_index);

                if (c == -1)
                    break;
                //           qDebug("optarg: %s", optarg);
                switch (c)
                {
                case 0:
                    /* If this option set a flag, do nothing else now. */
                    if (long_options[option_index].flag != 0)
                        break;
                    streamText << tr("Opcion: ") << long_options[option_index].name ;
                    if (optarg)
                        streamText << tr(" con argumento: ") <<  optarg << endl;
                    break;
                case 'l':
                    commands['l'] = "list";
                    break;
                case 'C':
                    commands['C'] = "list";
                    break;
                case 'd':
                    commands['d'] = "documents";
                    break;
                case 'D':
                    commands['D'] = optarg;
                    break;
                case 'c':
                    commands['c'] = "check";
                    break;
                case 'p':
                    commands['p'] = optarg;
                    break;
                case 'f':
                    commands['f'] = optarg;
                    break;
                case 'a':
                    if (!commands.contains('a') ) {
                        commands['a'] = optarg;
                    }
                    else {
                        commands['a'] = commands['a']+"," +QString(optarg);
                    }
                    break;
                case 'r':
                    if (!commands.contains('r') ) {
                        commands['r'] = optarg;
                    }
                    else {
                        commands['r'] = commands['r']+"," +QString(optarg);
                    }
                    break;
                case 'g':
                    commands['g'] = optarg;
                    break;
                case 'v':
                    commands['v'] = optarg;
                    //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(!commands['v'].trimmed().startsWith("-"), tr("Debe especificar un nombre de variable, falta este parametro")) );
                    if (commands['v'].trimmed().startsWith("-") ) {
                        SafetYAWL::streamlog
                                << SafetLog::Error
                                <<
                                   tr("Debe especificar un nombre de variable, falta este parametro");
                    }
                    break;
                case 'k':
                    commands['k'] = optarg;
                    //			(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(!commands['k'].trimmed().startsWith("-"), tr("Debe especificar un valor para la clave, falta este parametro")) );
                    break;
                case 't':
                    commands['t'] = optarg;
                    break;
                case 'T':
                    commands['T'] = "template";
                    break;
                case 's':
                    commands['s'] = "sign";
                    break;
                case 'S':
                    commands['S'] = optarg;
                    break;
                case 'h':

                    commands['h'] = "help";
                    break;
                case 'V':
                    commands['V'] = "version";
                case '?':
                    /* getopt_long already printed an error message. */
                    break;

                default:
                    parsed = false;
                }
            }

            //	qDebug("!commands.contains('f'): %d", !commands.contains('f'));
            if ( commands.contains('h') || commands.contains('V') || commands.contains('T')) return true;
            if ( !commands.contains('f')  ) {
                streamText << tr("*** No se especifico la ruta del archivo de flujo de trabajo (.xml) *** \n");
                streamText  <<  tr("Opcion: -f <archivo> o --file <archivo> \n");
                //		streamText.flush();
                sendStreamToOutput();
                parsed = false;
                processCommand('f');
            }

            if (verbose_flag)
                streamText << tr("La opci�n (verbose) est� activa\n");

            /* Print any remaining command line arguments (not options). */
            if (optind < argc)
            {
                //	   qDebug("optind: %d argc: %d", optind, argc);
                streamText << tr("Parametro(s) invalido(s): ");
                while (optind < argc)
                    streamText <<  argv[optind++];
                streamText << endl;
                parsed = false;
            }

            if (!parsed ) return false;

            configurator->openXML(commands['f']);
            //      configurator->openDataSources();
            configurator->convertXMLtoObjects();

            // llamada a la funcion que habilita las fuentes de datos
            //qDebug("MainWindow: configurator->openDataSources()");

            //qDebug("SafetYAWL::databaseOpen: %d", SafetYAWL::databaseOpen);

            //configurator->loadPlugins();

            if ( commands.contains('c') ) {
                streamText << tr("Chequeado...!") << endl;
            }

            return parsed;
        }


        void MainWindow::sendStreamToOutput() {
            streamText.flush();
            outputText = "";

        }


bool MainWindow::processCommand(int command) {
    bool result = true;
    Q_CHECK_PTR( configurator->getWorkflows().at(0) );
    switch ( command ) {
    case 'f':
        if (commands['f'].length() == 0 ) {
            return false;
        }
        // 		e = QFile::exists( commands['f'] );
        // 		if (!e) {
        // 			streamText << tr("No es posible leer el archivo de flujo de trabajo de nombre: \"%1\" ").arg(commands['f']) << endl;
        // 			parsed = false;
        // 			quit();
        // 		}
        break;
    case 'l':
        listTasks();
        break;
    case 'd':
        qDebug("...listDocuments...(1)...");
        listDocuments();
        break;
    case 'D':
        manageData();
        break;
    case 'g':
        result = genGraph();
        break;
    case 's':
        signDocument();
        break;
    case 'S':
        calStatistics();
        break;
    case 'T':
        /* now Nothing */
        break;
    case 'h':
        displayUsage();
        break;
    case 'V':
        version();
        break;

    default:;
    }

    return result;
}



void MainWindow::displayUsage() {
    streamText << tr("Uso: safet --file <archivo de flujo de trabajo> [comandos]") << endl;
    streamText << tr("     safet -f <archivo de flujo de trabajo> [comandos]") << endl;
    streamText << tr("Cliente Safet de linea de comandos, version 0.1.beta") << endl;
    streamText << tr("Tipee 'safet --help ' para ayuda sobre los comandos.") << endl;
    streamText << tr("Tipee 'safet --version' o 'safet -V' para ver la version del cliente de linea de comandos.") << endl;
    streamText << endl;
    streamText << endl;
    streamText << tr("Los comandos reciben parametros de tipo variable, clave o subcomando, o algunos") << endl;
    streamText << tr("de los comandos no requieren ningun parametro. Si no se proveen par�metros a") << endl;
    streamText << tr("estos comandos, se generara un mensaje de error.") << endl;
    streamText << endl;
    streamText << tr("Comandos disponibles:") << endl;
    streamText << endl;
    streamText << tr("-f --file=ARCHIVO\tIndica el archivo (ARCHIVO) de flujo de trabajo a procesar]") << endl;
    streamText << tr("\t\t\tGeneramente debe tener extension .xml y se valida con el archivo de") << endl;
    streamText << tr("\t\t\tdefinicion yawlworkflow.dtd") << endl;
    streamText << endl;
    streamText << tr("-v --variable=NOMBRE\tSelecciona el nombre de la variable a procesar. La variable") << endl;
    streamText << tr("\t\t\tdebe estar incluida en el archivo de flujo de trabajo a procesar, e identificada") << endl;
    streamText << tr("\t\t\tcon el atributo 'id'.") << endl;
    streamText << endl;
    streamText << tr("-k --key=CLAVE\t\tIndica la clave para un comando de firma, u otro comando que") << endl;
    streamText << tr("\t\t\tlo requiera, generalmente debe ir acompanada del parametro que selecciona el nombre") <<endl;
    streamText << tr("\t\t\tde la variable") << endl;
    streamText << endl;
    streamText << tr("-s --stat=SUBCOMANDO\tGenera la estadistica seleccionada en el subcomando SUBCOMANDO.") << endl;
    streamText << tr("\t\t\tLos subcomandos posibles son \"ndocs\" (lista la cantidad de documentos para todas las") << endl;
    streamText << tr("\t\t\tvariables) y \"path\" (indica en que actividad/tarea se encuentra el token segun") << endl;
    streamText << tr("\t\t\tla clave indicada con el comando -k o --key)") << endl;
    streamText << endl;
    streamText << tr("-g --graph=ARCHIVO\tGenera un archivo grafico (png,svg,etc.) del flujo") << endl;
    streamText << tr("\t\t\tde trabajo. La extension del archivo indica el formato en que se escribe.") << endl;
    streamText << tr("\t\t\tSi no se especifica parametro se genera por defecto un archivo con ") << endl;
    streamText << tr("\t\t\tel nombre 'output.png' en el directorio actual") << endl;
    streamText << endl;
    streamText << tr("-c --check\t\tChequea que el archivo de flujo de trabajo indicado") << endl;
    streamText << tr("\t\t\ten la opcion -f o --file cumpla todas las reglas (Sintaxis,enlace con") << endl;
    streamText << tr("\t\t\trepositorio de datos, etc.) para ser procesado por un comando") << endl;
    streamText << endl;
    streamText << tr("-l --listtasks\t\tLista todas las tareas/actividades que contiene el archivo") << endl;
    streamText << tr("\t\t\tde flujo de trabajo indicado en la opcion -f o --file") << endl;
    streamText << endl;
    streamText << tr("-V --version\t\tMuestra la version actual de la aplicacion de linea ") << endl;
    streamText << tr("\t\t\tde comandos 'safet'.") << endl;
    streamText << endl;
    streamText << tr("-h --help\t\tMuestra este texto de ayuda") << endl;
    streamText << endl;
    streamText << tr("-d --listdocuments\tMuestra los documentos presentes en la variable indicada") << endl;
    streamText << tr(" \t\t\tcon el parametro -v o --variable") << endl;
    streamText << endl;
    streamText << endl;
    streamText << tr("Safet es una herramienta para gestion de flujos de trabajos basados en Redes de ") << endl;
    streamText << tr("Petri y patrones (AND,OR,XOR, etc), y que incorpora Firma ") << endl;
    streamText << tr("Electronica bajo el modelo de Infraestructura de Clave Publica.") << endl;
    streamText << endl;
    streamText << endl;
    streamText << tr("  Software utilizado principalmente en safet:") << endl;
    streamText << endl;
    streamText << tr("  Libreria Qt:  referirse a: http://www.trolltech.com") << endl;
    streamText << tr("   LibDigidoc:  referirse a: http://www.openxades.org") << endl;
    streamText << tr("     Graphviz:  referirse a: http://www.graphviz.org") << endl;
    streamText << tr("        DbXml:  referirse a: http://www.sleepycat.com") << endl;
    streamText << endl;

}


void MainWindow::calStatistics() {

    if ( commands['S'].compare("ndocs", Qt::CaseInsensitive ) == 0 ) {
        streamText << "Estadisticas para el archivo: <" << commands['f'] << ">:" << endl;
        foreach(QString name,configurator->getWorkflows().at(0)->variablesId()){
            SafetVariable *v = configurator->getWorkflows().at(0)->searchVariable( name );
            Q_CHECK_PTR( v );
            streamText << tr("Variable <") << name << tr(">  Numero de fichas: <") << configurator->getWorkflows().at(0)->numberOfTokens(*v) << ">" << endl;
        }
        streamText << endl;
    } else if ( commands['S'].compare("path", Qt::CaseInsensitive ) == 0 ) {
        //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento")) );
        SafetYAWL::evalAndCheckExit(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento"));

        QString info = 	commands['k'];
        (*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['t'].length() > 0 , tr("No se especifico un nombre de tarea  (--task <nombre>) para calcular e imprimir los caminos")) );
        QString result = configurator->getWorkflows().at(0)->printPaths(commands['t'],info);
        streamText << tr("Camino(s) para : ") << info << endl;
        streamText << result << endl;
    } else {
        streamText << tr("Argumento del comando Estadisticas -S <arg> o -stat <arg> invalido. Opciones: ndocs, percent, path") << endl;
    }
}



void MainWindow::signDocument() {
    //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento")) );
    SafetYAWL::evalAndCheckExit(commands['k'].length() > 0 , tr("No se especifico el valor de la clave (--keyvalue <valor>) para firmar (sign) el documento"));

    //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(commands['v'].length() > 0 , tr("No se especifico un nombre de variable  (--variable <nombre>) para firmar (sign) el documento")) );
    SafetYAWL::evalAndCheckExit(commands['v'].length() > 0 , tr("No se especifico un nombre de variable  (--variable <nombre>) para firmar (sign) el documento"));


    SafetVariable *v = configurator->getWorkflows().at(0)->searchVariable( commands['v'] );

    //(*SafetYAWL::evalExit())( SafetYAWL::streamlog.eval(v != NULL , tr("No existe la variable : \"%1\" en el documento de flujo de trabajo").arg(commands['v'])) );
    SafetYAWL::evalAndCheckExit(v != NULL , tr("No existe la variable : \"%1\" en el documento de flujo de trabajo").arg(commands['v']));

    QString derror;
    QString documentid = configurator->getWorkflows().at(0)->signDocument(v, commands['k'],derror);
    if (documentid.length() > 0 ) {
        streamText << tr("Documento con id:")  << documentid << " firmado correctamente OK!" << endl;
    }
    else {
        streamText << " NO se introdujo la CLAVE del documento a firmar. NO se realizo la FIRMA." << endl;
    }

}


bool MainWindow::genGraph() {
    char buffer[512];



    QString info;
    if ( commands.contains('p') ) {
        qDebug("***...command['p']: |%s|",qPrintable(commands['p']));
        configurator->loadPlugins(commands['p']);
    }

    if ( commands.contains('k') ) {
        info = 	commands['k'];
    }

    strncpy(buffer, qPrintable(SafetYAWL::getConf()["Plugins.Graphviz/graphtype"]), 4);
    SYD << tr("...MainWindow::doRenderGraph.....buffer:|%1|")
           .arg(buffer);

    SafetYAWL::streamlog.initAllStack();
    QString img = configurator->getWorkflows().at(0)->generateGraph(buffer, info);


    if ( img.isEmpty() ) {
        queryForErrors();

        return false ;
    }

    //SafetYAWL::lastinfodate = "";
    _currentjson = img;
    //_currentjson =  drawWorkflow(img); // Dibujar el flujo



    return true;
}


void MainWindow::doCipherFile(bool iscipher) {

#ifdef SAFET_OPENSSL // Criptosistema OpenSSL
    // ** Cifrando
    if (iscipher) {
        QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf";
        SafetCipherFile myfile(fileconf);
        qDebug("doCipherFile fileconf: %s", qPrintable(fileconf));
        myfile.open(QIODevice::ReadOnly);
        myfile.setPassword("caracas");
        QString fileconfcipher = SafetYAWL::pathconf+ "/" + "auth.conf.bin";
        qDebug("doCipherFile fileconfcipher: %s", qPrintable(fileconfcipher));
        myfile.cipherInFile(fileconfcipher);
        qDebug("...ok!");
        myfile.close();
    }
    else {
        // ****** Leyendo cifrado
        QString fileconf = SafetYAWL::pathconf+ "/" + "auth.conf.bin";
        SafetCipherFile myfile(fileconf);
        myfile.open(QIODevice::ReadOnly);
        myfile.setPassword("caracas");
        myfile.readAllCipher();
        QString line;
        while( true ) {
            line = myfile.readCipherLine();
            if (line.isNull()) {
                break;
            }
            qDebug("%s",qPrintable(line));
        }

    }
#endif // OpenSSL
}
void MainWindow::setParsValues(const QMap<QString,QString>& values) {
    _currparsvalues = values;
}

void MainWindow::evalParsValues(SafetWorkflow *wf) {
    SafetYAWL::streamlog
            << SafetLog::Debug
            << tr("MainWindow::evalParsValues()..._currconfvalues.keys().count():%1")
               .arg(_currparsvalues.keys().count());
    Q_CHECK_PTR(wf);
    QMap<uint,QString> mypars;
    bool ok;
    QVector<QString> mytmpvt(20);
    int countpars = _currparsvalues.keys().count();
    if ( countpars > 20 ) {
        SYE << tr("El n�mero de parametros del flujo de trabajo es excesivo: \"%1\""
                  ".Tiene que ser menor o igual a veinte (20)")
               .arg(countpars);
        return;
    }
    for(int i=0; i< countpars;i++) {
        QString mykey = _currparsvalues.keys().at(i);
        QString myvalue = _currparsvalues[mykey];
        uint myintkey = mykey.toUInt(&ok);
        if (myintkey < 20 ) {
            mytmpvt[myintkey] = myvalue;
            mypars[myintkey+1] = myvalue;
            SYD << tr("...***MainWindow::evalParsValues...mytmpvt[%1+1]:|%2|")
                   .arg(myintkey)
                   .arg(myvalue);

        }
    }
    wf->putParameters(mypars);
}

void MainWindow::setConffileValues(const QMap<QString,QString>& values) {
    _currconfvalues = values;
}

void MainWindow::evalConffileValues() {

    SafetYAWL::streamlog
            << SafetLog::Debug
            << tr("MainWindow::evalConffileValues()..._currconfvalues.keys().count():%1")
               .arg(_currconfvalues.keys().count());
    for(int i=0; i< _currconfvalues.keys().count();i++) {
        QString mykey = _currconfvalues.keys().at(i);
        QString myvalue = _currconfvalues[mykey];

        if ( SafetYAWL::getConf().getMap().contains(mykey)) {
            SafetYAWL::getConf().getMap()[mykey] = myvalue;
        }
    }
    _currconfvalues.clear();

}

void MainWindow::listTasks()
{
    Q_CHECK_PTR( MainWindow::configurator );
    QString outtasks = configurator->getWorkflows().at(0)->listTasks( commands['C'].length() > 0 );
    streamText << "Lista de identificadores  de tareas/actividades:"  << endl;
    streamText << outtasks;
}



void MainWindow::manageData() {
    //	streamText << "Data: <" << commands['D'] << ">" << endl;
    SafetTextParser parser;
    parser.setStr( commands['D'].toUtf8() );
    QString str = "agregar,eliminar,actualizar,mostrar";
    QString commandstr = "INSERT INTO,DELETE,UPDATE,SELECT";
    parser.setCommands(str.split(","),commandstr.split(","));
    QString xml = parser.toXml();
    SafetYAWL::streamlog << SafetLog::Debug << tr("Cadena XML generada de la entrada: \n\"%1\"").arg(xml);
    //	qDebug("\n\n....xml:\n%s", qPrintable(xml));
    parser.processInput( xml );
    parser.openXml();
    parser.processXml();
}


void MainWindow::version() {
    streamText << SafetYAWL::toUcode(QString(INFO_VERSION)) << endl;

}


QMap<QString,QDateTime> MainWindow::doGetPlannedGraph(const QString& namegraph) {
    QMap<QString,QDateTime> result;

    doRestoreGraph(Safet::PLANNEDGRAPHSFILENAME);
    if (graphs().contains(namegraph)) {
        SYW << tr("El nombre de grafo \"%1\" no se encuentra en el repositorio")
               .arg(namegraph);
        return result;
    }

    QString code =  graphs()[namegraph].first;

    if (code.isEmpty()) {
        SYW << tr("El registro para el grafo \"%1\" est� vac�o")
               .arg(namegraph);
        return result;
    }

    QStringList mylist = code.split("\n",QString::SkipEmptyParts);

    foreach(QString node, mylist) {
        QStringList flist = node.split(",",QString::SkipEmptyParts);
        QString s = flist.at(flist.count()-1);
        SYD << tr(".......MainWindow::doGetPlannedGraph...s:|%1|")
               .arg(s);
        QStringList parlist = s.split("...",QString::SkipEmptyParts);
        Q_ASSERT(parlist.count() > 3 );
        SYD << tr("parlist.count() > 3 ");
        QDateTime mydate;
        mydate.fromString(parlist.at(3));


    }

    return result;
}

void MainWindow::listDocuments(const QString& key) {
    char buffer[20];
    strncpy(buffer, qPrintable(SafetYAWL::getConf()["Graphviz.Plugins/graphtype"]), 3);
    buffer[3] = 0;
    QString code = configurator->getWorkflows().at(0)->generateCodeGraph(buffer,key,false);


    SYD << tr("..............****MainWindow::listDocuments....");

    SYD << tr(".....listDocuments...MainWindow::listDocuments....code:\n|%1|\n")
           .arg(code);
    SYD << tr("..............****MainWindow::listDocuments....");


    QList<SafetYAWL::Miles> ordermiles;

    QString namefields = "completekey,wftask,currkey,rol,timeprev,timecurr,secondswait,porc";

    QString namevalues;




    if(!code.isEmpty()) { // !code.isEmpty
        SYD << tr("1.Existe: %1").arg(QFile::exists("/home/vbravo/.safet/stats/stats.db"));
        SafetBinaryRepo myrepo;

        SYD << tr("...***MainWindow::listDocuments***....(1)...");
        SYD << tr("2.Existe: %1").arg(QFile::exists("/home/vbravo/.safet/stats/stats.db"));
        if (!myrepo.fileExists()) {
            myrepo.open();
            if (!myrepo.createTable("graphs","completekey text primary key,wftask text, currkey text,"
                                    "rol text,timeprev integer, timecurr "
                                    "integer, secondswait integer, porc integer"))  {
                SYW << tr("...MainWindow::listDocuments....No se pudo crear el archivo de estad�sticas");
            }
            else {
                SYW << tr("...MainWindow::listDocuments....Creado el archivo de estad�sticas");
            }

        }
        {
            myrepo.open();
        }

        QMap<QString,QDateTime> myplanneds = doGetPlannedGraph("POA Proyecto Seguridad Formato DDOC 2011 Version 1");
        SafetWorkflow::ExtraInfoSearchKey& sk = configurator->getWorkflows().at(0)->searchKey();
        SYD << tr("...sk.key():|%1|").arg(sk.key);
        SYD << tr("sk.extrainfo.keys(): |%1|").arg(sk.extrainfo.keys().count());

        QDateTime previewdate;

        SYD << tr("...........MainWindow::listDocuments...workflow id:|%1|")
               .arg(configurator->getWorkflows().at(0)->id());

        SYD << tr("...........MainWindow::listDocuments...workflow key:|%1|")
               .arg(key);
        for( int i = 0; i < sk.extrainfo.keys().count(); i++) {
            QString mykey = sk.extrainfo.keys().at(i);
            SYD << tr("...%1->%2")
                   .arg(mykey)
                   .arg(sk.extrainfo[ mykey]);

            QString mydata = sk.extrainfo[ mykey];
            QStringList mylist = mydata.split(QRegExp(";|<br/>"));
            //            foreach(QString d, mylist) {
            //                SYD << tr ("...d:|%1|").arg(d);
            //            }
            if (mylist.count() < 3 ) {
                continue;
            }
            QDateTime mydate = QDateTime::fromString(mylist.at(1),"dd/MM/yyyy hh:mmap");
            if (!mydate.isValid()) {
                SYD << tr(".....listDocuments....no Valid datetime");
                continue;
            }

            SafetYAWL::Miles mymile;
            mymile.nametask = mykey;

            mymile.ts = mydate;

            mymile.secondswait = 0;


            mymile.humandate = mylist.at(2);
            QStringList myinfolist = mylist.at(0).split(QRegExp("\\s+"),QString::SkipEmptyParts);
            mymile.rol = myinfolist.at(0);
            mymile.porc = "";
            if (myinfolist.count() > 1 ) {
                mymile.porc = myinfolist.at(1);
            }

            int pos = -1;
            for(int j=0; j < ordermiles.count(); j++) {
                if ( mymile.ts  < ordermiles.at(j).ts ) {
                    pos = j;
                    break;

                }
            }
            if (pos == -1 ) {
                pos = ordermiles.count();
            }
            SYD << tr ("....MainWindow::listDocuments insertando en la posicion: %1")
                   .arg(pos);
            ordermiles.insert(pos,mymile);
            previewdate  = mydate; // Colocar fecha anterior
        }


        for(int i = 0; i< ordermiles.count();i++) {
            int days;
            SafetYAWL::Miles &m = ordermiles[i];

            m.humanwait = tr("n/a");

            namevalues =  "'"+configurator->getWorkflows().at(0)->id()+m.nametask+key+"',";
            namevalues += "'"+configurator->getWorkflows().at(0)->id()+m.nametask+"',";
            namevalues += "'"+key+"',";
            namevalues += "'"+m.rol +"',";
            if ( i > 0 ) {
                SafetYAWL::Miles &prev = ordermiles[i-1];
                m.secondswait = m.ts.toTime_t()-prev.ts.toTime_t();
                m.humanwait = SafetWorkflow::humanizeDate(days,prev.ts.toString("dd/MM/yyyy hh:mm:ssap"),
                                                          "dd/MM/yyyy hh:mm:ssap",
                                                          m.ts,
                                                          SafetWorkflow::WaitTime);

                SYD << tr("...........m.prevdate:|%1|").arg(prev.ts.toTime_t());
                SYD << tr("m.secondstowait (prev):|%1|").arg(prev.secondswait);

                namevalues += QString("%1,").arg(prev.ts.toTime_t());
                namevalues += QString("%1,").arg(m.ts.toTime_t());
                namevalues += QString("%1,").arg(m.ts.toTime_t()-prev.ts.toTime_t());
                QString mporc = m.porc;
                if (mporc.endsWith("%")) {
                    mporc.chop(1);
                }
                namevalues += QString("%1").arg(mporc);

            }
            else {
                namevalues += QString("%1,").arg(0);
                namevalues += QString("%1,").arg(m.ts.toTime_t());
                namevalues += QString("%1,").arg(0);
                QString mporc = m.porc;
                if (mporc.endsWith("%")) {
                    mporc.chop(1);
                }
                namevalues += QString("%1").arg(mporc);
            }

            SYD << tr("...........m.task:|%1|").arg(m.nametask);
            SYD << tr("...........m.date:|%1|").arg(m.ts.toTime_t());
            SYD << tr("...........m.porc:|%1|").arg(m.porc);
            SYD << tr("m.rol:|%1|").arg(m.rol);

            myrepo.append("graphs",namefields, namevalues);

            SYD << tr(".........MainWindow::listDocuments....namefields:|%1|")
                   .arg(namefields);
            SYD << tr(".........MainWindow::listDocuments....namevalues:|%1|")
                   .arg(namevalues);

        }



        QString myvalues = myrepo.search("graphs");
        SYD << tr("........MainWindow::listDocuments...values...(1):\n|%1|\n")
               .arg(myvalues);


        QList<QSqlField> myfields;
        _listprincipalcount = ordermiles.count();
        _listprincipaltitle = tr("Lista por Clave \"%1\"")
                .arg(key)+_listprincipaltitle;
        QString myjson = getJSONMiles(ordermiles,myfields);

        _listprincipalvariable = "";
        _listprincipalkey = key;

        generateJScriptreports(myjson,myfields);
        executeJSCodeAfterLoad();




    } // !code.isEmpty



}

QString MainWindow::getJSONMiles(const QList<SafetYAWL::Miles>& miles,
                                 QList<QSqlField>& myfields) {
    QString str;
    QTextStream out(&str);
    myfields.clear();
    QSqlField f;
    f.setName(tr("Tarea"));
    f.setType(QVariant::String);
    myfields.append(f);
    f.setName(tr("Rol"));
    myfields.append(f);
    f.setName(tr("Fecha"));
    myfields.append(f);
    f.setName(tr("Tiempo"));
    myfields.append(f);
    f.setName(tr("Espera"));
    myfields.append(f);
    f.setName(tr("Porc"));
    myfields.append(f);

    for( int i = 0; i < miles.count(); i++) {
        QString cadena("");
        out << "\t\t{ ";
        cadena.append(" ");
        cadena.append(tr("Tarea"));
        cadena.append(": \"");
        cadena.append(miles.at(i).nametask);
        cadena.append("\", ");
        cadena.append(" ");
        cadena.append(tr("Rol"));
        cadena.append(": \"");
        cadena.append(miles.at(i).rol);
        cadena.append("\", ");
        cadena.append(" ");
        cadena.append(tr("Fecha"));
        cadena.append(": \"");
        cadena.append(miles.at(i).ts.toString("dd/MM/yyyy hh:mmap"));
        cadena.append("\", ");
        cadena.append(" ");
        cadena.append(tr("Tiempo"));
        cadena.append(": \"");
        cadena.append(miles.at(i).humandate);
        cadena.append("\"");
        cadena.append(", ");
        cadena.append(tr("Espera"));
        cadena.append(": \"");
        cadena.append(miles.at(i).humanwait);
        cadena.append("\"");
        cadena.append(", ");
        cadena.append(tr("Porc"));
        cadena.append(": \"");
        cadena.append(miles.at(i).porc);
        cadena.append("\"");
        out << cadena ;
        out << "},\n";
        cadena.clear();
    }
    str.chop(2);
    return str;
}

void MainWindow::listDocuments() {

    qDebug("...*listDocuments...commands['v']: %s", qPrintable(commands['v']));
    qDebug("...MainWindow::listDocuments()....");
    QString info;
    if ( commands.contains('k') ) {
        info = 	commands['k'];
        listDocuments(info);
        return;
    }

    SafetVariable* var = configurator->getWorkflows().at(0)->searchVariable( commands['v'] );

    if (var == NULL ) {
        streamText << tr("La variable \"%1\" NO existe en la especificacion de flujo de trabajo. Revise el nombre en el documento de flujo de trabajo").arg(commands['v']) << endl;
        SafetYAWL::streamlog
                << SafetLog::Error
                <<  tr("La variable \"%1\" NO existe en la especificacion de flujo de trabajo."
                       "Revise el nombre en el documento de flujo de trabajo")
                    .arg(commands['v']);
        return;
    }

    if ( commands.contains('p') ) {
        configurator->loadPlugins(commands['p']);
    }
    QList<QSqlField> fields;

    SafetVariable *myvar = configurator->getWorkflows().at(0)->searchVariable((commands['v']));

    if ( myvar != NULL ) {
        if ( !myvar->description().isEmpty()) {
            _listprincipaltitle = myvar->description()+_listprincipaltitle;
        }
        else {
            _listprincipaltitle = QString("%1%2").arg(commands['v']).arg(_listprincipaltitle);
        }
    }
    else {
        _listprincipaltitle = QString("%1%2").arg(commands['v']).arg(_listprincipaltitle);

    }


    QString documents = configurator->getWorkflows().at(0)->getDocuments(commands['v'],
                                                                         fields,_listprincipalcount,
                                                                         SafetWorkflow::JSON, "");

    qDebug("..........MainWindow::listDocuments...documents:\n|%s|\n",qPrintable(documents));
    SYD << tr("....MainWindow::listDocuments..._listprincipalcount:|%1|")
           .arg(_listprincipalcount);

    _listprincipalvariable = commands['v'];
    _listprincipalkey = "";


    _currentjson = documents;
    generateJScriptreports(documents,fields);


}


void MainWindow::iterateMap(const QMap<QString,QVariant>& map) {

    QList<QVariant> mylist = map.values("record");
    for ( int i = 0 ; i < mylist.count(); i++) {
        qDebug("\n..%d. Registro:", i+1);
        qDebug("id: %s", qPrintable(mylist.at(i).toMap()[ "id" ].toString()));
        qDebug("description: %s", qPrintable(mylist.at(i).toMap()[ "description" ].toString()));
        qDebug("summary: %s", qPrintable(mylist.at(i).toMap()[ "summary" ].toString()));
    }

}







void MainWindow::successVerification(QStringList list, const QString& msg) {
    /*  // funcion comentada para eliminar dependencia de digidocpp
    qDebug("...........**......MainWindow::successVerification(QStringList list, const QString& msg) {");
    Q_CHECK_PTR(dockShowResult);
    QString message;
    if ( msg.isEmpty()) {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(tr("Verificaci�n exitosa....<b>ok!</b>"));
    }
    else {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(msg);
    }

    dockShowResult->addHeadHtml(message);
    qDebug("....successVerification........message: %s",qPrintable(message));
    dockShowResult->addOptionsDigidoc(digiDocument,list);
    showShowResultWidget(DockSbMenu::Show);
*/
}

void MainWindow::successVerification(QStringList list, const QString& msg, SafetDocument doc){
    qDebug("...........**......MainWindow::successVerification(QStringList list, const QString& msg, SafetDocument doc)");

    QString message;
    if ( msg.isEmpty()) {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(tr("Verificaci�n exitosa....<b>ok!</b>"));
    }
    else {
        message = QString("<table><tr><td><font color=green>%1</font></td></tr></table>").arg(msg);
    }


}

QString MainWindow::renderMessageStack(QStack<QPair<QDateTime,QString> >& stack, SafetLog::Level l){
    QString result;
    while ( !stack.isEmpty() ) {
        QPair<QDateTime,QString> c = stack.pop();
        result += c.second;
        result += "\n";
    }
    result.chop(1);

    return result;
}

// ** Servicios de Escritorio (QDesktopServices)

void MainWindow::browse( const QUrl &url )
{
    QUrl u = url;
    u.setScheme( "file" );
    bool started = false;
#if defined(Q_OS_WIN32)
    started = QProcess::startDetached( "cmd", QStringList() << "/c" <<
                                       QString( "explorer /select,%1" ).arg( QDir::toNativeSeparators( u.toLocalFile() ) ) );
#elif defined(Q_OS_MAC)
    started = QProcess::startDetached("/usr/bin/osascript", QStringList() <<
                                      "-e" << "on run argv" <<
                                      "-e" << "set vfile to POSIX file (item 1 of argv)" <<
                                      "-e" << "tell application \"Finder\"" <<
                                      "-e" << "select vfile" <<
                                      "-e" << "activate" <<
                                      "-e" << "end tell" <<
                                      "-e" << "end run" <<
                                      // Commandline arguments
                                      u.toLocalFile());
#endif
    if( started )
        return;
 //   QDesktopServices::openUrl( QUrl::fromLocalFile( QFileInfo( u.toLocalFile() ).absolutePath() ) );
}

void MainWindow::mailTo( const QUrl &url )
{
#ifdef SAFET_ESMTP // correo electronico
#if defined(Q_OS_WIN32)
    QString file = url.queryItemValue( "attachment" );
    QByteArray filePath = QDir::toNativeSeparators( file ).toLatin1();
    QByteArray fileName = QFileInfo( file ).fileName().toLatin1();
    QByteArray subject = url.queryItemValue( "subject" ).toLatin1();

    MapiFileDesc doc[1];
    doc[0].ulReserved = 0;
    doc[0].flFlags = 0;
    doc[0].nPosition = -1;
    doc[0].lpszPathName = filePath.data();
    doc[0].lpszFileName = fileName.data();
    doc[0].lpFileType = NULL;

    // Create message
    MapiMessage message;
    message.ulReserved = 0;
    message.lpszSubject = subject.data();
    message.lpszNoteText = "";
    message.lpszMessageType = NULL;
    message.lpszDateReceived = NULL;
    message.lpszConversationID = NULL;
    message.flFlags = 0;
    message.lpOriginator = NULL;
    message.nRecipCount = 0;
    message.lpRecips = NULL;
    message.nFileCount = 1;
    message.lpFiles = (lpMapiFileDesc)&doc;

    QLibrary lib("mapi32");
    typedef ULONG (PASCAL *SendMail)(ULONG,ULONG,MapiMessage*,FLAGS,ULONG);
    SendMail mapi = (SendMail)lib.resolve("MAPISendMail");
    if( mapi )
    {
        mapi( NULL, 0, &message, MAPI_LOGON_UI|MAPI_DIALOG, 0 );
        return;
    }
#elif defined(Q_OS_MAC)
    CFURLRef emailUrl = CFURLCreateWithString(kCFAllocatorDefault, CFSTR("mailto:info@example.com"), NULL), appUrl = NULL;
    bool started = false;
    if(LSGetApplicationForURL(emailUrl, kLSRolesEditor, NULL, &appUrl) == noErr)
    {
        CFStringRef appPath = CFURLCopyFileSystemPath(appUrl, kCFURLPOSIXPathStyle);
        if(appPath != NULL)
        {
            if(CFStringCompare(appPath, CFSTR("/Applications/Mail.app"), 0) == kCFCompareEqualTo)
            {
                started = QProcess::startDetached("/usr/bin/osascript", QStringList() <<
                                                  "-e" << "on run argv" <<
                                                  "-e" << "set vattachment to (item 1 of argv)" <<
                                                  "-e" << "set vsubject to (item 2 of argv)" <<
                                                  "-e" << "tell application \"Mail\"" <<
                                                  "-e" << "set composeMessage to make new outgoing message at beginning with properties {visible:true}" <<
                                                  "-e" << "tell composeMessage" <<
                                                  "-e" << "set subject to vsubject" <<
                                                  "-e" << "tell content" <<
                                                  "-e" << "make new attachment with properties {file name: vattachment} at after the last paragraph" <<
                                                  "-e" << "end tell" <<
                                                  "-e" << "end tell" <<
                                                  "-e" << "activate" <<
                                                  "-e" << "end tell" <<
                                                  "-e" << "end run" <<
                                                  // Commandline arguments
                                                  url.queryItemValue("attachment") <<
                                                  url.queryItemValue("subject"));
            }
            else if(CFStringFind(appPath, CFSTR("Entourage"), 0).location != kCFNotFound)
            {
                started = QProcess::startDetached("/usr/bin/osascript", QStringList() <<
                                                  "-e" << "on run argv" <<
                                                  "-e" << "set vattachment to (item 1 of argv)" <<
                                                  "-e" << "set vsubject to (item 2 of argv)" <<
                                                  "-e" << "tell application \"Microsoft Entourage\"" <<
                                                  "-e" << "set vmessage to make new outgoing message with properties" <<
                                                  "-e" << "{subject:vsubject, attachments:vattachment}" <<
                                                  "-e" << "open vmessage" <<
                                                  "-e" << "activate" <<
                                                  "-e" << "end tell" <<
                                                  "-e" << "end run" <<
                                                  // Commandline arguments
                                                  url.queryItemValue("attachment") <<
                                                  url.queryItemValue("subject"));
            }
            else if(CFStringCompare(appPath, CFSTR("/Applications/Thunderbird.app"), 0) == kCFCompareEqualTo)
            {
                // TODO: Handle Thunderbird here? Impossible?
            }
            CFRelease(appPath);
        }
        CFRelease(appUrl);
    }
    CFRelease(emailUrl);
    if( started )
        return;
#elif defined(Q_OS_LINUX)
    QByteArray thunderbird;
    QProcess p;
    QStringList env = QProcess::systemEnvironment();
    if( env.indexOf( QRegExp("KDE_FULL_SESSION.*") ) != -1 )
    {
        p.start( "kreadconfig", QStringList()
                 << "--file" << "emaildefaults"
                 << "--group" << "PROFILE_Default"
                 << "--key" << "EmailClient" );
        p.waitForFinished();
        QByteArray data = p.readAllStandardOutput().trimmed();
        if( data.contains("thunderbird") )
            thunderbird = data;
    }
    else if( env.indexOf( QRegExp("GNOME_DESKTOP_SESSION_ID.*") ) != -1 )
    {
        p.start( "gconftool-2", QStringList()
                 << "--get" << "/desktop/gnome/url-handlers/mailto/command" );
        p.waitForFinished();
        QByteArray data = p.readAllStandardOutput();
        if( data.contains("thunderbird") )
            thunderbird = data.split(' ').value(0);
    }
    if( !thunderbird.isEmpty() )
    {
        bool started = p.startDetached( thunderbird, QStringList() << "-compose"
                                        << QString( "subject='%1',attachment='%2,'" )
                                        .arg( url.queryItemValue( "subject" ) )
                                        .arg( QUrl::fromLocalFile( url.queryItemValue( "attachment" ) ).toString() ) );
        if( started )
            return;
    }
#endif
    QDesktopServices::openUrl( url );
#endif // Correo Electronico
}

void MainWindow::manageDataSources(){

    //QString driver = "database.driver.";
    QString hostName = "database.host.";
    QString dataBaseName = "database.db.";
    QString userName = "database.user.";
    QString portName = "database.port.";

    int i = 0;
    //    QString driverConf = SafetYAWL::getAuthConf()
    //                         .getValue("Database", driver+QString::number(i+1)).toString();
    QString hostNameConf = SafetYAWL::getAuthConf()
            .getValue("Database", hostName+QString::number(i+1)).toString();
    //qDebug("hostname: %s", qPrintable(hostNameConf));

    QString dataBaseNameConf = SafetYAWL::getAuthConf()
            .getValue("Database", dataBaseName+QString::number(i+1)).toString();
    //qDebug("database: %s", qPrintable(dataBaseNameConf));
    QString userNameConf = SafetYAWL::getAuthConf()
            .getValue("Database", userName+QString::number(i+1)).toString();
    //qDebug("user: %s", qPrintable(userNameConf));
    QString portConf = SafetYAWL::getAuthConf()
            .getValue("Database", portName+QString::number(i+1)).toString();
    //qDebug("port: %s", qPrintable(portConf ));

    /*if ( result != QDialog::Accepted ){
            return;
    }*/

    return;
}



void MainWindow::setEnableCompletingButtons(bool b){

//    qDebug("MainWindow::setEnableCompletingButtons(bool b)");
//    // habilitar el boton de enviar
//    completingButtonForm->setEnabled(b);
//    completingButtonCons->setEnabled(b);
//    //completingButtonSign->setEnabled(false);
//    completingButtonConf->setEnabled(b);
//    completingButtonUsers->setEnabled(b);
}


void MainWindow::doLoadConfiguration() {
    QString fileName = QDir::homePath()+"/MyDocs/stories.tar";

    if (fileName.isEmpty()) {
        return;
    }
    if (!QFile::exists(fileName)) {
        return;
    }
    QStringList mylist = MainWindow::uncompresstar(fileName);
    qDebug("...(*)...mylist: %d", mylist.count());
    QStringList newlist;

    int maxlenstr = 0;
    QString principaldir;
    foreach(QString s, mylist) {
        if (maxlenstr == 0 ) {
            newlist.push_front(s);
            maxlenstr = s.length();
        }
        else if (s.length() < maxlenstr)  {
            maxlenstr = s.length();
            newlist.push_front(s);
        }
        else{
            newlist.push_back(s);
        }


    }
    Q_ASSERT(newlist.count() > 0 );

    int i = 0;
    principaldir = newlist.at(0);
    foreach(QString s, newlist) {
        if ( !processDirTar(s, i == 0) ) {
            return;
        }

        i++;
    }

}


QStringList MainWindow::uncompresstar(const QString& filename) {
    QStringList files;
    int argc = 5;
    char **argv = NULL;

    argv = new char*[argc];
    argv[0] = new char[30];
    strcpy(argv[0],"inflow");
    argv[1] = new char[30];
    strcpy(argv[1],"-C");

    argv[2] = new char[1024];
    //qDebug("......QDir::tempPath(): %s", qPrintable(QDir::tempPath()));
    QString rootdir;
    QStringList mytempfiles = SafetYAWL::getTempNameFiles(1);

    rootdir = mytempfiles.at(0);
    if (QFile::exists(rootdir)) {
        QFile::remove(rootdir);
    }
    QString xdir = rootdir.section("/",-1);
    //qDebug("......QDir::tempPath()tempdir.fileName() (2) xdir: |%s|", qPrintable(xdir));
    QDir mytempdir(QDir::tempPath());
    mytempdir.mkdir(xdir);
    rootdir  = QDir::tempPath() + "/" + xdir;

    bool mkresult = mytempdir.mkpath(rootdir);
    if (!mkresult ) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("Error descomprimiendo configuración."
                      "No es posible crear el directorio: \"%1\"")
                   .arg(rootdir);
        return files;
    }
    qDebug("..ok!");
    qDebug("creado..:%s", qPrintable(rootdir));
    SYD            << tr("Descomprimiendo archivos: Creado el directorio temporal: \"%1\"")
                      .arg(rootdir);

    //qDebug("....(arg2)..QDir::tempPath()tempdir.fileName() (2) rootdir: |%s|", qPrintable(rootdir));

    strcpy(argv[2],qPrintable(rootdir));

    //    argv[3] = new char[30];
    //    strcpy(argv[3],"-z");

    argv[3] = new char[30];
    strcpy(argv[3],"-x");

    argv[4] = new char[1024];
    strcpy(argv[4],qPrintable(filename));
    //    qDebug("....uncompress.tar: |%s|", qPrintable(filename));
    //    qDebug("....uncompress.tar: option extract |%s|", argv[3]);

    //dotar(argc,argv);
    mytempdir.cd(rootdir);
    mytempdir.setFilter(QDir::Files|  QDir::NoSymLinks);
    mytempdir.setSorting(QDir::Size | QDir::Reversed);

    QFileInfoList list = mytempdir.entryInfoList();
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        QString namedir = fileInfo.fileName();
        if ( !namedir.startsWith(".")) {
            files.append(rootdir+"/"+ namedir);
            //            qDebug(".....GetSignDocumentDIalog..files: %s",
            //                   qPrintable(rootdir+"/"+namedir));
        }
    }
    for(int i=0; i< argc; i++) {
        delete argv[i];
    }
    delete[] argv;
    return files;


}


void MainWindow::doSaveFTP() {

    //bool ok;


//    QString fileName;
//    fileName = QString(QDir::homePath()+"/.safet/mydb.db");
//
//
//    FtpWindow myftpwindow;
//
//
//
//    myftpwindow.setLocalFileName(fileName);
//    QString remoteName = QString(".safet/")+fileName.section("/",-1);
//    qDebug("...MainWindow::doSaveFtpConfiguration...setLocalFileName:|%s|",qPrintable(fileName));
//    myftpwindow.setRemoteFileName(remoteName);
//    myftpwindow.setWindowTitle(tr("Subir configuraci�n a servidor FTP:\"%1\"").arg(remoteName));
//    qDebug("...MainWindow::doSaveFtpConfiguration...remoteName:|%s|",qPrintable(remoteName));
//    int result = myftpwindow.exec();


}


void MainWindow::doLoadFTP() {

//    qDebug("...MainWindow::doLoadFTP....(1)...");
//
//    QString fileName;
//    FtpWindow myftpwindow(NULL,FtpWindow::Downloader);
//
//
//    myftpwindow.setLocalFileName(fileName);
//    myftpwindow.setRemoteFileName(fileName);
//    myftpwindow.setWindowTitle(tr("Descargar configuraci�n desde Servidor FTP"));
//    int result = myftpwindow.exec();
//
//    qDebug("...MainWindow::doLoadFTP....(1)...result...:|%1|",result);
//    if ( result == QDialog::Rejected ) {
//        qDebug("...MainWindow::doLoadFTP....(1)...Rejected..");
//        return;
//    }
//
//    fileName = myftpwindow.localFileName();
//    qDebug("...MainWindow::doLoadFTP....(1)...localFileName:|%s|",qPrintable(fileName));
//
//    if (fileName.isEmpty()) {
//        qDebug("...MainWindow::doLoadFTP....(2)...");
//        return;
//    }
//    if (!QFile::exists(fileName)) {
//        qDebug("...MainWindow::doLoadFTP....(3)...");
//        SYE << tr("Al bajar el archivo \"%1\" desde el servidor FTP, no se guard� correctamente")
//               .arg(fileName);
//        return;
 //   }
}

bool MainWindow::processDirTar(const QString& f, bool isprincipal) {

}


void MainWindow::doDelAllFilesOnDirectory(const QString &d) {
    QDir dir(d);
    if ( !dir.exists() ) {
        SYD << tr("No se puede eliminar archivos de un directorio que no existe: \"%1\"")
               .arg(d);
        qDebug("...No existe el directorio:|%s|", qPrintable(d));
        return;
    }

    dir.setFilter( QDir::Files );
    dir.setSorting(QDir::Size | QDir::Reversed);
    QStringList filters;
    filters << "*";
    dir.setNameFilters( filters );
    QStringList filelist = dir.entryList(QDir::Files);
    qDebug("Numero de archivos a eliminar: %d",filelist.count());
    foreach(QString f, filelist) {
        bool isdeleting = dir.remove(f);
        if ( !isdeleting) {
            qDebug("No se borro el archivo: %s",qPrintable(f));
        }

    }


}

QString MainWindow::generateListForTar(const QString& folder, QStringList& myfiles,
                                       const QStringList& exts) {


    QString result;
    QString newdir = Safet::datadir;
    if (newdir.startsWith(".")) {
        newdir = newdir.mid(1);
    }
    if (!folder.isEmpty()) {
        result =  QDir::tempPath()+"/"+newdir +"."+folder+".tar";
    }
    else {
        result =  QDir::tempPath()+"/" + newdir +".tar";
    }
    QString confpath = _currconfpath + "/" + Safet::datadir + "/" + folder;

    QDir dirconf(confpath);
    dirconf.setFilter(QDir::Files | QDir::NoSymLinks);
    if (!dirconf.exists()) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("Error al intentar abrir el directorio para empaquetado: \"%1\"")
                   .arg(confpath);
        return QString("");

    }


    QFileInfoList mylist = dirconf.entryInfoList();

    for (int i=0; i<mylist.size(); i++)
    {
        QFileInfo fileInfo = mylist.at(i);
        foreach(QString e, exts) {
            if (fileInfo.filePath().endsWith("."+e)) {
                myfiles.append(fileInfo.filePath());
                //qDebug("...fileInfo.filePath(): %s", qPrintable(fileInfo.filePath()));
                break;
            }
        }
    }
    return result;

}

void MainWindow::loadAuthorization() {

    int countperm = 1;
    while ( true ) {
        QString operation =  SafetYAWL::getAuthConf()[QString("Permises/operation.%1").arg(countperm)];
        QString accounts = SafetYAWL::getAuthConf()[QString("Permises/accounts.%1").arg(countperm)];
        QString types =  SafetYAWL::getAuthConf()[QString("Permises/types.%1").arg(countperm)];
        QString roles =  SafetYAWL::getAuthConf()[QString("Permises/roles.%1").arg(countperm)];
        QString thesign;
        if (operation.trimmed().length() == 0 ) {
            break;
        }
        QStringList info;

        info.append(accounts);
        info.append(types);
        info.append(roles);
        info.append(thesign);

        MainWindow::permises[ operation ] = info;
        countperm++;

    }

}

void MainWindow::loadInfo() {


    int countuser = 1;

    while ( true ) {
        QString account = SafetYAWL::getAuthConf()[QString("Auth/account.%1").arg(countuser)];
        if ( account.trimmed().length() == 0 ) {
            break;
        }
        QString pass = SafetYAWL::getAuthConf()[QString("Auth/pass.%1").arg(countuser)];
        QString role = SafetYAWL::getAuthConf()[QString("Auth/role.%1").arg(countuser)];
        QString realname = SafetYAWL::getAuthConf()[QString("Auth/realname.%1").arg(countuser)];
        if ( pass.trimmed().length() > 0 ) {
            QStringList mylist;
            mylist.append(pass);
            mylist.append(role);
            mylist.append(realname);
            //users[ account ] = mylist; // Quitar comentario para ACTIVARUSER
        }
        countuser++;
    }

}


bool MainWindow::logout() {
    if ( MainWindow::currentaccount.isEmpty() ) {
        return false;
    }
    MainWindow::currentaccount = "";
    MainWindow::currentrole = "";
//    MainWindow::currentrealname = "";
    return true;

}

bool MainWindow::login(const QString& name, const QString& pass) {

//    QString curuser = name;
    // Quitar para colocar usuarios

//    if ( !users.contains(curuser) ) {

//        SYE  <<
//                tr("El usuario \"%1\" no existe en la base de datos de autorizaci�n");

//        return false;

//    }
//    Q_ASSERT( users[curuser].count() > 0 );

//    QByteArray textvalue = QByteArray(qPrintable(pass));
//    QByteArray hashvalue = QCryptographicHash::hash( textvalue, QCryptographicHash::Sha1 );
//    if ( users[curuser].at(0) != QString(hashvalue.toHex().data()) ) {
//        SafetYAWL::streamlog << SafetLog::Error <<
//                                trUtf8("La contrase�a para el usuario \"%1\" es incorrecta");
//        qDebug("Contrase�a incorrecta");
//        return false;
//    }

//    MainWindow::currentaccount = curuser;
//    SafetYAWL::setCurrentAuthUser(MainWindow::currentaccount);
//    SYA << tr("El usuario (sistema de autorizacion a cambiado a \"%1\"")
//           .arg(SafetYAWL::currentAuthUser());

//    if ( users[curuser].count() > 1 ) {
//        MainWindow::currentrole = users[curuser].at(1);
//        if ( users[curuser].count() > 2 ) {
//            MainWindow::currentrealname = users[curuser].at(2);
//        }
//    }
//    else {
//        MainWindow::currentrole = "";
//    }

    return true;
}

void MainWindow::doSaveConfiguration() {


}

QString MainWindow::currentError() {
    QString result;
    result = _currenterror;
    _currenterror = "";
    return result;
}

QString MainWindow::currentJSON()   {
    QString result = _currentjson;

    _currentjson = QLatin1String("");
    return result;

}



QString MainWindow::delNodeToXMLWorkflow(const QString& fname,
                                         const QString& nodename) {
    QString result;
    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);

        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);


        return result;
    }


    bool iscontent = doc.setContent(&file);

    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        return result;
    }
    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();


    QDomNode n = root.firstChild();

    QDomElement mydelelement, oldbeforeelement;

    QList<QDomElement> beforeelements;
    QString afternode;

    while( !n.isNull() ) {

        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                qDebug("...***MainWindow::delNodeToXMLWorkflow..id:|%s|",
                       qPrintable(myid));

                QDomElement myport = e.firstChildElement("port");
                QDomElement myconn = myport.firstChildElement("connection");

                if (myid == nodename ) {
                    mydelelement  = e;
                    afternode = myconn.attribute("source").simplified();
                    n = n.nextSibling();
                    continue;
                }

                while( !myconn.isNull()) {
                    if (myconn.attribute("source").simplified() == nodename) {
                        qDebug("....delNodeToXmlWorkflow...beforenode..:|%s|",
                               qPrintable(myid));
                        beforeelements.append(e);
                    }
                    myconn = myconn.nextSiblingElement("connection");
                }
            }
            else {
                qDebug("...No atributo ID");

            }

        }

        n = n.nextSibling();
    }

    if (afternode.isEmpty()) {
        SYE << tr("Error en los enlaces del flujo de trabajo (enlace del siguiente nodo)");
        return result;
    }



    foreach(QDomElement beforeelement, beforeelements) {
        // cambiar enlace del anterior  (beforenode)
        oldbeforeelement = beforeelement;
        QDomElement myport = beforeelement.firstChildElement("port");
        QDomElement oldport = myport;
        QDomElement myconn, oldconn, searchconn;
        oldconn = myconn;
        int howconns = 0;

        myconn = myport.firstChildElement("connection");

        while(!myconn.isNull()) {
            qDebug("....before...myconn.attribute:|%s|", qPrintable(myconn.attribute("source")));
            if ( myconn.attribute("source").simplified() == nodename ) {
                searchconn = myconn;
                oldconn = myconn;
            }
            howconns++;
            myconn = myconn.nextSiblingElement("connection");
        }

        Q_ASSERT(howconns >= 1);
        Q_ASSERT(!searchconn.isNull());

        if ( howconns > 1 ) {

            myport.removeChild(searchconn);
            howconns--;
            if (howconns == 1 ) {
                myport.setAttribute("pattern","none");
            }
        }
        else {
            // Obtener el siguiente elemento
            QDomElement mydelport = mydelelement.firstChildElement("port");
            QDomElement mydelconn = mydelport.firstChildElement("connection");

            searchconn.setAttribute("options",mydelconn.attribute("options"));
            searchconn.setAttribute("query",mydelconn.attribute("query"));
            searchconn.setAttribute("source",mydelconn.attribute("source"));
            myport.replaceChild(searchconn,oldconn);

        }

        beforeelement.replaceChild(myport, oldport);
        root.replaceChild(beforeelement,oldbeforeelement);
    }



    // eliminar el nodo

    root.removeChild(mydelelement);

    //        QDomElement oldautofilter = myautofilter;
    //        myautofilter.setAttribute("source",nodename);
    //        mydelelement.replaceChild(myautofilter,oldautofilter);
    //        myautofilter = myautofilter.nextSiblingElement("autofilter");

    //    root.replaceChild(beforeelement, olde);


    qDebug("...MainWindow::delNodeToXMLWorkflow.....(2)...:|%s|",
           qPrintable(fname));
    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
        SYE << tr("No es posible escribir el archivo \"%1\" "
                  "con el nodo \"%2\" agregado")
               .arg(fname)
               .arg(nodename);
        qDebug("error open file writeonly");
        return result;
    }

    result = fname;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();

    return result;
}

QString MainWindow::changeConnXMLWorkflow(const QString& fname,
                                          const QString& currnode,
                                          const QString& nextnode,
                                          const QString& newnode,
                                          const QString& newoptions,
                                          const QString& newquery) {
    QString result;
    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible cambiar la conexi�n en el archivo")
               .arg(fname);
        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible cambiar la conexi�n en el archivo")
               .arg(fname);

        return result;
    }


    bool iscontent = doc.setContent(&file);
    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        return result;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();

    qDebug("...MainWindow::changeConnXMLWorkflow.....(1)...");


    QDomNode n = root.firstChild();


    QDomElement mynewconn, oldport, olde;

    QString afternode;
    while( !n.isNull() ) {

        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            olde = e;
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                qDebug("...*MainWindow::changeNodeXMLWorkflow..id:|%s|",
                       qPrintable(myid));

                if (myid == currnode) {
                    QDomElement myport = e.firstChildElement("port");
                    oldport = myport;
                    QDomElement myconn = myport.firstChildElement("connection");
                    while(!myconn.isNull()) {
                        afternode = myconn.attribute("source");
                        if (afternode == nextnode) {
                            mynewconn = myconn;
                            mynewconn.setAttribute("source",newnode);
                            if (newoptions.isEmpty()) {
                                mynewconn.setAttribute("options",newnode);
                            }
                            else {
                                mynewconn.setAttribute("options",newoptions);
                            }

                            if (!newquery.isEmpty()) {
                                mynewconn.setAttribute("query",newquery);
                            }
                            myport.replaceChild(mynewconn,myconn);
                            e.replaceChild(myport,oldport);
                            root.replaceChild(e,olde);
                            break;
                        }
                        myconn = myconn.nextSiblingElement("connection");
                    }
                }

            }

        }

        n = n.nextSibling();
    }

    qDebug("...MainWindow::changeConnXMLWorkflow.....(2)...:|%s|",
           qPrintable(fname));
    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
        SYE << tr("No es posible escribir el archivo \"%1\" con el nodo \"%2\" cambiado")
               .arg(fname)
               .arg(currnode);
        qDebug("error open file writeonly");
        return result;
    }

    result = fname;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();


    return result;

}

QString MainWindow::addNodeToXMLWorkflow(const QString& fname,
                                         const QString& beforenode,
                                         const QString& nodename,
                                         bool isparallel,
                                         const QString& options,
                                         const QString& query,
                                         const QString& nodetitle,
                                         const QString &documentsource
                                         ) {

    QString result;
    QDomDocument doc;
    QFile file(fname);
    if (!QFile::exists(fname) ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no existe en el sistema de archivos"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);

        return result;
    }

    bool isopen = file.open(QIODevice::ReadOnly);
    if (!isopen ) {
        SYE << tr("El archivo de flujo de trabajo \"%1\" no se puede leer"
                  ".No es posible agregar un estado al archivo")
               .arg(fname);


        return result;
    }


    bool iscontent = doc.setContent(&file);

    if (!iscontent) {
        SYE << tr("El formato del archivo XML \"%1\" no es v�lido")
               .arg(fname);
        return result;
    }

    file.close();
    QDomElement docElem = doc.documentElement();
    QDomNode root = docElem.firstChild();



//    qDebug("...MainWindow::addNodeToXMLWorkflow....root.childNodes().length()...len:|%d|",root.childNodes().length());

//    int nnodes = root.childNodes().length() - 4 ; // 4 -> Star,End,SafetNode, Other
//    if ( nnodes >= 6  ) {
//        SYE << tr("Step limit reached (6 steps)!");
//        return result;
//    }
    QDomNode n = root.firstChild();


    QDomElement mynewelement, beforeelement, afterelement;

    QString afternode;

    // Chequear si el paso ya existe

    while( !n.isNull() ) {
        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                qDebug("...*********************************************************MainWindow::addNodeToXMLWorkflow..id:|%s|",
                       qPrintable(myid));
                qDebug("...*********************************************************MainWindow::addNodeToXMLWorkflow..nodename:|%s|",
                       qPrintable(nodename));

                if (myid == nodename) {

                    qDebug("...por aqui...");
                    SYE << tr("Node name (%1)already exists. Choose other name, please.")
                           .arg(nodename);
                    return result;
                }


            }
            else {
                qDebug("...No atributo ID");

            }

        }

        n = n.nextSibling();

    }
    n = root.firstChild();

    while( !n.isNull() ) {

        if ( n.isElement() ) {
            QDomElement e = n.toElement(); // try to convert the node to an element.
            QDomNamedNodeMap attributeMap = e.attributes();
            QDomNode attribute = attributeMap.namedItem("id");
            if (!attribute.isNull()) {
                QString myid = attribute.nodeValue();
                qDebug("...*MainWindow::addNodeToXMLWorkflow..id:|%s|",
                       qPrintable(myid));

                if (myid == beforenode) {
                    beforeelement = e;
                    QDomElement myport = beforeelement.firstChildElement("port");
                    QDomElement myconn = myport.firstChildElement("connection");
                    afternode = myconn.attribute("source");
                }
                if (myid == afternode) {
                    afterelement = e;
                    break;
                }


            }
            else {
                qDebug("...No atributo ID");

            }

        }

        n = n.nextSibling();
    }

    qDebug("...afternode:|%s|",qPrintable(afternode));
    if (afternode.isEmpty()) {
        SYE << tr("Error en los enlaces del flujo de trabajo");
        qDebug("afternode is empty");
        return result;
    }
    QDomElement olde = beforeelement;

    // Connection After Node
    QDomElement myafterport, oldp;
    QDomElement myafterconn, oldc;
    myafterport = afterelement.firstChildElement("port");
    myafterconn = myafterport.firstChildElement("connection");
    QString myaftersource = myafterconn.attribute("source");
    QString myafteroptions = myafterconn.attribute("options");
    QString myafterquery = myafterconn.attribute("query");
    qDebug("................myaftersource:|%s|", qPrintable(myaftersource));
    qDebug("...............myafteroptions:|%s|", qPrintable(myafteroptions));



    mynewelement = beforeelement.cloneNode().toElement();
    QString oldnodename = mynewelement.attribute("id");
    mynewelement.setAttribute("id",nodename);
    mynewelement.setAttribute("title",nodetitle);
    if ( isparallel) {
        myafterport = mynewelement.firstChildElement("port");
        oldp = myafterport;
        myafterconn = myafterport.firstChildElement("connection");
        oldc = myafterconn;
        myafterconn.setAttribute("source",myaftersource);
        myafterconn.setAttribute("query",myafterquery);
        myafterconn.setAttribute("options",myafteroptions);
        myafterport.replaceChild(myafterconn,oldc);
        mynewelement.replaceChild(myafterport,oldp);
    }



    // Variable
    QDomElement myvariable = mynewelement.firstChildElement("variable");
    QDomElement oldvariable = myvariable;

    myvariable.setAttribute("id",QString("v%1")
                            .arg(nodename));
    if (!documentsource.isEmpty()) {
        myvariable.setAttribute("documentsource",
                                documentsource);
    }
    QString myrolfield = myvariable.attribute("rolfield");
    QString timestampfield = myvariable.attribute("timestampfield");

    myrolfield.replace(QString("'%1'").arg(oldnodename),QString("'%1'").arg(nodename));
    timestampfield.replace(QString("'%1'").arg(oldnodename),QString("'%1'").arg(nodename));
    myvariable.setAttribute("rolfield",myrolfield);
    myvariable.setAttribute("timestampfield",timestampfield);

    // reemplazando variable
    mynewelement.replaceChild(myvariable,oldvariable);


    root.insertAfter(mynewelement,beforeelement);

    // Connection
    QDomElement myport = beforeelement.firstChildElement("port");
    QDomElement oldport = myport;
    QDomElement myconn, oldconn;
    myconn = myport.firstChildElement("connection");
    oldconn = myconn;
    if (isparallel ) {
        myconn = myconn.cloneNode().toElement();
    }

    myconn.setAttribute("source",nodename);
    if (!query.isEmpty()) {
        myconn.setAttribute("query",query);
    }
    if (!options.isEmpty()) {
        myconn.setAttribute("options",options);
    }
    else {
        myconn.setAttribute("options",nodename);
    }
    if (isparallel) {
        myport.appendChild(myconn);
        myport.setAttribute("pattern", "or");
        beforeelement.replaceChild(myport, oldport);
    }
    else {
        myport.replaceChild(myconn,oldconn);
    }
    beforeelement.replaceChild(myport, oldport);

    // Autofilter
    if (isparallel) {
        QDomElement myautofilter = mynewelement.firstChildElement("autofilter");
        while(!myautofilter.isNull()) {

            QDomElement oldautofilter = myautofilter;
            myautofilter.setAttribute("source",myaftersource);
            mynewelement.replaceChild(myautofilter,oldautofilter);
            myautofilter = myautofilter.nextSiblingElement("autofilter");
        }

    }
    else {
        QDomElement myautofilter = mynewelement.firstChildElement("autofilter");
        while(!myautofilter.isNull()) {

            QDomElement oldautofilter = myautofilter;
            myautofilter.setAttribute("source",nodename);
            mynewelement.replaceChild(myautofilter,oldautofilter);
            myautofilter = myautofilter.nextSiblingElement("autofilter");
        }
    }

    root.replaceChild(beforeelement, olde);


    qDebug("...MainWindow::addNodeToXMLWorkflow.....(2)...");

    //QString mynamefile = SafetYAWL::getTempNameFiles(1).at(0);

    qDebug("...MainWindow::addNodeToXMLWorkflow.....(2)...:|%s|",
           qPrintable(fname));
    QFile myfile(fname);
    if ( !myfile.open(QIODevice::WriteOnly))  {
        SYE << tr("No es posible escribir el archivo \"%1\" con el nodo \"%2\" agregado")
               .arg(fname)
               .arg(nodename);
        return result;
    }

    result = fname;

    QTextStream out(&myfile);

    out << doc.toString();

    myfile.close();

    return result;
}






#ifdef SAFET_ESMTP // correo electronico
#include <auth-client.h>
#include <libesmtp.h>
#endif // ESTMP

#include <fcntl.h>
#include <signal.h>
#include <openssl/ssl.h>
#include <ctype.h>

#ifdef SAFET_ESMTP // correo electronico
        static void
        build_message(FILE *fp);

        int
        handle_invalid_peer_certificate(long vfy_result)
        {
            const char *k ="rare error";
            switch(vfy_result) {
            case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT:
                k="X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT"; break;
            case X509_V_ERR_UNABLE_TO_GET_CRL:
                k="X509_V_ERR_UNABLE_TO_GET_CRL"; break;
            case X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE:
                k="X509_V_ERR_UNABLE_TO_DECRYPT_CERT_SIGNATURE"; break;
            case X509_V_ERR_UNABLE_TO_DECRYPT_CRL_SIGNATURE:
                k="X509_V_ERR_UNABLE_TO_DECRYPT_CRL_SIGNATURE"; break;
            case X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY:
                k="X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY"; break;
            case X509_V_ERR_CERT_SIGNATURE_FAILURE:
                k="X509_V_ERR_CERT_SIGNATURE_FAILURE"; break;
            case X509_V_ERR_CRL_SIGNATURE_FAILURE:
                k="X509_V_ERR_CRL_SIGNATURE_FAILURE"; break;
            case X509_V_ERR_CERT_NOT_YET_VALID:
                k="X509_V_ERR_CERT_NOT_YET_VALID"; break;
            case X509_V_ERR_CERT_HAS_EXPIRED:
                k="X509_V_ERR_CERT_HAS_EXPIRED"; break;
            case X509_V_ERR_CRL_NOT_YET_VALID:
                k="X509_V_ERR_CRL_NOT_YET_VALID"; break;
            case X509_V_ERR_CRL_HAS_EXPIRED:
                k="X509_V_ERR_CRL_HAS_EXPIRED"; break;
            case X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD:
                k="X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD"; break;
            case X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD:
                k="X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD"; break;
            case X509_V_ERR_ERROR_IN_CRL_LAST_UPDATE_FIELD:
                k="X509_V_ERR_ERROR_IN_CRL_LAST_UPDATE_FIELD"; break;
            case X509_V_ERR_ERROR_IN_CRL_NEXT_UPDATE_FIELD:
                k="X509_V_ERR_ERROR_IN_CRL_NEXT_UPDATE_FIELD"; break;
            case X509_V_ERR_OUT_OF_MEM:
                k="X509_V_ERR_OUT_OF_MEM"; break;
            case X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT:
                k="X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT"; break;
            case X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN:
                k="X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN"; break;
            case X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY:
                k="X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY"; break;
            case X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE:
                k="X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE"; break;
            case X509_V_ERR_CERT_CHAIN_TOO_LONG:
                k="X509_V_ERR_CERT_CHAIN_TOO_LONG"; break;
            case X509_V_ERR_CERT_REVOKED:
                k="X509_V_ERR_CERT_REVOKED"; break;
            case X509_V_ERR_INVALID_CA:
                k="X509_V_ERR_INVALID_CA"; break;
            case X509_V_ERR_PATH_LENGTH_EXCEEDED:
                k="X509_V_ERR_PATH_LENGTH_EXCEEDED"; break;
            case X509_V_ERR_INVALID_PURPOSE:
                k="X509_V_ERR_INVALID_PURPOSE"; break;
            case X509_V_ERR_CERT_UNTRUSTED:
                k="X509_V_ERR_CERT_UNTRUSTED"; break;
            case X509_V_ERR_CERT_REJECTED:
                k="X509_V_ERR_CERT_REJECTED"; break;
            }
            printf("SMTP_EV_INVALID_PEER_CERTIFICATE: %ld: %s\n", vfy_result, k);
            return 1; /* Accept the problem */
        }


        void event_cb (smtp_session_t session, int event_no, void *arg,...)
        {
            va_list alist;
            int *ok;

            va_start(alist, arg);
            switch(event_no) {
            case SMTP_EV_CONNECT:
            case SMTP_EV_MAILSTATUS:
            case SMTP_EV_RCPTSTATUS:
            case SMTP_EV_MESSAGEDATA:
            case SMTP_EV_MESSAGESENT:
            case SMTP_EV_DISCONNECT: break;
            case SMTP_EV_WEAK_CIPHER: {
                int bits;
                bits = va_arg(alist, long); ok = va_arg(alist, int*);
                printf("SMTP_EV_WEAK_CIPHER, bits=%d - accepted.\n", bits);
                *ok = 1; break;
            }
            case SMTP_EV_STARTTLS_OK:
                puts("SMTP_EV_STARTTLS_OK - TLS started here."); break;
            case SMTP_EV_INVALID_PEER_CERTIFICATE: {
                long vfy_result;
                vfy_result = va_arg(alist, long); ok = va_arg(alist, int*);
                *ok = handle_invalid_peer_certificate(vfy_result);
                break;
            }
            case SMTP_EV_NO_PEER_CERTIFICATE: {
                ok = va_arg(alist, int*);
                puts("SMTP_EV_NO_PEER_CERTIFICATE - accepted.");
                *ok = 1; break;
            }
            case SMTP_EV_WRONG_PEER_CERTIFICATE: {
                ok = va_arg(alist, int*);
                puts("SMTP_EV_WRONG_PEER_CERTIFICATE - accepted.");
                *ok = 1; break;
            }
            case SMTP_EV_NO_CLIENT_CERTIFICATE: {
                ok = va_arg(alist, int*);
                puts("SMTP_EV_NO_CLIENT_CERTIFICATE - accepted.");
                *ok = 1; break;
            }
            default:
                printf("Got event: %d - ignored.\n", event_no);
            }
            va_end(alist);
        }



        /* Callback to prnt the recipient status */
        void
        print_recipient_status (smtp_recipient_t recipient,
                                const char *mailbox, void *arg)
        {
            const smtp_status_t *status;

            status = smtp_recipient_status (recipient);
            printf ("%s: %d %s", mailbox, status->code, status->text);
        }

        static int
        _mutt_libesmtp_auth_interact (auth_client_request_t request,
                                      char **result, int fields, void *arg)
        {
            int i;

            for (i = 0; i < fields; i++) {
                if (request[i].flags & AUTH_USER) {
                    result[i] = "vbravo";
                }
                else if (request[i].flags & AUTH_PASS) {
                    result[i] = "4cajicA!";
                }
            }

            return 1;
        }
#endif // ESMTP

        void SAFETsendMail(const char *messageContents)
        {
#ifdef SAFET_ESMTP // correo electronico
            smtp_session_t session;
            smtp_message_t message;
            smtp_recipient_t recipient;
            const smtp_status_t *status;
            struct sigaction sa;
            char *host = "mail.cenditel.gob.ve:25";
            char *from = "vbravo@cenditel.gob.ve";
            char *subject = "Prueba con esmtp";
            const char *recipient_address = "victorrbravo@gmail.com";
            auth_context_t authctx = NULL;
            char tempFileName[1000];
            int tempFd;
            FILE *fp;
            enum notify_flags notify = (notify_flags) (Notify_SUCCESS | Notify_FAILURE);

            auth_client_init();
            session = smtp_create_session();
            message = smtp_add_message(session);

            /* Ignore sigpipe */
            sa.sa_handler = SIG_IGN;
            sigemptyset(&sa.sa_mask);
            sa.sa_flags = 0;
            sigaction(SIGPIPE, &sa, NULL);

            /* Set the Subject: header.  For no reason, we want the supplied subject
                 to override any subject line in the message headers. */
            //        if (subject != NULL) {
            //            smtp_set_header(message, "Subject", "prueba");
            //                smtp_set_header_option(message, "Subject", Hdr_OVERRIDE, 1);
            //        }

            smtp_starttls_enable(session,Starttls_ENABLED);
            smtp_set_server(session, host);
            authctx = auth_create_context ();
            auth_set_mechanism_flags (authctx, AUTH_PLUGIN_PLAIN, 0);
            auth_set_interact_cb (authctx, _mutt_libesmtp_auth_interact, NULL);
            smtp_auth_set_context (session, authctx);





            smtp_set_eventcb(session, event_cb, NULL);

            /* Set the reverse path for the mail envelope.  (NULL is ok)
                 */

            smtp_set_header(message, "From", "vbravo",from);
            smtp_set_header(message, "To", "Victor Bravo","victorrbravo@gmail.com");
            //        smtp_set_header(message, "Subject", "Prueba de mensaje");
            //        smtp_set_header(message, "Message-Id", NULL);
            smtp_set_reverse_path(message, from);


            /* Prepare message */
            memset(tempFileName, 0, sizeof(tempFileName));
            sprintf(tempFileName, "/tmp/messageXXXXXX");
            tempFd = mkstemp(tempFileName);
            fp = fdopen(tempFd, "w");
            build_message(fp);
            //        fprintf(fp, "%s", messageContents);
            fclose(fp);
            fp = fopen(tempFileName, "r");
            smtp_set_message_fp(message, fp);





            recipient = smtp_add_recipient(message, recipient_address);

            smtp_dsn_set_notify (recipient, notify);

            /* Initiate a connection to the SMTP server and transfer the
                 message. */
            if (!smtp_start_session(session)) {
                char buf[128];

                fprintf(stderr, "SMTP server problem %s\n", smtp_strerror(smtp_errno(),
                                                                          buf, sizeof buf));
            } else {
                /* Report on the success or otherwise of the mail transfer.
                         */

                status = smtp_message_transfer_status(message);
                printf("%d %s", status->code, (status->text != NULL) ? status->text
                                                                     : "\n");
                smtp_enumerate_recipients(message, print_recipient_status, NULL);
            }

            /* Free resources consumed by the program.
                 */
            smtp_destroy_session(session);
            fclose(fp);
            auth_client_exit();
#endif
        }




#ifdef SAFET_ESMTP // correo electronico
        static void
        build_message(FILE *fp)
        {

            fprintf(fp, "Return-Path: <%s>\r\n", "vbravo@cenditel.gob.ve");
            fprintf(fp, "Subject: %s\r\n", "Correo de activacion Gestion");
            //  fprintf(fp, "From: %s %s\r\n", "vbravo","vbravo@cenditel.gob.ve");
            //  fprintf(fp, "To: %s %s\r\n", "Victor Bravo","victorrbravo@gmail.com");
            fprintf(fp, "MIME-Version: 1.0\r\n");
            fprintf(fp, "Content-Type: text/plain;\r\n");
            fprintf(fp, "  charset=iso-8859-1\r\n");
            fprintf(fp, "Content-Transfer-Encoding: 7bit\r\n");
            //  fprintf(fp, "X-send-pr-version: Evolution \r\n");
            //  fprintf(fp, "X-GNATS-Notify: \r\n");
            //  fprintf(fp, "\r\n\r\n");
            //  fprintf(fp, ">Submitter-Id:	%s \r\n", "1");
            //  fprintf(fp, ">Originator:	%s \r\n", "vbravo@cenditel.gob.ve");
            //  fprintf(fp, ">Organization:	%s \r\n", "CENDITEL");
            //  fprintf(fp, ">Confidential:	no \r\n");
            //  fprintf(fp, ">Synopsis:	%s \r\n", "Correo de activacion");
            //  fprintf(fp, ">Severity:	%s \r\n", "prueba6");
            //  fprintf(fp, ">Priority:	%s \r\n", "(Highest)");
            //  fprintf(fp, ">Category:	%s \r\n", "prueba8");
            //  fprintf(fp, ">Class:		%s \r\n", "prueba9");
            //  fprintf(fp, ">Release:	%s \r\n", "prueba10");

            fprintf(fp, ">Environment:\r\n");
            fprintf(fp, "\r\n\r\n");
            fprintf(fp, "%s\r\n", "prueba11");
            fprintf(fp, "\r\n\r\n");

            fprintf(fp, ">Description:\r\n");
            fprintf(fp, "\r\n\r\n");
            fprintf(fp, "%s\r\n", "Mensaje de activacion");
            fprintf(fp, "\r\n\r\n");

            fprintf(fp, ">How-To-Repeat:\r\n");
            fprintf(fp, "\r\n\r\n");
            fprintf(fp, "%s\r\n", "prueba13");
            fprintf(fp, "\r\n\r\n");

            fprintf(fp, ">Fix:\r\n");
            fprintf(fp, "\r\n\r\n");
            fprintf(fp, "%s\r\n", "prueba14");
            fprintf(fp, "\r\n\r\n");

        }
#endif // ESMTP


