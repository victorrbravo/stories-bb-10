/****************************************************************************
**
** Copyright (C) 2006-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/
/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 V�ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H
//#include <QWebView>
//#include <QMainWindow>
#include <QTextStream>
#include <QSettings>
//#include <QGraphicsSvgItem>
#include <QMutex>
#include <QWaitCondition>
//#include <QApplication>
//#include <QTcpServer>
//#include <QStackedWidget>
#include <QDeclarativeView>

#include "../src/SafetYAWL.h"

#include "graphicsworkflow.h"
#include "graphicssvgitemlive.h"
#include "threadconsole.h"
#include "threadinfoserver.h"
//#include "SafetConsoleApp.h"
//#include "principalframe.h"
#include "SafetTextParser.h"
//#include "DigiDoc.h"
#include "SafetDocument.h"
//#include "QProgressIndicator.h"
//#include "inflowthread.h"
#include "threadconsole.h"

class QAbstractItemModel;
class QComboBox;
class QCmdCompleter;
class QLabel;
class QLineEdit;
class QProgressBar;
class QDomDocument;
class DomModel;
class ThreadConsole;
//class GraphicsWorkflow;
class GraphicsSvgItemLive;
class InflowThread;
//class ThreadConsole;


const QString SAFETCONSOLEFILE = "defconsole.xml";
const QString  SAFETCONFIGFILE = "defconfigure.xml";
const QString  SAFETUSERSFILE = "defusers.xml";
const QString  SAFETMANAGEMENTSIGNFILE = "defmanagementsignfile.xml";
const uint SAFETREACHLIMIT = 16;


namespace Safet {
    const QString LOADFLOWFILE_NAMEFIELD = QObject::tr("Cargar_archivo_flujo");
const QString AUTOFILTERFLOW_NAMEFIELD = QObject::tr("Autofiltro");
const QString RECURSIVEFILTERFLOW_NAMEFIELD = QObject::tr("Filtro_recursivo");
const QString GRAPHSFILENAME = "graphs";
const QString PLANNEDGRAPHSFILENAME = "plannedgraphs";
const QString COLONMARK = "##SAFETCOLON##";
const QString COMMA = "##SAFETCOMMA##";
const QString SEPARATOR = "**SAFETSEPARATOR**";

const QString TEMPLATETULUM = ""
        "<?xml version='1.0' encoding='UTF-8'?>\n"
        "<!DOCTYPE yawl SYSTEM 'file:///home/user/.safet/dtd/yawlworkflow.dtd'>\n"
        "<yawl version=\"0.01\">\n"
        " <workflow desc=\"Trayectos\" id=\"Trayectos\">\n"
        "  <token keysource=\"trayecto\" key=\"trayecto.nombre\"/>\n"
        "  <condition type=\"start\" id=\"start\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"select status from trayecto\" options=\"_Livenote\" tokenlink=\"\" source=\"_Livenote\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        "  <task title=\"_Livenote\" id=\"_Livenote\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Muralla\" tokenlink=\"\" source=\"Muralla\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"_LivenoteRecords\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Muralla\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Primera_Casa\" tokenlink=\"\" source=\"Primera_Casa\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vMuralla\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Muralla') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Muralla') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Primera_Casa\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Casa_Real\" tokenlink=\"\" source=\"Casa_Real\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vPrimera_Casa\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Primera_Casa') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Primera_Casa') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Casa_Real\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Faro\" tokenlink=\"\" source=\"Faro\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vCasa_Real\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Casa_Real') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Casa_Real') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Faro\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Playa\" tokenlink=\"\" source=\"Playa\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vFaro\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Faro') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Faro') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Playa\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"\" tokenlink=\"\" source=\"end\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vPlaya\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Playa') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Playa') as tiempo\"/>\n"
        "  </task>\n"
        "  <condition id=\"end\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection source=\"\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        " </workflow>\n"
        " <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\"/>\n"
        "</yawl>\n";

const QString TEMPLATEPIZZA = ""
        "<?xml version='1.0' encoding='UTF-8'?>\n"
        "<!DOCTYPE yawl SYSTEM 'file:///home/user/.safet/dtd/yawlworkflow.dtd'>\n"
        "<yawl version=\"0.01\">\n"
        " <workflow desc=\"Trayectos\" id=\"Trayectos\">\n"
        "  <token keysource=\"trayecto\" key=\"trayecto.nombre\"/>\n"
        "  <condition type=\"start\" id=\"start\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"select status from trayecto\" options=\"_Livenote\" tokenlink=\"\" source=\"_Livenote\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        "  <task title=\"_Livenote\" id=\"_Livenote\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Ingredientes\" tokenlink=\"\" source=\"Ingredientes\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"_LivenoteRecords\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Ingredientes\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Revolver\" tokenlink=\"\" source=\"Revolver\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vIngredientes\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Ingredientes') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Ingredientes') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Revolver\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Reposar\" tokenlink=\"\" source=\"Reposar\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vRevolver\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Revolver') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Revolver') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Reposar\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Amasar\" tokenlink=\"\" source=\"Amasar\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vReposar\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Reposar') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Reposar') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Amasar\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Tapar\" tokenlink=\"\" source=\"Tapar\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vAmasar\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Amasar') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Amasar') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Tapar\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Aceite\" tokenlink=\"\" source=\"Aceite\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vTapar\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Tapar') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Tapar') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Aceite\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Salsa\" tokenlink=\"\" source=\"Salsa\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vAceite\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Aceite') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Aceite') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Salsa\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Horno\" tokenlink=\"\" source=\"Horno\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vSalsa\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Salsa') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Salsa') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Horno\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Compartir\" tokenlink=\"\" source=\"Compartir\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vHorno\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Horno') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Horno') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Compartir\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"\" tokenlink=\"\" source=\"end\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vCompartir\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Compartir') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Compartir') as tiempo\"/>\n"
        "  </task>\n"
        "  <condition id=\"end\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection source=\"\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        " </workflow>\n"
        " <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\"/>\n"
        "</yawl>\n";


const QString TEMPLATETOLEDO = ""

        "<?xml version='1.0' encoding='UTF-8'?>\n"
        "<!DOCTYPE yawl SYSTEM 'file:///home/user/.safet/dtd/yawlworkflow.dtd'>\n"
        "<yawl version=\"0.01\">\n"
        " <workflow desc=\"Trayectos\" id=\"Trayectos\">\n"
        "  <token keysource=\"trayecto\" key=\"trayecto.nombre\"/>\n"
        "  <condition type=\"start\" id=\"start\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"select status from trayecto\" options=\"_Livenote\" tokenlink=\"\" source=\"_Livenote\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        "  <task title=\"_Livenote\" id=\"_Livenote\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Renfe\" tokenlink=\"\" source=\"Renfe\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"_LivenoteRecords\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Renfe\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Castillo\" tokenlink=\"\" source=\"Castillo\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vRenfe\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Renfe') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Renfe') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Castillo\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Tren\" tokenlink=\"\" source=\"Tren\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vCastillo\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Castillo') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Castillo') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Tren\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Vuelta\" tokenlink=\"\" source=\"Vuelta\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vTren\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Tren') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Tren') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Vuelta\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Catedral\" tokenlink=\"\" source=\"Catedral\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vVuelta\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Vuelta') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Vuelta') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Catedral\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Plaza\" tokenlink=\"\" source=\"Plaza\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vCatedral\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Catedral') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Catedral') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Plaza\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"\" tokenlink=\"\" source=\"end\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vPlaza\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Plaza') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Plaza') as tiempo\"/>\n"
        "  </task>\n"
        "  <condition id=\"end\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection source=\"\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        " </workflow>\n"
        " <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\"/>\n"
        "</yawl>\n";


const QString TEMPLATEGET_LOAN = ""
        "<?xml version='1.0' encoding='UTF-8'?>\n"
        "<!DOCTYPE yawl SYSTEM 'file:///home/user/.safet/dtd/yawlworkflow.dtd'>\n"
        "<yawl version=\"0.01\">\n"
        " <workflow desc=\"Trayectos\" id=\"Trayectos\">\n"
        "  <token keysource=\"trayecto\" key=\"trayecto.nombre\"/>\n"
        "  <condition type=\"start\" id=\"start\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"select status from trayecto\" options=\"_Livenote\" tokenlink=\"\" source=\"_Livenote\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        "  <task title=\"_Livenote\" id=\"_Livenote\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Learn_about\" tokenlink=\"\" source=\"Learn_about\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"_LivenoteRecords\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Learn_about\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Financial\" tokenlink=\"\" source=\"Financial\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vLearn_about\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Learn_about') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Learn_about') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Financial\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Needs\" tokenlink=\"\" source=\"Needs\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vFinancial\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Financial') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Financial') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Needs\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Prepare\" tokenlink=\"\" source=\"Prepare\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vNeeds\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Needs') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Needs') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Prepare\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"Planning\" tokenlink=\"\" source=\"Planning\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vPrepare\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Prepare') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Prepare') as tiempo\"/>\n"
        "  </task>\n"
        "  <task title=\"\" id=\"Planning\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection query=\"true\" options=\"\" tokenlink=\"\" source=\"end\"/>\n"
        "   </port>\n"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vPlanning\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Planning') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Planning') as tiempo\"/>\n"
        "  </task>\n"
        "  <condition id=\"end\">\n"
        "   <port side=\"forward\" type=\"split\">\n"
        "    <connection source=\"\"/>\n"
        "   </port>\n"
        "  </condition>\n"
        " </workflow>\n"
        " <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\"/>\n"
        "</yawl>\n";




const QString TEMPLATEFLOWSUGARLOAF = ""
        "<?xml version='1.0' encoding='UTF-8'?>"
        "<!DOCTYPE yawl SYSTEM 'file:///home/user/.safet/dtd/yawlworkflow.dtd'>"
        "<yawl version=\"0.01\">"
        " <workflow desc=\"Trayectos\" id=\"Trayectos\">"
        "  <token keysource=\"trayecto\" key=\"trayecto.nombre\"/>"
        "  <condition type=\"start\" id=\"start\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"select status from trayecto\" options=\"_Livenote\" tokenlink=\"\" source=\"_Livenote\"/>"
        "   </port>"
        "  </condition>"
        "  <task title=\"_Livenote\" id=\"_Livenote\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Bondhino\" tokenlink=\"\" source=\"Bondhino\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"_LivenoteRecords\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Bondhino\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Vermelha_beach\" tokenlink=\"\" source=\"Vermelha_beach\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vBondhino\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Bondhino') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Bondhino') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Vermelha_beach\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Urca\" tokenlink=\"\" source=\"Urca\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vVermelha_beach\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Vermelha_beach') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Vermelha_beach') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Urca\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Botafogo\" tokenlink=\"\" source=\"Botafogo\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vUrca\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Urca') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Urca') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Botafogo\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Sugarloaf\" tokenlink=\"\" source=\"Sugarloaf\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vBotafogo\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Botafogo') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Botafogo') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Sugarloaf\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"\" tokenlink=\"\" source=\"end\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vSugarloaf\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Sugarloaf') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Sugarloaf') as tiempo\"/>"
        "  </task>"
        "  <condition id=\"end\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection source=\"\"/>"
        "   </port>"
        "  </condition>"
        " </workflow>"
        " <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\"/>"
        "</yawl>";

const QString TEMPLATEFLOWTIRAMISU = ""
        "<?xml version='1.0' encoding='UTF-8'?>"
        "<!DOCTYPE yawl SYSTEM 'file:///home/user/.safet/dtd/yawlworkflow.dtd'>"
        "<yawl version=\"0.01\">"
        " <workflow desc=\"Trayectos\" id=\"Trayectos\">"
        "  <token keysource=\"trayecto\" key=\"trayecto.nombre\"/>"
        "  <condition type=\"start\" id=\"start\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"select status from trayecto\" options=\"_Livenote\" tokenlink=\"\" source=\"_Livenote\"/>"
        "   </port>"
        "  </condition>"
        "  <task title=\"_Livenote\" id=\"_Livenote\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Coffee\" tokenlink=\"\" source=\"Coffee\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"_LivenoteRecords\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Coffee\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Egg_whites\" tokenlink=\"\" source=\"Egg_whites\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vCoffee\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Coffee') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Coffee') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Egg_whites\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Cream\" tokenlink=\"\" source=\"Cream\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vEgg_whites\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Egg_whites') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Egg_whites') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Cream\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Biscuits\" tokenlink=\"\" source=\"Biscuits\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vCream\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Cream') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Cream') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Biscuits\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Spread\" tokenlink=\"\" source=\"Spread\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vBiscuits\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Biscuits') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Biscuits') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Spread\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"Refrigerator\" tokenlink=\"\" source=\"Refrigerator\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vSpread\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Spread') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Spread') as tiempo\"/>"
        "  </task>"
        "  <task title=\"\" id=\"Refrigerator\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection query=\"true\" options=\"\" tokenlink=\"\" source=\"end\"/>"
        "   </port>"
        "   <variable config=\"1\" documentsource=\"select nombre from trayecto\" type=\"sql\" tokenlink=\"\" id=\"vRefrigerator\" rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Refrigerator') as rol\" scope=\"task\" timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Refrigerator') as tiempo\"/>"
        "  </task>"
        "  <condition id=\"end\">"
        "   <port side=\"forward\" type=\"split\">"
        "    <connection source=\"\"/>"
        "   </port>"
        "  </condition>"
        " </workflow>"
        " <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\"/>"
        "</yawl>";

const QString TEMPLATEFLOWFILE = ""
        "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        "<!DOCTYPE yawl SYSTEM \"file:///home/user/.safet/dtd/yawlworkflow.dtd\">\n"
        "<yawl  version=\"0.01\">\n"
        "<workflow id=\"Trayectos\" desc=\"Trayectos\">\n"
        "        <token key=\"trayecto.nombre\"  keysource=\"trayecto\">\n"
                "</token>\n"
        "<condition id=\"start\" type=\"start\">\n"
        "                <port side=\"forward\" type=\"split\" >\n"
        "                        <connection   source=\"_Livenote\" query=\"select status from trayecto\"  options=\"_Livenote\" tokenlink=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "        </condition>\n"
        "        <task id=\"_Livenote\"  title=\"_Livenote\" >\n"
        "                <port side=\"forward\" type=\"split\" >\n"
        "                        <connection source=\"Place1\" query=\"select status from trayecto\"  options=\"Place1\"\n"
        "tokenlink=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "                <variable id=\"_LivenoteRecords\" scope=\"task\"  type=\"sql\" config=\"1\"   tokenlink=\"\" documentsource=\"select nombre from trayecto\" \n"
        "rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as rol\"\n"
        " timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = '_Livenote') as tiempo\">\n"
        "                </variable>      		\n"
        "        </task>\n"
        "        <task id=\"Place1\"  title=\"Place1\" >\n"
        "                <port side=\"forward\" type=\"split\" >\n"
        "                        <connection source=\"end\" query=\"true\"  options=\"\"\n"
        "tokenlink=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "                <variable id=\"Place1Records\" scope=\"task\"  type=\"sql\" config=\"1\"   tokenlink=\"\" documentsource=\"select nombre from trayecto\" \n"
        "rolfield=\"(select ifnull(picture,'')  || '**SAFETSEPARATOR**' || ifnull(textinfo,'') from trayecto_registro where name_live_story=trayecto.nombre and status = 'Place1') as rol\"\n"
        " timestampfield=\"(select fechaaccion from trayecto_registro where name_live_story=trayecto.nombre and status = 'Place1') as tiempo\">\n"
        "                </variable>      		\n"
        "        </task>\n"
        "<condition id=\"end\">\n"
        "                <port side=\"forward\" type=\"split\">\n"
        "                        <connection source=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "        </condition>\n"
        "</workflow>\n"
        "        <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\">\n"
        "        </configuration>\n"
        "</yawl>\n";


const QString TEMPLATEQUERYFLOW = ""
        "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        "<!DOCTYPE yawl SYSTEM \"file:///home/user/.safet/dtd/yawlworkflow.dtd\">\n"
        "<yawl  version=\"0.01\">\n"
        "<workflow id=\"Consulta_Trayectos\" desc=\"Consulta_Trayectos\">\n"
        "        <token key=\"trayecto_consulta.id\"  keysource=\"trayecto_consulta\">\n"
                "</token>\n"
        "<condition id=\"start\" type=\"start\">\n"
        "                <port side=\"forward\" type=\"split\" >\n"
        "                        <connection   source=\"Consultas\" query=\"select id from trayecto_consulta\"  options=\"&gt;=0\" tokenlink=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "        </condition>\n"
        "        <task id=\"Consultas\"  title=\"Consultas\" >\n"
        "                <port side=\"forward\" type=\"split\" >\n"
        "                        <connection source=\"end\" query=\"true\"  options=\"\"\n"
        "tokenlink=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "                <variable id=\"vConsultas\" scope=\"task\"  type=\"sql\" config=\"1\"   tokenlink=\"\" documentsource=\"select title,brand,textquery,icon from trayecto_consulta\" \n"
        "rolfield=\"\"\n"
        " timestampfield=\"\">\n"
        "                </variable>      		\n"
        "        </task>\n"
        "<condition id=\"end\">\n"
        "                <port side=\"forward\" type=\"split\">\n"
        "                        <connection source=\"\">\n"
        "                        </connection>\n"
        "                </port>\n"
        "        </condition>\n"
        "</workflow>\n"
        "        <configuration id=\"1\" connectstring=\"psql:seguridad.cenditel.gob.ve:ticket:vbravo:vbravo_\">\n"
        "        </configuration>\n"
        "</yawl>\n";


const QString TEMPLATEINPUTFILE = ""
        "<!DOCTYPE operations SYSTEM \"file:///home/user/.safet/dtd/yawlinput.dtd\">\n"
        "<operations suffix=\":\" commandname=\"operacion\">\n"
        "<operation  name=\"Routes\"  desc=\"Agregar,Modificar,Eliminar,Listar\" icon=\"\">\n"
        "</operation>\n"
        "<operation  name=\"New_step\" desc=\"Pasa de estado una determinada actividad \" icon=\"qrc:/addstep.png\"> \n"
        "	<command id =\"1\" type=\"actualizar\" table=\"trayecto\">\n"
        "		<fields>\n"
        "			<field type=\"combolisttable\" options=\"nombre:trayecto:where flujo='/home/user/.safet/flowfiles/flujogeneraltrayectos.xml'::nombre\" mandatory=\"yes\"  order=\"desc\" \n"
        " title=\"id\" primarykey=\"yes\"  changekey=\"yes\">\n"
        "				nombre\n"
        "			</field>	\n"
        "		 <field type=\"comboflow\" mandatory=\"yes\" options=\"next\"\n"
        "                                  path=\"/home/user/.safet/flowfiles/flujogeneraltrayectos.xml\" title=\"Next_status\" changefor=\"id\" >\n"
        "                                status\n"
        "                        </field>	\n"
        "		<field type=\"filename\" mandatory=\"no\" title=\"Picture\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.png);;Todos (*)\" \n"
        " options=\"load::pictures\"  >\n"
        "			lastpicture\n"
        "		</field>\n"
        "                <field type=\"stringbox\" mandatory=\"no\" primarykey=\"no\" title=\"Info\"   >\n"
        "			 lasttextinfo \n"
        "		</field>\n"
        "    <field changefor=\"status\" title=\"Date\" type=\"datetime\" mandatory=\"no\">\n"
        "                                fechacreacion\n"
        "                        </field>\n"
        "		</fields>\n"
        "	</command>\n"
        "     <command id =\"1\" type=\"agregar\" table=\"trayecto_registro\" >\n"
        "	        <fields>\n"
        "    <field title=\"Date\" format=\"time_t\" type=\"datetime\" mandatory=\"no\">\n"
        "                        fechaaccion\n"
        "                </field>\n"
        "	                <field type=\"string\" literal=\"_USERNAME\" mandatory=\"yes\" >\n"
        "	                        propietario\n"
        "	                </field>\n"
        "	                <field type=\"string\"  mandatory=\"yes\" title=\"id\">\n"
        "	                        name_live_story\n"
        "	                </field>\n"
        "		        <field type=\"string\" mandatory=\"no\" primarykey=\"no\" title=\"Next_status\" >\n"
        "			   status\n"
        "		        </field>\n"
        "		        <field type=\"string\" mandatory=\"no\" primarykey=\"no\" title=\"Picture\" >\n"
        "			   picture\n"
        "		        </field>\n"
        "		        <field type=\"string\" mandatory=\"no\" primarykey=\"no\" title=\"Info\" >\n"
        "			   textinfo\n"
        "		        </field>\n"
        "		        <field type=\"string\" mandatory=\"no\"  title=\"story_name\" literal=\"ninguno\" >\n"
        "			   story_name\n"
        "		        </field>\n"
        "	        </fields>\n"
        "	</command> \n"
        "</operation>\n"
        "</operations>\n"
        "";
const int NOKEY = 10;

const QString SAFETCONF = ""
        "*\n"
        "* SAFET Sistema Automatizado para la Firma Electrnica y Estampado de Tiempo - Componente\n"
        "* XPCOM de Mozilla Firefox para aplicar firma electronica\n"
        "*\n"
        "* Copyright (C) 2008 Vctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo\n"
        "* (aaraujo@cenditel.gob.ve)\n"
        "*\n"
        "* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigacin en Tecnologas Libres\n"
        "*\n"
        "* Este programa es software libre; Usted puede usarlo bajo los trminos de la licencia de\n"
        "* software GPL versin 2.0 de la Free Software Foundation.\n"
        "*\n"
        "* Este programa se distribuye con la esperanza de que sea til, pero SI NINGUNA GARANTA;\n"
        "* tampoco las implcitas garantas de MERCANTILIDAD o ADECUACIN A UN PROPSITO PARTICULAR.\n"
        "* Consulte la licencia GPL para ms detalles. Usted debe recibir una copia de la GPL junto\n"
        "* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,\n"
        "* 5 Piso, Boston, MA 02110-1301, USA.\n"
        "*\n"
        "*/\n"

        "# Archivo de configuracion de la libreria libsafet\n"
        "# ================================================\n"

        "# Variables de presentacin del flujo\n"
        "# Seccion para la interfaz Web\n"

        "[Workflow]\n"

        "# Establece el color por defecto\n"
        "defaultcolor = white\n"

        "# Establece el color activo\n"
        "activecolor = white\n"

        "# Establece el color pasivo\n"
        "traccolor =  white\n"


        "# Variables para establecer imagenes de operadores de fujo de trabajo\n"

        "[Operators]\n"

        "# Operador split.xor\n"
        "split.xor = imgs/xor.png\n"

        "# Operador split.and\n"
        "split.and = imgs/and.png\n"

        "# Operador split.or\n"
        "split.or = imgs/or.png\n"

        "# Operador join.xor\n"
        "join.xor = imgs/join-xor.png\n"

        "# Operador join.and\n"
        "join.and = imgs/join-and.png\n"

        "# Operador join.or\n"
        "join.or = imgs/join-or.png\n"

        "# Ningun operador\n"
        "none =  imgs/none.png\n"


        "# Informacion sobre el registro de eventos.\n"
        "# Por defecto Encendidas (on) todas las categorias para el registro (log)\n"
        "[Log]\n"
        "# Establece la ruta absoluta donde reside el archivo de registro\n"
        "log.filepath = /home/user/.safet/log\n"

        "# Habilita la opcion de registro de mensajes de depuracion\n"
        "log.debug = on\n\n"

        "# Habilita la opcion de registro de de mensajes de acciones\n"
        "log.action = off\n\n"

        "# Habilita la opcion de registro de de mensajes de advertencia\n"
        "log.warning = on\n\n"

        "# Habilita la opcion de registro de de mensajes de error\n"
        "log.error = on\n\n"

        "# Salida del registro de ejecucion, las opciones son: default (file), file y screen\n"
        "log.output = file \n"

        "# Informacion sobre repositorio de documentos asociados a flujos de trabajos\n"
        "[XmlRepository]\n"

        "# Establece si el repositorio XML es remoto\n"
        "xmlrepository.remote = off\n"

        "# En caso de utilizar un repositorio de documentos XML remoto se debe \n"
        "# especificar la informacion de acceso al mismo a traves de un servicio web\n"
        "xmlrepository.remote.ip = 150.187.36.6\n"
        "xmlrepository.remote.urlwebservice = http://150.187.36.6/cgi-bin/safet\n"

        "# Establece el tipo de repositorio de documentos XML. Valores posibles:\n"
        "# dir: para repositorio basado en un directorio local\n"
        "# dbxml: para repositorio DBXML\n"
        "xmlrepository.type = dir\n"

        "# Establece la ruta absoluta al repositorio XML\n"

        "xmlrepository.path = /home/user/.safet/xmlrepository\n"

        "# Establece el nombre del repositorio XML\n"
        "xmlrepository.name = container1\n"


        "#Opciones para la entrada/modificacion de datos\n"

        "[Input]\n"

        "# Establece la ruta absoluta para la entrada/modificacion de datos\n"
        "input.path = /home/user/.safet/input\n"
        "#input.file = deflista.xml\n"
        "input.file = deftrac.xml\n"

        "# Opciones generales referentes al funcionamiento del tipo de aplicacion : Consola/Web/Grfica\n"
        "[System]\n"
        "system.evalexit = on\n"

        "# Informacin referente a la base de datos o repositorio \n"
        "# para el acceso de la libreria\n"


        "[Widgets]\n"

//         "widgets.getfilewidget.* = /home/user/.safet/flowfiles/safetQueries.qml\n"
         "widgets.getfilewidget.* = \n"


        "widgets.texteditwidget.wiki.leftbold = '''\n"
        "widgets.texteditwidget.wiki.rightbold = '''\n"
        "widgets.texteditwidget.wiki.leftitalic = ''\n"
        "widgets.texteditwidget.wiki.rightitalic = ''\n"
        "widgets.texteditwidget.wiki.leftunderline = __\n"
        "widgets.texteditwidget.wiki.rightunderline = __\n"

        "widgets.combowidget.shrinklen = 88\n"


        "widgets.arguments.* = por_hito\n"
        "widgets.arguments.* = por_propietario\n"
        "widgets.arguments.* = por_version\n"


        "[StoriesOptions]\n"

         "storiesoptions.usesensor = on\n"

         "storiesoptions.showinfodialog = on\n"

         "storiesoptions.typeinfographic = on\n"

        " storiesoptions.usecamera = on\n"


        "[GeneralOptions]\n"

        "generaloptions.consoleactions.* = safet\n"


        "# Titulo para el gr�fico de flujo de trabajo\n"

        "generaloptions.currentflowtitle = Gr�fico SAFET\n"

        "# Opciones On (Incluir en el grfico, No incluir), Off si no se quiere incluir\n"

        "generaloptions.currentflowtitle.font = Dejavu Sans\n"

        "# Tamao de la fuente para el texto de informacin en cada flujo de trabajo\n"
        "generaloptions.currentflowtitle.size = 18\n"

        "# Separacin de la fuente para el texto de informacin en cada flujo de trabajo\n"
        "generaloptions.currentflowtitle.separation = 10\n"



        "generaloptions.currentflowtitle.include = on\n"
        "# Ver http://en.wikipedia.org/wiki/List_of_tz_zones_by_name\n"
        "generaloptions.timezone = America/Caracas\n"

        "generaloptions.extrainfo.showonly = off\n"
        "generaloptions.extrainfo.showhumanize = on\n"

        "# Como se muestra la cuenta en la pagina Web campos realname es el Nombre del usuario, accountname el nombre de la cuenta\n"

        "generaloptions.account.format = %realname [<b>%accountname</b>/%rolname]\n"


        "generaloptions.savedqueries = none\n"


        "# Se colocan las opciones generales del sistema SAFET (Lista de acciones guardadas, entre otros)\n"

        "[Database]\n"

        "# Informacion de configuracion de fuentes de datos.\n"
        "# Una fuente de datos esta asociada a una conexion con una base\n"
        "# de datos relacional. Es posible tener varias fuentes de datos, cada\n"
        "# una representada por un indice.\n"

        "# Especifica el numero de fuentes de datos diferentes\n"
        "database.datasources = 1\n"

        "# Para realizar la conexion de base de datos asociada a una fuente de datos\n"
        "# se requieren varios elementos:\n"

        "# Nombre de la fuente de datos 1\n"
        "database.sourcename.1 = database1\n"

        "# Especifica el controlador asociado a la fuente de datos\n"
        "database.driver.1 = psql\n"

        "# Especifica si la fuente de datos esta activa o no\n"
        "database.actived.1 = on\n"

        "# Especifica el nombre de host para la fuente de datos\n"
        "database.host.1 = 172.16.137.10\n"

        "# Especifica el nombre de la base de datos para la fuente de datos\n"
        "database.db.1 = none\n"

        "# Especifica el puerto de conexin para la base de datos \n"
        "database.port.1 = 5432\n"

        "# Especifica el nombre de usuario para la fuente de datos\n"
        "database.user.1 = user\n"

        "# Especifica la contrasena de acceso para la fuente de datos\n"
        "database.password.1 = user\n"


        "#Valores para el calculo de estad�sticas\n"
        "[Stats]\n"
        "#Fecha de inicio de calculo de estad�stica, * significa que no hay fecha colocada\n"
        "stats.fromdate = *\n"
        "#Fecha de fin de calculo de estad�stica, * significa que no hay fecha colocada\n"
        "stats.todate = *\n"


        "# Establece variables de configuracion para la libreria Libdigidoc utilizada por Libsafet\n"
        "[Libdigidoc]\n"
        "# Ruta del archivo de configuracin digidoc para usarlo con protocolo OCSP\n"
        "libdigidoc.configfilepath = /home/user/.safet/digidoc.conf\n"


        "libdigidoc.x509storepath =  /home/user/.safet/certs\n"

        "# Tipo de validacin de firma de Digidoc \"ocsp\" : via protocolo OCSP, \"keystore\": Repositorio\n"
        "# de claves del navegador Mozilla / Firefox\n"
        "#libdigidoc.validationtype = keystore\n"

        "libdigidoc.mimestypes.* = application/vnd.sun.xml.writer sxw\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.writer.template stw\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.writer.global sxg\n"
        "libdigidoc.mimestypes.* = application/vnd.stardivision.writer sdw vor\n"
        "libdigidoc.mimestypes.* = application/vnd.stardivision.writer-global sgl\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.calc sxc\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.calc.template stc\n"
        "libdigidoc.mimestypes.* = application/vnd.stardivision.calc sdc\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.impress sxi\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.impress.template sti\n"
        "libdigidoc.mimestypes.* = application/vnd.stardivision.impress sdd sdp\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.draw sxd\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.draw.template std\n"
        "libdigidoc.mimestypes.* = application/vnd.stardivision.draw sda\n"
        "libdigidoc.mimestypes.* = application/vnd.sun.xml.math sxm\n"
        "libdigidoc.mimestypes.* = application/vnd.stardivision.math smf\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.text odt\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.text-template ott\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.text-web oth\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.text-master odm\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.graphics odg\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.graphics-template otg\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.presentation odp\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.presentation-template otp\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.spreadsheet ods\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.spreadsheet-template ots\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.chart odc\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.formula odf\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.database odb\n"
        "libdigidoc.mimestypes.* = application/vnd.oasis.opendocument.image odi\n"
        "libdigidoc.mimestypes.* = application/pdf pdf\n"
        "libdigidoc.mimestypes.* = application/xml xml\n"
        "libdigidoc.mimestypes.* = text/plain txt\n"
        "libdigidoc.mimestypes.* = text/css css\n"
        "libdigidoc.mimestypes.* = text/xml xml\n"
        "libdigidoc.mimestypes.* = text/html html \n"
        "libdigidoc.mimestypes.* = application/x-gzip tgz\n"
        "libdigidoc.mimestypes.* = application/zip zip\n"

        "# Opciones para los Autofiltros (Autofilter)\n"
        "[Autofilter]\n"
        "autofilter.datetime.period = 672\n"


        "#Opciones valores por defecto \n"

        "[DefaultValues]\n"

        "defaultvalues.report = yes\n"
        "defaultvalues.digidoc.manifest = Investigador\n"
        "defaultvalues.digidoc.city = M�rida\n"
        "defaultvalues.digidoc.state = M�rida\n"
        "defaultvalues.digidoc.country = VE\n"
        "defaultvalues.digidoc.zip = 5101\n"
        "defaultvalues.panel.info.secondstohide = 4000\n"

        "defaultvalues.graphs.daysproject = 45\n"


        "[Reports]\n"
        "# Configuraciones para mostrar informacin en la aplicacin Inflow\n"

        "# tipo de protocolo (file,http,ftp,entre otros)\n"
        "reports.protocol = file \n"

        "# ruta para obtener la plantilla (HTML)\n"
        "reports.path = /home/user/.safet/reports\n"

        "# nombre de la plantilla HTML + AJAX\n"
        "reports.general.template = /home/user/.safet/reports/sf_plantillaListaRedes.html\n"

        "# nombre de la plantilla para documento a firmar\n"
        "reports.documenttosign.template = /home/user/.safet/reports/sf_plantillaFirma01.html\n"

        "# Opciones Para los gr�ficos de flujo de trabajo\n"
        "[Graphs]\n"

        "# opciones para el texto de informacin en cada flujo de trabajo\n"
        "# opciones on/off\n"
        "graphs.infotext.include = on\n"

        "# Formato  para el texto de informacin en cada flujo de trabajo (el %date indica Fecha y %time Hora, %datetime Hora y Fecha)\n"
        "graphs.infotext.format = Generado %datetime\n"

        "# Nombre de la fuente para el texto de informacin en cada flujo de trabajo\n"
        "graphs.infotext.font = Book Antiqua\n"

        "# Tamao de la fuente para el texto de informacin en cada flujo de trabajo\n"
        "graphs.infotext.size = 14\n"

        "# Separacin de la fuente para el texto de informacin en cada flujo de trabajo\n"
        "graphs.infotext.separation = 30\n"



        "# Opciones para las librerias adiciones [Plugins]\n"

        "[Plugins]\n"
        "# Ruta donde se encuentra las librerias adicionales (lugins)\n"
        "plugins.path = /home/user/PySafet/ \n"




        "# Opciones particulares de cada complemento \n"
        "[Plugins.Graphviz]\n"

        "# Informaci�n a mostrar en cuadro de informaci�n extra Porc,Tokens,Total,InfoText,InfoDate\n"
        "#plugins.graphviz.extrainfo.show = Porc,Tokens,Total,InfoText,InfoDate\n"
        "plugins.graphviz.extrainfo.show = InfoText,InfoDate\n"


        "# Tipo de Gr�fico svg o png\n"
        "plugins.graphviz.graphtype = png\n"

        "# Este son los parmetros para dibujar el grafo utilizando el plugins \"plugviz\" que\n"
        "# se entrega con el nucleo de SAFET.\n"
        "# Los colores por nombre posibles estan en la siguiente tabla: (Tomado de Guia de dot (Graphviz) 2006)\n"
        "# Whites             Reds               Yellows              turquoise     \n"
        "# antiquewhite       coral              darkgoldenrod     \n"
        "#                                                            Blues\n"
        "# azure              crimson            gold     \n"
        "# bisque             darksalmon         goldenrod            aliceblue\n"
        "# blanchedalmond     deeppink           greenyellow          blue     \n"
        "# cornsilk           firebrick          lightgoldenrod       blueviolet\n"
        "# floralwhite        hotpink            lightgoldenrodyellow cadetblue     \n"
        "# gainsboro          indianred          lightyellow          cornflowerblue\n"
        "# ghostwhite         lightpink          palegoldenrod        darkslateblue\n"
        "# honeydew           lightsalmon        yellow               deepskyblue     \n"
        "# ivory              maroon             yellowgreen          dodgerblue     \n"
        "# lavender           mediumvioletred                         indigo\n"
        "#                                       Greens\n"
        "# lavenderblush      orangered                               lightblue     \n"
        "# lemonchiffon       palevioletred      chartreuse           lightskyblue     \n"
        "# linen              pink               darkgreen            lightslateblue     \n"
        "# mintcream          red                darkolivegreen       mediumblue\n"
        "# mistyrose          salmon             darkseagreen         mediumslateblue\n"
        "# moccasin           tomato             forestgreen          midnightblue\n"
        "# navajowhite        violetred          green                navy\n"
        "# oldlace                               greenyellow          navyblue\n"
        "#                    Browns\n"
        "# papayawhip                            lawngreen            powderblue\n"
        "# peachpuff          beige              lightseagreen        royalblue     \n"
        "# seashell           brown              limegreen            skyblue     \n"
        "# snow               burlywood          mediumseagreen       slateblue     \n"
        "# thistle            chocolate          mediumspringgreen    steelblue     \n"
        "# wheat              darkkhaki          mintcream\n"
        "                                                             Magentas\n"
        "# white              khaki              olivedrab     \n"
        "# whitesmoke         peru               palegreen            blueviolet\n"
        "#                    rosybrown          seagreen             darkorchid     \n"
        "# Greys              saddlebrown        springgreen          darkviolet\n"
        "# darkslategray      sandybrown         yellowgreen          magenta     \n"
        "# dimgray            sienna                                  mediumorchid     \n"
        "#                                       Cyans\n"
        "# gray               tan                                     mediumpurple     \n"
        "# gray                                  aquamarine           mediumvioletred\n"
        "#                    Oranges\n"
        "# lightgray                             cyan                 orchid     \n"
        "# lightslategray     darkorange         darkturquoise        palevioletred     \n"
        "# slategray          orange             lightcyan            plum     \n"
        "#                    orangered          mediumaquamarine     purple     \n"
        "# Blacks                                mediumturquoise      violet\n"
        "# black                                 paleturquoise        violetred     \n"


        "plugins.graphviz.extrainfo.showwheretokens = off\n"

        "#Color activo de para ser utilizado en los grficos \n"
        "plugins.graphviz.task.fillcolor = #f1f1f1\n"

        "#Color activo de la lnea para ser utilizado en los grficos \n"
        "plugins.graphviz.task.color = black\n"


        "# Atributo utilizado en la estadstica Opciones posibles (Color/Size/Line/Custom)\n"
        "plugins.graphviz.stats.attribute = Color\n"

        "# Tamao mximo para la estadstica de (Tamano Maximo)\n"
        "plugins.graphviz.stats.sizemax = 2 \n"

        "# Tamao mnimo  para la estadstica de (Tamano Minimo)\n"
        "plugins.graphviz.stats.sizemin = 1\n"

        "# Color para dibujar estadistica\n"
        "plugins.graphviz.stats.colorgradient = #ffff00\n"

        "# Color para dibujar estadistica un gradiente adicional\n"
        "plugins.graphviz.stats.othergradient = #ff0000\n"

        "# Color del texto para cuadro de estad�stica\n"
        "plugins.graphviz.stats.fontcolor = black\n"

        "# Color de fondo  para cuadro de estad�stica\n"
        "plugins.graphviz.stats.fillcolor = antiquewhite\n"

        "# Estilo de la l�nea del cuadro de estad�stica \n"
        "plugins.graphviz.stats.style = dashed\n"

        "# Mostrar cuadro de estad�stica\n"
        "plugins.graphviz.showstats = yes\n"


        "#Direcci�n del grafo  TB (Arriba-Abajo) LR (Izquierda-Derecha)\n"
        "plugins.graphviz.graph.rankdir = TB\n"

        "# Tamao del fuente para todos los nodos\n"
        "plugins.graphviz.graph.fontsize = 12\n"

        "# Separador del rank  \n"
        "plugins.graphviz.graph.ranksep = 0.5 equally\n"


        "# Figura para la tarea (Task)\n"
        "plugins.graphviz.task.shape = box\n"

        "# Estilo de la Figura para la tarea (Task) filled,bold,dotted,empty\n"
        "plugins.graphviz.task.style = filled\n"

        "#Color activo de para ser utilizado en los gr�ficos \n"
        "plugins.graphviz.condition.fillcolor = #FFFFFF \n"

        "#Color activo de la lnea para ser utilizado en los grficos \n"
        "plugins.graphviz.condition.color = black\n"

        "# Figura para la (Condition) (box, ellipse, circle, etc.)\n"
        "plugins.graphviz.condition.shape = ellipse\n"

        "# Estilo de la Figura para la tarea (Condition)\n"
        "plugins.graphviz.condition.style = filled\n"
        "";

const QString AUTHCONF = ""
        "/*\n"
        "* SAFET Sistema Automatizado para la Firma Electrnica y Estampado de Tiempo - Componente\n"
        "* XPCOM de Mozilla Firefox para aplicar firma electronica\n"
        "*\n"
        "* Copyright (C) 2008 Vctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo\n"
        "* (aaraujo@cenditel.gob.ve)\n"
        "*\n"
        "* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigacin en Tecnologas Libres\n"
        "*\n"
        "* Este programa es software libre; Usted puede usarlo bajo los trminos de la licencia de\n"
        "* software GPL versin 2.0 de la Free Software Foundation.\n"
        "*\n"
        "* Este programa se distribuye con la esperanza de que sea til, pero SI NINGUNA GARANTA;\n"
        "* tampoco las implcitas garantas de MERCANTILIDAD o ADECUACIN A UN PROPSITO PARTICULAR.\n"
        "* Consulte la licencia GPL para ms detalles. Usted debe recibir una copia de la GPL junto\n"
        "* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,\n"
        "* 5 Piso, Boston, MA 02110-1301, USA.\n"
        "*\n"
        "*/\n"

        "# Archivo de configuracion de autenticaci�n y autorizaci�n de la libreria libsafet\n"
        "# ================================================\n"

        "[Database]\n"

        "# Informacion de configuracion de fuentes de datos.\n"
        "# Una fuente de datos esta asociada a una conexion con una base\n"
        "# de datos relacional. Es posible tener varias fuentes de datos, cada\n"
        "# una representada por un indice.\n"

        "# Especifica el numero de fuentes de datos diferentes\n"
        "database.datasources = 1\n"

        "# Para realizar la conexion de base de datos asociada a una fuente de datos\n"
        "# se requieren varios elementos:\n"

        "# Nombre de la fuente de datos 1\n"
        "database.sourcename.1 = database1\n"

        "# Especifica el controlador asociado a la fuente de datos\n"
        "database.driver.1 = sqlite\n"

        "# Especifica si la fuente de datos esta activa o no\n"
        "database.actived.1 = on\n"

        "# Especifica el nombre de host para la fuente de datos\n"
        "database.host.1 = localhost\n"

        "# Especifica el nombre de la base de datos para la fuente de datos\n"

        "database.db.1 = /home/user/.safet/mydb.db \n"

        "# Especifica el puerto de conexin para la base de datos \n"
        "database.port.1 = 5432\n"

        "# Especifica el nombre de usuario para la fuente de datos\n"
        "database.user.1 = victor\n"

        "# Especifica la contrasena de acceso para la fuente de datos\n"
        "database.password.1 = 123456\n"


        "[Titles]\n"

        "titles.application = Estudiantes\n"


        "[Roles]\n"

        "roles.name.1 = Desarrollador\n"
        "roles.description.1 = Codifica\n"

        "roles.name.2 = Administrador\n"
        "roles.description.2 = Funciones de administraci�n\n"



        "[Auth]\n"
        "# Valores escritos por SAFET - INFLOW mi� may 5 08:52:38 2010\n"
        "auth.account.1 = vbravo\n"
        "auth.pass.1 = 8c59edc50217248b0184251fe0496bba1455e1a5\n"
        "auth.realname.1 = V�ctor Rafael Bravo\n"
        "auth.role.1 = Administrador\n"

        "auth.account.2 = admin\n"
        "auth.pass.2 = 22cd2d1c596f4091e248aff0e4aa0d47c84b2b36\n"
        "auth.realname.2 = Usuario de Administrador\n"
        "auth.role.2 = Administrador\n"


        "auth.account.3 = aaraujo\n"
        "auth.pass.3 = 1e8f6cbc5ba45a9b3502371dc2cfe41c3090a6db\n"
        "auth.realname.3 = Antonio Araujo\n"
        "auth.role.3 = Administrador\n"



        "auth.account.4 = pbuitrago\n"
        "auth.pass.4 = dcafbc1156f25f771dc6168a4a066a7aa1f24d5b\n"
        "auth.realname.4 = Pedro Buitrago\n"
        "auth.role.4 = Administrador\n"

        "auth.account.5 = earaujo\n"
        "auth.pass.5 = ddc6fdd484d5a71b9619beb8b19a7ea06980d8ff\n"
        "auth.realname.5 = Ender Araujo\n"
        "auth.role.5 = Desarrollador\n"

        "[Permises]\n"

        "permises.operation.1 = agregar_ticket\n"
        "permises.accounts.1 = vbravo;aaraujo\n"
        "permises.types.1 = read;execute;modify\n"
        "permises.roles.1 = Desarrollador;Administrador\n"

        "permises.operation.2 = modificar_ticket\n"
        "permises.accounts.2 = vbravo\n"
        "permises.types.2 = read;execute;modify\n"
        "permises.roles.2 = Desarrollador;Administrador\n"

        "permises.operation.3 = agregar_wiki\n"
        "permises.accounts.3 = vbravo\n"
        "permises.types.3 = read;execute;modify\n"
        "permises.roles.3 = Desarrollador;Administrador\n"

        "permises.operation.4 = New_step\n"
        "permises.accounts.4 = pbuitrago\n"
        "permises.types.4 = read;exec;modify\n"
        "permises.roles.4 = Administrador\n"

        "permises.operation.5 = borrar_ticket\n"
        "permises.accounts.5 = vbravo\n"
        "permises.types.5 = read;execute;modify\n"
        "permises.roles.5 = Desarrollador;Administrador\n"


        "permises.operation.6 = agregar_hito\n"
        "permises.accounts.6 = vbravo\n"
        "permises.types.6 = read;execute;modify\n"
        "permises.roles.6 = Administrador\n"

        "permises.operation.7 = modificar_hito\n"
        "permises.accounts.7 = vbravo\n"
        "permises.types.7 = read;execute;modify\n"
        "permises.roles.7 = Administrador\n"

        "permises.operation.8 = borrar_hito\n"
        "permises.accounts.8 = vbravo\n"
        "permises.types.8 = read;execute;modify\n"
        "permises.roles.8 = Administrador\n"

        "permises.operation.9 = agregar_componente\n"
        "permises.accounts.9 = vbravo\n"
        "permises.types.9 = read;execute;modify\n"
        "permises.roles.9 = Administrador\n"

        "permises.operation.10 = modificar_componente\n"
        "permises.accounts.10 = vbravo\n"
        "permises.types.10 = read;execute;modify\n"
        "permises.roles.10 = Administrador\n"

        "permises.operation.11 = borrar_componente\n"
        "permises.accounts.11 = vbravo\n"
        "permises.types.11 = read;execute;modify\n"
        "permises.roles.11 = Administrador\n"


        "permises.operation.12 = agregar_prioridad\n"
        "permises.accounts.12 = vbravo\n"
        "permises.types.12 = read;execute;modify\n"
        "permises.roles.12 = Administrador\n"

        "permises.operation.13 = modificar_prioridad\n"
        "permises.accounts.13 = vbravo\n"
        "permises.types.13 = read;execute;modify\n"
        "permises.roles.13 = Administrador\n"

        "permises.operation.14 = borrar_prioridad\n"
        "permises.accounts.14 = vbravo\n"
        "permises.types.14 = read;execute;modify\n"
        "permises.roles.14 = Administrador\n"

        "permises.operation.15 = agregar_resolucion\n"
        "permises.accounts.15 = vbravo\n"
        "permises.types.15 = read;execute;modify\n"
        "permises.roles.15 = Administrador\n"

        "permises.operation.16 = modificar_resolucion\n"
        "permises.accounts.16 = vbravo\n"
        "permises.types.16 = read;execute;modify\n"
        "permises.roles.16 = Administrador\n"

        "permises.operation.17 = borrar_resolucion\n"
        "permises.accounts.17 = vbravo\n"
        "permises.types.17 = read;execute;modify\n"
        "permises.roles.17 = Administrador\n"


        "permises.operation.18 = agregar_version\n"
        "permises.accounts.18 = vbravo\n"
        "permises.types.18 = read;execute;modify\n"
        "permises.roles.18 = Administrador\n"

        "permises.operation.19 = modificar_version\n"
        "permises.accounts.19 = vbravo\n"
        "permises.types.19 = read;execute;modify\n"
        "permises.roles.19 = Administrador\n"

        "permises.operation.20 = borrar_version\n"
        "permises.accounts.20 = vbravo\n"
        "permises.types.20 = read;execute;modify\n"
        "permises.roles.20 = Administrador\n"

        "# Operaciones de Consulta, Lista de datos y gr�ficos\n"

        "permises.operation.21 = Listar_datos\n"
        "permises.accounts.21 = vbravo\n"
        "permises.types.21 = read;execute;modify\n"
        "permises.roles.21 = Desarrollador;Administrador\n"


        "permises.operation.22 = Listar_datos_con_autofiltro\n"
        "permises.accounts.22 = vbravo\n"
        "permises.types.22 = read;execute;modify\n"
        "permises.roles.22 = Administrador\n"


        "permises.operation.23 = Listar_datos_con_filtrorecursivo\n"
        "permises.accounts.23 = vbravo\n"
        "permises.types.23 = read;execute;modify\n"
        "permises.roles.23 = Administrador\n"

        "permises.operation.24 = Generar_gr�fico_coloreado\n"
        "permises.accounts.24 = vbravo\n"
        "permises.types.24 = read;execute;modify\n"
        "permises.roles.24 = Administrador\n"


        "permises.operation.25 = Generar_gr�fico_para_clave\n"
        "permises.accounts.25 = vbravo\n"
        "permises.types.25 = read;execute;modify\n"
        "permises.roles.25 = Administrador\n"


        "permises.operation.26 = Generar_gr�fico_con_autofiltro\n"
        "permises.accounts.26 = vbravo\n"
        "permises.types.26 = read;execute;modify\n"
        "permises.roles.26 = Desarrollador;Administrador\n"


        "permises.operation.27 = Generar_gr�fico_con_filtrorecursivo\n"
        "permises.accounts.27 = vbravo\n"
        "permises.types.27 = read;execute;modify\n"
        "permises.roles.27 = Administrador\n"

        "# Vi�etas\n"

        "permises.operation.28 = Formulario\n"
        "permises.accounts.28 = vbravo\n"
        "permises.types.28 = read;execute;modify\n"
        "permises.roles.28 = Administrador;Desarrollador\n"


        "permises.operation.29 = Consulta\n"
        "permises.accounts.29 = vbravo\n"
        "permises.types.29 = read;execute;modify\n"
        "permises.roles.29 = Administrador;Desarrollador\n"


        "permises.operation.30 = Firma/Certificados\n"
        "permises.accounts.30 = vbravo\n"
        "permises.types.30 = read;execute;modify\n"
        "permises.roles.30 = Administrador\n"

        "permises.operation.31 = Reportes\n"
        "permises.accounts.31 = vbravo\n"
        "permises.types.31 = read;execute;modify\n"
        "permises.roles.31 = Administrador;Desarrollador\n"


        "permises.operation.32 = Flujo\n"
        "permises.accounts.32 = vbravo\n"
        "permises.types.32 = read;execute;modify\n"
        "permises.roles.32 = Administrador;Desarrollador\n"


        "permises.operation.33 = Configuraci�n\n"
        "permises.accounts.33 = vbravo\n"
        "permises.types.33 = read;execute;modify\n"
        "permises.roles.33 = Administrador\n"

        "permises.operation.34 = Salida\n"
        "permises.accounts.34 = vbravo\n"
        "permises.types.34 = read;execute;modify\n"
        "permises.roles.34 = Administrador\n"


        "permises.operation.35 = Usuarios/Roles\n"
        "permises.accounts.35 = vbravo\n"
        "permises.types.35 = read;execute;modify\n"
        "permises.roles.35 = Administrador\n"

        "permises.operation.36 = agregar_ticket_implementarsafet\n"
        "permises.accounts.36 = vbravo;aaraujo\n"
        "permises.types.36 = read;execute;modify\n"
        "permises.roles.36 = Desarrollador;Administrador\n"

        "permises.operation.37 = agregar_ticket_desplieguesafet\n"
        "permises.accounts.37 = vbravo;aaraujo\n"
        "permises.types.37 = read;execute;modify\n"
        "permises.roles.37 = Desarrollador;Administrador\n"

        "permises.operation.38 = agregar_ticket_registrobugssafet\n"
        "permises.accounts.38 = vbravo;aaraujo\n"
        "permises.types.38 = read;execute;modify\n"
        "permises.roles.38 = Desarrollador;Administrador\n"


        "permises.operation.39 = agregar_tipo\n"
        "permises.accounts.39 = vbravo\n"
        "permises.types.39 = read;execute;modify\n"
        "permises.roles.39 = Administrador\n"

        "permises.operation.40 = modificar_tipo\n"
        "permises.accounts.40 = vbravo\n"
        "permises.types.40 = read;execute;modify\n"
        "permises.roles.40 = Administrador\n"

        "permises.operation.41 = borrar_tipo\n"
        "permises.accounts.41 = vbravo\n"
        "permises.types.41 = read;execute;modify\n"
        "permises.roles.41 = Administrador\n"

        "permises.operation.42 = Aceptar_ticket\n"
        "permises.accounts.42 = vbravo\n"
        "permises.types.42 = read;execute;modify\n"
        "permises.roles.42 = Administrador\n"


        "permises.operation.43 = enlazar_subtarea\n"
        "permises.accounts.43 = vbravo\n"
        "permises.types.43 = read;execute;modify\n"
        "permises.roles.43 = Administrador\n"


        "permises.operation.44 = Tama�o_de_la_letra\n"
        "permises.accounts.44 = vbravo\n"
        "permises.types.44 = read;execute;modify\n"
        "permises.roles.44 = Administrador\n"


        "permises.operation.45 = Tipo_de_la_letra\n"
        "permises.accounts.45 = vbravo\n"
        "permises.types.45 = read;execute;modify\n"
        "permises.roles.45 = Administrador\n"

        "permises.operation.46 = Separaci�n_del_gr�fico\n"
        "permises.accounts.46 = vbravo\n"
        "permises.types.46 = read;execute;modify\n"
        "permises.roles.46 = Administrador\n"

        "permises.operation.47 = borrar_enlace_entre_tareas\n"
        "permises.accounts.47 = vbravo\n"
        "permises.types.47 = read;execute;modify\n"
        "permises.roles.47 = Administrador\n"

        "permises.operation.48 = agregar_estudiante\n"
        "permises.accounts.48 = vbravo\n"
        "permises.types.48 = read;execute;modify\n"
        "permises.roles.48 = Administrador\n"


        "permises.operation.49 = modificar_estudiante\n"
        "permises.accounts.49 = vbravo\n"
        "permises.types.49 = read;execute;modify\n"
        "permises.roles.49 = Administrador\n"


        "permises.operation.50 = agregar_sesi�n\n"
        "permises.accounts.50 = vbravo\n"
        "permises.types.50 = read;execute;modify\n"
        "permises.roles.50 = Administrador\n"


        "permises.operation.51 = agregar_grupo\n"
        "permises.accounts.51 = vbravo\n"
        "permises.types.51 = read;execute;modify\n"
        "permises.roles.51 = Administrador\n"


        "permises.operation.52 = modificar_sesi�n\n"
        "permises.accounts.52 = vbravo\n"
        "permises.types.52 = read;execute;modify\n"
        "permises.roles.52 = Administrador\n"


        "permises.operation.53 = modificar_grupo\n"
        "permises.accounts.53 = vbravo\n"
        "permises.types.53 = read;execute;modify\n"
        "permises.roles.53 = Administrador\n"


        "permises.operation.54 = agregar_estudiante_a_sesi�n\n"
        "permises.accounts.54 = vbravo\n"
        "permises.types.54 = read;execute;modify\n"
        "permises.roles.54 = Administrador\n"

        "permises.operation.55 = modificar_estudiante_a_sesi�n\n"
        "permises.accounts.55 = vbravo\n"
        "permises.types.55 = read;execute;modify\n"
        "permises.roles.55 = Administrador\n"


        "permises.operation.56 = borrar_sesi�n\n"
        "permises.accounts.56 = vbravo\n"
        "permises.types.56 = read;execute;modify\n"
        "permises.roles.56 = Administrador\n"


        "permises.operation.57 = borrar_grupo\n"
        "permises.accounts.57 = vbravo\n"
        "permises.types.57 = read;execute;modify\n"
        "permises.roles.57 = Administrador\n"


        "permises.operation.58 = Delete_live_story\n"
        "permises.accounts.58 = vbravo\n"
        "permises.types.58 = read;execute;modify\n"
        "permises.roles.58 = Administrador\n"

        "permises.operation.59 = Add_new_live_story\n"
        "permises.accounts.59 = vbravo\n"
        "permises.types.59 = read;execute;modify\n"
        "permises.roles.59 = Administrador\n"


        "permises.operation.60 = Add_step_into_story\n"
        "permises.accounts.60 = vbravo\n"
        "permises.types.60 = read;execute;modify\n"
        "permises.roles.60 = Administrador\n"

        "permises.operation.61 = Delete_step_in_story\n"
        "permises.accounts.61 = vbravo\n"
        "permises.types.61 = read;execute;modify\n"
        "permises.roles.61 = Administrador\n"


        "permises.operation.62 = Create_new_story\n"
        "permises.accounts.62 = vbravo\n"
        "permises.types.62 = read;execute;modify\n"
        "permises.roles.62 = Administrador\n"
        "";

const QString DEFTRACXML = QString::fromLatin1(""
        "<?xml version='1.0' encoding='UTF-8'?>\n"
        "<!DOCTYPE operations SYSTEM 'file:///home/user/.safet/dtd/yawlinput.dtd'>\n"
        "<!--\n"
        "Documento  : deftrac.xml\n"
        "Creado     : V�ctor Bravo\n"
        "Autor      : Victor Bravo\n"
        "Descripcion: Archivo de Entrada para SAFET - Inflow\n"
        "-->\n"
        "<operations suffix=\":\" commandname=\"operacion\">\n"
        " <operation desc=\"Agregar,Modificar,Eliminar,Listar\" icon=\"\" name=\"Live\"/>\n"
       "<operation desc=\"Pasa de estado una determinada actividad \" icon=\"qrc:/addstep.png\" name=\"New_step_tiramisu\">"
       "  <command table=\"trayecto\" type=\"actualizar\" id=\"1\">"
       "   <fields>"
       "    <field title=\"id\" changekey=\"yes\" options=\"nombre:trayecto:where flujo='/home/user/.safet/flowfiles/tiramisu.xml'::nombre\" order=\"desc\" type=\"combolisttable\" mandatory=\"yes\" primarykey=\"yes\">"
       "                                nombre"
       "                        </field>"
       "    <field changefor=\"id\" title=\"Next_status\" path=\"/home/user/.safet/flowfiles/tiramisu.xml\" options=\"next\" type=\"comboflow\" mandatory=\"yes\">"
       "                                status"
       "                        </field>"
       "    <field title=\"Picture\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.png);;Todos (*)\" options=\"load::pictures\" type=\"filename\" mandatory=\"no\">"
       "                        lastpicture"
       "                </field>"
       "    <field title=\"Info\" type=\"stringbox\" mandatory=\"no\" primarykey=\"no\">"
       "                         lasttextinfo"
       "                </field>"
       "    <field changefor=\"status\" title=\"Date\" type=\"datetime\" mandatory=\"no\">\n"
       "                                fechacreacion\n"
       "                        </field>\n"
       "   </fields>"
       "  </command>"
       "  <command table=\"trayecto_registro\" type=\"agregar\" id=\"1\">"
       "   <fields>"
       "    <field title=\"Date\" format=\"time_t\" type=\"datetime\" mandatory=\"no\">\n"
       "                        fechaaccion\n"
       "                </field>\n"
       "    <field type=\"string\" literal=\"_USERNAME\" mandatory=\"yes\">"
       "                                propietario"
       "                        </field>"
       "    <field title=\"id\" type=\"string\" mandatory=\"yes\">"
       "                                name_live_story"
       "                        </field>"
       "    <field title=\"Next_status\" type=\"string\" mandatory=\"no\" primarykey=\"no\">"
       "                           status"
       "                        </field>"
       "    <field title=\"Picture\" type=\"string\" mandatory=\"no\" primarykey=\"no\">"
       "                           picture"
       "                        </field>"
       "    <field title=\"Info\" type=\"string\" mandatory=\"no\" primarykey=\"no\">"
       "                           textinfo"
       "                        </field>"
       "    <field title=\"story_name\" type=\"string\" literal=\"tiramisu\" mandatory=\"no\">"
       "                           story_name"
       "                        </field>"
       "   </fields>"
       "  </command>"
       " </operation>"
       "<operation desc=\"Pasa de estado una determinada actividad \" icon=\"qrc:/addstep.png\" name=\"New_step_sugarloaf_mountain\">"
       "  <command table=\"trayecto\" type=\"actualizar\" id=\"1\">"
       "   <fields>"
       "    <field title=\"id\" changekey=\"yes\" options=\"nombre:trayecto:where flujo='/home/user/.safet/flowfiles/sugarloaf_mountain.xml'::nombre\" order=\"desc\" type=\"combolisttable\" mandatory=\"yes\" primarykey=\"yes\">"
       "                                nombre"
       "                        </field>"
       "    <field changefor=\"id\" title=\"Next_status\" path=\"/home/user/.safet/flowfiles/sugarloaf_mountain.xml\" options=\"next\" type=\"comboflow\" mandatory=\"yes\">"
       "                                status"
       "                        </field>"
       "    <field title=\"Picture\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.png);;Todos (*)\" options=\"load::pictures\" type=\"filename\" mandatory=\"no\">"
       "                        lastpicture"
       "                </field>"
       "    <field title=\"Info\" type=\"stringbox\" mandatory=\"no\" primarykey=\"no\">"
       "                         lasttextinfo"
       "                </field>"
       "    <field changefor=\"status\" title=\"Date\" type=\"datetime\" mandatory=\"no\">\n"
       "                                fechacreacion\n"
       "                        </field>\n"

       "   </fields>"
       "  </command>"
       "  <command table=\"trayecto_registro\" type=\"agregar\" id=\"1\">"
       "   <fields>"
       "    <field title=\"Date\" format=\"time_t\" type=\"datetime\" mandatory=\"no\">\n"
       "                        fechaaccion\n"
       "                </field>\n"
       "    <field type=\"string\" literal=\"_USERNAME\" mandatory=\"yes\">"
       "                                propietario"
       "                        </field>"
       "    <field title=\"id\" type=\"string\" mandatory=\"yes\">"
       "                                name_live_story"
       "                        </field>"
       "    <field title=\"Next_status\" type=\"string\" mandatory=\"no\" primarykey=\"no\">"
       "                           status"
       "                        </field>"
       "    <field title=\"Picture\" type=\"string\" mandatory=\"no\" primarykey=\"no\">"
       "                           picture"
       "                        </field>"
       "    <field title=\"Info\" type=\"string\" mandatory=\"no\" primarykey=\"no\">"
       "                           textinfo"
       "                        </field>"
       "    <field title=\"story_name\" type=\"string\" literal=\"sugarloaf_mountain\" mandatory=\"no\">"
       "                           story_name"
       "                        </field>"
       "   </fields>"
       "  </command>"
       " </operation>"

        " <operation desc=\"Agregar un nuevo ticket para la fase de Conceptualizaci�n\" icon=\"qrc:/plus.png\" name=\"Add_new_live_story\" >\n"
        "  <command table=\"trayecto\" type=\"agregar\" id=\"1\">\n"
        "   <fields>\n"
        "    <field title=\"Live_story_name\" type=\"string\" mandatory=\"yes\" validation=\"[a-zA-Z0-9_\s\-\\s]{2,18}\">\n"
        "			nombre\n"
        "		</field>\n"
        "    <field title=\"From_story_name\" desc=\"Cargar un archivo de flujo para generar un reporte\" changekey=\"yes\" filter=\"Archivos de flujo (*.xml);;Todos (*)\" options=\"load::flowfiles\" type=\"filename\" mandatory=\"yes\">\n"
        "			flujo\n"
        "		</field>\n"
        "    <field title=\"fecha_creaci�n\" input=\"no\" format=\"time_t\" type=\"datetime\" function=\"datetime('now')\" mandatory=\"yes\">\n"
        "			fechacreacion\n"
        "		</field>\n"
        "    <field title=\"Ultima_modificaci�n\" input=\"no\" format=\"time_t\" type=\"datetime\" function=\"datetime('now')\" mandatory=\"yes\">\n"
        "			ultimafecha\n"
        "		</field>\n"
        "    <field title=\"Usuario\" type=\"string\" literal=\"_USERNAME\" mandatory=\"yes\">\n"
        "			usuario\n"
        "		</field>\n"
        "    <field title=\"Estado\" type=\"string\" literal=\"partida\" mandatory=\"yes\">\n"
        "			status\n"
        "		</field>\n"
        "   </fields>\n"
        "  </command>\n"
        " </operation>\n"
        " <operation desc=\"Agregar un nuevo ticket para la fase de Conceptualizaci�n\" icon=\"qrc:/minus.png\" name=\"Delete_live_story\">\n"
        "  <command table=\"trayecto\" type=\"eliminar\" id=\"1\">\n"
        "   <fields>\n"
        "    <field title=\"id\" changekey=\"yes\" options=\"nombre:trayecto::nombre\" order=\"desc\" type=\"combolisttable\" mandatory=\"yes\" primarykey=\"yes\">\n"
        "				nombre\n"
        "			</field>\n"
        "   </fields>\n"
        "  </command>\n"
        "  <command table=\"trayecto_registro\" type=\"eliminar\" id=\"1\">\n"
        "   <fields>\n"
        "    <field title=\"id\" order=\"desc\" type=\"string\" mandatory=\"yes\" primarykey=\"yes\">\n"
        "				name_live_story\n"
        "			</field>\n"
        "   </fields>\n"
        "  </command>\n"
       "  <command table=\"trayecto_consulta\" type=\"eliminar\" id=\"1\">\n"
       "   <fields>\n"
       "    <field title=\"id\" order=\"desc\" type=\"string\" mandatory=\"yes\" primarykey=\"yes\">\n"
       "				Live_story\n"
       "			</field>\n"
       "   </fields>\n"
       "  </command>\n"

        " </operation>\n"

        " <operation desc=\"Modificar un registro\" icon=\"\" name=\"Change_info_live_step\" icon=\"qrc:/changeicon.png\">\n"
        "  <command table=\"trayecto_registro\" type=\"actualizar\" id=\"1\">\n"
        "   <fields>\n"
        "    <field title=\"Name_live_story\" changekey=\"yes\" options=\"name_live_story:trayecto_registro::name_live_story\" type=\"combolisttable\" mandatory=\"yes\" primarykey=\"yes\">\n"
        "                                name_live_story\n"
        "                        </field>\n"
        "			<field type=\"combolisttable\"  changekey=\"yes\" title=\"status\" options=\"status:trayecto_registro:where name_live_story='{#1}'::status\" mandatory=\"yes\" primarykey=\"no\" changefor=\"Name_live_story\">\n"
        "				status\n"
        "			</field>\n"
        "    <field changefor=\"status\" title=\"Date\" type=\"datetime\" mandatory=\"no\">\n"
        "                                fechaaccion\n"
        "                        </field>\n"
        "    <field changefor=\"status\" title=\"Picture\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.png);;Todos (*)\" options=\"load::pictures\" type=\"filename\" mandatory=\"no\">\n"
        "			picture\n"
        "		</field>\n"
        "    <field changefor=\"status\" title=\"Info\" type=\"stringbox\" mandatory=\"no\">\n"
        "			 textinfo \n"
        "		</field>\n"
        "   </fields>\n"
        "  </command>\n"
        " </operation>\n"
        " <operation desc=\"Agregar un nuevo ticket para la fase de Conceptualizaci�n\" icon=\"qrc:/deletestepicon.png\" name=\"Delete_live_step\">\n"
        "  <command table=\"trayecto_registro\" type=\"eliminar\" id=\"1\">\n"
        "   <fields>\n"
        "    <field title=\"Name_live_story\" changekey=\"yes\" options=\"name_live_story:trayecto_registro::name_live_story\" type=\"combolisttable\" mandatory=\"yes\" primarykey=\"yes\">\n"
        "                                name_live_story\n"
        "                        </field>\n"
        "			<field type=\"combolisttable\" options=\"status:trayecto_registro:where name_live_story='{#1}'::status\" mandatory=\"yes\" primarykey=\"no\" title=\"Name_status\" changefor=\"Name_live_story\">\n"
        "				status\n"
        "			</field>\n"
        "   </fields>\n"
        "  </command>\n"
        " </operation>\n"
       "<operation  name=\"Show_live_story_flow\" icon=\"qrc:/showicon.png\">\n"
       "	<command id =\"1\" type=\"agregar\" table=\"trayecto\">\n"
       "		<fields>\n"
       "			<field type=\"filename\" mandatory=\"yes\" title=\"Cargar_archivo_flujo\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.xml);;Todos (*)\" changekey=\"yes\" options=\"load::flowfiles\"  title=\"Name_story\">\n"
       "				Name_story\n"
       "			</field>\n"
       "			<field type=\"combolisttable\" options=\"nombre:trayecto:where flujo='{#1}'::nombre\" mandatory=\"yes\" primarykey=\"no\" title=\"Live_story\" changefor=\"Name_story\">\n"
       "				Live_story\n"
       "			</field>\n"
       "		</fields>\n"
       "	</command>\n"
       "</operation>\n"
       " <operation desc=\"Agregar un nuevo ticket para la fase de Conceptualizaci�n\" icon=\"qrc:/minus.png\" name=\"safet_Delete_live\">\n"
       "  <command table=\"trayecto\" type=\"eliminar\" id=\"1\">\n"
       "   <fields>\n"
       "    <field title=\"flujo\" type=\"string\" mandatory=\"yes\" primarykey=\"yes\">\n"
       "				flujo\n"
       "			</field>\n"
       "   </fields>\n"
       "  </command>\n"
      "  <command table=\"trayecto_consulta\" type=\"eliminar\" id=\"1\">\n"
      "   <fields>\n"
      "    <field title=\"flujo\" order=\"desc\" type=\"string\" mandatory=\"yes\" primarykey=\"yes\">\n"
      "				Story_name\n"
      "			</field>\n"
      "   </fields>\n"
      "  </command>\n"

       " </operation>\n"
        "<operation  name=\"safet_Add_query\"  desc=\"Agregar un nuevo ticket para la fase de Conceptualizaci�n\" icon=\"qrc:/plus.png\">\n"
        "<command id =\"1\" type=\"agregar\" table=\"trayecto_consulta\" >\n"
        "	<fields>\n"
        "	  <field type=\"datetime\" mandatory=\"yes\"  function=\"rowid+1 FROM trayecto_consulta order by rowid desc limit 1\" format=\"integer\" input=\"no\">\n"
        "			id\n"
        "		</field>\n"
        "		<field type=\"string\" mandatory=\"yes\" literal=\"Graph\" title=\"brand\">\n"
        "			brand\n"
        "		</field>\n"
        "		<field type=\"string\" mandatory=\"yes\" title=\"Description\">\n"
        "			title\n"
        "		</field>\n"
        "		<field type=\"string\" mandatory=\"yes\" title=\"Live_story\">\n"
        "			Live_story\n"
        "		</field>\n"
        "		<field type=\"string\" mandatory=\"yes\" title=\"Story_name\">\n"
        "			Story_name\n"
        "		</field>\n"
        "		<field type=\"stringbox\" mandatory=\"yes\" title=\"Text_query\">\n"
        "			textquery\n"
        "		</field>\n"
        "		<field type=\"string\" literal=\"qrc:/lasticon.png\"  mandatory=\"yes\" title=\"icon\">\n"
        "			icon\n"
        "		</field>\n"
        "	<field type=\"datetime\" mandatory=\"yes\"  input=\"no\" function=\"datetime('now')\" format=\"time_t\" title=\"fecha_creaci�n\">\n"
        "			currdate\n"
        "		</field>\n"
        "</fields>\n"
        "</command>\n"
        "</operation>\n"
       " <operation desc=\"Agregar un nuevo ticket para la fase de Conceptualizaci�n\" icon=\"qrc:/deletestepicon.png\" name=\"safet_delete_steps\">\n"
       "  <command table=\"trayecto_registro\" type=\"eliminar\" id=\"1\">\n"
       "   <fields>\n"
       "    <field title=\"story_name\"  type=\"string\" mandatory=\"yes\" primarykey=\"yes\">\n"
       "                                story_name\n"
       "                        </field>\n"
       "   </fields>\n"
       "  </command>\n"
       " </operation>\n"
       " <operation desc=\"Agregar un nuevo ticket para la fase de Conceptualizaci�n\" icon=\"qrc:/deletestepicon.png\" name=\"safet_delete_step\">\n"
       "  <command table=\"trayecto_registro\" type=\"eliminar\" id=\"1\">\n"
       "   <fields>\n"
       "    <field title=\"story_name\"  type=\"string\" mandatory=\"yes\" primarykey=\"yes\">\n"
       "                                story_name\n"
       "                        </field>\n"
       "			<field type=\"string\"  mandatory=\"yes\" title=\"Name_status\" >\n"
       "				status\n"
       "			</field>\n"

       "   </fields>\n"
       "  </command>\n"
       " </operation>\n"
        "<operation  name=\"safet_Delete_query\"  desc=\"elimina todas las consultas o una unica\" icon=\"qrc:/plus.png\">\n"
        "<command id =\"1\" type=\"eliminar\" table=\"trayecto_consulta\" >\n"
        "	<fields>\n"
        "	  <field type=\"string\" mandatory=\"yes\"  primarykey=\"yes\">\n"
        "			id\n"
        "	</field>\n"
        "</fields>\n"
        "</command>\n"
        "</operation>\n"
        "<operation  name=\"safet_Delete_title\"  desc=\"elimina todas las consultas o una unica\" icon=\"qrc:/plus.png\">\n"
        "<command id =\"1\" type=\"eliminar\" table=\"trayecto_consulta\" >\n"
        "	<fields>\n"
        "	  <field type=\"string\" mandatory=\"yes\"  primarykey=\"yes\">\n"
        "			title\n"
        "	</field>\n"
        "</fields>\n"
        "</command>\n"
        "</operation>\n"

        "</operations>\n"
        "");

        const QString DEFCONSOLEXML = ""
                "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
                "<!--\n"
                "Documento  : deftrac.xml\n"
                "Creado     : V�ctor Bravo\n"
                "Autor      : Victor Bravo\n"
                "Descripcion: Archivo de Entrada para SAFET - Inflow\n"
                "-->\n"
                "<!DOCTYPE operations SYSTEM \"file:///home/user/.safet/dtd/yawlinput.dtd\">\n"
                "<operations suffix=\":\" commandname=\"operacion\"  >  \n"
                "<operation  name=\"Story_flows\"  desc=\"Flujos de trabajo b�sicos\" icon=\"\">\n"
                "</operation>\n"
                "<operation  name=\"Show_live_story_flow\" >\n"
                "	<command id =\"1\" type=\"agregar\" table=\"trayecto\">\n"
                "		<fields>\n"
                "			<field type=\"filename\" mandatory=\"yes\" title=\"Cargar_archivo_flujo\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.xml);;Todos (*)\" changekey=\"yes\" options=\"load::flowfiles\"  title=\"Name_story\">\n"
                "				Name_story\n"
                "			</field>\n"
                "			<field type=\"combolisttable\" options=\"nombre:trayecto:where flujo='{#1}'::nombre\" mandatory=\"yes\" primarykey=\"no\" title=\"Live_story\" changefor=\"Name_story\">\n"
                "				Live_story\n"
                "			</field>\n"
                "		</fields>\n"
                "	</command>\n"
                "</operation>\n"
                "<operation  name=\"Listar_datos\" >\n"
                "<command id =\"1\" type=\"agregar\" table=\"lista\">\n"
                "	<fields>\n"
                "		<field type=\"filename\" mandatory=\"yes\" title=\"Cargar_archivo_flujo\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.xml);;Todos (*)\" changekey=\"yes\"  >\n"
                "			Cargar_archivo_flujo\n"
                "		</field>\n"
                "		<field type=\"combovar\" mandatory=\"yes\" validation=\"\" changefor=\"Cargar_archivo_flujo\" >\n"
                "			Variable\n"
                "		</field>\n"
                "	</fields>\n"
                "</command>\n"
                "</operation>\n"
                "</operations>\n"
        "";

        const QString DEFCONFIGUREXML = ""
                "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
                "<!DOCTYPE operations SYSTEM \"file:///home/user/.safet/dtd/yawlinput.dtd\">\n"
                "<operations suffix=\":\" commandname=\"operacion\" >  \n"
                "<operation  name=\"Values_to_store\" >\n"
                "<command id =\"1\" type=\"actualizar\" table=\"lista\">\n"
                "	<fields>\n"
                "		<field type=\"string\" mandatory=\"yes\" title=\"UseSensor\" primarykey=\"yes\">\n"
                "			storiesoptions.usesensor\n"
                "		</field>\n"
                "		<field type=\"string\" mandatory=\"no\" title=\"ShowInfoDialog\">\n"
                "			storiesoptions.showinfodialog\n"
                "		</field>\n"
                "		<field type=\"string\" mandatory=\"no\" title=\"TypeInfographic\">\n"
                "			storiesoptions.typeinfographic\n"
                "		</field>\n"
                "		<field type=\"string\" mandatory=\"no\" title=\"UseCamera\">\n"
                "			storiesoptions.usecamera\n"
                "		</field>\n"

                "		<field type=\"string\" mandatory=\"no\" title=\"savedqueries\">\n"
                "			generaloptions.savedqueries\n"
                "		</field>\n"
                "	</fields>\n"
                "</command>\n"
                "</operation>\n"
                "</operations>\n"
                "";

        const QString DEFFLOWXML = ""
                "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
                "<!--\n"
                "Documento  : deftrac.xml\n"
                "Creado     : V�ctor Bravo\n"
                "Autor      : Victor Bravo\n"
                "Descripcion: Archivo de Entrada para SAFET - Inflow\n"
                "-->\n"
                "<!DOCTYPE operations SYSTEM \"file:///home/user/.safet/dtd/yawlinput.dtd\">\n"
                "<operations suffix=\":\" commandname=\"operacion\"  >  \n"
                "<operation  name=\"Story\"  desc=\"Flujos de trabajo b�sicos\" icon=\"\">\n"
                "</operation>\n"
                "<operation  name=\"Create_new_story\" icon=\"qrc:/createicon.png\">\n"
                "	<command id =\"1\" type=\"agregar\" table=\"lista\">\n"
                "		<fields>\n"
                "		<field type=\"string\" mandatory=\"yes\" options=\"property::currinputmethodhints: Qt.ImhNoAutoUppercase\" title=\"Name_story\" validation=\"[a-zA-Z0-9_\s\-\\s]{2,18}\" >\n"
                "			Story_name\n"
                "		</field>\n"
                "        <field title=\"Live_story_name\" type=\"string\" mandatory=\"yes\" validation=\"[a-zA-Z0-9_\s\-\\s]{2,18}\">\n"
                "			Live_story_name\n"
                "		</field>\n"
                "		</fields>\n"
                "	</command>\n"
                "</operation>\n"
                "<operation  name=\"Delete_story\" icon=\"qrc:/delstoryicon.png\">\n"
                "	<command id =\"1\" type=\"agregar\" table=\"lista\">\n"
                "		<fields>\n"
                "			<field type=\"filename\" mandatory=\"yes\" title=\"Story_name\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.xml);;Todos (*)\"\n"
                "		options=\"load::flowfiles\">\n"
                "				Story_name\n"
                "			</field>\n"
                "		</fields>\n"
                "	</command>\n"
                "</operation>\n"

                "<operation  name=\"Add_step_into_story\" icon=\"qrc:/plus.png\">\n"
                "	<command id =\"1\" type=\"agregar\" table=\"lista\">\n"
                "		<fields>\n"
                "			<field type=\"filename\" mandatory=\"yes\" title=\"Story_name\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.xml);;Todos (*)\"\n"
                "		options=\"load::flowfiles\">\n"
                "				Story_name\n"
                "			</field>\n"
                "		<field type=\"combotask\" mandatory=\"yes\" validation=\"\\w+\" changefor=\"Story_name\" >\n"
                "			Previous_step\n"
                "		</field>\n"
                "		<field type=\"string\" mandatory=\"yes\" title=\"Name_step\" validation=\"[a-zA-Z0-9_\\s]{2,18}\">\n"
                "			Name_step\n"
                "		</field>\n"

                "		</fields>\n"
                "	</command>\n"
                "</operation>\n"
                "<operation  name=\"Delete_step_in_story\" icon=\"qrc:/minus.png\">\n"
                "	<command id =\"1\" type=\"agregar\" table=\"lista\" icon=\"qrc:/delete.png\">\n"
                "		<fields>\n"
                "			<field type=\"filename\" mandatory=\"yes\" title=\"Story_name\" desc=\"Cargar un archivo de flujo para generar un reporte\" filter=\"Archivos de flujo (*.xml);;Todos (*)\"\n"
                " options=\"load::flowfiles\">\n"
                      "				Story_name\n"
                "			</field>\n"
                "		<field type=\"combotask\" mandatory=\"yes\" validation=\"\\w+\" changefor=\"Story_name\" >\n"
                "			Name_step\n"
                "		</field>\n"
                "		</fields>\n"
                "	</command>\n"
                "</operation>\n"
                "</operations>\n"
                "";

        const QString YAWLINPUTDTD = ""
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

                "<!--\n"
                "    Document   : yawlinput.dtd\n"
                "    Created on : 21 de junio de 2010, 15:46\n"
                "    Author     : pbuitrago\n"
                "    Description:\n"
                "        Lenguaje para entrada de datos de SAFET\n"
                "-->\n"

                "<!-- TODO define your own vocabulary/syntax. Example follows:  -->\n"
                "<!ELEMENT operations (operation*) > <!-- operations va a tener como elemento: operation -->\n"

                "<!ATTLIST operations	suffix CDATA #IMPLIED \n"
                "			commandname CDATA #IMPLIED >\n"

                "<!ELEMENT operation (command*) > <!-- operation va a tener como elemento: command -->\n"
                "<!ATTLIST operation   	name CDATA #IMPLIED  \n"
                "			desc CDATA #IMPLIED\n"
                "			icon CDATA #IMPLIED >\n"

                "<!ELEMENT command (fields) > <!-- command va a tener como elemento: fields -->\n"
                "<!ATTLIST command       id CDATA #IMPLIED  \n"
                "                        type (actualizar|eliminar|agregar) \"agregar\"\n"
                "                        table CDATA #IMPLIED > \n"

                "<!ELEMENT fields (field+) > <!-- fields va a tener como elemento: field -->\n"
                "			  \n"

                "<!ELEMENT field (#PCDATA) > <!-- no tiene elemento -->\n"
                "<!ATTLIST field		type CDATA #IMPLIED   \n"
                "			mandatory CDATA #IMPLIED\n"
                "			title CDATA #IMPLIED\n"
                "			primarykey (yes|no) \"no\"\n"
                "			validation CDATA #IMPLIED \n"
                "			options CDATA #IMPLIED\n"
                "			function CDATA #IMPLIED \n"
                "			format (time_t|integer) \"time_t\" \n"
                "			desc CDATA #IMPLIED\n"
                "			literal CDATA #IMPLIED\n"
                "			sequence CDATA #IMPLIED\n"
                "			typesequence (next|current) \"next\"\n"
                "			input CDATA #IMPLIED\n"
                "			path CDATA #IMPLIED\n"
                "			visible CDATA #IMPLIED\n"
                " 			order (asc|desc|none) \"asc\" \n"
                "			icon CDATA #IMPLIED\n"
                "			index CDATA #IMPLIED  \n"
                "			filter CDATA #IMPLIED \n"
                "			repetible (yes|no) \"no\" > \n"
                "";
        const QString YAWLWORKFLOWDTD = ""
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

                "<!--\n"
                "    Document   : yawlworkflow.dtd\n"
                "    Created on : 22 de enero de 2008, 15:46\n"
                "    Author     : vbravo\n"
                "    Description:\n"
                "        Una definicion para gestionar archivos de flujos de trabajo (workflow)\n"
                "        con el lenguaje YAWL (Yet Another Workflow Language) \n"
                "        http://www.yawl-system.com dentro del proyecto SAFET :\n"
                "        Sistema Automatizado para Firma Electronica y Estampillado de Tiempo\n"

                "    TODO define vocabulary identification data\n"
                "    PUBLIC ID  : -//vendor//vocabulary//EN\n"
                "    SYSTEM ID  : http://server/path/yawlworkflow.dtd\n"
                "-->\n"

                "<!-- TODO define your own vocabulary/syntax. Example follows:  -->\n"
                "<!ELEMENT yawl (workflow,configuration*)>\n"
                "<!ATTLIST yawl\n"
                "version  CDATA #REQUIRED> <!-- must be \"0.01\"> -->\n"
                "<!ELEMENT workflow (token,(task|compositetask|variable|condition|parameter)*)>\n"
                "<!ELEMENT token (#PCDATA) >\n"
                "<!ATTLIST token     key CDATA #REQUIRED                    \n"
                "                    description CDATA #IMPLIED\n"
                "                    type (sql|file|webpage) \"sql\"\n"
                "                    config CDATA #IMPLIED\n"
                "                    keysource CDATA #REQUIRED>\n"
                "<!ELEMENT configuration (#PCDATA) >\n"
                "<!ATTLIST configuration  id CDATA #REQUIRED\n"
                "                         connectstring CDATA #REQUIRED>\n"
                "<!ATTLIST workflow  id CDATA #REQUIRED\n"
                "                    desc CDATA #IMPLIED>\n"
                "<!ELEMENT task (variable|port|autofilter|recursivefilter)*>\n"
                "<!ATTLIST task id CDATA #REQUIRED \n"
                "               title  CDATA #IMPLIED\n"
                "               numericinfo  CDATA #IMPLIED\n"
                "               textualinfo  CDATA #IMPLIED\n"
                "               multiplicity (unique|multiple) \"unique\"\n"
                "          type (atomic|composite) \"atomic\"\n"
                "          report (yes|no) \"no\"  >\n"
                "<!ELEMENT compositetask (workflow)>\n"
                "<!ELEMENT variable (document*)>\n"
                "<!ATTLIST variable  id CDATA #REQUIRED \n"
                "                    tokenlink CDATA #IMPLIED\n"
                "                    description CDATA #IMPLIED\n"
                "                    scope (net|task) \"net\"\n"
                "                    type (sql|file|webpage) \"sql\"\n"
                "                    usage (input|output|inout) \"input\"\n"
                "                    config CDATA #IMPLIED\n"
                "                    source CDATA #IMPLIED\n"
                "                    rolfield CDATA #IMPLIED\n"
                "                    timestampfield CDATA #IMPLIED\n"
                "                    documentsource CDATA #REQUIRED>\n"
                "<!ELEMENT document (#PCDATA)  >\n"
                "<!ATTLIST document  key CDATA #REQUIRED>\n"
                "<!ELEMENT connection (#PCDATA) >\n"
                "<!ATTLIST connection source  CDATA #IMPLIED\n"
                "		     query CDATA #IMPLIED\n"
                "		     options CDATA #IMPLIED\n"
                "		     tokenlink CDATA #IMPLIED>					 \n"
                "<!ELEMENT port (connection*) >\n"
                "<!ELEMENT autofilter (#PCDATA) >\n"
                "<!ATTLIST autofilter id CDATA #REQUIRED \n"
                "		side (bottom|down|up|forward) \"forward\"\n"
                "	       source  CDATA #REQUIRED\n"
                "               type (join|split)  #REQUIRED\n"
                "               query  CDATA #REQUIRED\n"
                "               subquery  CDATA #IMPLIED\n"
                "               options  CDATA #IMPLIED		\n"
                "               tokenlink  CDATA #IMPLIED\n"
                "               pattern (none|and|or|xor) \"or\"	       \n"
                "               report (yes|no) \"no\"\n"
                "               subreport (yes|no) \"no\"\n"
                "		>\n"
                "<!ELEMENT recursivefilter (#PCDATA) >\n"
                "<!ATTLIST recursivefilter id CDATA #REQUIRED \n"
                "		side (bottom|down|up|forward) \"forward\"\n"
                "	       source  CDATA #REQUIRED\n"
                "               type (join|split)  #REQUIRED\n"
                "               query  CDATA #REQUIRED\n"
                "               options  CDATA #IMPLIED		\n"
                "               tokenlink  CDATA #IMPLIED\n"
                "               pattern (none|and|or|xor) \"or\"\n"
                "	       initial CDATA #REQUIRED\n"
                "	       filter CDATA #REQUIRED\n"
                "               report (yes|no) \"no\"  \n"
                "	       onlyfathers (yes|no) \"no\" >\n"

                "<!ATTLIST port side (bottom|down|up|forward) #REQUIRED\n"
                "               type (join|split)  #REQUIRED\n"
                "               query  CDATA #IMPLIED\n"
                "               tokenlink  CDATA #IMPLIED\n"
                "               options  CDATA #IMPLIED\n"
                "               drop    CDATA #IMPLIED  \n"
                "	       config CDATA #IMPLIED\n"
                "               pattern (none|and|or|xor) \"none\">\n"
                "<!ELEMENT condition (port+) >\n"
                "<!ATTLIST condition id CDATA #REQUIRED\n"
                "               numericinfo  CDATA #IMPLIED\n"
                "               textualinfo  CDATA #IMPLIED \n"
                "               type (start|end|inprocess) \"inprocess\"\n"
                "            report (yes|no) \"no\"  >\n"
                "<!ELEMENT taskid (#PCDATA)>\n"
                "<!ELEMENT name (#PCDATA)>\n"
                "<!ELEMENT link (#PCDATA)>\n"
                "<!ELEMENT source (#PCDATA)>\n"
                "<!ELEMENT connectstring (#PCDATA)>\n"
                "<!ELEMENT parameter (#PCDATA)  >\n"
                "<!ATTLIST parameter	type CDATA #IMPLIED   \n"
                "			mandatory CDATA #IMPLIED\n"
                "			title CDATA #IMPLIED\n"
                "			primarykey (yes|no) \"no\"\n"
                "			validation CDATA #IMPLIED \n"
                "			options CDATA #IMPLIED\n"
                "			function CDATA #IMPLIED \n"
                "			format (time_t|integer) \"time_t\" \n"
                "			desc CDATA #IMPLIED\n"
                "			literal CDATA #IMPLIED\n"
                "			sequence CDATA #IMPLIED\n"
                "			typesequence (next|current) \"next\"\n"
                "			input CDATA #IMPLIED\n"
                "			path CDATA #IMPLIED\n"
                "			visible CDATA #IMPLIED\n"
                " 			order (asc|desc|none) \"asc\" \n"
                "			icon CDATA #IMPLIED\n"
                "			index CDATA #IMPLIED  \n"
                "			filter CDATA #IMPLIED \n"
                "			repetible (yes|no) \"no\"\n"
                "			configurekey CDATA #IMPLIED \n"
                "			defaultvalue CDATA #IMPLIED > \n"

                "";


}


class MainWindow : public QObject 
{

    Q_OBJECT



    QString _currenterror;

  //  QToolBar *standardbar;
  ////  QTextEdit *myoutput;
//    GraphicsWorkflow *gviewoutput;
    QLabel *titleApplication;

   // QStandardItemModel *listviewoutputmodel;
//    QWebView* weboutput;
    QString _dirInput; // Directorio de archivos imágenes, iconos, archivos utilitarios

    // *** Valores de formato para listado y gráfico de flujo
    QString _listprincipaltitle;    
    int _listprincipalcount;
    QString _listprincipalvariable;
    QString _listprincipalkey;


    // *** Valores de formato para listado y gráfico de flujo
    QString  _pathdoc; // Archivo de entrada para generar la consulta
    int verbose_flag;
    bool parsed;

    bool _hastories;

    QMap<int,QString> commands;
    //QMap<QString,QString> _permiseshierarchy; // jerarquia de permisos

    QStringList filecompleters;

    static QStringList _qmlpages;


    QString _lasttapandhold;
    friend class ThreadConsole;

    bool initApp; // para iniciar la aplicacion
    bool _issmartmenuvisible;

     QMutex *mutexconsole;
     bool runningConsole;



    // Cadena que mantiene la ruta completa de un documento SafetDocument
    // cargado con SafetDocument::readDigidocFile
    QString pathOfSafetDocument;

     bool _checkgoprincipal; // Chequeo para habilitación de barra de botones




    bool _isloadeditactions; // Se cargaron las acciones

    // indicador de progreso
    //QProgressIndicator* progressIndicator;


    /** variable para thread
    *
    */
    //static ThreadConsole myThreadConsole;


    bool _isgui;
    //QMap<QString,QStringList> users;

    QString _currentjson;
    QString _inputpath;
    QString _keypath;

     uint _wfnumpars;
     QQueue<QString> _qmlCommandList;
     QQueue<QString> _savedQmlCommandList;
public:

    // Estructura utilitaria para listar documentos por clave



    // indicador de progreso
//    QProgressIndicator* progressIndicator;

    // Enumeraciones


    // Datos estaticos
    static SafetYAWL *configurator;
    static MainWindow *mymainwindow;
    //static QStringList sbmenulist;
    static QString _lasttitle;
    static QString _lastoperation;

    static bool _issigning;
    static QString _currentCommonName;

    // puntero a Thread
//    static InflowThread * mythread;

    static QString showString;
    static int _lastnumbersteps;


    static int _seconstohide; // Segundos para ocultar panel de resultados
    static QTimer _panelinfotimer;
    static QString _currconfpath; // Ruta de acceso para el directorio .safet p.e. /home/vbravo
    QString _mediapath;
    QString _templatepath;




    static QMap<QString,QPair<QString,QString> > _graphs;

    // Primer campo: nombre de la operacion
    // Primer elemento del QStringList corresponde a los usuarios para esa operación
    // Segundo elemento del QStringList corresponde a las acciones que pueden ejecutar los usuarios
    // Tercer elemento del QStringList corresponde al SHA1 (firma de la operacion)

    static QMap<QString,QStringList> permises; // Estructura para guardar los permisos

    static QString currentaccount; // cuenta actual de usuario
    static QString currentrole; // rol de la cuenta actual de usuario
    static QString _lastchangekey;
    // Funciones
    /**
      \brief ejecuta un funcion de safet segun los parametros que se encuentran en memoria
      (como consola)
     \return retorna true si se ejecutó correctamente, false en caso contrario
     */
    bool executeParsed();
    /**
      \brief Retornando JSON Actual
      */
    QString currentJSON();

    bool parse(int &argc, char **argv);
    bool parseArgs(const QString& a);

    void refreshListView(const QString& doc);
    void buildModel(QMap<QString,QVariant>& mymap);

    bool isGui() const { return _isgui;}

    QStringList& getFileCompleters() { return filecompleters;}
    /*
      \brief Funcion que revisa la pila de errores y devuelve Verdadero (true)
             si existen errores. También muestra el panel de errores con los mensajes de errores
      \return Verdadero (true) si existen errores en la pila, Falso (false) de otra forma
      */
    bool queryForErrors();

public:
    MainWindow(const QString& path = QLatin1String(""));
    ~MainWindow();
    void setupToolbar();
    void setupTabWidget();


   void loadAuthorization(); 
   void loadInfo();
   
   bool login(const QString& name, const QString& pass);
   bool logout(); 

   void setMediaPath(const QString& m);
   QString mediaPath();

   void setTemplatePath(const QString& t) { _templatepath = t; }
   QString templatePath() { return _templatepath;}

   void setKeyPath(const QString& m);
   QString keyPath();

   QMutex* mutexConsole() { Q_CHECK_PTR(mutexconsole); return mutexconsole;}


    /**
     ** \brief Obtiene la plantilla de estilo principal
     * \return Plantilla de estilo en formato CSS
     */

    QString getPrincipalCSS() const;


    /**
    * \brief Borra todos los archivos que se encuentran en el directorio d
      \param d Directorio donde se eliminarán los archivos
    */
    static void doDelAllFilesOnDirectory(const QString& d);
    /**
    * \brief Funcion delegada (Callback) para gestionar un error
    */
    static void evalEventOnExit(SafetLog::Level);


    /**
      * \brief Retorna el mapa de los grafos visibles (nombre, codigo para ser generado)
      */
    static QMap<QString,QPair<QString,QString> >& graphs() { return _graphs; }

    /**
     * \brief Evalua si la firma de una operacion es válida segun un determinado permiso
              (read,view,modify)
        \param op Firma de la operación
        \param permise Tipo de permiso (read,view,modify);

     */
     static bool doPermiseExecOperation(const QString& op, QMap<QString,QString>& phierarchy,
                                        const QString& permise = "read");

     /**
      * \brief Evalua si la firma de una operacion es válida segun un determinado permiso
               (read,view,modify)
         \param op Firma de la operación
         \param permise Tipo de permiso (diferentes tipos de lecturay);
         \return mapas de tipo de accion (claves) -> permiso para el usuario actual

      */
     static QMap<QString,bool> doPermiseExecOperationAction(const QString& op);

        /*
          *  \brief Coloca el conjunto de comandos para ser utilizado en la consola
          * \param cmds conjunto de comandos
          */
        void setCommands(QMap<int,QString>& cmds) { commands = cmds;}

    /**
    * \brief Funcion delegada (Callback) para gestionar la entrada
    */
    static QString evalEventOnInput(SafetYAWL::TypeInput,const QString&, bool&);

    static void doRestoreGraph(const QString& gfn = Safet::GRAPHSFILENAME);



    static QStringList uncompresstar(const QString &filename);


    /**
      \brief Escribe en disco una de los archivos plantilla (deftrac.xml, safet.conf);
      \param filename Nombre del archivo a crear
      \param tpl texto de la plantilla a escribir Safet::SAFETCONF,Safet::AUTHCONF
      \return true si no hubo errores, false en caso contrario
      */
    static bool writeDefaultFile(const QString &filename, const QString &tpl);


     // Metodos para la Consola
    void listTasks();
    void listDocuments(const QString& key);
    void listDocuments();
    void manageData();
    bool genGraph();
    void signDocument();
    void calStatistics();
    /**
        * \brief Muestra la ayuda del uso de la aplicacion de consola safet
        */
    void displayUsage();

    void version();

    /*
          * \brief Genera una Consola (Shell)
          * \param command número de parámetro
          * \return true si el resultado fue exitoso, falso en caso contrario
          */
    bool  processCommand(int command);

    /**
       \brief Dada una sentencia SQL (sql) se procesa un archivo de texto ( por defecto se procesa safet.conf)
              modificando las diferentes opciones seg�n  la expresi�n SQL
       \param sql Sentencia SQL (INSERT, UPDATE, etc..)
       \param filename ruta completa  del archivo de configuraci�n, si no se especifica se toma por defecto safet.conf
       \param multiplefields Admite sustituciones de campo multiples terminados en ".*"
       \param numberreg Número de registro a cambiar
       */
    void proccessConfFile(const QString& sql, const QString& filename = "", bool multiplefields = false);
    void setCommands(const QMap<int,QString>& cmds) { commands = cmds; }
    void setModelCompleter(int opt);
    bool loadWidgetPlugins(const QString& f, bool add = false);
    void loadSettings();
    void writeSettings();
    QString evalJS(const QString &js);


    /**
      \brief Traduce el contenido de la pila "stack" a una cadena en formato HTML, en funcion
      de del tipo de mensaje denotado por "l"
      \param stack Pila de mensajes, al finalizar esta pila estara vacia
      \param l Tipo del mensaje (Error, Warning, Action,Debug)
      \return Cadena en formato HTML que contiene hora y texto de cada mensaje
      */
    QString renderMessageStack(QStack<QPair<QDateTime,QString> >& stack, SafetLog::Level l);

    /**
      * Verificaciones
      */
    void successVerification(QStringList list = QStringList(), const QString& msg = "");

    /** Funcion agregada para eliminar dependencia de digidocpp realiza la verificacion
    * de un contenedor. Se utiliza para desplegar el widget SafetSignatureWidget
    * \param list lista de cadena
    * \param msg mensaje
    * \param doc objeto SafetDocument asociado al contenedor
    */
    void successVerification(QStringList list, const QString& msg, SafetDocument doc);


    /**
      Copia los archivos iniciales para el directorio "home" / .safet
      **/
    void copyInitialFiles();


    //QStackedWidget* getStackedWidget() { return stackWidget;}

    /**
      \brief Cifrar o descifrar  un archivo indicado por pathname
      \param pathname ruta completa del archivo a cifrar
      \param operation 0 cifrar y 1 para descifrar
      */
    void doCipherFile(bool iscipher = false);


    /**
      \brief Comparar dos gráficos (first,second) y generar un tercero con la diferencia de fichas
             entre los dos
      \param first Fuente del primer gráfico
      \param second Fuente del segundo gráfico
      \return Fuente del gráfico con la estadística
      */
    QString generateGraphCompared(const QString& first, const QString& second);


    /**
      \brief Dibuja el gráfico especificado por "code" en la pestana de flujos de trabajos (Gráfico)
      y le agrega la etiqueta de fecha "datetag"
      \param code Código del gráfico a dibujar
      \param datetag Etiqueta de la fecha
      */
    void doRenderGraph(const QString& code, const QString& datetag);

    /**
      \brief Habilita/Deshabilita los botones enviar de las pestanas una vez que se conecta
      a la fuente de datos correctamente
      **/
    void setEnableCompletingButtons(bool b);



    /**
      * \brief Chequea la lista de argumentos que se pasa en un cuadro de edición , según la
                lista colocada en el archivo de configuración (widgets.arguments.*)
      * \param s Cadena a chequar en el formato <campo>:<value>
      */
    static void checkSafetArguments(const QString &s);


    /**
      \brief Carga la plantilla que se muestra en la pestaña de "Reportes". Buscar la  etiqueta <SAFET/>
      \param json texto a reemplizar
      \param filename ruta completa de la plantilla
      */
    QString loadReportTemplate(const QString& json, const QString &filename = QString(""),
                               const QString& nameoperation = QString("Listar_datos"));



    QStringList autoComplete(const QString &path);



    /**
      \brief Obtiene algunos valores de un formulario HTML de la operacion "o" filtrado por
            clave keyvalue
      */
    QString formFieldsForKey(const QString &o, const QString& fieldname,
                             const QString &keyvalue, DomModel* mymodel);
    /**
      \brief Generar una cadena en lenguaje HTML que contiene el registro encontrado seg�n los parametros
      operation, fieldname y key
      Devuelve una cadena vac�a si no consigue el valor
      */
    QString generateModifyHTML(const QString &operation, const QString &fieldname, const QString &key);
    /**
      * \brief Agrega el texto HTML para los par�metros seg�n la operaci�n "nameoperation"
      * \return texto HTML con cuadro  de di�logo basado en jquery.ui
      */
    QString addParametersDialog(const QString &nameoperation);
    /**
      \brief Devuelve valor interno (innerHTML, JQuery) para el cuadro di�logo de par�metros de SAFET

      */
    QString getFlowParameters(const QString &flowfilename);
    QString getWidget(SafetParameter *p); // Obtener el objeto HTML del widget seg�n el par�metro
    /**
      * \brief Cargar las acciones o Consultas guardardas
      * \return Lista de acciones
      */
    QMap<QString,QString> loadEditActions();

    /**
      * \brief Genera un men� con los enlaces a los m�dulos principales
      */
    QString generateModulesMenu();

    /**
      * \brief Convierte una operacion del tipo operacion:nombre_n... a un t�tulo
               Sin caracteres "_" y sin la palabra "operacion:"
               */
    static QString convertOpToTitle(const QString &op);
    QString returnFormLink(const QString& nameoperation); // "Retornar a Consulta"

    /**
      \brief Registrar el ingreso de un usuario al sistema
      \param user nombre del usuario
      */
    void registerLogin(const QString &user);

    /**
      \brief Registrar la salida de un usuario al sistema
      \param user nombre del usuario
      */
    void registerLogout(const QString &user);

    /**
      * \brief Escribir un mensaje (message) en el registro de eventos
      */
    void log(const QString& message);
    /**
      ** \brief reemplaza marcas como ##SAFETCOLON## (Safet::COLONMARK)
      */
    static QString replaceMarks(const QString &s);


    /**
      \brief Normalizar las rutas de las im�genes
      */
    static QString normPicture(const QString &p, const QString fieldname = QString("source"));

    /**
      * \brief
      * \return Retorna dos par�metros, el primero el c�digo del grafo,
            el segundo la hora y fecha que se realiz� el grafo
            */
    QStringList lastInfoGraph();
    /**
      * \brief  Borra un gr�fico del archivo por defecto (graphs.gph)
      * \param n nombre del gr�fico a borrar
      * \return true si el gr�fico fue eliminado, false en caso contrario
      */
    bool deleteGraph(const QString &n, const QString& gfn = Safet::GRAPHSFILENAME);

    /**
      * \brief  Guarda lo que est� contenido en en el mapa est�tico graphs()
      * \param gfn nombre del repositorio de grafos (sin extensi�n)
      * \return true si el gr�fico fue eliminado, false en caso contrario
      */
     bool doSaveCompleteMapGraphs(const QString &gfn = Safet::GRAPHSFILENAME);
    /**
      \brief Crea un registro de grafo en el archivo de grafos planificados (~/.safet/graphs/plannedgraphs.gph)
      \param pathflow ruta completa del archivo de flujo de trabajo
      \param name Nombre del grafo
      \return true si la acci�n fue exitosa, false en caso contrario
      */
    bool doGeneratePlannedGraph(const QString &pathflow, const QString &name);

    /**
      \brief Modifica la fecha de un codigo de grafo
      */
    QString doModifyDateGraph(const QString &nametask, const QDateTime &mydate, const QString &code);

    /**
      *  \brief Obtener un mapa con las tareas y fecha del grafo guardado con nombre "namegraph"
      */
    QMap<QString, QDateTime> doGetPlannedGraph(const QString &namegraph);
    /**
      Cargar jerarquia de permisos
      */

    static QMap<QString,QString> loadPermisesHierarchy();
    /**
      \brief chequea si el permiso tiene jerarquia
      */
    static bool checkPermiseInHierarchy(const QString &permise, const QMap<QString, QString> &myphierarchy, const QString &realtypes);
    /**
      \brief extrae los parametros de una consulta (todos los parametros que tenga como prefijo
      la palabra "parameters."
      */
    QString extractParameters(const QString &action);
    /**
      \brief Funciones para la gestion de archivos de SafetWorkflow
            add - agregar estado
            del - borrar estado
            modify - modificar estado

      */
     QString addNodeToXMLWorkflow(const QString &fname,
                                 const QString& beforenode = QString(),                                  
                                 const QString &nodename = QString("newnode"),
                                 bool isparallel = false,
                                 const QString &options = QString(),
                                 const QString &query = QString(),
                                 const QString &nodetitle = QString(),
                                 const QString &documentsource = QString()
                                  );


     /**
       \brief
       \param fname Nombre del flujo del trabajo
       \param nodename Nombre del nodo a eliminar
       \return nombre del archivo modificado (flujo de trabajo) generalmente "fname"
       */
     QString delNodeToXMLWorkflow(const QString &fname, const QString &nodename);

     /**
       \brief
       \param fname Nombre del flujo de trabajo
       \param currnode Nodo a cambiar la conexi�n
       \param nextnode proximo Nodo la conexi�n a cambiar
       \param newnode Nuevo Nodo a la que se cambiar� la conexi�n
       \param  nombre del archivo modificado (flujo de trabajo) generalmente "fname"
       */
     QString changeConnXMLWorkflow(const QString &fname,
                                   const QString &currnode,
                                   const QString &nextnode,
                                   const QString &newnode,
                                   const QString& newoptions = QString(),
                                    const QString&  newquery  = QString());

     /**
       \brief Agrega un nuevo grupo de elementos al menu de formulario
              que corresponde con newff
       */
     bool addXmlMenuGroupForm(const QString &newff, const QString& ncmd = tr("New_step"));

     /**
       \brief Elimina un elemento del menu de formulario
              que corresponde con newff
       */
     bool delXmlMenuGroupForm(const QString &newff, const QString& ncmd = tr("New_step"));

     QMap<QString,QDateTime> getDatesGraph(const QString& code);




     /**

            \brief Reinicia el directorio ~/.safet/ (Versi�n Stories)
             \return retorna verdadero si logr� crear todos los
archivos necesarios, falso en caso contrario
            */













     /**
       \brief Chequea si el campo es una fecha (Espresion regular) y lo conviete a formato timeT
       */
     static QString toTime_t(const QString &v, const QString& exp = "'\\d\\d/\\d\\d/\\d\\d\\d\\d \\d\\d:\\d\\d:\\d\\d'");

public Q_SLOTS: // Slots para el manejo de los servicios de Escritorio (QDesktopServices)

     // Funciones est�ticas
     static QString unconvertTitle(const QString &t);

     QString photoGallery();

     bool toBatch();


     /**
       \brief Chequea si una opci�n est� prendida "on/yes" o apagada "no/off"
       \param op Opcion a consultar formato por ejemplo "Reports/general.template"
       */
     bool checkBoolOption(const QString &op);
     /**
       \brief Coloca el texto para mostrarle la camara al usuario

       */
     QString declarativeCamera(const QString &action, const QString &namelive, const QString &namestep);


     void changeStoriesParams(const QString &usesensor, const QString &infodialog, const QString &typeinfog,
                              const QString& usecamera);

      /**
        \brief Carga algunas muestras de stories
        */
     bool loadSamples();
     bool checkLoadSamples();


     static  QString tsFromFile(const QString &path);

     static QString extractFileName(const QString &p);

     static QString lastChangekey() { return _lastchangekey; }
     static void setLastChangekey(const QString& s) { _lastchangekey = s; }

     //bool reachStepLimit() { qDebug("n:%d",MainWindow::_lastnumbersteps); return false; }
     bool reachStepLimit() { return MainWindow::_lastnumbersteps >= SAFETREACHLIMIT?false:true; }

     int steps() { return MainWindow::_lastnumbersteps; }
     /** \brief  Busca en la base de datos la lista de la ultimas consultas y la muestra en el menu

       **/
     QString generateLastStoriesMenu();

     bool hasStories()  { return _hastories; }

     void setLastTapAndHold(const QString& t) { _lasttapandhold.isEmpty()?_lasttapandhold = t: _lasttapandhold = ""; }
     QString lastTapAndHold() { return _lasttapandhold; }

     void saveQmlCommandList() { _savedQmlCommandList = _qmlCommandList; }
     void restoreQmlCommandList() { _qmlCommandList = _savedQmlCommandList; }
     void addQmlCommandList(const QString& cmd) {
         qDebug("............MainWindow::addQmlCommandList....cmd:|%s|\n\n",qPrintable(cmd));
         if ( !_qmlCommandList.contains(cmd)) {
                _qmlCommandList.enqueue(cmd);
         }

     }
     void listQmlCommandList();
     void clearQmlCommandList() { _qmlCommandList.clear(); }
     bool emptyQmlCommandList() { return _qmlCommandList.empty(); }
     QString dequeueQmlCommand() {

         QString myresult = (_qmlCommandList.empty())?QString(""):_qmlCommandList.dequeue();
         qDebug("............dequeueQmlCommand...myresult:|%s|",qPrintable(myresult));
         return myresult;
     }
     int lenQmlCommandList() { return _qmlCommandList.length(); }
     /**
        \brief Genera el modelo (ListModel/QML)  para el menu principal
        \return El modelo generado para el menu principal
       */
    QString generatePrincipalMenu();

    bool clearAllInfoStories();

     void savePage(const QString& value);
     int countPages()  { return _qmlpages.count(); }
     QString qmlPage(int pos);
     void clearPages();

     void setLasttitle(const QString& l) { MainWindow::_lasttitle = l; }
     QString lasttitle() { return _lasttitle; }
     void setLastOperation(const QString& l) { MainWindow::_lastoperation = l; }
     QString lastOperation() { return _lastoperation; }

     QString checkIfDateTime(const QString &d);
     void grabQML(QDeclarativeView& comp);
     /**
       \brief Genera informaci�n imagen / texto para un determinado estado de la historia
       */
     QString generateStepInfo(const QString &ref, const QString& nodename, int typepage = 0,
                              const QString& title = QString("Infographic"),int posinit = 0, bool lastpage = true);

     QStringList testQML();

     /**

       Retorna la ruta completa del archivo de entrada que se est� procesando
       */
     QString inputPath() { return _inputpath;}
     void setInputPath(const QString& p) { _inputpath = p;}


     void browse( const QUrl &url );
     void mailTo( const QUrl &url );

     void toSend(bool sign = false);

     void doLoadFTP();
     void doSaveFTP();

     /**
       \brief  Busca los valores de los campos a modificar cuando selecciona un objeto QML
      **/
     QString generateModifyQML(const QString &operation, const QString &fieldname, const QString &key);


     /**
              * \brief Obtener una cadena HTML, con el men� para acceder
                a las operaciones de Formulario
     **/
     QString menuCommands(const QString& currfile = QString());
     /**
       \brief genera el texto HTML Cabecera correspondiente a la operaci�n que se�alada en "o"
       */
     QString generateFormHead(const QString &o = QString(), const QString& mymodule = QString(), const QString& mydefault = QString());

     /**
       \brief genera el texto HTML de final de p�gina correspondiente a la operaci�n que se�alada en "o"
       */
     QString generateFormFooter(const QString &o = QString());

     /**
       \brief Obtiene el formulario para la operaci�n especifica como "o"
       */
     QString menuForm(const QString &o);

     QString toInputForm(const QString& action, const QString& textquery = QString(""));
     QString toInputConsole(const QString& action);
     QString toInputConfigure(const QString &action);
     QString toInputFlow(const QString &action);


     /**
            \brief Muestra el �ltimo o los �ltimos errores que se presentaron en la ejecutaci�n
            por ejemplo con toInputForm() o toInputConsole()
            **/
     QString currentError();
     void setParsValues(const QMap<QString, QString> &values);
     void evalParsValues(SafetWorkflow *wf);
    /**
      *\brief Colocar valores de configuraci�n intermedios (Conffile)
      *\param values Valores seg�n formato <Seccion>/<Clave> que se pasa directo al mapa de configuracion

      */
    void setConffileValues(const QMap<QString,QString>& values);

    /**
        \brief Agregar Fecha y Hora seg�n formato
        \return Retorna Cadena con la fecha y/o hora
        */
    QString addInfoGraphDateText();

    /**
      \brief Devuelve el t�tulo actual colocado en el archivo safet.conf
      \return Retorna texto colocado en (generaloptions.currentflowtitle)
      */
    QString currentGraphTitle();

    /**
      \brief Funciones para guardar, restaurar y comparar grafos
      \param mypars recibe tres parametros para guardar en el archivo,
      el primer es el nombre del grafo, el segundo es el c�digo del grafo y
      el tercero es la etiqueta de fecha.
      \return true si se logr� guardar el grafo, false en caso contrario
      */

    bool doSaveGraph(const QStringList& mypars);

    QString doCompareGraphs(const QString& firstgraph, const QString& secondgraph);





private slots:

    void timeHideResult();

    void about();

    void toLoadWeb();

    void toInputSign();

    void toInputUsers();
    void toDelAllWorkflow();
    void toDelOneWorkflow();
    void toClearTextEdit();
//    void selInputCombo(int);
    void checkSelInputTab(int opt);
    void selInputTab(int);
    void doQuit();
    void setToInputForm();
    void setToInputConsole();
    void setToInputReports();
    void setToInputFlowGraph();
    void setToInputManagementSignDocument();
    void doGetSignDocument();
    void doSendSignDocument();
    void checkGoPrincipal();

    void addToHistoryList();
    void editToHistoryList();
    void delToHistoryList();
    void saveToHistoryList();


  //  void insertFromHistoryList(QListWidgetItem *);
    void showSmartMenu();
    void showSuccessfulMessage(const QString& m);

    QString drawWorkflow(const QString& filename);


    void toChangeUser();
    void doLoadConfiguration();
    void doSaveConfiguration();

    // establece la ruta completa de un documento SafetDocument
    // cargado con SafetDocument::readDigidocFile
    void setPathOfSafetDocument(QString path) { pathOfSafetDocument = path; }

    // retorna la ruta completa de un documento SafetDocument
    // cargado con SafetDocument::readDigidocFile
    QString getPathOfSafetDocument() { return pathOfSafetDocument; }

    void doInsertInAuthConfFile(QRegExp& rx); // Insertar un grupo de campos en una sección
    /**
     \brief Busca los campos que coinciden con la clave key y devuelve los
            nombres separados por ","
     \param key Clave en el archivo de autorización (auth conf) de la
     \return Campos separados por el caracter ","
     */
    QString searchFieldsInAuthConf(const QString& key);

public slots:
    void linkClickedSbMenu(const QUrl& url);
    void linkClickedSbResult(const QUrl& url);
    void setEnabledToolBar(bool e = true); // Habilitar o Desahabilitar barra de herramientas
    void doExit();
    /**
      \brief Funcion para preguntar antes de salir
      */
    bool maybeSave();
    /*
      * \brief va a la pantalla principal
      */
    void goPrincipal();



    // slot para ejecutar acciones cuando se termina de ejecutar el thread de MainWindow
    void threadEndJob();

    // se encarga de ejecutar executedParsed() a traves del thread de MainWindow
    void processMainWindowThread();

    // slot para desplegar el dialogo para modificar la configuracion de la fuente de datos
    void manageDataSources();

protected:
    void saveWindowState();
    void restoreWindowState();

    void closeEvent(QCloseEvent *event);    
    void moveEvent(QMoveEvent *event);
    void showEvent(QShowEvent *event);


    QPoint m_lastPos;
    QSize m_lastSize;

    QString outputText;
    QTextStream streamText;
    void sendStreamToOutput();
    void iterateMap(const QMap<QString,QVariant>& map);
    void createDockWindow();    
    void createDockShowResultWindow();
    virtual void resizeEvent ( QResizeEvent * event );    
    void buildMenuOnPanelsbMenu(const QString& option);


    /**
       *\brief Genera la lista de archivos (en "myfiles") para un paquete tar
       *\param folder Nombre de ruta completa de la carpeta para generar el archivo tar
       *\param myfiles parámetro de salida con la lista de archivos agregada
       *\param exts Lista de extensiones para cargar los archivos (xml,png,html,etc...)
       *\return nombre del archivo .tar
       */
    QString generateListForTar(const QString& folder, QStringList& myfiles,
                               const QStringList& exts);


    /**
      \brief Genera una cadena en formato JSON dado un conjunto de hitos
      \param miles Lista de hitos ordenados por fecha (orden cronologico)
      \param Parametro de entrada (campos que se agregan en el documento JSON)
      \return cadena JSON
      */

    QString getJSONMiles(const QList<SafetYAWL::Miles>& miles,QList<QSqlField>& myfields);
private:

    /**
      * \brief Evalua el mapa de configuraciones actual (_currconfvalues)
      */
    void evalConffileValues();
    /**
      * \brief Procesa un archivo tar (f) y descomprime los archivos en
      el directorio de configuración por defecto
      * \param f archivo con extensión .tar a descomprimir
      * \param isprincipal si es el directorio principal
      * \return retorna verdadero si la operación fue exitosa, falso en caso contrario
      */
    bool processDirTar(const QString& f, bool isprincipal = false);
    bool searchInHistoryList(const QString& str);
    QString getScriptLen(const QSqlField& f); // Calcular la longitud del campo para la tabla generada en Listar_Datos

    void createMenu();
    void configureStatusBar();
    QAbstractItemModel *modelFromFile(const QString& fileName);

    void generateJScriptreports(const QString& documents, const QList<QSqlField>& fields);
    void setupStackedWebviews(const QIcon& icon, const QString& name, const QString& desc = QString(""));


//    QListWidget *listEditForm, *listEditCons, *listEditSign, *listEditConf;
//    QToolButton *buttonListEditConsAdd,*buttonListEditConsEdit, *buttonListEditConsDel;
//    QToolButton *buttonListEditConsSave;
//
//    QCommandLinkButton* completingButtonForm;
//    QCommandLinkButton* cancelButtonForm;
//    QCommandLinkButton* showdockButtonForm;
//
//    QCommandLinkButton* completingButtonCons;
//    QCommandLinkButton* cancelButtonCons;
//    QCommandLinkButton* showdockButtonCons;
//
//    QCommandLinkButton* completingButtonSign;
//    QCommandLinkButton* cancelButtonSign;
//    QCommandLinkButton* showdockButtonSign;
//
//
//    QCommandLinkButton* completingButtonConf;
//    QCommandLinkButton* cancelButtonConf;
//    QCommandLinkButton* showdockButtonConf;
//
//    QCommandLinkButton* completingButtonUsers;
//    QCommandLinkButton* cancelButtonUsers;
//    QCommandLinkButton* showdockButtonUsers;
//
//    //    QComboBox *comboMode;
//    QTabBar *centralBar;
//    QTabWidget *centralWidget;
//    QStackedWidget *stackWidget;
//    QList<QWebView*> stackedwebviews;

//    PrincipalFrame *principalFrame;

    QCmdCompleter *completer;

    // Utilizado para reporte en web
    QString jscriptarray;
    QString jscriptcolumns;
    bool jscriptload;
    QString currentDocuments;
    QList<QSqlField> currentFields;

    // para mostrar el Assistant

    // Objeto SafetDocument asociado a MainWindow
    //SafetDocument safetDocument;

    QMap<QString,QString> _currconfvalues;
    QMap<QString,QString> _currparsvalues;

  private slots:
    void executeJSCodeAfterLoad( /*bool ok */ );



};


#endif // MAINWINDOW_H
