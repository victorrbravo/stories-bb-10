#include "inflowfirmadoc.h"
#include "ui_inflowfirmadoc.h"
#include <QWebView>
#include <QWebFrame>

InflowFirmaDoc::InflowFirmaDoc(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::InflowFirmaDoc)
{
    m_ui->setupUi(this);
    jscriptload = false; // No cargar el script
    _listprincipaltitle = "";
    _listprincipalcount = 0;

    m_ui->webDocument->page()->mainFrame()->setScrollBarPolicy(Qt::Horizontal,
                                                               Qt::ScrollBarAlwaysOff);

    connect(m_ui->webDocument->page(), SIGNAL(loadFinished (bool)), this,
            SLOT(executeJSCodeAfterLoad(bool)) );

    connect(m_ui->buttonImprimir,SIGNAL(clicked()),this,SLOT(doPrint()));
    connect(m_ui->buttonVerPDF,SIGNAL(clicked()),this,SLOT(viewpdf()));
    connect(m_ui->buttonGuardar,SIGNAL(clicked()),this,SLOT(savepdf()));


}

InflowFirmaDoc::~InflowFirmaDoc() {
    delete m_ui;
}

void InflowFirmaDoc::doPrint() {
    qDebug("...InflowFirmaDoc::doprint....");
    QPrintDialog dialog(&printer, this);

    if (dialog.exec()) {
        m_ui->webDocument->print(&printer);
    }

}

void InflowFirmaDoc::savepdf() {
    QString myfile = QFileDialog::getSaveFileName(this, tr("Guardar archivo a firmar"),
                                                  QDir::homePath()+ "/reportesafet.pdf",
                               tr("Documentos PDF (*.pdf)"));
    printer.setOutputFileName(myfile);
    printer.setPrinterName("Print to File (PDF)");
    printer.setOutputFormat(QPrinter::PdfFormat);
    m_ui->webDocument->print(&printer);

}

void InflowFirmaDoc::viewpdf() {

    QString  myfile = SafetYAWL::getTempNameFiles(1).at(0)+".pdf";
    qDebug("...viewpdf...:|%s|",qPrintable(myfile));
    printer.setOutputFileName(myfile);
    printer.setPrinterName("Print to File (PDF)");
    printer.setOutputFormat(QPrinter::PdfFormat);
    m_ui->webDocument->print(&printer);
    QDesktopServices::openUrl(myfile);


}

void InflowFirmaDoc::loadSignersInfo(const QList<SafetWorkflow::InfoSigner>& l) {
    m_ui->listSigners->clear();
    for(int i=0; i < l.count(); i++) {

        QListWidgetItem *myitem = NULL;
        QString infosigner = "[ " + l.at(i).commonName +  " ] /" + l.at(i).issuer + "/"+
                             l.at(i).date+" "+l.at(i).hour+"/"+l.at(i).location;

        if ( l.at(i).isvalid ) {
            myitem = new QListWidgetItem(QIcon(":/seal.png"),infosigner);
        }
        else {
            myitem = new QListWidgetItem(QIcon(":/del.png"),infosigner);
        }
        m_ui->listSigners->addItem(myitem);
    }

}

void InflowFirmaDoc::generateJScriptreports(const QString& documents,
                                            const QList<QSqlField>& fields,
                                            int ncounts,
                                            const QString& entitytosign,
                                            QList<SafetWorkflow::InfoSigner>& signers)
{

    jscriptload = true;
    currentDocuments = documents;
//    qDebug("....currentDocuments...generateJScriptreports: %s", qPrintable(currentDocuments));
//    qDebug("..fields.count()...: %d", fields.count());
    currentFields = fields;
    _listprincipalcount = ncounts;
    _listprincipaltitle = entitytosign;
    _entityToSign = entitytosign;

    QString currentreg, totalregs;
    for(int i=0; i < signers.count(); i++) {
        currentreg = "{";
        currentreg += "commonName:";
        currentreg += "\""+ signers.at(i).commonName+"\",";
        currentreg += "issuer:";
        currentreg += "\""+ signers.at(i).issuer+"\",";
        currentreg += "date:";
        currentreg += "\""+ signers.at(i).date+"\",";
        currentreg += "hour:";
        currentreg += "\""+ signers.at(i).hour+"\",";
        currentreg += "location:";
        currentreg += "\""+ signers.at(i).location+"\",";
        currentreg += "role:";
        currentreg += "\""+ signers.at(i).role+"\",";
        currentreg += "hexsign:";
        currentreg += "\""+ QString(signers.at(i).hexsign.toHex().data())+"\",";
        currentreg += "digest:";
        currentreg += "\""+ QString(signers.at(i).digest.toHex().data())+"\",";

        currentreg += "isvalid:";
        if ( signers.at(i).isvalid ) {
            currentreg +=  "true ";
        }
        else {
            currentreg +=  "false ";
        }
        currentreg += "},";
        totalregs = totalregs + currentreg;
    }
    totalregs.chop(1);
    _currentSigners = totalregs;
    m_ui->webDocument->reload();
}

void InflowFirmaDoc::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        m_ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void InflowFirmaDoc::executeJSCodeAfterLoad(bool ok ) {
    qDebug("...jscriptload:%d", jscriptload);
    qDebug("...ok:%d", ok);
    if ( !jscriptload ) {
        return;
    }
    if ( !ok ) {
//        SafetYAWL::streamlog
//                << SafetLog::Error
//                << tr("Ocurrio un error al cargar el reporte (%1)")
//                .arg(m_ui->webDocument->url().toString());
        return;
    }

    qDebug("...currentDocuments:\n|%s|\n",
           qPrintable(currentDocuments));
    // ** Reemplazar caracteres especiales, hacen falta pruebas aqui
     currentDocuments.replace("\n","");
     // ** Reemplazar caracteres especiales, hacen falta pruebas aqui
     QString data;
    data = QString("safettitle = '%1';\n").arg(_listprincipaltitle);
    data += QString("safetlistcount = %1;\n").arg(_listprincipalcount);
    data += "safetlist = [";
    data += currentDocuments;
    data += "];";


    jscriptcolumns = "safetcolumns = [";
    int i = 0;
    foreach(QSqlField f, currentFields ) {

        QString scriptlen = SafetYAWL::getScriptLen(f);
         jscriptcolumns
                 += QString("{ key: \"%1\",label:\"%1\",width:%2,"
                            "resizeable:true,sortable:true},\n")
                    .arg(f.name())
                    .arg(scriptlen);

         i++;
    }
    if ( i > 0 ) {
        jscriptcolumns.chop(2);
    }
    jscriptcolumns += "];";
    QString code;
    code = data;    
    code += "\nsafetsigners = [";
    code += _currentSigners;
    code += "];\n";
    code += " ";
    code += jscriptcolumns;
    code += " ";
    qDebug("...*code*...:\n%s\n",qPrintable(code));
    evalJS(code);

    code = "safetproccessData();";
    evalJS(code);

}


    // Calcular la longitud del campo para la tabla generada en Listar_Datos

QString InflowFirmaDoc::evalJS(const QString &js) {
     Q_CHECK_PTR( m_ui->webDocument );
     qDebug("***.....evalJS...QWebFrame *frame = m_ui->webDocument->page()->mainFrame();");
     QWebFrame *frame = m_ui->webDocument->page()->mainFrame();
    return frame->evaluateJavaScript(js).toString();
}


void InflowFirmaDoc::loadReportTemplate() {



    QString nameProtocol = SafetYAWL::getConf()["Reports/protocol"]+ "://";
    QString namePath = SafetYAWL::getConf()["Reports/path"];
    QString namePage = SafetYAWL::getConf()["Reports/documenttosign.template"];
    namePage = namePage.section("/",-1);
    qDebug("...namePage: |%s|",qPrintable(namePage));

    QString fullUrl = nameProtocol + namePath + "/" + namePage;
    QUrl url(fullUrl);
    qDebug("***...InflowFirmaDoc::loadReportTemplate....fullUrl: |%s|",qPrintable(fullUrl));

    m_ui->webDocument->load(url);

}
