/*
* SAFET Sistema Automatizado para la Firma Electr�nica y Estampado de Tiempo
* Copyright (C) 2008 Víctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*
* CENDITEL Fundacion Centro Nacional de Desarrollo e Investigaci�n en Tecnologías Libres
*
* Este programa es software libre; Usted puede usarlo bajo los t�rminos de la licencia de
* software GPL versi�n 2.0 de la Free Software Foundation.
*
* Este programa se distribuye con la esperanza de que sea �til, pero SI NINGUNA GARANT�A;
* tampoco las impl��citas garant��as de MERCANTILIDAD o ADECUACIÓN A UN PROP�SITO PARTICULAR.
* Consulte la licencia GPL para m�s detalles. Usted debe recibir una copia de la GPL junto
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5� Piso, Boston, MA 02110-1301, USA.
*
*/

/****************************************************************************
**
** Copyright (C) 2004-2006 Trolltech ASA. All rights reserved.
**
** This file is part of the example classes of the Qt Toolkit.
**
** This file may be used under the terms of the GNU General Public
** License version 2.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of
** this file.  Please review the following information to ensure GNU
** General Public Licensing requirements will be met:
** http://www.trolltech.com/products/qt/opensource.html
**
** If you are unsure which license is appropriate for your use, please
** review the following information:
** http://www.trolltech.com/products/qt/licensing.html or contact the
** sales department at sales@trolltech.com.
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

//#include <QtGui>
#include "SafetYAWL.h"
#include "getfilewidget.h"
#include "mainwindow.h"

GetFileWidget::GetFileWidget(const QString& t, QWidget *parent, bool istextparent )
    : CmdWidget(t, parent,istextparent)
{

    _showfilename = true;

    myloadff = false;
    myloadpic = false;

}

GetFileWidget::~GetFileWidget() {

}


void GetFileWidget::setText(const QString &newText) {
    _currtext = newText;
}


void GetFileWidget::showDialog() {

}



bool GetFileWidget::isValid(QString& value) {
     _currtext = value;
     QString optionsstr = conf()["options"].toString();
     if ( optionsstr.startsWith("save")) {
         return CmdWidget::isValid(value);
     }
     else {
         if (myloadpic) {
             return true;
         }
        else if ( !QFile::exists(value)) {
            return false;
        }
    }
    return true;
}

void GetFileWidget::buildWidget() {
    updateCombo();

}

void GetFileWidget::setFocus ( Qt::FocusReason reason ) {
//     QWidget::setFocus ( reason );
//     filecombo->setFocus( reason);
     
     
}
void GetFileWidget::insertAndClose() {    
/*    if ( _texteditparent ) {
        if ( filecombo) {
            QString value = filecombo->currentText().simplified();
            _texteditparent->insertPlainText(value.toLatin1());
            _texteditparent->insertPlainText("\n");
        }
    }

    close();*/
}

void GetFileWidget::updateCombo() {

    SYD << tr("...GetFileWidget::updateCombo()....(1)");
    QStringList myoptions = conf()["options"].toString().split(",",QString::SkipEmptyParts);


    myloadff = false;
    myloadpic = false;
    foreach(QString op, myoptions) {
        QString myfield = op.section("::",0,0).trimmed();
        if (myfield.compare("show",Qt::CaseSensitive) == 0) {
            QString myvalue = op.section("::",1,1).trimmed();
            if (myvalue.compare("id",Qt::CaseSensitive) == 0 ) {
                _showfilename = false;
                break;
            }
        }
        else if ( myfield.compare("load",Qt::CaseSensitive) == 0) {
            QString myvalue = op.section("::",1,1).trimmed();
            if (myvalue.compare("flowfiles",Qt::CaseSensitive) == 0 ) {
                myloadff = true;
                break;
            }
            else if (myvalue.compare("pictures",Qt::CaseSensitive) == 0 ) {
                myloadpic = true;
                break;
            }


        }

    }
    if (myloadff ) {
        QString mydirff = SafetYAWL::pathconf + "/flowfiles";
        QDir mydir(mydirff);
        mydir.setFilter(QDir::Files | QDir::NoSymLinks);

            qDebug(".......GetFileWidget...append...mydirff:|%s|", qPrintable(mydirff));
        if (!mydir.exists()) {
            //SYE << tr(" There an error of configuration directory (.safet)");
            qDebug(" There an error of configuration directory (.safet)");
            return;
        }
        //obtener el numero de archivos existentes dentro del contenedor
        QFileInfoList list = mydir.entryInfoList();

        for (int i=0; i<list.size(); i++) {
            QFileInfo fileInfo = list.at(i);

            QString myname = fileInfo.fileName();
            QString myentry;
            if (!myname.startsWith("safet")) {
                myentry = mydirff+ "/" + myname;
                _options.append(myentry);
            }

            qDebug(".......GetFileWidget...append...:|%s|", qPrintable(myentry));
        }

        return;

    }
    else if ( myloadpic ) {
        QString mydirpic = MainWindow::_currconfpath + "/" + "MyDocs/Pictures" ;
        QDir mydir(mydirpic);
        mydir.setFilter(QDir::Files | QDir::NoSymLinks);

            qDebug(".......GetFileWidget...append......mydirpic:|%s|", qPrintable(mydirpic));
        if (!mydir.exists()) {
            SYE << tr(" There an error of configuration Pictures directory ");

            return;
        }
        //obtener el numero de archivos existentes dentro del contenedor
        QFileInfoList list = mydir.entryInfoList();

        for (int i=0; i<list.size(); i++) {
            QFileInfo fileInfo = list.at(i);


            //if (fileInfo.fileName().endsWith(".jpg",Qt::CaseInsensitive) ) {
                QString myentry = mydirpic+ "/" + fileInfo.fileName();
                _options.append(myentry);
            //}

        }
        mydirpic = MainWindow::_currconfpath + "/" + "MyDocs/DCIM" ;
        mydir.setPath(mydirpic);
        mydir.setFilter(QDir::Files | QDir::NoSymLinks);

        if (!mydir.exists()) {
            //SYW << tr(" There an error of configuration Pictures directory ");
            qDebug("...GetFileWidget::updateCombo dir no exist");
            return;
        }
        //obtener el numero de archivos existentes dentro del contenedor
        list = mydir.entryInfoList();

        for (int i=0; i<list.size(); i++) {
            QFileInfo fileInfo = list.at(i);

            //if (fileInfo.fileName().endsWith(".jpg",Qt::CaseInsensitive) ) {
                QString myentry = mydirpic+ "/" + fileInfo.fileName();
                _options.append(myentry);
            //}
        }
        return;

    }


    QString mime = SafetYAWL::getConf()["Widgets/getfilewidget.*"];
        QStringList mimes  = mime.
                         split(SafetYAWL::LISTSEPARATORCHARACTER,QString::SkipEmptyParts);

        _options.clear();
        _wfids.clear();

//        SYD << tr("...GetFileWidget::updateCombo()....(1)...mimes.count():|%1|")
//               .arg(mimes.count());

        foreach(QString s, mimes) {
            SafetYAWL *myconfigurator = new SafetYAWL(SafetYAWL::pathconf);

            if ( myconfigurator ) {

//                SYD << tr("...GetFileWidget...SafetYAWL::pathconf...:|%1|")
//                       .arg(SafetYAWL::pathconf);
                myconfigurator->openXML(s);
                myconfigurator->convertXMLtoObjects();
                SafetWorkflow *mywf = myconfigurator->getWorkflow();
                if ( mywf ) {
                    _wfids.append(mywf->id());
                    _options.append(s);
                }
                else {
                    SYD << tr("....GetFileWidget::updateCombo()...NO mywf...");
                    _options.append(s);
                }
                delete myconfigurator;
            }
            else {
                SYD << tr("....GetFileWidget::updateCombo()...NO myconfigurator...");
                _options.append(s);
            }
        }
        qDebug("....updateCombo...(2)...");


}

QString GetFileWidget::qmlForKey(const QString& k) {
    conf()["keyvalue"] = k;
    updateCombo();

    QString result;
    result  = QString("%1</SAFETMODELSEPARATOR>%2%3")
            .arg(tr("<none>"))
            .arg(tr("<none>"))
            .arg(SafetYAWL::LISTSEPARATOR);

    foreach(QString s, options()){
        if (!s.trimmed().isEmpty()) {
        QString newitem;
            if ( myloadpic) {
                newitem = QString("%1</SAFETMODELSEPARATOR>%2%3")
                    .arg(s.section('/',-1).replace(QRegExp("\\.[a-zA-Z0-9]{2,3}"),""))
                    .arg(s)
                    .arg(SafetYAWL::LISTSEPARATOR);
            }
            else {
                newitem = QString("%1</SAFETMODELSEPARATOR>%2%3")
                    .arg(s.section('/',-1).replace(QRegExp("\\.[a-zA-Z0-9]{2,3}"),""))
                    .arg(s)
                    .arg(SafetYAWL::LISTSEPARATOR);

            }

            result += newitem;
        }
    }

    result.chop(SafetYAWL::LISTSEPARATOR.length());
    return result;
}


QString GetFileWidget::qml() {
    QString result;
    QString properties;
    foreach(QString option, options()) {
        if (option.startsWith("property::")) {
            properties += option.mid(QString("property::").length());
            properties += "\n";
        }
    }



    result =  QString("  Row {\n"
            "    id: row%2\n"
            "objectName: \"row%2\"\n"
            "    height: 65\n"
            "    width: parent.width\n"
            "    spacing: 20\n"
            "    SelectBox {\n"
            "      selecttext: \"...\"\n"
            "      selecttitle: \"%1\"\n"
            "     %3         \n")
            .arg(caption())
            .arg(caption().left(2)+caption().simplified().right(1))
            .arg(properties);

        if (myloadpic) {

            result += "      showimages: true\n";
        }
        else {
            result += "      showimages: false\n";
        }


      result += QString("      selectmodel: ListModel {\n"
                      "%1")
            .arg(tr("ListElement { name: \"<none>\"; value:\"<none>\"}\n"));


    foreach(QString s, options()){
        if (!s.trimmed().isEmpty()) {
        QString newitem;
        if (myloadpic ) {
//        newitem = QString("        ListElement { name: \"%2\"; value:\"%1\"}\n")
//                .arg(s.section('/',-1))
//                .arg(s.section('/',-1));
//                //.arg(s);
//            result += newitem;
        }
        else {
            newitem = QString("        ListElement { name: \"%1\"; value:\"%2\"}\n")
                    .arg(s.section('/',-1).replace(QRegExp("\\.[a-zA-Z0-9]{2,3}"),""))
                    .arg(s);
                result += newitem;

        }
        }
    }

    result += "      }\n"
              "    }\n"
              "  }\n";


   // qDebug("...GetFileWidget::qml...result:|\n%s|",qPrintable(result));
    return result;

}

QString GetFileWidget::html() {

    updateCombo();



    QString result = QString("<select name=\"%1\" id=\"%1\">\n")
            .arg(caption());

    result += QLatin1String("<option value=\"\"></option>");
    int i = 0;
    foreach(QString s, options()){
        if (s.trimmed().isEmpty()) {
            continue;
        }
        QString newitem;
        if ( _showfilename ) {
            newitem = QString("<option value=\"%1\">%1</option>\n")
                    .arg(s);
        }
        else {
            newitem = QString("<option value=\"%2\">%1</option>\n")
                    .arg(i<_wfids.count()?_wfids.at(i):s)
                    .arg(s);
        }

        i++;
        result += newitem;
    }


    result += QLatin1String("</select>");
    SYD << tr("...GetFileWidget::html()...:|%1|")
           .arg(result);
    return result;

}

QString GetFileWidget::text() const {
        QString result = "";
        result = _currtext;
        return result;
}               
