/*
* SAFET Sistema Automatizado para la Firma ElectrÃ³nica y Estampado de Tiempo
* Copyright (C) 2008 VÃ­ctor Bravo (vbravo@cenditel.gob.ve), Antonio Araujo (aaraujo@cenditel.gob.ve
*     
* CENDITEL Fundacion Centro Nacional de Desarrollo e InvestigaciÃ³n en TecnologÃ­as Libres 
*  
* Este programa es software libre; Usted puede usarlo bajo los tÃ©rminos de la licencia de 
* software GPL versiÃ³n 2.0 de la Free Software Foundation. 
* 
* Este programa se distribuye con la esperanza de que sea Ãºtil, pero SI NINGUNA GARANTIA;
* tampoco las implÃ­citas garantÃ­as de MERCANTILIDAD o ADECUACION A UN PROPopITO PARTICULAR.
* Consulte la licencia GPL para mÃ¡s detalles. Usted debe recibir una copia de la GPL junto 
* con este programa; si no, escriba a la Free Software Foundation Inc. 51 Franklin Street,
* 5º Piso, Boston, MA 02110-1301, USA.
*
*/


#include <QSqlField>
#include "SafetYAWL.h"
#include "SafetWorkflow.h"
#include "SafetConfFile.h"
#include <locale.h>

//#include "threadconsole.h"
#include "../inflow/mainwindow.h"
//** En la compilacion incluir la c.onstantes -DSAFET_DBXML

#ifdef SAFET_DBXML   // Incluir la biblioteca dbxml para soporte de Base de Datos XML Nativa 
#include "SafetDbXmlRepository.h"
#endif
// ** Soporte DbXml Base de datos XML Nativa 
#include <QPointer>

// archivos incluidos para soportar llamadas a metodos remotos del servicio web safet
#ifdef SAFET_GSOAP // Definido conexion a servicios Web
#include "soapH.h"
#include "safet.nsmap"
#endif  // Definido conexion a servicios Web


SafetWorkflow::SafetWorkflow() {
     ptoken = NULL;	
     _tablesource = "";
     isStartingKeyFilter = true;
     shapecount = 0;
     RESOURCESFILES[ "node.split.xor"] = SafetYAWL::getConfFile().getValue("Operators", "split.xor").toString();
     RESOURCESFILES[ "node.split.or"] =  SafetYAWL::getConfFile().getValue("Operators", "split.or").toString();
     RESOURCESFILES[ "node.split.and"] =  SafetYAWL::getConfFile().getValue("Operators", "split.and").toString();
     RESOURCESFILES[ "node.join.or"] =  SafetYAWL::getConfFile().getValue("Operators", "join.xor").toString();
     RESOURCESFILES[ "node.join.and"] = SafetYAWL::getConfFile().getValue("Operators", "join.and").toString();
     RESOURCESFILES[ "node.join.xor"] = SafetYAWL::getConfFile().getValue("Operators", "join.xor").toString();
     RESOURCESFILES[ "node.none"] = SafetYAWL::getConfFile().getValue("Operators", "none").toString();
     RESOURCESFILES[ "node.split.none"] = "imgs/none.png";
     RESOURCESFILES[ "node.join.none"] = "imgs/none.png";
     RESOURCESFILES[ "node."] = "imgs/none.png";
     RESOURCESFILES[ "condition.start"] = "imgs/start.png";
     RESOURCESFILES[ "condition.end"] = "imgs/end.png";
     RESOURCESFILES[ "edge.and"] = "[style=bold";
     RESOURCESFILES[ "edge.or"] =  "[style=dotted";
     RESOURCESFILES[ "edge.xor"] = "[style=dashed";
     RESOURCESFILES[ "edge.none"] = "[style=solid";
     RESOURCESFILES[ "edge."] = "[style=solid";
// Valores inciales para la conversion de SVG a JSON
     SHAPEMAP[ "ellipse" ] = 0;
     SHAPEMAP[ "path" ] = 1;
     SHAPEMAP[ "text" ] = 2;
     SHAPEMAP[ "polygon" ] = 3;					
     oriminy = -500;
     orimaxy= 0;
     oriminxx = 0;
     orimaxx = 500;
     parser.setWorkflow(this);
     setNumberOfTokens( 0 );
     _xmlRepository = NULL;


}

SafetWorkflow::SafetWorkflow(const SafetWorkflow& other) {
    _xmlRepository = NULL;
}

SafetWorkflow::~SafetWorkflow() {
     if ( ptoken != NULL) delete ptoken;
 //    qDebug("saliendo...~SafetWorkflow...");
     foreach(SafetTask* mytask, tasklist){
          if ( mytask) delete mytask;
     }

     foreach(SafetCondition* mycondition, conditionlist){
          if ( mycondition) delete mycondition;
     }
     if ( _xmlRepository != NULL ) {
         delete _xmlRepository;
     }



}


void SafetWorkflow::addTask(SafetTask* t) {
     Q_CHECK_PTR( t );
     tasklist.push_back(t);
     t->setParent( qobject_cast<SafetXmlObject*>(this));
     nodemap.insert(t->id(),qobject_cast<SafetNode*>(t));
     visitnodemap.insert( t->id(),false);

}

QMap<uint,QString> SafetWorkflow::changeHumanizeParameters(const QMap<uint,QString>& l) {
    QMap<uint,QString> result;
    QDateTime mydate;
    SYD << tr("...SafetWorkflow::changeHumanizeParameters...l.count():%1").arg(l.count());
    foreach(uint numkey, l.keys()) {
        QString p = l[numkey];
        QString strout = l[numkey];

        SYD << tr("...SafetWorkflow::changeHumanizeParameters...strout:|%1|").arg(strout);
        if (p == Safet::AnyOneObject ) {
            strout = Safet::SqlAllValues;
        }
        else {
            mydate = QDateTime::fromString(p, Safet::DateFormat);
            SYD << tr("...SafetWorkflow::changeHumanizeParameters...."
                      "...p....:|%2|"
                      "mydate.isValid():|%1|")
                   .arg(mydate.isValid())
                   .arg(p);
            if (mydate.isValid()) {
                strout = QString("%1").arg(mydate.toTime_t());
            }
        }
        result[numkey]=strout;
    }

    return result;
}

bool SafetWorkflow::putParameters(const QMap<uint,QString>& p) {
    bool result = false;

    QMap<uint,QString> list = changeHumanizeParameters(p);


     QString strin, strout;
    foreach(SafetTask* task, getTasklist()) {
        strin = task->title();
        strout = replaceArg(strin,p);

        if (strin != strout ) {
            if ( strout.indexOf(QRegExp("\\s+NULL\\s*"),Qt::CaseInsensitive) == -1 ) {
                task->setTitle(strout);
            }
            else {
                task->setTitle(Safet::AnyOneObject);
            }
        }
        foreach(SafetPort *port, task->getPorts()) {
            if (port->type() == "split" ) {

                foreach (SafetConnection* conn, port->getConnectionlist()) {
                    strin = conn->options();
                    strout = replaceArg(strin,list);

                    if (strin != strout ) {
                        result = true;
                        conn->setOptions(strout);
                    }

                }

            }
        }

    }
    foreach(SafetCondition* cond, getConditionlist()) {
        strin = cond->title();
        strout = replaceArg(strin,p);
        if (strin != strout ) {
            cond->setTitle(strout);
        }
        foreach(SafetPort *port, cond->getPorts()) {
            if (port->type() == "split" ) {

                foreach (SafetConnection* conn, port->getConnectionlist()) {
                    strin = conn->options();
                    SYD << tr("...SafetWorkflow::putParameters....condition...strin:|%1|")
                           .arg(strin);
                    strout = replaceArg(strin,list);
                    SYD << tr("...SafetWorkflow::putParameters....condition...strout:|%1|")
                           .arg(strout);
                    SYD << tr("...SafetWorkflow::putParameters....condition...list.count():|%1|")
                           .arg(list.count());

                    if (strin != strout ) {
                        result = true;
                        conn->setOptions(strout);
                    }

                }

            }
        }

    }


    return result;
}

QString SafetWorkflow::replaceArg(const QString& strin, const QMap<uint,QString>& l) {
    QString result = strin;
   QString pattern = QString("(\\=|>|<|<\\=|>\\=|IS|IN|LIKE)?\\s*\\{\\#([1-9]+)\\}");
   QRegExp rx;
   rx.setPattern(pattern);
   int pos = 0;
   while(true) {
       pos  = strin.indexOf(rx,pos);
       if (pos == -1) break;

           bool ok;
           int numpar = rx.cap(2).toInt(&ok);
           QString stringfinded;
           if (l.contains(numpar)) {
               QString mynumpar = l[numpar].trimmed();
               stringfinded = QString("{#%1}").arg(numpar);

               if (!mynumpar.isEmpty()) {
                    result.replace(stringfinded,mynumpar);
                    qDebug("......:SafetWorkflow::replaceArg...mynumpar:|%s|",qPrintable(mynumpar));
               }

           }
           pos += stringfinded.length()==0?5:stringfinded.length();

   }
   pos = 0;   
   QString newresult = result;
   while(true) {
          pos  = newresult.indexOf(rx,pos);
          if (pos == -1) break;
           result.replace(rx.cap(0)," "+Safet::SqlAllValues);
          pos += rx.cap(0).length()==0?5:rx.cap(0).length();
   }

    return result.trimmed();

}

void SafetWorkflow::addChild(SafetXmlObject* o) {
	Q_CHECK_PTR(o);
	SafetXmlObject::addChild(o);
	SafetTask *ptask;
	SafetCondition* pcond;
        SafetParameter* pparameter;
	switch(SafetYAWL::getClassReference(SafetYAWL::translateNS(o->metaObject()->className()))) {
	case 2: // SafetTask
		ptask = qobject_cast<SafetTask*>(o);
		Q_ASSERT(ptask);
                ptask->setTitle(SafetYAWL::replaceArgsflow(ptask->title()));
		tasklist.push_back(ptask);
		nodemap.insert(ptask->id(),qobject_cast<SafetNode*>(o));
		visitnodemap.insert( ptask->id(),false);              

		break;
	case 5:
		pcond = qobject_cast<SafetCondition*>(o);
                Q_ASSERT( pcond);
                pcond->setTitle(SafetYAWL::replaceArgsflow(pcond->title()));
		conditionlist.push_back(pcond);
		nodemap.insert(pcond->id(),qobject_cast<SafetNode*>(o));
		visitnodemap.insert( pcond->id(),false);
		break;
	case 9:
		Q_ASSERT(ptoken == NULL);
		ptoken = qobject_cast<SafetToken*>(o);
		break;
        case 12:
                pparameter = qobject_cast<SafetParameter*>(o);
                Q_CHECK_PTR( pparameter);
                parameterlist.append(pparameter);
                break;


	default:;
	}

}

QSet<QString> SafetWorkflow::getKeys(const QString& idtask) {
	QSet<QString> myset;
	SafetTask *curtask;
	QList<SafetTask*>::iterator i;
	QStack<QString> keys;
	
	//QSqlQuery query();
	QSqlQuery query(SafetYAWL::currentDb); // <-- SafetYAWL::currentDb puntero a db actual
	 for (i = tasklist.begin(); i != tasklist.end(); ++i) {
		curtask = *i;
		if (curtask->id() == idtask) {
			SafetVariable *curvar;
			QList<SafetVariable*>::iterator j;
			for (j = curtask->getVariables().begin(); j != curtask->getVariables().end(); ++j) {
				curvar = *j;
				query = getSQLDocuments(*curvar);
				break; // Documento para una sola variable, se necesita verificar el algoritmo para el caso que existen dos o mas variables
			}							
		}
	}
	while( query.next() ) {
		myset.insert( query.value(0).toString().trimmed() );
	}
	QString namefields;
	for( int i = 0; i < query.record().count(); i++){
		namefields += query.record().fieldName(i) + " ";
	}	
	namefields.chop(1);
	return myset;
}

int SafetWorkflow::getQuerySize(QSqlQuery& qry) {
    int n = -1;
    bool hassize = SafetYAWL::currentDb.driver()->hasFeature(QSqlDriver::QuerySize);
    if (hassize) {
        return qry.size();
    }
    else {
        n = 0;
        while (qry.next()) {
            n++;
        }
        //while (qry.previous());
        qry.first();
        qry.previous();
    }

    return n;
}



QString SafetWorkflow::getDocuments(const QString& idvariable, QList<QSqlField>& fields,
                                    int &howmanydocuments,
                                    OutputFormat of,const QString& info) {
    QString str;
    qDebug("....getDocuments...entrando...");

    SafetVariable *curvar = searchVariable( idvariable );
    Q_CHECK_PTR( curvar );
    //QSqlQuery query = getSQLDocuments(*curvar);
    QSqlQuery query(SafetYAWL::currentDb); // <-- SafetYAWL::currentDb puntero a db actual
    query = getSQLDocuments(*curvar);
    SafetDocument mydocument;
    howmanydocuments = getQuerySize(query);
    if ( getQuerySize(query) >  0 ) {
        switch (of) {
            case SafetWorkflow::XML:
                str = mydocument.getXmlQuery(query,info);
                break;
            case SafetWorkflow::JSON:
                str = mydocument.getJsonQuery(query,fields,info);
                break;
            case SafetWorkflow::SVG:
                break;
            default:;
        }
    }

    if ( SafetYAWL::curOutputInterface != NULL ) {
        QMap<QString,QString> mymap;
        return SafetYAWL::curOutputInterface->renderDocuments(str,mymap);
    }

    return str;
}

QString SafetWorkflow::getDocuments(const QString& idvariable,
                                    OutputFormat of,const QString& info) {
    QString str;
    QList<QSqlField> fields;
    int howmanydocuments;
    qDebug("....getDocuments...entrando...");

    SafetVariable *curvar = searchVariable( idvariable );
    Q_CHECK_PTR( curvar );
    //QSqlQuery query = getSQLDocuments(*curvar);
    QSqlQuery query(SafetYAWL::currentDb); // <-- SafetYAWL::currentDb puntero a db actual
    query = getSQLDocuments(*curvar);
    SafetDocument mydocument;
    howmanydocuments = SafetWorkflow::getQuerySize(query);
    if ( SafetWorkflow::getQuerySize(query) >  0 ) {
        switch (of) {
            case SafetWorkflow::XML:
                str = mydocument.getXmlQuery(query,info);
                break;
            case SafetWorkflow::JSON:
                str = mydocument.getJsonQuery(query,fields,info);
                break;
            case SafetWorkflow::SVG:
                break;
            default:;
        }
    }

    if ( SafetYAWL::curOutputInterface != NULL ) {
        QMap<QString,QString> mymap;
        return SafetYAWL::curOutputInterface->renderDocuments(str,mymap);
    }

    return str;
}





QList<QString> SafetWorkflow::getFieldsOfDocument(const QString& idtask) {
	QList<QString> mylist;
	SafetTask *curtask = searchTask(idtask);
	Q_ASSERT_X(curtask != NULL,"getFieldsOfDocument", qPrintable(tr("No se encuentra en el flujo de trabajo la tarea : %s").arg(qPrintable(idtask))));
	SafetVariable *curvar;
	//QSqlQuery query;
	QSqlQuery query(SafetYAWL::currentDb);
	QList<SafetVariable*>::iterator j;
	for (j = curtask->getVariables().begin(); j != curtask->getVariables().end(); ++j) {
		curvar = *j;
		query = getSQLDocuments(*curvar);
		break; // Documento para una sola variable, se necesita verificar el algoritmo para el caso que existen dos o mas variables
	}
	
	Q_ASSERT( curtask->getVariables().size() > 0);	
	SafetDocument mydocument;
        if ( SafetWorkflow::getQuerySize(query) >  0 ) {
		mylist = mydocument.getFields(query);
	}						
	return mylist;
}


QList<QVariant::Type> SafetWorkflow::getInfosOfDocument(const QString& idtask) {
	QList<QVariant::Type> mylist;
	SafetTask *curtask = searchTask(idtask);
	Q_ASSERT_X(curtask != NULL,"getFieldsOfDocument", qPrintable(tr("No se encuentra en el flujo de trabajo la tarea : %s").arg(qPrintable(idtask))));
	SafetVariable *curvar;
	//QSqlQuery query;
	QSqlQuery query(SafetYAWL::currentDb);
	QList<SafetVariable*>::iterator j;
	for (j = curtask->getVariables().begin(); j != curtask->getVariables().end(); ++j) {
		curvar = *j;
		query = getSQLDocuments(*curvar);
		break; // Documento para una sola variable, se necesita verificar el algoritmo para el caso que existen dos o mas variables
	}
	
	Q_ASSERT( curtask->getVariables().size() > 0);	
	SafetDocument mydocument;
        if ( SafetWorkflow::getQuerySize(query) >  0 ) {
		mylist = mydocument.getInfos(query);
	}						
	return mylist;
}

QSqlQuery SafetWorkflow::getQueryFromTask(const QString& idtask) {
	SafetTask *curtask = searchTask(idtask);
	Q_ASSERT_X(curtask != NULL,"getFieldsOfDocument", qPrintable(tr("No se encuentra en el flujo de trabajo la tarea : %s").arg(qPrintable(idtask))));
	SafetVariable *curvar;
	//QSqlQuery query;
	QSqlQuery query(SafetYAWL::currentDb);
	QList<SafetVariable*>::iterator j;
	for (j = curtask->getVariables().begin(); j != curtask->getVariables().end(); ++j) {
		curvar = *j;
		query = getSQLDocuments(*curvar);
		break; 
	}
	return query;	
}


int SafetWorkflow::numberOfTokens(const QString& idtask) {
        int result = 0;
//	SafetTask *curtask = searchTask(idtask);
//	Q_ASSERT_X(curtask != NULL,"numberOfTokens", qPrintable(tr("No se encuentra en el flujo de trabajo la tarea : %s").arg(qPrintable(idtask))));
//	SafetVariable *curvar;
//	QList<SafetVariable*>::iterator j;
//	for (j = curtask->getVariables().begin(); j != curtask->getVariables().end(); ++j) {
//		curvar = *j;
//		result = result + numberOfTokens(*curvar);
//	}
	return result;
}

SafetTask* SafetWorkflow::searchTask(const QString& idtask) {
        foreach(SafetTask* curtask, tasklist) {
//                qDebug("....*curtask: %s", qPrintable(curtask->id()));
                Q_CHECK_PTR( curtask );
		if (curtask->id() == idtask) {
			return  curtask;
		}
	}
	return NULL;
}

SafetCondition* SafetWorkflow::searchCondition(const QString& idcon) {
        foreach(SafetCondition* cur, conditionlist) {
                Q_CHECK_PTR( cur);
                if (cur->id() == idcon) {
                        return  cur;
                }
        }
        return NULL;
}


int SafetWorkflow::numberOfTokens(const SafetVariable& v) {

    QSqlQuery myquery =  getSQLDocuments(v);
    int result = SafetWorkflow::getQuerySize( myquery );
    SYD << tr("...**numberOfTokens...tama�o de la consulta: |%1|")
                            .arg(result);
    return result;
}

QString  SafetWorkflow::getStackExpression(const SafetVariable& v, QStack<QString> &splitresults, QStack<QString> &joinresults) {

	QString sql = v.documentsource();
	Q_CHECK_PTR( SafetYAWL::evalExit() );

        if ( sql.length() == 0  ) {
            SafetYAWL::streamlog
                    << SafetLog::Error
                    <<
                    tr("La sentencia sql \"%1\" tiene longitud cero").arg(sql);
            return QString("");

     }

	Q_CHECK_PTR(ptoken);	
	Q_ASSERT_X(sql.length() > 0, "getStackExpression", qPrintable(tr("La sentencia sql tiene longitud cero")));
	SafetSQLParser localparser;
        localparser.setWorkflow(this);
	localparser.setStr(sql);
	localparser.parse();
        if (localparser.error() ==  SafetParser::INCORRECT) {
            SafetYAWL::streamlog
                    << SafetLog::Error
            << tr("NO es correcta la sentencia SQL: \"%1\" ").arg(sql);
            return QString("");

        }
	_tablesource = localparser.getTablesource();	
	// Sentencias Where para las VARIABLES (Documentos)
	for (int i=0; i< localparser.getWhereclauses().count(); i++ ) {
		whereset.push_back( localparser.getWhereclauses().toList().at(i));
	}


        joinWhereset += localparser.getJoinclauses();
	// Se debe relacionar con los joins necesarios para realizar la relaciÃ³n con el token 
//        qDebug("**...(1)...sql: %s", qPrintable(sql));
	sql = localparser.dropWhereClauses();
 //       qDebug("**...(2)...sql: %s", qPrintable(sql));
        QRegExp rx("^\\s*SELECT\\b"); // Colocar el campo clave en la lista de campos de la sentencia SQL
    	rx.setCaseSensitivity(Qt::CaseInsensitive);
	sql.replace(rx,"SELECT "+getToken()->key()+",");

	 SafetTask* mytask = qobject_cast<SafetTask*> (v.parent());
	 Q_CHECK_PTR( mytask );

	 generateKeyFilterStack( mytask->id(), splitresults, joinresults); // generar la pila de resultados dado 	 
	 // identificador de la tarea padre de la variable
	//results =   generateKeyFilterStack( v.parent() ); // generar la pila de resultados dado el identificado dela tarea padre de la variable buscada

	_tablesource = localparser.getTablesource();	
        //SafetYAWL::debugQueue( tokenlinkqueue );
        //SafetYAWL::debugQueue( tokenlinkqueueInPath );


	return sql;
}


QString SafetWorkflow::removeOutParenthesis(const QString& s) {
    QString result = s;

    QRegExp rxInitial;
    QRegExp rxEnd;

    rxInitial.setPattern("(\\()+(\\s*SELECT)");
    rxEnd.setPattern("(\\)\\s*)(\\))+(\\s+UNION)");

    int pos = 0;
    SYD << tr("....SafetWorkflow::removeOutParenthesis....(1)...s:|%1|" )
    .arg(s);
    while (1) {
        pos = result.indexOf(rxInitial,pos);
        if (pos == -1 ) {
            break;
        }
        result.replace(rxInitial.cap(0), rxInitial.cap(2));
        pos += rxInitial.cap(0).length()+1;
    }

    pos = 0;
    while (1) {
        pos = result.indexOf(rxEnd,pos);
        if (pos == -1 ) {
            break;
        }
        result.replace(rxEnd.cap(0), rxEnd.cap(1)+rxEnd.cap(3));
        pos += rxEnd.cap(0).length()+1;
    }
    SYD << tr("....SafetWorkflow::removeOutParenthesis....(2)" );



    return result;

}

QSqlQuery SafetWorkflow::getSQLDocuments(const SafetVariable& v) {
	resetContainers();	
	QString sql = v.documentsource();
	QStack<QString> splitresults, joinresults;  
	sql  =getStackExpression(v, splitresults, joinresults);  // Obtener la pila de resultado desde la variable
        //QString tokenlink = processTokenlink(v.tokenlink(), true);

//        qDebug("...(4)...sql: %s", qPrintable(sql));
        //SafetYAWL::debugStack(splitresults,tr("REDEFINE..(1)..."));
        redefineSplitOperators(splitresults);
        //SafetYAWL::debugStack(splitresults,tr("REDEFINE..(2)..."));
	QString splitoperation = evalSQLKeyFilterStack(sql, splitresults);


//	generateInformationSignTemp(v);

        QRegExp rx("\\s+AND\\s+\\(?\\s*TRUE\\s*\\)?");
	rx.setCaseSensitivity( Qt::CaseInsensitive );
	splitoperation.remove( rx );

        SYD << tr("....SafetYAWL::currentDb.driverName():|%1|")
        .arg(SafetYAWL::currentDb.driverName());

        if ( SafetYAWL::currentDb.driverName().toUpper() == "QSQLITE"
             /* Chequear si es SQLITE */ )  {
            splitoperation = removeOutParenthesis(splitoperation);
        }

	QSqlQuery query(splitoperation,SafetYAWL::currentDb); // <-- SafetYAWL::currentDb puntero a db actual


        if ( query.lastError().type() != QSqlError::NoError ) {
                        SYE <<tr("Error SQL tipo: \"%2\". Mensaje: \"%3\". Errores en la  Sentencia SQL:\n\"%1\"")
                        .arg(splitoperation)
                        .arg(query.lastError().type())
                        .arg(query.lastError().driverText());

           return query;
        }


        if (SafetWorkflow::getQuerySize(query) == 0 ) {
            SYW << tr("El resultado de la Sentencia SQL:\n\"%1\" no presenta registros").arg(splitoperation);
        }

     //   qDebug("dbg...yes...SQL: |%s|",qPrintable(splitoperation));
	return query;	
}

void SafetWorkflow::resetContainers() {
	whereset.clear(); 
	opeStack.clear();
	joinOpeStack.clear();
	joinWhereset.clear();
	tokenlinkqueue.clear();
	lastkeyset.clear();
	tokenlinkqueueInPath.clear();
	currentfilterkey = "";
	// Reiniciar las variables
}

QString SafetWorkflow::evalSQLKeyFilterStack(const QString& sql, QStack<QString> &e) {
	QStack<QString> mystack;
        //SafetYAWL::debugStack( e ); // Funcion agregada para listar las tareas de la pila
	if ( e.count() ==  1 ) {
		QString newsql = e.pop();
                //QString tokenlink = generateJoinString();

//		sql = sql + tokenlink;
		QString path = newsql.section('|',1,1);
		newsql.remove("|" + path);
		SafetSQLParser localparser;
                localparser.setWorkflow(this);
		localparser.setStr(sql);
		localparser.parse();
                QString tokenlink = generateJoinString(path.toInt(), localparser.getTablesource() );
                SafetYAWL::streamlog
                        << SafetLog::Debug
                        << tr("Tokenlink: \"%1\"").arg(tokenlink);

		QString joinsql = sql + tokenlink;
		QString result  = addSQLPrefix(joinsql, newsql);	
                SafetYAWL::streamlog
                        << SafetLog::Debug
                        << tr("Obteniendo consulta con numero de elementos de la pila igual a uno (1): \"%1\"").arg(result);
  		return   result;
	}
	while (!e.isEmpty() ) {
		QString newsql, result = e.pop();
			
		if ( getSQLOperator(result).length() == 0 ) {
			mystack.push(result);			  
		} else  {
			newsql = processSQLOperation(sql, mystack, result);	
		}
 	}

 	Q_ASSERT(mystack.count() > 0 ); 	
        QString result = mystack.pop();
        SafetYAWL::streamlog << SafetLog::Debug << tr("Obteniendo consulta varios elementos en la pila: \"%1\"").arg(result);
        return result;
}




QString SafetWorkflow::processTokenlink(const QString& s, bool final, int npath) {

	    QRegExp rx("(\\[[a-zA-Z_0-9\\.,:]*\\])");
	    int newpos, pos;
	    QString sql, str = s;
	    while (true) {
		pos = rx.indexIn(str);
		if ( pos < 0 ) break;
			QString internal = rx.cap(1);		    
			QRegExp rxInternal("([a-zA-Z_0-9\\.:]+),?([a-zA-Z_0-9\\.:]+)?");
			int posinternal = rxInternal.indexIn(internal);		    
			QStringList links = rxInternal.capturedTexts();
			if ( posinternal >  -1 )  {	
				QString sqltoken = generateSQLToken(links, final,npath);
				sql = sql + sqltoken;
			}
		newpos = pos+rx.matchedLength();
		str = str.mid(newpos);
	    }
	return sql;

}

QString SafetWorkflow::generateSQLToken(const QStringList &list, bool final, int npath) {


    SafetYAWL::streamlog
            << SafetLog::Debug
            << tr("Generando generateSQLToken");
    foreach(QString l, list) {
        SafetYAWL::streamlog
                << SafetLog::Debug
                << tr("   -->  l:%1").arg(l);

    }

    if ( ptoken == NULL ) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("No se ha cargado en el archivo de flujo de trabajo el elemento \"token\"."
                      "Se sugiere colocarlo al principio del archivo de flujo de trabajo, para que sea leído de primero");
            return QString("");
    }
	Q_ASSERT_X(_tablesource.length() > 0, "generatSQLToken", 
		qPrintable(tr("La tabla origen no tiene ningun nombre")));
	QString sql, linkleft, linkright, jointable; // algunas variables necesarias para generar el enlace a la ficha (tokenlink)
	if (list.at(2).length() == 0 ) { // Caso de una sola tabla en el tokenlink
//            SafetYAWL::streamlog
//                    << SafetLog::Debug
//                    << tr("(Sola) Analizando expresion: |%1|")
//                    .arg(list.at(1));
		sql =  " JOIN " + ptoken->keysource();
		bool found = false;
		foreach(QString s, tokenlinkqueue) {
			if ( s.indexOf( QRegExp("^" + sql+"\\b",Qt::CaseInsensitive) ) > -1   ) {
				found = true;
				break;
			}
		}

                QRegExp rx("([a-zA-Z_0-9\\.]+):?([a-zA-Z_0-9\\.\\(\\)]+)?");
		int pos = rx.indexIn(list.at(1));
                if (pos < 0 ) {
                    SafetYAWL::streamlog
                            << SafetLog::Error
                            << tr("El siguiente tokenlink  \"%1\" presenta errores").arg(list.at(1));
                       return QString();
                }

		QString left,right;		
		left = rx.cap(1);
		right = rx.cap(2);
                SafetYAWL::streamlog
                        << SafetLog::Debug
                        << tr("rx(1): |%1| rx(2): |%2|")
                        .arg(left)
                        .arg(right);

		if (right.length() > 0  ) { // Caso de una sola tabla en el tokenlink con tabla explicita
                    SafetYAWL::streamlog
                            << SafetLog::Debug
                            << tr("right.length() > 0");

                        QString mysql = " JOIN " +  right + " ON " + right + "." + left
			+ " = " + ptoken->keysource() + "." +  ptoken->key();					
			if ( npath > -1 )  {						
				mysql = mysql + "|" + QString("%1").arg(npath);
			}
			if ( !tokenlinkqueueInPath.contains(mysql) ) {

                            SafetYAWL::streamlog
                                    << SafetLog::Debug
                                    << tr("Agregando a cola tokenlinkqueueInPath: |%1|")
                                    .arg(mysql);
				tokenlinkqueueInPath.enqueue(mysql);
			}			
		}	
                SafetYAWL::streamlog
                        << SafetLog::Debug
                        << tr("sql");

                sql = sql + " ON " + left + "=" + ptoken->key();
                SafetYAWL::streamlog
                        << SafetLog::Debug
                        << tr("found: %2 ... final: %3 sql....|%1|")
                        .arg(sql)
                        .arg(found)
                        .arg(final);

                if ( !found  && final ) {
                    SafetYAWL::streamlog
                            << SafetLog::Debug
                            << tr("Agregando a cola tokenlinkqueue: |%1|")
                            .arg(sql);

			tokenlinkqueue.enqueue( sql );
		}		
		return sql;
	}
	
	for(int i=1; i < list.size(); i++) {
//            SafetYAWL::streamlog
//                    << SafetLog::Debug
//                    << tr("Analizando expresion: |%1|")
//                    .arg(list.at(i));

		QRegExp rx("([a-zA-Z_0-9\\.]+):?([a-zA-Z_0-9\\.]+)?");
		int pos = rx.indexIn(list.at(i));
		if ( pos > -1 ) {
			if (linkleft.length() == 0 ) {
				linkleft = rx.cap(1);
				if (rx.cap(2).length() > 0  ) {
					jointable = rx.cap(2);
				}	
			}
			else if (rx.cap(2).length() > 0  ) {
				linkright = rx.cap(1);
				if ( jointable.length() > 0  ) {
					QString mysql = " JOIN " +  jointable + " ON " + jointable + "." + linkleft
					+ " = " + rx.cap(2) + "." + linkright;					
					if ( npath > -1 )  {						
						mysql = mysql + "|" + QString("%1").arg(npath);
					}
					if ( !tokenlinkqueueInPath.contains(mysql)  && !final) {
                                            SafetYAWL::streamlog
                                                    << SafetLog::Debug
                                                    << tr("Agregando a cola (2)tokenlinkqueueInPath: |%1|")
                                                    .arg(mysql);
						tokenlinkqueueInPath.enqueue(mysql);
					}
					sql = " JOIN " +  rx.cap(2) + " ON " + _tablesource + "." + linkleft
                                        + "=" + rx.cap(2) + "." + linkright;
					jointable = "";
				}  else {				
					sql = " JOIN " +  rx.cap(2);
					bool found = false;
					foreach(QString s, tokenlinkqueue) {
						if ( s.indexOf( QRegExp("^" + sql+"\\b",Qt::CaseInsensitive) ) > -1   ) {
							found = true;
							break;
						}
					}
					sql = sql + " ON " + _tablesource + "." + linkleft
					+ " = " + rx.cap(2) + "." + linkright;
					if ( !found  && final ) {
                                            SafetYAWL::streamlog
                                                    << SafetLog::Debug
                                                    << tr("Agregando a cola (2) tokenlinkqueue:|%1|")
                                                    .arg(sql);
						tokenlinkqueue.enqueue( sql );
					}		
				}			
				_tablesource = rx.cap(2);
				return sql;
			}
		}
	}    
	return QString();
}



QString SafetWorkflow::printPaths(const QString& idtask, const QString& info) {
	QList<QQueue<QString> > paths = generatePaths(idtask);  // Generar los caminos a un nodo
        Q_CHECK_PTR( SafetYAWL::evalExit() );
        SafetYAWL::evalAndCheckExit(paths.count() > 0, tr("No existen caminos para la tarea indicada: \"%1\" (Revise el documento de flujo de trabajo)").arg(idtask));
	QString result;
	for( int i=paths.count()-1; i>=0 ; i--) {
		
		QQueue<QString> path = paths.at(i);
		while(!path.empty()) {
			QString nodename = path.dequeue();
			SafetNode *node = nodemap[ nodename ];			
			Q_CHECK_PTR( node );
			int type = SafetYAWL::getClassReference(SafetYAWL::translateNS(node->metaObject()->className()));
			if ( type == 2 ) { // Nodo es una Tarea
				QSet<QString> keys = getKeys(nodename);
				if ( keys.contains(info) ) {
					result += "[ " +nodename + " ]";		
				}	
				else {
					result += nodename;		
				}
				
			}
			else {
					result += "( *" +nodename + " )";		
			}
			result += "-->";	
		}
		result.chop(3);
		result += "\n";
	}
	return result;
}



void SafetWorkflow::redefineSplitOperators(QStack<QString>& splitresults){

    QList<SafetPort*> myports;
    foreach(SafetTask* task, tasklist){
        if (task->inport() != NULL) {
            myports.append(task->inport());
        }
    }

    int countport = 0;
    for(int i=0; i< splitresults.count();i++){
        if (SafetYAWL::PATTERNS.contains(splitresults.at(i)) ) {
            if ( countport < myports.count()) {
                QString newpattern = myports.at(countport)->pattern();
                splitresults[i] = newpattern;
            }
        }
    }

}



void SafetWorkflow::generateKeyFilterStack(const QString& idtask, QStack<QString>& splitresults, QStack<QString>& joinresults){

    QList<QQueue<QString> > paths = generatePaths(idtask);  // Generar los caminos a un nodo

    //SafetYAWL::debugPaths(paths); // Depuracion de la lista de caminos
    if (paths.count() ==  0) {
        SafetYAWL::streamlog
                << SafetLog::Error
                <<
        tr("No existen caminos para la tarea indicada: \"%1\" (Revise el documento de flujo de trabajo)").arg(idtask);
        return;
    }

    QSet<QString> myset;
    QString ope;

    lastkeyset.clear();

    //int pathssize = paths.size();
    int ipath = 0;
    SafetPort *splitPort = NULL, *joinPort = NULL;

    //QMap<QString,QSet<QString> > dropkey;
    for( int i=paths.count()-1; i>=0 ; i--,ipath++) {

        if ( !opeStack.isEmpty() ) {
            ope =  opeStack.pop(); // Chequear operadores
            if ( getSQLOperator(ope).length() > 0 ) {
                splitresults.push(ope);
                ope =  opeStack.pop(); // Visualizar el operador aqui
            }
        }

        QQueue<QString> path = paths.at(i);
        QStack<QString> reversepath, prevnodes;
        QString splitresult, joinresult;
        int countprevnodes = 0;

        // Recorrido (Caminata por cada Path)
        countprevnodes = walkPath(path,reversepath,prevnodes,splitresult,joinresult,splitPort,joinPort,idtask,ipath); // Recorrido


        myset = lastkeyset; // Se actualiza el conjunto de claves (lastkeyset)
        lastkeyset.clear();
        QString keyresult = generateKeyFilterString();

        splitresult = generateWhereClauses(splitresult,ipath);        
        SafetYAWL::streamlog << SafetLog::Debug << tr("Consulta Nro %1 generada: \"%2\"").arg(ipath).arg(splitresult);
        splitresults.push(splitresult+ "|" + QString("%1").arg(ipath));//Agregar numero de camino
        // Operador SPLIT  JOIN
        if ( countprevnodes > 0 ) {
            joinresult  = joinresult + ((joinWhereset.count() > 0 ) ? " AND " : " ");
            for(int i = 0; i < joinWhereset.count(); i++) {
                joinresult = joinresult + joinWhereset.toList().at(i);
                if (i < joinWhereset.count()-1) joinresult += " AND ";
            }
            joinWhereset.clear();
        }
        isStartingKeyFilter = true;
        keysoptions.clear();
        dropkeys.clear();
        currentfilterkey = "";
    }
    lastkeyset = myset;
}


int SafetWorkflow::walkPath(QQueue<QString>& path, QStack<QString>& reversepath, QStack<QString>& prevnodes, QString& splitresult, QString& joinresult,SafetPort* splitPort, SafetPort* joinPort, const QString& idtask, int ipath) {
    QString nodename, prevnodename;
    SafetNode *node, *nextnode;
        SafetNode::PortType type = SafetNode::SPLIT;
    int countprevnodes = 0;
    int curroption = 0;
    while(!path.empty()) {
        QString query;
//          qDebug("...inside walkPath...(1)...");
        nodename = path.dequeue();
//          qDebug("......nodename: %s", qPrintable(nodename));
        node = nodemap[ nodename ];
//          qDebug("......nodename: %s", qPrintable(nodename));
        Q_CHECK_PTR( node );
        splitPort    =  getOperationPort(type, node); // Obtener el puerto del siguiente/anterior nodo
        nextnode = getOperationNode(type, path, reversepath);
        curroption = calculateCurroption(SafetNode::SPLIT,node,nextnode); // Calcular la  ruta actual
        if (idtask.compare(node->id(), Qt::CaseInsensitive) == 0) break;
        if ( splitPort != NULL ) {
            query  = generateSQLPortKeyFilterExpression(splitPort, curroption,ipath);
            SafetYAWL::streamlog
                    << SafetLog::Debug
                    << tr("              ***generateSQLPortKeyFilterExpression...:%1")
                    .arg(query);
            splitresult   = generateKeyFilterExpression(query, node, curroption,path,false,ipath);
            SafetYAWL::streamlog
                    << SafetLog::Debug
                    << tr("              ***generateSQLPortKeyFilterExpression..(splitresult).:%1")
                    .arg(splitresult);

        }

        joinPort = getOperationPort(SafetNode::JOIN, node);

        if (  joinPort != NULL ) {
            if ( prevnodes.isEmpty() ) {
                prevnodes = node->prev();
            }
            SafetNode* prevnode = getOperationNode(SafetNode::JOIN, path, reversepath);
            int prevoption = calculateCurroption(SafetNode::JOIN,node,prevnode);
            prevnodename = prevnode->id();
            if  ( prevnodes.contains( prevnodename ) ) {
                Q_CHECK_PTR( prevnode );
                splitPort    =  getOperationPort(SafetNode::SPLIT, prevnode);
                query  = generateSQLPortKeyFilterExpression(splitPort, prevoption,ipath);
                joinresult   = generateKeyFilterExpression(query,prevnode, curroption, path,true,ipath);
                countprevnodes++;
            }
        }
//          qDebug("...inside walkPath...(4)...");
        reversepath.push( nodename );
    }
    return countprevnodes;
}


int SafetWorkflow::calculateCurroption( SafetNode::PortType type, SafetNode *node, SafetNode *nextnode) {
    if ( nextnode == NULL ) return 0;
    Q_CHECK_PTR(node);
    int i;

    SafetPort* myport = NULL;
    if ( type == SafetNode::SPLIT )
        myport = node->outport();
    else if ( type == SafetNode::JOIN )
        myport = node->inport();
    Q_CHECK_PTR( myport );
    for(i = 0; i< myport->getConnectionlist().count(); i++) {
        SafetConnection* con = node->port()->getConnectionlist().at(i);
        Q_CHECK_PTR( con );
        if ( con->source().compare(nextnode->id(),Qt::CaseInsensitive) == 0 ){
            return i;
        }
    }
    SafetYAWL::evalAndCheckExit( i < node->port()->getConnectionlist().count(), tr("La tarea o condicion con el identificador: \"%1\" no esta conectada correctamente").arg(node->id()));
    return 0;
}

SafetNode* SafetWorkflow::getOperationNode(SafetNode::PortType type, const QQueue<QString>& path,
                                                                                const QStack<QString>& reversepath ) {
        SafetNode* mynode = NULL;
        switch (type) 	{
                case SafetNode::SPLIT:
                                if ( !path.empty() ) mynode = nodemap[ path.head() ];
                                break;
                case SafetNode::JOIN:
                                if ( !reversepath.isEmpty() ) mynode = nodemap[ reversepath.top() ];
                                break;
                default:;
        }
        return mynode;
}

SafetPort* SafetWorkflow::getOperationPort(SafetNode::PortType type, SafetNode* node) {
	SafetPort* myport = NULL;
	Q_CHECK_PTR( node );
	switch(type) {
		case SafetNode::SPLIT:
				myport = node->outport();
				break;
		case SafetNode::JOIN:
				myport = node->inport();
				break;				
		default:;
	}
	return myport;
}


QList<QQueue<QString> > SafetWorkflow::generatePaths(const QString& idtask, ItemSearched t) {
	SafetCondition* start = getStartCondition();
	SafetNode* node = start, *parent;
	QStack<SafetWorkflow::PathHistory> localstack;
	QList<QQueue<QString> > paths;
	QString nodename = start->id();	
        QQueue<QString> mypath;
        while ( true ) {
                 if (idtask == nodename) { // Aca encuentra el camino
			 	  mypath.enqueue(nodename);
			 	  paths.push_front(mypath);	  
			 	  if (localstack.isEmpty() )   break;
 		    	  SafetWorkflow::PathHistory h = localstack.pop();
                              nodename = h.nodename();
			      mypath = h.history();
			   	  node = nodemap[ nodename ];
			   	  Q_CHECK_PTR( node );	
			   	  if ( mypath.count() > 0 ) {			   		  
			   		  parent = nodemap[ mypath.last() ];
					  SafetPort* parentport = parent->port();
					  Q_CHECK_PTR( parentport );
			   		  if ( getSQLOperator(parentport->pattern()).length() > 0 ) {
				   		  Q_CHECK_PTR(parent);
			   		  }
			   	  }
		 }			
 	 	mypath.enqueue(nodename);
		nodename = pushLeftChild(node, localstack, mypath);
		if ( nodename.length() == 0 ) 	{
			while (!localstack.isEmpty() && nodename.length() == 0) {
				SafetWorkflow::PathHistory h = localstack.pop();
				nodename = h.nodename();
				mypath = h.history();
			}			
			if ( nodename.length() != 0) {
				node = nodemap[ nodename ];
				Q_ASSERT(node);
			   	  if ( mypath.count() > 0 ) {			   		  
			   		  parent = nodemap[ mypath.last() ];
			   		  Q_CHECK_PTR(parent);
			   	  }
				continue;
			}
			if (localstack.isEmpty() ) {			
				if ( mypath.last().compare( idtask,Qt::CaseInsensitive)==0 ) paths.push_front( mypath );
				break;			
			}
		}
		node = nodemap[ nodename ];					
	}
	return paths;
}

QStringList SafetWorkflow::searchNextValues(const QString& keyvalue) {
     QStringList result;


     return result;
}


QStringList  SafetWorkflow::getTempNameFiles(int n) {
    QStringList list;
    if (n == 0 ) return list;
    Q_ASSERT(n > 0 );
    
    QList<QTemporaryFile*> filelist;
    
    for (int i= 0; i < n; i++) {
    	filelist.push_back(new QTemporaryFile());
    	Q_CHECK_PTR( filelist.at(i) );
    	bool result = filelist.at(i)->open();
    	Q_ASSERT_X(result, "getTempNameFile", qPrintable(tr("No fue posible abrir el archivo temporal")));
    	list.push_back(filelist.at(i)->fileName());
    }

    Q_ASSERT(n == filelist.count()); // Deben ser igual el numero de la lista de archivos
    for (int i= 0; i < n; i++) {
    	filelist.at(i)->close();
    	delete filelist.at(i); 
    }
    return list;
}

QString SafetWorkflow::pushAllChild(SafetNode *node,QStack<QString>& localstack,
                                    QMap<QString,QString>& results,
                                    const QString& info,
                                    QMap<int,QString>& orderresults,
                                    int& countresults,
                                    bool norender) {
     Q_CHECK_PTR(node);
     QString pop = "", label= "";
     QStack<QString> childstack = node->next();
     if ( childstack.isEmpty() ) {
         return pop;
     }
     QString newedge;
     QStringList optionlist = node->port()->options().split("|");
     for( int i=childstack.count()-1; i>=0;i--) {
          if (childstack[i].length() > 0 ) {
               SafetNode* childnode = nodemap[ childstack[i] ]; // Colocar el camino, por si es
                                                                // necesario
               Q_CHECK_PTR(childnode);
               if (  i < optionlist.count() )
                    label = optionlist [i ];

               newedge = printNodeInformation(node, childstack[i], "", info,norender);
               localstack += childstack[i];

               QRegExp rx("Siguiente:\\s*([a-zA-Z_0-9;\\.]+)\\s*(,[a-zA-Z_0-9]+)?");
               int pos = rx.indexIn( results[ node->id() ]);

               if ( pos != -1 ) {
                    QString oldnodes = rx.cap(1);
                    newedge = printNodeInformation(node, childstack[i], oldnodes,info,norender);
               }
               if ( !(node->id().isEmpty())) {
                    results[ node->id() ] = newedge;

                    if ( !visitnodemap[node->id()]) {
                        orderresults[ countresults ]  = node->id();
                        countresults++;
                    }
               }

          }
     }
     if (localstack.isEmpty()) {
         return pop;
     }
     pop = localstack.pop();

     return pop;
}


QString SafetWorkflow::printNodeInformation(SafetNode *node, const QString& nextnode,
                                             const QString& oldnextnode,
                                             const QString& info,
                                             bool norender) {
     Q_CHECK_PTR( node );
     QString newnode, result;
     SafetWorkflow* wf = qobject_cast<SafetWorkflow*>(this);
     Q_CHECK_PTR( wf );


     Safet::SafetStats *_mystats = new Safet::SafetStats();
     _mystats->setWorkflow( wf );

     if ( numberOfTokens() == -1 ) {
           _mystats->setWorkflow( qobject_cast<SafetWorkflow*>(this) );

     }


     if ( oldnextnode.length() == 0 ) {
          newnode = nextnode;
     }
     else if ( oldnextnode.indexOf( nextnode ) == -1 ) {
          newnode = oldnextnode + ";" + nextnode;
     }
     else {
          newnode = oldnextnode;
     }
     Safet::SafetStats::StatsType t = Safet::SafetStats::None;
     QString myinfo;

     if (info.compare("coloured", Qt::CaseInsensitive) == 0 ) {
          t = Safet::SafetStats::Coloured;
     }
     else if ( info.length() > 0 ) {
          t = Safet::SafetStats::ForTokenKey;
     }
     if ( t != Safet::SafetStats::None ) {
          QString attr = SafetYAWL::getConf()["Plugins.Graphviz/stats.attribute"];
          QStringList infos = _mystats->processInfo(node, t, attr,norender,info);
          if (infos.count() > 0 ) {
            myinfo =  infos.at(0);
            if ( infos.count() > 1 ) {
                infonodemap[node->id()] = infos.at(1);
            }
          }

     }
     QString typeNode = "Task";
     int ntype = SafetYAWL::getClassReference
                 (SafetYAWL::translateNS(node->metaObject()->className()));
     if ( ntype == 5 ) typeNode = "Condition";
     QString nodetitle = node->title();
     QString title = (nodetitle.length() == 0)?tr(""):tr(", ")
                     + tr("Titulo:")+nodetitle+",";
     QString moreinfo = myinfo.length() == 0?tr(""):myinfo;
     saveStates(node->id(),newnode.split(";",QString::SkipEmptyParts));
     result  = tr("Nodo:") + node->id() +", " + tr("Tipo:")+typeNode+", "
               +tr("Siguiente:") + newnode +
               ", "
               +tr("Union:")+(node->inport()!=NULL?node->inport()->pattern():"none")
               +", "
               +tr("Patron:")+ node->port()->pattern()+title+moreinfo;

     if (_mystats) {
         delete _mystats;
     }
     return result;
}

void SafetWorkflow::saveStates(const QString &currstate, const QStringList& nextstates) {
    QStringList myemptylist;
    if (!_nexts.contains(currstate)) {
        _nexts[currstate] = myemptylist;
    }
    foreach(QString p, nextstates) {
        if (!_previouses.contains(p)) {
            _previouses[p] = myemptylist;
        }
        if ( !_previouses[p].contains(currstate)) {
            _previouses[p].append(currstate);
        }
        if ( !_nexts[currstate].contains(p)) {
            _nexts[currstate].append(p);
        }
    }

}

QString SafetWorkflow::pushLeftChild(SafetNode *node, QStack<SafetWorkflow::PathHistory>& localstack,
		QQueue<QString>& qqueuepath) {
	Q_CHECK_PTR(node);
	QString pop = "";
	QStack<QString> childstack = node->next();
	if ( childstack.isEmpty() ) return pop;
	SafetPort* port = node->port();
	Q_CHECK_PTR(port);
	if (getSQLOperator(port->pattern()).length() > 0 ) opeStack.push_front(port->pattern());
	int j=0;
	for( int i=childstack.count()-1; i>=0;i--) {
		PathHistory pathhistory(childstack[i]); 
		if (childstack[i].length() > 0 ) {
			SafetNode* childnode = nodemap[ childstack[i] ]; // Colocar el camino, por si es necesario
			Q_CHECK_PTR(childnode);			
			childnode->setOption(i);
			SafetPort* childport = childnode->port();
			Q_CHECK_PTR( childport );
			pathhistory.history() += qqueuepath;
			localstack.push(pathhistory);
			if (getSQLOperator(port->pattern()).length() > 0 &&
                            getSQLOperator(childport->pattern()).length() == 0	) {
                            opeStack.push_front(childstack[j++]);
                        }
		}
	}
	if (localstack.isEmpty()) return pop;
	PathHistory h = localstack.pop(); 
	pop = h.nodename();
	qqueuepath = h.history();
	return pop; 
}




void SafetWorkflow::resetVisitNodeMap() {
	QList<QString> mylist = visitnodemap.keys();
		 for (int i = 0; i < mylist.count(); i++) {
			 visitnodemap[ mylist.at(i) ] = false;
	}
}

QString SafetWorkflow::drawDotGraph(const QString& dotfile,  char* filetype) {
    QStringList list  = getTempNameFiles(2);
//    Q_ASSERT( list.count() == 2 );
//    GVC_t *gvc; graph_t *g; FILE *fpin, *fpout;
//    gvc = gvContext();
//    Q_CHECK_PTR( gvc );
//    fpin = fopen (qPrintable(list.at(0)),"w+");
//    fpout = fopen (qPrintable(list.at(1)),"w+");
//     fwrite( qPrintable(dotfile), sizeof(char), dotfile.length()+1, fpin);
//     fflush(fpin);
//     fclose(fpin);
//    fpin = fopen (qPrintable(list.at(0)),"r");
//    Q_CHECK_PTR(fpin);
//    g = agread(fpin);
//    gvLayout(gvc, g, "dot");
//    gvRender(gvc, g,  filetype, fpout);
//    gvFreeLayout(gvc, g);
//    agclose(g);
//    gvFreeContext(gvc);
//    fclose(fpin);
//    fclose(fpout);
//
    return list.at(1);
}

QList<double> SafetWorkflow::numericInfos(const QString& id) {
    QList<double>    result;
    bool includeall = id.trimmed().isEmpty();
    QList<SafetTask*> mytasks = getTasks();
    bool ok;
    foreach(SafetTask* mytask, mytasks) {
        Q_CHECK_PTR(mytask);
        double value = mytask->numericinfo().toDouble(&ok);
        if (!ok) {
            SYE << tr("Error al tratar del convertir el atributo \"numericinfo\": "
                      "\"%1\"")
                   .arg(mytask->numericinfo());
            return result;
        }
        if (!includeall ) {
            if ( mytask->id()==id) {
                result.append(value);
                return result;
            }
        }
        else {
            result.append(value);
        }

    }

    return result;

}

QStringList SafetWorkflow::textualInfos(const QString& id) {
    QStringList result;
    bool includeall = id.trimmed().isEmpty();
    QList<SafetTask*> mytasks = getTasks();
    bool ok;
    foreach(SafetTask* mytask, mytasks) {
       Q_CHECK_PTR(mytask);
        if (!includeall ) {
            if ( mytask->id()==id) {
                result.append(mytask->textualinfo());
                return result;
            }
        }
        else {
            result.append(mytask->textualinfo());
        }

    }
    return result;

}

QStringList SafetWorkflow::listNextStates(const QString& id, SafetWorkflow::NextStates st) {
     QStringList result;
     if ( id.isEmpty()) {
         return result;
     }
     SafetYAWL::_isstatstokenfound = false; // Inicializando por si es un unico token
     QString code = generateCodeGraph("png",id,true);

     SafetYAWL::_isstatstokenfound = false;
     QStringList mylist = code.split("\n",QString::SkipEmptyParts);
     bool ok;
     if ( st == SafetWorkflow::AllNotCompleted ) {
         qDebug("...not completed");
         QString myCurrentState;
         for(int  i=mylist.count()-1; i >=0; i--) {
              QString sec = mylist.at(i);
              QRegExp rx("Nodo:\\s*([a-zA-Z0-9\\+_\\.]+)\\s*,");
              int pos = rx.indexIn( sec );
              Q_ASSERT ( pos >= 0 );
              QString newnode = rx.cap(1);

              rx.setPattern("Patron:\\s*([a-zA-Z0-9_\\.]+)\\s*,");
              pos = rx.indexIn( sec );
              Q_ASSERT ( pos >= 0 );
              // Para el color
              rx.setPattern("info\\.task\\.color:\\s*([a-zA-Z0-9_\\.]+)\\s*,?");
              pos = rx.indexIn( sec );
              double porc = 0;
              if  ( pos > 0 ) {
                   porc = rx.cap(1).toDouble(&ok);
                   if ( porc > 0.0 ) { // es
                        myCurrentState = newnode;
                        break;
                   }
              }
         }

         QString  comp = SafetYAWL::getConf()["ExtraInfo/infotext.completed"];

         for(int  i=mylist.count()-1; i >=0; i--) {
             QString sec = mylist.at(i);
             QRegExp rx("Nodo:\\s*([a-zA-Z0-9\\+_\\.]+)\\s*,");
             int pos = rx.indexIn( sec );
             Q_ASSERT ( pos >= 0 );
             QString newnode = rx.cap(1);

             QString myinfos = sec.section(",",-1);

             QStringList mylist =  myinfos.split("...", QString::SkipEmptyParts);

             if (mylist.count() < 3
                     &&   newnode != tr("start")
                     &&   newnode != tr("end")) {
                 result.append(newnode);
             }
             else {

                 if ( mylist.count() > 2 ) {

                     QString myinfotext = mylist.at(2);
                     if (!comp.isEmpty() && myinfotext.indexOf(comp) != -1) {
                         result.append(newnode);
                         continue;
                     }

                 }

             }

         }

     }
     else {
         for(int  i=mylist.count()-1; i >=0; i--) {
             QString sec = mylist.at(i);
             QRegExp rx("Nodo:\\s*([a-zA-Z0-9\\+_\\.]+)\\s*,");
             int pos = rx.indexIn( sec );
             Q_ASSERT ( pos >= 0 );
             QString newnode = rx.cap(1);

             rx.setPattern("Patron:\\s*([a-zA-Z0-9_\\.]+)\\s*,");
             pos = rx.indexIn( sec );
             Q_ASSERT ( pos >= 0 );
             // Para el color
             rx.setPattern("info\\.task\\.color:\\s*([a-zA-Z0-9_\\.]+)\\s*,?");
             pos = rx.indexIn( sec );
             double porc = 0;
             if  ( pos > 0 ) {
                 porc = rx.cap(1).toDouble(&ok);

                 if ( porc > 0.0 ) { // es el nodo identificado con ID
                     SafetNode* node = nodemap[ newnode ];
                     Q_CHECK_PTR( node );
                     Q_ASSERT (_previouses.contains(newnode));
                     Q_ASSERT (_nexts.contains(newnode));
                     foreach(QString p, _previouses[newnode]) {
                         SYD << tr("....SafetWorkflow::listNextStates...previouses p:|%1|")
                                .arg(p);
                         foreach(QString nb, _nexts[p]) {
                             if ( nb != newnode) {
                                 result.append(nb);
                             }
                         }
                     }

                     foreach(QString p, _nexts[newnode]) {
                         SYD << tr("....SafetWorkflow::listNextStates..._nexts p:|%1|")
                                .arg(p);
                     }



                     SafetPort* port = node->outport();
                     QString query = port->query(0);
                     rx.setPattern("\\s*([a-zA-Z0-9_][a-zA-Z0-9_\\.\\-]*)(\\s+SIGN)"
                                   "\\s+([a-zA-Z0-9_][a-zA-Z0-9_\\.\\s]+"
                                   "|#ANYBODY#\\s*)");
                     pos = rx.indexIn( query );
                     if ( pos >= 0 ) {
                         QStringList commons = rx.cap(3).split(",");
                         result = commons;
                         QString namevar = rx.cap(1).simplified();
                         result += namevar;
                         result += QString("<SIGN>");
                         return result;
                     }
                     for(int i=0; i< port->getConnectionlist().count();i++) {
                         if ( port->options(i).simplified().length() > 0  ) {
                             result.append(port->options(i));
                         }

                     }
                     if ( result.count() > 0 ) {
                         return result;
                     }
                     else {
                         result.append(SafetYAWL::ENDOFFLOW);
                         return result;
                     }
                 }

             }
         }
     }


     qDebug("...SafetWorkflow::listNextStates....result...append inicial");
     return result;
}

QString SafetWorkflow::generateCodeGraph(char* filetype, const QString& info, bool norender) {
     qDebug("...generateCodeGraph....");
     QString result;
     resetVisitNodeMap();
     QStack<QString> localstack;
     SafetCondition* start = getStartCondition();
     Q_CHECK_PTR( start );
     SafetNode* node = start;
     QString nodename = start->id();
     SYD << tr("....SafetWorkflow::generateCodeGraph....start->id():|%1|")
            .arg(nodename);
     QMap<QString,QString> results;
     QMap<int,QString> orderresults;
     int countresults = 0;
     QString oldnodename;
     _nexts.clear();
     _previouses.clear();

     while ( true ) {
          if ( !visitnodemap[ nodename ] ) {
               SYD << tr("....SafetWorkflow::generateCodeGraph...!visitnodemap:|%1|")
                      .arg(nodename);
               results[ nodename ] = "";
               orderresults[countresults ] = nodename;
               countresults++;

               visitnodemap[nodename] = true;
          }
          oldnodename = nodename;
          nodename = pushAllChild(node, localstack, results, info, orderresults,countresults,
                                  norender);
          if ( nodename.length() == 0 ) 	{
               while (!localstack.isEmpty() && nodename.length() == 0) {
                   nodename = localstack.pop();
               }
               if ( nodename.length() != 0) {
                    node = nodemap[ nodename ];
                    continue;
               }
               results[ oldnodename ] = printNodeInformation(node, tr("<EOF>"), "", info,norender);
               break;
          }

          node = nodemap[ nodename ];
     }





     proccessExtraInfo(results,info,orderresults);
     for( int i = 0 ; i < orderresults.values().count(); i++) {
                QString mynode = orderresults[i];
               result += results[mynode ] +  "\n";
//               SYD <<  tr("...mynode:%1->|%2|").arg(i).arg(mynode);
     }

//     SYD << ("\n.......*results.........");
//     for(int i  = 0; i < results.keys().count(); i++ ) {
//         SYD << tr("|%1|").arg(results[ results.keys().at(i)]);

//     }
//     SYD << tr(".......results.........\n");

     return result;
}


void SafetWorkflow::proccessExtraInfo( QMap<QString,QString>& codes,const QString& info,
                                        QMap<int,QString>& orders) {

    qDebug("...proccessExtraInfo....****..info:|%s|", qPrintable(info));
    QMap <QString,SafetTask*> timespans;

    for(int i=0; i < orders.count();i++) {
        //        QString mynode = orders[i];
        //        QString mytask = codes[ mynode];
        if ( !orders.contains(i)) {
            continue;
        }
        QString mytask = orders[i];
        //        qDebug("**...mytask...:|%s|",
        //               qPrintable(mytask));

        bool ok;
        QString derror;
        bool showonly = SafetYAWL::getConf()["GeneralOptions/extrainfo.showonly"]=="on"?true:false;


        bool showhuman = SafetYAWL::getConf()["GeneralOptions/extrainfo.showhumanize"]=="on"?true:false;
        QList<SafetWorkflow::InfoSigner> infosigners;

        for(int j=0; j < getTasklist().count(); j++) {

            SafetTask *searchtask = getTasklist().at(j);
            QString newsql;

            if (searchtask->id() == mytask) {
                QString rolvalue, tsvalue;
                SafetVariable* myvar = searchtask->getVariables().at(0);
                QString rolfield = myvar->rolfield();
                QString tsfield = myvar->timestampfield();


                if (!rolfield.isEmpty() && !tsfield.isEmpty()) {
                    QString mysql = myvar->documentsource();
                    SafetSQLParser localparser;
                    localparser.setStr(mysql);
                    localparser.setWorkflow(this);
                    localparser.parse();



                    setTablesource(localparser.getTablesource());
                    QString link = processTokenlink(myvar->tokenlink());



                    if (!rolfield.startsWith("cn::")) {
                        localparser.addFieldToList(rolfield);
                    }
                    else {
                        int nrosign = 0;

                        infosigners =
                                getSignersDocument(myvar,info,derror);
                        if (infosigners.count() > 0 ) {
                              SafetWorkflow::InfoSigner signature =  infosigners.at(nrosign);
                              rolvalue = signature.commonName;

                        }
                        else {
                            rolvalue = tr("firma::N/A");
                        }


                    }
                    if (!tsfield.startsWith("cn::")) {
                        localparser.addFieldToList(tsfield);
                    }
                    else {
                        int nrosign = 0;
                        if (infosigners.count() > 0 ) {
                            QString spanvalue = "<br/>N/A*";
                            SafetWorkflow::InfoSigner signature =  infosigners.at(nrosign);
                            tsvalue = signature.date + " "+signature.hour;
                            tsvalue.chop(1);                            
                            QStack<QString> nexts = searchtask->next();
                            tsvalue = adjustTimeZone(tsvalue);

                            while ( !nexts.isEmpty()) {
                                QString nexttask = nexts.pop();
                                qDebug("SafetWorkflow::processExtraInfo.searchtask:%s",qPrintable(searchtask->id()));
                                qDebug("SafetWorkflow::processExtraInfo...nexttask:%s",qPrintable(nexttask));
                                searchtask->setTsValue(tsvalue);
                                timespans[nexttask] = searchtask;
                            }
                            qDebug("SafetWorkflow::processExtraInfo...timespans contains"
                                   " \"%s\":%d",qPrintable(searchtask->id()),
                                   timespans.contains(searchtask->id()));
                            int days;
                            if ( timespans.contains(searchtask->id())) {

                                    qDebug("SafetWorkflow::processExtraInfo...timespans.contains:%s\n",
                                           qPrintable(searchtask->id()));
                                    spanvalue = tr("<br/>")+humanizeDate(days,timespans[searchtask->id()]->tsvalue(),
                                                              "dd/MM/yyyy hh:mmap",
                                                             QDateTime::fromString(tsvalue,
                                                             "dd/MM/yyyy hh:mmap"),
                                                                         SafetWorkflow::WaitTime).trimmed();



                            }

                            if ( showhuman) {
                                tsvalue += "<br/>"+humanizeDate(days,tsvalue,"dd/MM/yyyy hh:mmap",
                                                     QDateTime::currentDateTime(),
                                                     SafetWorkflow::SinceWhenTime)
                                        + spanvalue;
                            }

                        }
                        else {
                            tsvalue = tr("firma::N/A");
                        }
                    }
                    if (rolvalue.isEmpty() || tsvalue.isEmpty()) {
                        newsql = "SELECT ";
                        for(int k=0; k< localparser.getFields().count();k++){
                            newsql += localparser.getFields().at(k);
                            newsql += ",";
                        }
                        newsql.chop(1);
                        newsql += " FROM " + localparser.getTablesource();


                        newsql += " " + link + " ";
                        if (info.compare("coloured",Qt::CaseInsensitive)  != 0 ) {
                            newsql += " WHERE ";
                            newsql += ptoken->key() + "=";
                            bool addquotes =  !(SafetYAWL::canTypeConvert(info, QVariant::Bool)
                                                || SafetYAWL::canTypeConvert(info, QVariant::Int));
                            newsql += (addquotes?"'":"")+info+(addquotes?"'":"");
                        }

                        QSqlQuery query( SafetYAWL::currentDb );
                        query.prepare(  newsql );
                        bool executed = query.exec();

                        if (!executed ) {
                            SYE <<
                                    tr("no se ejecuto correctamente la sentencia SQL: \"%1\"")
                                    .arg(newsql);
                            break;
                        }

                        if (!query.next()) {
                            SYW
                                    <<
                                    tr("Advertencia:No existen registros para la clave: \"%1\"")
                                    .arg(newsql);
                            break;
                        }
                        if (rolvalue.isEmpty()) {
                            if (tsvalue.isEmpty()) {
                                rolvalue = query.value(query.record().count()-2).toString().trimmed();
                            }
                            else {
                                rolvalue = query.value(query.record().count()-1).toString().trimmed();
                            }
                        }
                        QDateTime mydate;

                        if (tsvalue.isEmpty()) {
                            QString mystr = query.value(query.record().count()-1)
                                            .toString().trimmed();
                            int intdate = mystr.toInt(&ok);
//                            qDebug(".......**.........intdate:|%d|",intdate);
                            if (intdate == 0 ) {
                             rolvalue = tr("firma::N/A");
                            }
                            else {
                                QString spanvalue = QString("<br/>N/A");
                                mydate = mydate.fromTime_t(intdate);
                                tsvalue = mydate.toString("dd/MM/yyyy hh:mmap");                                
                                QStack<QString> nexts = searchtask->next();
                                qDebug("SafetWorkflow::processExtraInfo...nexts.count():%d",nexts.count());
                                while ( !nexts.isEmpty()) {
                                    QString nexttask = nexts.pop();
                                    qDebug("SafetWorkflow::processExtraInfo.searchtask:|%s|",qPrintable(searchtask->id()));
                                    qDebug("SafetWorkflow::processExtraInfo...nexttask:|%s|",qPrintable(nexttask));
                                    searchtask->setTsValue(tsvalue);
                                    timespans[nexttask] = searchtask;
                                    qDebug("SafetWorkflow::processExtraInfo...nexts.tsvalue:%s",
                                           qPrintable(tsvalue));
                                }

                                int days;
                                if ( timespans.contains(searchtask->id())) {
                                        spanvalue = tr("<br/>")+humanizeDate(days,
                                                         timespans[
                                                         searchtask->id()]->tsvalue(),
                                                         "dd/MM/yyyy hh:mmap",
                                                         QDateTime::fromString(tsvalue,
                                                               "dd/MM/yyyy hh:mmap"),
                                                    SafetWorkflow::WaitTime).trimmed();                     ;

                                }

                                if ( showhuman) {
                                    tsvalue += "<br/>"+humanizeDate(days,tsvalue,

                                              "dd/MM/yyyy hh:mmap",
                                               QDateTime::currentDateTime(),
                                               SafetWorkflow::SinceWhenTime
                                                                    ).trimmed()
                                     + spanvalue;
                                }


                            }
                        }


                    }


                    if (rolvalue != tr("firma::N/A")  ) {
                        codes[mytask] = QString("%1%2...%3...")
                                        .arg(codes[mytask])
                                        .arg(rolvalue)
                                        .arg(tsvalue);
                    }
                    else {
                        if (showonly) {
                            QRegExp rx;
                            rx.setPattern(",\\s*(\\d+)...\\d+...\\s*");
                            int pos = codes[mytask].indexOf(rx);
                            int value = 0;
                            bool ok;
                            if ( pos > 0 ) {
                                value = rx.cap(1).toInt(&ok);
                                if (value == 0) {
                                    codes[mytask].remove(rx);
                                }
                            }
                        }
                    }

                    qDebug("...proccessExtraInfo...codes[mytask]...:|%s|",
                           qPrintable( codes[mytask]));


                }
                else {
                    if (showonly) {
                        QRegExp rx;
                        rx.setPattern(",\\s*(\\d+)...\\d+...\\s*");
                        int pos = codes[mytask].indexOf(rx);
                        int value = 0;
                        bool ok;
                        if ( pos > 0 ) {
                            value = rx.cap(1).toInt(&ok);
                            if ( value == 0) {
                                codes[mytask].remove(rx);
                            }
                        }
                    }

                }
                _searchkey.key = info;
                if ( !rolvalue.isEmpty() && !tsvalue.isEmpty() ) {
                    _searchkey.extrainfo[mytask]= rolvalue.trimmed()+";"
                                                  +tsvalue.trimmed();
                }

                break;
            }
        }

    }

    return ;
}

QString SafetWorkflow::renderGraph(const QString& code, const char* ext) {
     QString fileName = "dfs.agl.tmp"; // Nombre del Archivo grafico (temporal)

      // ParÃ¡metros del grÃ¡fico

     return fileName;
}


QString SafetWorkflow::generateGraph(char* filetype, const QString& info) {

    if ( !checkSourcesTask())  {

        return QString(""); // Chequear que todos las tareas existan
    }
    QString codeGraph, parsedCodeGraph, result;
    SafetYAWL::_isstatstokenfound = false; // Inicializando por si es un unico token
    codeGraph = generateCodeGraph(filetype, info);

    qDebug("......****generateGraph......codeGraph... (1):|%s|",qPrintable(codeGraph));


    QString newCodeGraph;
    if (!codeGraph.isEmpty()) {
        QList<SafetYAWL::Miles> ordermiles;
        SafetWorkflow::ExtraInfoSearchKey& sk = searchKey();


        QDateTime previewdate;

        for( int i = 0; i < sk.extrainfo.keys().count(); i++) {
            QString mykey = sk.extrainfo.keys().at(i);

            QString mydata = sk.extrainfo[ mykey];
            QStringList mylist = mydata.split(QRegExp(";|<br/>"));

            qDebug("....mydata:|%s|",qPrintable(mydata));
            if (mylist.count() < 3 ) {
                continue;
            }
            QDateTime mydate = QDateTime::fromString(mylist.at(1),"dd/MM/yyyy hh:mmap");
            if (!mydate.isValid()) {
                qDebug("....Error generateGraph....");
                continue;
            }

            SafetYAWL::Miles mymile;
            mymile.nametask = mykey;

            mymile.ts = mydate;

            mymile.secondswait = 0;


            mymile.humandate = mylist.at(2);
            QString myrol = mylist.at(0);
            mymile.rol = myrol;
            mymile.porc = "";
//            qDebug("....mylist.at(0):|%s|",qPrintable(mylist.at(0)));

            int pos = -1;
            for(int j=0; j < ordermiles.count(); j++) {
                if ( mymile.ts  < ordermiles.at(j).ts ) {
                    pos = j;
                    break;

                }
            }
            if (pos == -1 ) {
                pos = ordermiles.count();
            }
            SYD << tr ("....MainWindow::listDocuments insertando en la posicion: %1")
                   .arg(pos);
            ordermiles.insert(pos,mymile);
            previewdate  = mydate; // Colocar fecha anterior
        }



        QList<int> mymarknodes;
        QStringList mylist = codeGraph.split("\n",QString::SkipEmptyParts);
        for(int i = 0; i< ordermiles.count();i++) {
            int days;
            SafetYAWL::Miles &m = ordermiles[i];

            QString currnode;
            QString currtask = m.nametask;


            int marknode = 0;
            foreach(QString n,  mylist) {
                if ( n.startsWith(QString("Nodo:%1").arg(currtask))) {
                    currnode = n;
                    break;
                }
                marknode++;
            }
            if (marknode < mylist.count()) {
                mymarknodes.append(marknode);
            }
            QString newextra = currnode.section(",",-1);
            newextra = newextra.section("...",0,1)+"...";
            QDateTime mynow = QDateTime::currentDateTime();

            m.humanwait = tr("n/a");
            newextra += m.rol+" "+m.porc+"...";
            QString newextrainfo;
            if ( i > 0 ) {
                SafetYAWL::Miles &prev = ordermiles[i-1];
                m.secondswait = m.ts.toTime_t()-prev.ts.toTime_t();
                m.humanwait = SafetWorkflow::humanizeDate(days,prev.ts.toString("dd/MM/yyyy hh:mm:ssap"),
                                                          "dd/MM/yyyy hh:mm:ssap",
                                                          m.ts,
                                                          SafetWorkflow::WaitTime);


                newextrainfo = QString("%1<br/>%2<br/>%3...")
                        .arg(m.ts.toString("dd/MM/yyyy hh:mmap"))
                        .arg(SafetWorkflow::humanizeDate(days,m.ts.toString("dd/MM/yyyy hh:mm:ssap"),
                                                         "dd/MM/yyyy hh:mm:ssap",
                                                         mynow,
                                                         SafetWorkflow::SinceWhenTime))
                        .arg(SafetWorkflow::humanizeDate(days,prev.ts.toString("dd/MM/yyyy hh:mm:ssap"),
                                                         "dd/MM/yyyy hh:mm:ssap",
                                                         m.ts,
                                                         SafetWorkflow::WaitTime));


            }
            else {
                newextrainfo = QString("%1<br/>%2<br/>%3...")
                        .arg(m.ts.toString("dd/MM/yyyy hh:mmap"))
                        .arg(SafetWorkflow::humanizeDate(days,m.ts.toString("dd/MM/yyyy hh:mm:ssap"),
                                                         "dd/MM/yyyy hh:mm:ssap",
                                                         mynow,
                                                         SafetWorkflow::SinceWhenTime))

                        .arg(tr("n/a"));

            }
            newextra += newextrainfo;

            currnode = currnode.section(",",0,-2)+","+newextra;
            newCodeGraph += currnode + "\n";

        }
        int marknode = 0;
        foreach(QString n,  mylist) {
            if (!mymarknodes.contains(marknode)) {
                newCodeGraph += n + "\n";
            }
            marknode++;
        }

        codeGraph = newCodeGraph;
    }


    QString einfocoloured = SafetYAWL::getConf()["ExtraInfo/coloured"];

    if (einfocoloured == "yes") {
        codeGraph = SafetWorkflow::paintGraph(newCodeGraph);

    }

//    QString einfosumm = SafetYAWL::getConf()["ExtraInfo/summarize"];

//    if (einfosumm == "yes") {
//        codeGraph = SafetWorkflow::paintStats(codeGraph);
//    }



    SafetYAWL::_isstatstokenfound = false;
    QString type = QString(filetype);
    SafetYAWL::lastgraph = codeGraph;
    if ( SafetYAWL::curOutputInterface == NULL ) {




        QString myqml = doGenerateQML(codeGraph);

       result = myqml;
       qDebug("....SafetWorkflow::generateGraph.....after doGenerateQML()");

        return result;
    }

    Q_CHECK_PTR( SafetYAWL::curOutputInterface ) ; // Chequear que exista una Interfaz (Complemento )

    QMap<QString,QString> mymap = SafetYAWL::getConf().getMap();



    parsedCodeGraph = SafetYAWL::curOutputInterface->parseCodeGraph(codeGraph, mymap);

#ifdef SAFETLOG_STDOUT
    SafetYAWL::fileout.close();
    SafetYAWL::fileout.open(stdout, QIODevice::WriteOnly);
     SafetYAWL::streamlog.setDevice(&SafetYAWL::fileout);
#else
    SafetYAWL::filelog.close();
    SafetYAWL::filelog.open(QIODevice::Append);
    SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);
#endif


    result = SafetYAWL::curOutputInterface->renderGraph(parsedCodeGraph, type, mymap);
    if (result.startsWith("ERROR")) {
        SYE << tr("Ocurri� el siguiente error al generar el gr�fico de flujo \"%1\"")
               .arg(result);
        return QString("");
    }





#ifdef SAFETLOG_STDOUT
     SafetYAWL::fileout.close();
     SafetYAWL::fileout.open(stdout, QIODevice::WriteOnly);
     SafetYAWL::streamlog.setDevice(&SafetYAWL::fileout);
#else
    SafetYAWL::filelog.close();
    SafetYAWL::filelog.open(QIODevice::Append);
    SafetYAWL::streamlog.setDevice(&SafetYAWL::filelog);
#endif



    QLocale::setDefault(QLocale::Latin);
    setlocale(LC_ALL,"");

    return result;
}




QString SafetWorkflow::doGenerateQML(const QString& code) {
    QString result;

    qDebug("....SafetWorkflow::doGenerateQML....lasttitle:|%s|",qPrintable(MainWindow::_lasttitle));
    QString listnodes; // Lista de nodos para la infograf�a
    QString listimages; // Lista de im�genes para la infograf�a
    QString listtexts; // Lista de im�genes para la infograf�a
    QString listtimes; // Lista de im�genes para la infograf�a
    QString listagos; // Lista de im�genes para la infograf�a
    QString listdates; // Lista de im�genes para la infograf�a

    QStringList mylist = code.split("\n",QString::SkipEmptyParts);
    int hflick = mylist.count()*130+210;
    int currentx = 180;
    int currenty = 40;
    QString mytitle = MainWindow::convertOpToTitle(MainWindow::_lasttitle).trimmed();
    if (mytitle.length() > 0 ) {
        mytitle = mytitle.mid(0,1).toUpper() + ((mytitle.length() > 1)? mytitle.mid(1): "");
    }

    MainWindow::_lastnumbersteps = mylist.count()-3;

    result += QString( "Text {\n"
                       "id: myTextTitle\n"
                       "anchors.horizontalCenter: parent.horizontalCenter\n"
                       "text:  \"%1\"\n"
                       "font.family: \"Helvetica\" \n"
                       "font.pointSize: 18\n"
                       "font.bold: true\n"
                       "clip: true\n"
                       "color: \"#003a00\"\n"
                       "}\n"
                       ).arg(mytitle);

    result +=  QString("Rectangle{\n"
            "   id: circle1\n"
            "   x: %1\n"
            "   y: %2\n"
            "   width: 100\n"
            "   height: 100\n"
            "   radius: width/2\n"
            "   border.width: 1;\n"
            "   color: \"white\"\n"
                               "Text {\n"
                               "     text: \"Start\"\n"
                               " anchors.centerIn: parent\n"
                               " font.pointSize: 14\n"
                               " }\n"

            "}\n")
            .arg(currentx)
            .arg(currenty);
    currentx = 100;
    currenty = 210;

    int rcount = 1;

    QString newnode;
    QString lastnode;
    QString mynextnode;
    QString mydelnode;
    QString mysafetnote = tr("<empty>");

    bool truelivenote  = false;
    QString firstdate, enddate;

    foreach (QString sec, mylist) {
        qDebug("\n.....doGenerateQML....sec:|%s|", qPrintable(sec));
        QString mytitle;
        QString myinfo = tr("");
        QString mypicture;
        QString mytextinfo;
        QStringList stats;

        QString lastfield = sec.section(",",-1);
        stats = lastfield.split("...");
        if (stats.count() > 0 ) {

            //myinfo = stats.at(0);


            qDebug("....SafetWorkflow:doGenerateQML....stats.count():|%d|",stats.count());
            if (stats.count() > 1 ) {
             //   myinfo += "";
               // myinfo += stats.at(1);
                if (stats.count() > 2 ) {
                 //   myinfo += "<br/>";
                   // myinfo += stats.at(2);

                    QString mytextualinfo;
                    mytextualinfo = stats.at(2);
                    mytextualinfo.replace(Safet::COMMA,",");
                    QStringList mylist = mytextualinfo.split("**SAFETSEPARATOR**");
                    if (mylist.count() > 0 ) {
                        mypicture = mylist.at(0).trimmed();
                        if (mylist.count() > 1 ) {
                            mytextinfo = mylist.at(1).trimmed();
                        }
                        if (stats.count() > 3 ) {
                           //  myinfo = stats.at(2) + "<br/>";
                            myinfo += stats.at(3);
                        }
                    }

                }

            }

        }


        QRegExp rx("Nodo:\\s*([a-zA-Z0-9\\+_\\.]+)\\s*,");
        int pos = rx.indexIn( sec );
        if (pos == -1 ) {
            SYE << tr("Error de sintaxis en el fuente del grafico (Falta Nodo:)");
            return QString("Error");
        }
        newnode = rx.cap(1);
        if (newnode == "_Livenote" ) {
            if ( !mytextinfo.isEmpty()) {
                //mysafetnote = tr("<br/><b>note:</b>%2").arg(mytextinfo);
                mysafetnote = tr(" %2").arg(mytextinfo);
            }
            truelivenote  = !mysafetnote.isEmpty() && mysafetnote != tr("<empty>");
            continue;
        }
        if (newnode == tr("start") || newnode == tr("end")) {
            continue;
        }

        QString titlePattern ="Titulo:\\s*([a-zA-Z\\s0-9;\\+_\\.][a-zA-Z\\s0-9;\\0351\\0341\\+\\-/:_\\s\\.]+[a-zA-Z\\s0-9;\\+_\\.])\\s*,";
        rx.setPattern(titlePattern);
        pos = rx.indexIn( sec );
        if (pos != -1)  {
            mytitle = rx.cap(1);
        }

        result += QString(
        "Image {\n"
         "    x: 210; y: %3; width: 40; height: 70\n"
         "    smooth: true\n"
         "    source: \"qrc:/arrowlightgreen.png\"\n"
         "   MouseArea {\n"
         "      anchors.fill: parent\n"

         "       onClicked: { banner0.text = \"%9\"; \n"
         "            if ( banner0.text.length > 0 ) {\n"
         "                   banner0.y = rect%5.y-50;\n"
         "                   banner0.show();\n"
         "            }\n "
                    "   else { \n"
                    "   console.log(\"arrow\");\n"
                    "     mymain.setInputPath(QDir::homePath()+\"/.safet/input/deftrac.xml\");\n"
                    "     console.log(\"myTitle:\"+mymain.lasttitle()+\"|\"+mymain.lastOperation());\n"
                    "     mymain.clearQmlCommandList();\n"
                    "     handleArrowClick(\"New step %12\",\"deftrac.xml\", \"%13<SAFETSEPARATOR/>%4\"); \n"
                    "     mymain.listQmlCommandList();\n"
                    "  }\n "
         "          }\n"
        "    }        \n"
            "GestureArea {\n"
            "  anchors.fill: parent\n"
              "  onTapAndHold: {\n"
                    "console.log(\"...executing...onTapAndHold...1...newnode:\"+\"%14\");\n"
                    "        mymain.setLastTapAndHold(\"%14\");\n"
                            "if (mymain.lastTapAndHold() == \"%14\") {\n"
                    "      if (!mymain.reachStepLimit() ) {\n"
                    "               console.log(\"not 1\");  \n"
                    "               banner0.text = \"Pro Version only allow Stories of 16 steps!\"; \n"
                    "               banner0.y = rect%5.y-50;\n"
                    "               banner0.show();\n"
                    "                return;  \n"
                    "      }  \n"
                    "     console.log(\"myTitle:\"+mymain.lasttitle()+\"|\"+mymain.lastOperation());\n"
                    "     mymain.clearQmlCommandList();\n"
//                    "     mymain.addQmlCommandList(\"storiesComponent\");\n"
//                    "     mymain.addQmlCommandList(mymain.lasttitle()+\"<SAFETSEPARATOR/>\"+mymain.lastOperation());\n"

                            "  handleArrowClick(\"Add step into story\",\"defflow.xml\", \"%12<SAFETSEPARATOR/>%14\"); \n"
//                            "     mymain.listQmlCommandList();\n"
                    "      }  \n"
                    "  }  \n"
            "}  \n"
         "}                      \n"
        "Rectangle {\n"
        "      id: rect%5\n"
        "      color: \"%8\"\n"
        "       radius: width-5\n"
        "         border.width: 1; x:%1; y:%2; width: 265; height: 80\n"
                           "Text {\n"
                           "     text: \"<b>%4</b><br/>%6\"\n"
                                  " font.pointSize: 18\n"
                                  " anchors.centerIn: parent\n"
                           " }\n"
            "MouseArea {\n"
                    "      anchors.fill: parent\n"
                    "      onClicked: { \n"
                    "               if (tabImage.checked ) {"
                    "               var results = mymain.generateStepInfo(\"%10\",\"%4\",1)\n"
                    "               \n"
                    "                       pageStack.push(Qt.createQmlObject(results,parent)); \n"
                    "               return;\n"
                    "} else if (tabText.checked ) {\n"
                    "               var results = mymain.generateStepInfo(\"%11\",\"%4\",0)\n"
                    "                       pageStack.push(Qt.createQmlObject(results,parent)); \n"
                    "               return;\n"
                    "} else {\n"
              "                          banner0.text = \"%7\"; \n"
                    "  }\n"
                    "            if ( banner0.text.length > 0 ) {\n"
                    "                     banner0.y = rect%5.y-50;\n"
                    "                   banner0.show(); \n"
                    "            }\n"
                    "      }\n"
              "      }\n"
                    "GestureArea {\n"
                    "  anchors.fill: parent\n"
                      "  onTapAndHold: {\n"
                          "        mymain.setLastTapAndHold(\"%4\");\n"
                                  "if (mymain.lastTapAndHold() == \"%4\") {\n"
                                        "if (mymain.steps() == 1 ) {\n"
                    "                       banner1.y = rect%5.y+150;\n"
                    "                      banner1.show(); \n"
                                        "}  else { \n"
                                      "     console.log(\"myTitle:\"+mymain.lasttitle()+\"|\"+mymain.lastOperation());\n"
                                      " handleArrowClick(\"Delete step in story\",\"defflow.xml\", \"%12<SAFETSEPARATOR/>%4\"); \n"
                                     "  \n"
                                    "}  \n"
                                  "}  \n"
                          "}  \n"
                    "}  \n"


         "  }\n "


                    )
                .arg(currentx)
                .arg(currenty)
                .arg(currenty-70)
                .arg(newnode)
                .arg(rcount)
                .arg(mytitle)
                .arg(myinfo.section("<br/>",0,1).trimmed()) //7
                .arg(myinfo.length()>0?"#00B000":"white")
                .arg(myinfo.section("<br/>",2,2).trimmed())
                .arg(mypicture) //10
                .arg(mytextinfo) // 11
                .arg(MainWindow::_lasttitle.section("/",0,0).toLower())
                .arg(MainWindow::_lasttitle.section("/",1,1))
                .arg(lastnode);


           lastnode = newnode;

           if (myinfo.length() == 0 ) {
               if (   mynextnode.isEmpty()) {
                      mynextnode = newnode;
               }
           }
           else {
               mydelnode = newnode;
           }


            listnodes += newnode  + SafetYAWL::LISTSEPARATOR;
            listimages += mypicture + SafetYAWL::LISTSEPARATOR;
            listtexts += mytextinfo + SafetYAWL::LISTSEPARATOR;
            listtimes += myinfo.section("<br/>",2,2) + SafetYAWL::LISTSEPARATOR;
            listagos += myinfo.section("<br/>",1,1) + SafetYAWL::LISTSEPARATOR;
            listdates += myinfo.section("<br/>",0,0) + SafetYAWL::LISTSEPARATOR;
            if (firstdate.isEmpty()) {
                firstdate = myinfo.section("<br/>",0,0);
            }
            if ( !myinfo.isEmpty()) {
                    enddate = myinfo.section("<br/>",0,0);
            }


        rcount++;
      currenty = currenty + 150;

    }

    qDebug("(1)....SafetWorkflow::doGenerateQML....firstdate:|%s|", qPrintable(firstdate));
    qDebug("(2)....SafetWorkflow::doGenerateQML....enddate:|%s|", qPrintable(enddate));

    int days = 0;
    QString timelapse;


    if (!firstdate.isEmpty() ) {
        timelapse= tr("<b>total time:</b>%1").arg(SafetWorkflow::humanizeDate(days,
                                                    firstdate,
                                                    "dd/MM/yyyy hh:mmap",
                                                    QDateTime::fromString(enddate,"dd/MM/yyyy hh:mmap")));
    }



    qDebug("(3)....SafetWorkflow::doGenerateQML....enddate:|%s|", qPrintable(timelapse));
    listnodes.chop(SafetYAWL::LISTSEPARATOR.length());
    listimages.chop(SafetYAWL::LISTSEPARATOR.length());
    listtexts.chop(SafetYAWL::LISTSEPARATOR.length());
    listtimes.chop(SafetYAWL::LISTSEPARATOR.length());
    listagos.chop(SafetYAWL::LISTSEPARATOR.length());
    listdates.chop(SafetYAWL::LISTSEPARATOR.length());

    currentx = 180;
    result += QString("Image {\n"
     "    x: 210; y: %2; width: 40; height: 70\n"
     "    smooth: true\n"
     "    source: \"qrc:/arrowlightgreen.png\"\n"
      "   MouseArea {\n"
      "      anchors.fill: parent\n"
      "       onClicked: {  \n"
                      "      if (!mymain.reachStepLimit() ) {\n"
                      "                console.log(\"not 2\");  \n"
                      "               banner0.text = \"Pro Version only allow Stories of 16 steps!\"; \n"
                      "               banner0.y = %3-50;\n"
                      "               banner0.show();\n"
                      "                return;  \n"
                      "      }  \n"
                 "     console.log(\"myTitle:\"+mymain.lasttitle()+\"|\"+mymain.lastOperation());\n"
                  "     mymain.clearQmlCommandList();\n"
//                  "     mymain.addQmlCommandList(\"storiesComponent\");\n"
//                  "     mymain.addQmlCommandList(mymain.lasttitle()+\"<SAFETSEPARATOR/>\"+mymain.lastOperation());\n"
                 "     handleArrowClick(\"Add step into story\",\"defflow.xml\", \"%4<SAFETSEPARATOR/>%5\"); \n"
                 "     mymain.listQmlCommandList();\n"
      "          }\n"
     "    }                       \n"
     "}                                 \n"
     "Rectangle{\n"
     "   id: circle2\n"
     "   x: %1\n"
     "   y: %3\n"
     "   width: 100\n"
     "   height: 100\n"
     "   radius: width/2\n"
     "   border.width: 1;\n"
     "   color: \"white\"\n"
                        "Text {\n"
                        "     text: \"end\"\n"
                        " anchors.centerIn: parent\n"
                        " font.pointSize: 14\n"
                        " }\n"
     "  }\n")
            .arg(currentx)
            .arg(currenty-130+60)
            .arg(currenty-130+80+50)
            .arg(MainWindow::_lasttitle.section("/",0,0).toLower())
            .arg(lastnode);
    result += QString("   InfoBanner{\n"
    "                 id: banner0\n"
    "                 text: qsTr(\"info!\")\n"
    "}\n");

    result += QString("   InfoBanner{\n"
    "                 id: banner1\n"
    "          text : qsTr(\"Stories have only one step, isn't possible delete it\")\n"
    "}\n");



    if (truelivenote || !timelapse.isEmpty() ) {

        result += QString( "Text {\n"
                           "id: mySafetNote\n"
//                           "anchors.horizontalCenter: parent.horizontalCenter\n"
                           "anchors.left: parent.left\n"
                           "anchors.leftMargin: 5\n"
                           "anchors.right: parent.right\n"
                           "anchors.rightMargin: 5\n"
                           "anchors.top: circle2.bottom\n"
                           "anchors.topMargin: 20\n"
                           "wrapMode: Text.WordWrap\n"
                           "text:  \"%1\"\n"
                           "font.family: \"Nokia Pure Text Light\" \n"
                           "font.pointSize: 16\n"
                           "clip: true\n"
                           "color: \"black\"\n"
                           "  GestureArea {\n"
                           "  anchors.fill: parent\n"
                           "        onTapAndHold: {\n"
                           "        mymain.setLastTapAndHold(\"%3\");\n"
                           "     if (mymain.lastTapAndHold() == \"%3\") {\n"
                           "           mymain.addQmlCommandList(mymain.lasttitle()+\"<SAFETSEPARATOR/>\"+mymain.lastOperation());\n"
                           "          mymain.toInputForm(\"operacion:Delete_live_step Name_live_story: %2 Name_status: %3 \");\n"
                           "          var myrepage = pageStack.clear(); \n"
                           "          myrepage = pageStack.push(mainPage,null,true); \n"
                           "          mymain.saveQmlCommandList();\n"
                           "          while( !mymain.emptyQmlCommandList()) {\n"
                           "                    var mycmd = mymain.dequeueQmlCommand();\n"
                           "                    handleCmdStack(mycmd);\n    "
                           "          }\n"
                           "          mymain.restoreQmlCommandList();\n"
                           "      }\n"
                           "      }\n"
                           "   }\n"
                           "  }\n"
                           ).arg(tr("%1%2").arg(timelapse).arg(mysafetnote))
                           .arg(MainWindow::_lasttitle.section("/",1,1))
                            .arg("_Livenote");
        qDebug("....SafetWorkflow::doGenerateQML...result:|%s|", qPrintable(result));

    }


    result +=  QString("  }\n"
                       "}\n");


    QString mycamera = QString("               handleCameraClick(\"%1\",\"%2\",\"%3\"); \n")
            .arg(MainWindow::_lasttitle.section("/",0,0).toLower()) //5
            .arg(MainWindow::_lasttitle.section("/",1,1))             //6
            .arg(mynextnode);



    QString mynocameranosheet = QString("  \n"
                                        "                var myOperation = \"operacion: New_step_%1 id:%2 Next_status:%3 \";\n"
                                        "               console.log(\"no sheet no camera myOperation:\"+ myOperation);\n"
                                        "                playSound.play();\n"
                                        "               var result = mymain.toInputForm(myOperation);\n"
                                        "    if (result == \"successfully!\") {\n"
                                        "            var mylasttitle = mymain.lasttitle();\n"
                                        "            var mylastoperation = mymain.lastOperation();\n"
                                        "           mymain.addQmlCommandList(mylasttitle+\"<SAFETSEPARATOR/>\"+mylastoperation);\n"
                                        "                playSound.play();\n"
                                        "                var myrepage = pageStack.clear(); \n"
                                        "        myrepage = pageStack.push(mainPage,null,true); \n"
                                        "              mymain.saveQmlCommandList();\n"
                                        "             while( !mymain.emptyQmlCommandList()) {\n"
                                        "                    var mycmd = mymain.dequeueQmlCommand();\n"
                                        "                    handleCmdStack(mycmd);\n    "
                                        "            }\n"
                                        "\n             "
                                        "            mymain.restoreQmlCommandList();\n"
                                        "         mymain.listQmlCommandList(); \n"
                                        "      }\n"

                                        )
            .arg(MainWindow::_lasttitle.section("/",0,0).toLower()) //5
            .arg(MainWindow::_lasttitle.section("/",1,1))             //6
            .arg(mynextnode);


    QString myaction = mycamera;

    if ( !MainWindow::mymainwindow->checkBoolOption("StoriesOptions/usecamera") && !MainWindow::mymainwindow->checkBoolOption("StoriesOptions/showinfodialog")) {
            myaction = mynocameranosheet;
    }
    else if (!MainWindow::mymainwindow->checkBoolOption("StoriesOptions/usecamera") && MainWindow::mymainwindow->checkBoolOption("StoriesOptions/showinfodialog")) {
        myaction = QString( "myInfoSheet.open();\n" );

    }

    QString mysensors = QString(""

    "  ProximitySensor {\n"
    "        id: proximitySensor\n"
    "        active: true\n"
    "        onReadingChanged: {   \n"
            " if (loadcamera == false) {\n"
    "        if ( proximitySensor.reading != null &&  proximitySensor.reading.close  && orientationSensor.reading.orientation ==  OrientationReading.FaceDown) {\n"
    "               console.log(\"(1)Proximity close and Right Up!!\");\n"
    "               loadcamera = true;\n"
    "               %1 \n"
    "         }\n"
    "      }\n"
    "      }\n"
    "    }\n"
    "OrientationSensor {\n"
    "        id: orientationSensor\n"
    "        active: true\n"

    "        onReadingChanged: {\n"
            " if (loadcamera == false) {\n"
    "        if ( proximitySensor.reading != null && proximitySensor.reading.close  && reading.orientation ==  OrientationReading.FaceDown ) {\n"
    "               console.log(\"(1)Proximity (O) close and Right Up!!\");\n"
    "               loadcamera = true;\n"
    "               %1 \n"
    "         }\n"
    "        }\n"
    "        }\n"
    "   }\n"

    "    Sheet {\n"
    "        id: myInfoSheet\n"
    "        anchors.fill: parent\n"
    "        rejectButtonText: \"Cancel\"\n"
    "        acceptButtonText: \"Accept\"\n"

    "        content: TextArea  { id: myInfoArea; anchors.fill: parent}\n"
    "        onAccepted:  {\n"
    "            var myOperation = \"\"; \n"
    "        if (myInfoArea.text.length == 0 ) {\n"
    "            myOperation = \"operacion:%2 id:%3 Next_status:%4\";\n"
    "        } else {\n"
    "            myOperation = \"operacion:New_step_%2 id:%3 Next_status:%4  Info:\"+ myInfoArea.text;\n"
    "        }\n"
    "            console.log(\"camera myOperation:\"+ myOperation);\n"
    "            mymain.addQmlCommandList(mymain.lasttitle()+\"<SAFETSEPARATOR/>\"+mymain.lastOperation());\n"
    "            mymain.toInputForm(myOperation);\n"
    "        var myrepage = pageStack.clear(); \n"
    "        myrepage = pageStack.push(mainPage,null,true); \n"
    "              mymain.saveQmlCommandList();\n"
    "             while( !mymain.emptyQmlCommandList()) {\n"
    "                    var mycmd = mymain.dequeueQmlCommand();\n"
    "                    handleCmdStack(mycmd);\n    "
    "            }\n"
        "\n"
        "         mymain.restoreQmlCommandList();\n"
        "         mymain.listQmlCommandList(); \n"
        "  }\n"
       "  }\n"

                                )
        .arg(myaction)
        .arg(MainWindow::_lasttitle.section("/",0,0).toLower())
        .arg(MainWindow::_lasttitle.section("/",1,1))
        .arg(mynextnode);


    QString imagesensor = QString( ""
                                   " Image {\n"
                                                   "id: sensorImage\n"
                                       "anchors.top: parent.top\n"
                                       "anchors.right: parent.right\n"
                                        "anchors.rightMargin: 8\n"
                                        "anchors.topMargin: 8\n"
                                                   "width: 48\n"
                                                   "height: 48\n"
                                       "source:  \"qrc:/arrowtosquare.svg\"\n"
                                   "}\n"
                                                   "Text {\n"
                                                   "anchors.top: sensorImage.bottom\n"
                                                   "anchors.topMargin: 3\n"
                                                   "anchors.left: sensorImage.left\n"
                                                    "anchors.leftMargin: -62\n"
                                                   " text: \"Face Down and Close\"\n"
                                                   " font.pointSize: 8\n"
                                                   "font.family: \"Helvetica\"\n"
                                                   "color: \"black\" \n"
                                                   "}\n");


    bool issensors = MainWindow::mymainwindow->checkBoolOption("StoriesOptions/usesensor");
    QString startresult = QString("import QtQuick 1.1\n"
                         "import com.nokia.meego 1.0\n"
                         "import com.nokia.extras 1.0\n"
                         "import Qt.labs.gestures 1.0\n"
                         "import QtMobility.sensors 1.2\n"
                         "import QtMultimediaKit 1.1\n"
                "   Page {\n"
                "     id: myGraphPage\n"
                 "    objectName: \"myGraphPage\"\n"
                     " property alias texttitle: myTextTitle.text\n"
                     " property alias flickobject: myFlickComplete\n"
                     " property bool loadcamera: false\n"
                 "orientationLock: PageOrientation.LockPortrait\n"
                "    tools: ToolBarLayout {\n"
                "           id: myConsolePageToolbar\n"
                "            ToolIcon { iconId: \"toolbar-back\"; onClicked: {\n"
//                "                        var mypop = mymain.dequeueQmlCommand();\n"
                    "                    pageStack.pop(); \n"
                    "                        }\n"
                    "             }\n"
//                     "       ToolIcon   { iconId: \"toolbar-share\"; onClicked: {\n"
//                     "                                                        pageStack.push(Qt.createQmlObject(mymain.generateStepInfo(\"%2\",\"%3\",%12,\"%4\"),parent)); \n"
//                     "                                                }\n"
//                     "             }\n"
                      "       ToolIcon   { iconId: \"toolbar-edit\"; onClicked: {\n"
                                  "    if ( !%8 ) {         \n"
                                  "console.log(\"handleArrowClick\");\n"
                                          "     handleArrowClick(\"New step %5\",\"deftrac.xml\", \"%6<SAFETSEPARATOR/>%7\"); \n"
                      "                } else {\n"
                                        "console.log(\"banner:\"+%1);\n"
                      "                 banner0.text = \"A note already exists!. Delete it to exchange for a new note.\"; \n"
                      "                 banner0.y = %10-100;\n"
                      "                 banner0.show();\n"
                      "                 }\n"
                      "                                                        }\n"
                      "             }\n"
                      " ButtonRow {\n"
                      "           TabButton {\n"
                      "               id: tabTime\n"
                      "               text: \"Time\"\n"
                      "           }\n"
                                 "TabButton {\n"
                                 "    id: tabText\n"
                                 "    text: \"Text\"\n"
                                 "}\n"
                                 "TabButton {\n"
                                 "    id: tabImage\n"
                                 "    text: \"Img\"\n"
                                 "}\n"
                             "}                     \n"
                                  "ToolIcon { \n"
                                  "iconId: \"toolbar-view-menu\"\n"
                                  "anchors.right: parent===undefined ? undefined : parent.right\n"
                                  "onClicked: (functionMenu.status == DialogStatus.Closed) ? functionMenu.open() : functionMenu.close()\n"
                                  "}\n"

                "      }\n"
                                  "Menu {"
                                  " id: functionMenu\n"
                                  "   visualParent: pageStack\n"
                                  "  MenuLayout {\n"
                                  "     MenuItem { text: \"Deleve live step\"; onClicked: { \n"
                                  "                 handleArrowClick(\"Delete live step\",\"deftrac.xml\", \"%6<SAFETSEPARATOR/>%14\"); \n"
                                  "        }\n"
                                  "}\n"
                                  "     MenuItem { text: \"Change info live step\"; onClicked: { \n"
                                  "                 handleArrowClick(\"Change info live step\",\"deftrac.xml\", \"%6<SAFETSEPARATOR/>%14\"); \n"
                                  "                 console.log(\"data default:\"+ \"%14\")\n"
                                  "        }\n"

                                  "}\n"
                                  "      MenuItem { text: \"Show Infographic\"; onClicked: { \n"
                                  "         console.log(\"showing infographics for menu\");\n"
                                  "         var results = mymain.generateStepInfo(\"%2\",\"%3\",%12,\"%4\");\n"
                                  "                 pageStack.push(Qt.createQmlObject(results,parent) ) ;\n"
                                  "      } \n"
                                    "}\n"
                                  "          }\n"
                                  "}\n"


                " %11 "

                "Flickable {\n"
                 "id: myFlickComplete \n"
                 "width: 480; height: 854\n"
                 "contentWidth: 480; contentHeight: %1\n"
                                  "%13\n"
                      )
            .arg(hflick+100)
            .arg((listimages + Safet::SEPARATOR +  listtexts + Safet::SEPARATOR + listtimes + Safet::SEPARATOR + listdates + Safet::SEPARATOR + listagos + Safet::SEPARATOR + mysafetnote+ Safet::SEPARATOR +timelapse).trimmed())
            .arg(listnodes.trimmed())
            .arg(tr("Infographic-") + mytitle)
            .arg(MainWindow::_lasttitle.section("/",0,0).toLower()) //5
            .arg(MainWindow::_lasttitle.section("/",1,1))             //6
            .arg("_Livenote") //7
            .arg(truelivenote?"true":"false")// 9
            .arg(currenty) // 10
            .arg(issensors?mysensors:"") //11
            .arg(MainWindow::mymainwindow->checkBoolOption("StoriesOptions/typeinfographic")?"3":"2") //12
            .arg(issensors?imagesensor:"")  // 13
            .arg(mydelnode); //14





    result = startresult + result;


    //qDebug("\n\n\n...........SafetWorkflow::doGenerateQML....mysafetnote:|%s|\n\n\n",qPrintable(mysafetnote));
    //qDebug("result:\n|%s\n|",qPrintable(result));
    return result;
}





QString SafetWorkflow::paintGraph(const QString& code) {
    QString result;
    QStringList mynodes = code.split("\n",QString::SkipEmptyParts);

    QString einfosumm = SafetYAWL::getConf()["ExtraInfo/summarize"];

    bool iseinfosumm = einfosumm == "yes";
    QRegExp rxColor;

    rxColor.setPattern("info\\.task\\.color:\\s*([0-9\\.]+)");

    QString colorcompleted = QString("info.task.color: 0.4");
    QString colorcurrent = QString("info.task.color: 1");
    QString colorpartial = QString("info.task.color: 0.1");
    QString replacecolor = colorcompleted;
    QString finalnode;
    int totalporc = 0;

    foreach(QString mynode, mynodes) {

        QString mynewnode = mynode;
        QString s = mynode.section(",",-1);


        QStringList myinfos =  s.split("...", QString::SkipEmptyParts);

        QString spartial = SafetYAWL::getConf()["ExtraInfo/infotext.completed"];
        if (iseinfosumm) {
            if (mynode.startsWith("Nodo:final,")) {

                mynewnode += QString("Total: %1%...  ...");
                finalnode = mynewnode;
                continue;
            }
            else {
                if (myinfos.count() > 2 ) {
                    QString myinfotext = myinfos.at(2);
                    QRegExp myrx("([0-9]+)\\%");
                    int pos = myinfotext.indexOf(myrx);
                    if (pos >= 0) {
                        bool ok;
                        totalporc += myrx.cap(1).toInt(&ok);
                    }
                }
            }
        }
        if (myinfos.count() > 2 ) {
            int pos = mynode.indexOf(rxColor);
            if (pos >= 0 ) {
                if ( rxColor.cap(1) == "1") {
                    replacecolor  = colorcurrent;
                }
                pos = s.indexOf(spartial);
                if ( pos>=0) {
                    replacecolor = colorpartial;
                }
            }
            else {
                result += mynewnode;
                result += "\n";
                continue;
            }

            mynewnode.replace(rxColor,replacecolor);
        }

        result += mynewnode;
        result += "\n";
    }

    if (iseinfosumm) {
        result += finalnode.arg(totalporc);
        result += "\n";
   }

    return result;
}

QString SafetWorkflow::paintStats(const QString& code) {
    QString result;
    QStringList mynodes = code.split("\n",QString::SkipEmptyParts);


    QString finalnode;
    int totalporc = 0;
    foreach(QString mynode, mynodes) {
        QString mynewnode = mynode;
        QString s = mynode.section(",",-1);
        QStringList myinfos =  s.split("...", QString::SkipEmptyParts);
        if (mynode.startsWith("Nodo:final,")) {

            mynewnode += QString("Total: %1%...  ...");
            finalnode = mynewnode;
        }
        else {
            if (myinfos.count() > 2 ) {
                QString myinfotext = myinfos.at(2);
                QRegExp myrx("([0-9]+)\\%");
                int pos = myinfotext.indexOf(myrx);
                if (pos >= 0) {
                    bool ok;
                    totalporc += myrx.cap(1).toInt(&ok);
                }
            }
            result += mynewnode;
            result += "\n";
        }
    }

    result += finalnode.arg(totalporc);
    result += "\n";


    return result;
}


QString SafetWorkflow::convertSvgShapeToJSON(const QDomElement& n) {
	
	QString shape = "";
	if (  !SHAPEMAP.contains(n.nodeName())  ) {
		return shape;
	}
	int ishape = SHAPEMAP[ n.nodeName() ];
	shape = "\t\t\t{\n";	
	QMap<QString,QString> map;
   	QDomNamedNodeMap attrs = n.attributes ();
	
   	for(int i = 0; i < attrs.count(); i++) {  		
   		 map[attrs.item(i).nodeName()] = attrs.item(i).nodeValue();
   	 }
	QString currentName = n.nodeName(), str = "%1";
	bool ok;
	switch ( ishape ) {
		case 0: // Ellipse;

			map["cy"] = scaleToJSON(map["cy"],0,500,0,500);	
			currentName += str.arg(shapecount++); 
			shape += "\t\t\t\t\"name\": \""+ currentName +"\",\n";
			shape += "\t\t\t\t\"shape\": {\n\t\t\t\t \"type\": \"ellipse\",\n";
			shape += "\t\t\t\t\"cx\": " + map["cx"] + ",\n";
			shape += "\t\t\t\t\"cy\": " + map["cy"] + ",\n";
			shape += "\t\t\t\t\"rx\": " + map["rx"] + ",\n";
			shape += "\t\t\t\t\"ry\": " + map["ry"] + "";
			break;
		case 1:
 			map["d"] = scaleToJSON(map["d"],0,500,0,500);
			currentName += str.arg(shapecount++); 
			shape += "\t\t\t\t\"name\": \""+ currentName +"\",\n";
			shape += "\t\t\t\t\"shape\": {\n\t\t\t\t \"type\": \"path\",\n";
			shape += "\t\t\t\t\"path\": \"" +  map["d"] + "\"";
			break;
		case 2: // texto
			map["x"] = str.arg(map["x"].toInt(&ok) - (n.text().length()*3.5));
 			map["y"] = scaleToJSON(map["y"],0,500,0,500);
			currentName += str.arg(shapecount++); 
			shape += "\t\t\t\t\"name\": \""+ currentName +"\",\n";
			shape += "\t\t\t\t\"shape\": {\n\t\t\t\t \"type\": \"text\",\n";
			shape += "\t\t\t\t\"text\": \"" +  n.text() + "\",\n";
			shape += "\t\t\t\t\"x\": " +  map["x"] + ",\n";
			shape += "\t\t\t\t\"y\": " +  map["y"] + "";
			break;
		case 3:
 			map["points"] = scaleToJSON(map["points"],0,500,0,500);
			map["points"] = map["points"].replace(QRegExp("\\s+"), ",");
			currentName += str.arg(shapecount++); 
			shape += "\t\t\t\t\"name\": \""+ currentName +"\",\n";
			shape += "\t\t\t\t\"shape\": {\n\t\t\t\t \"type\": \"polyline\",\n";
			shape += "\t\t\t\t\"points\": [" +  map["points"] + "]";
			break;
			break;
	}			
	shape += "\n\t\t\t\t},";
	QString style ="";
	if ( ishape != 2 ) {
		style = addShapeStyleToJSON( map[ "style" ] );
	}
	else {
		style =  "\n\t\t\t\t\"fill\": \"black\",\n";
		style +=  "\t\t\t\t\"stroke\": \"black\"\n";	
	}
	shape += style;		
	if ( style.length() == 0  )  shape.chop(1);
	shape += "\n\t\t\t},\n";	
	return shape;
}

QString SafetWorkflow::scaleToJSON(const QString& s, int minx, int maxx, int miny, int maxy) {
	QString newnumber = "%1";
// 	int rangeorix = orimaxx - oriminx;
// 	int rangeoriy = orimaxy - oriminy;
// 	int rangenewx = maxx - minx;
// 	int rangenewy = maxy - miny;
	bool ok;
	QRegExp rx("(\\-\\d+)");
	QString str = s;
	int pos = 0;
	while ((pos = rx.indexIn(str, pos)) != -1) {
		double n =  rx.cap(1).toInt(&ok) - oriminy + miny;
		if ((pos-1) >= 0 && (pos-1) < str.length()  ) {
			if ( str.at( pos -1) != 'e' ) 
				str.replace(pos, rx.cap(1).length(), newnumber.arg(n));
		}
		else {
			str.replace(pos, rx.cap(1).length(), newnumber.arg(n));
		}
		pos += rx.matchedLength();

	}
	return str;
}

QString SafetWorkflow::addShapeStyleToJSON( const QString& s ) {
	QString str;
	str += "\t\t\t\t\n";
	QStringList list = s.split(";", QString::SkipEmptyParts);
	for (int i = 0 ; i < list.count(); i++) {
		QStringList fieldlist = list.at(i).split(":");
		Q_ASSERT_X(fieldlist.count() == 2, "addShapeStyleToJSON",
		qPrintable(tr("El campo estilo (style) del archivo svg/xml no contiene un numero de campos correctos: %1").arg(fieldlist.count())));
		str += "\t\t\t\t\"" + fieldlist.at(0) + "\":" + "\"" + fieldlist.at(1) + "\",\n";
			
	}
	str.chop(2);
	str += "\n";
	return str;
}

QString SafetWorkflow::svgToJSON(const QString& s) {
 	 QString jstr = "[\n";
	 QString currentName = "\"name\": \""+ QString("flujo") +"\",\n";
	 jstr += "{\n\t\t"+ currentName +"\t\t\"children\": [\n";
 	 QDomDocument doc;	
	 QFile file(s);
	 Q_ASSERT_X(QFile::exists(s), "svgToJSON",qPrintable(tr("Archivo SVG no existe: %1").arg(s)));
	 bool result = file.open(QIODevice::ReadOnly);
	 Q_ASSERT_X(result, "svgToJSON","No es posible abrir el archivo en modo solo lectura");
	 result = doc.setContent(&file);
         Q_ASSERT_X(result, "svgToJSON","No es posible leer adecuadamente el contenido del archivo XML");
	 file.close();
	 QDomElement docElem = doc.documentElement();
	 QDomNode n = docElem.firstChild();
	 QStack<QDomNode> control;	 
	 while( true ) {
		
		 if ( n.isNull() ) {
 	    	 if (control.isEmpty() ) break;
 	    	 n = control.pop();
 	 	 }		
		 if ( n.isElement() ) {
//		     qDebug("Nodo: %s", qPrintable(n.nodeName()));
		     jstr = jstr + convertSvgShapeToJSON( n.toElement() );   		
		     QDomElement e = n.toElement(); // try to convert the node to an element.
		     if ( n.hasChildNodes() && n.firstChild().isElement() ) {
		    	 control.push(n.nextSibling());
		    	 n = n.firstChild();
		    	 continue;
		     }
		 }
	    	 
	     n = n.nextSibling();	     	     
	 }	
	jstr.chop(2);
	jstr += "\n\t\t]\n\t}\n]";
	return jstr;
}

QString SafetWorkflow::addJScriptToMap( const QString& map ) {
	QString str = map;
	QRegExp rx("/>"); // Colocar el campo clave en la lista de campos de la sentencia SQL
	QRegExp rxRemove("title=\"([a-zA-Z0-9_ ]+)\""); // ExpresiÃ³n regular de remociÃ³n
    	rx.setCaseSensitivity(Qt::CaseInsensitive);
	int pos = 0;
	while ( true ) {
		pos = rxRemove.indexIn( str, pos);
		if ( pos == -1 ) break;
		QString label = rxRemove.cap( 1 );
//		qDebug("captura %d: %s", pos, qPrintable(label) );
		QStringList fields = label.split(QRegExp("\\s+"));
		if (fields.count() ==2 ) {
			QString replacestring = "onclick=\"alert('Datos')\" onmouseover=\"Tip('Existe(n)  <b><font color=black size=3>"+fields.at(0)+"</font></b>  documentos de tipo <br/> <b><font color=black size=3>"+ fields.at(1) +"</font></b> pendiente(s)',SHADOW, true,BGCOLOR, '#EEEE00')\" onmouseout=\"UnTip()\" ";
			str.replace( rxRemove.cap( 0 ), replacestring);
		}
		pos = pos + label.length()+1;
	}
	return str;
}

QString SafetWorkflow::addGraphvizNode(const QString& s, char* filetype,const QString& info) {
	SafetNode *node = nodemap[ s ];
	Q_CHECK_PTR( node );
	QString result, nameport, namepattern, namelink, urlstring, 
         type; // Tipo de archivo a generar por graphviz (png, cmap, imap, ...)
	SafetCondition *cond;
	SafetTask *task;
	int ntokens = 0;
	QString strcolor;
	int intcolor;
	QSet<QString> keys = getKeys(s);
	switch(SafetYAWL::getClassReference(SafetYAWL::translateNS(node->metaObject()->className()))) {
	case 2: // task
		task = qobject_cast<SafetTask*>(node);
		Q_CHECK_PTR(task);
		urlstring = "#";
		namelink ="node.";
		for(int i=0; i < task->getPorts().count(); i++) {
				nameport = task->getPorts().at(i)->type().toLower();
				namepattern = task->getPorts().at(i)->pattern().toLower();
				if ( nameport.compare( "split") == 0 || nameport.compare( "join") == 0 ) {
					namelink +=  nameport + "." +  namepattern; 
					break;
				}
			if ( (i-1)  ==  task->getPorts().count() ) namelink += "none";			
		}	
	
        if ( s.compare(tr("start"), Qt::CaseInsensitive) != 0 &&
            s.compare(tr("end"), Qt::CaseInsensitive) != 0) {
			ntokens = numberOfTokens ( s ) ;
		}
		type =  filetype; // convertir de char* a QString
                intcolor = 255-(ntokens * 300)/6;
                strcolor = QString("#%1").arg(0, 2, 16,QLatin1Char('0'));
                strcolor = strcolor + QString("%1").arg(intcolor, 2, 16,QLatin1Char('0'));
                strcolor = strcolor + QString("%1").arg(0, 2, 16,QLatin1Char('0'));

                if ( type == "cmapx"  ||  type == "cmap"    ) {
                     QString alt = QString("%1 %2").arg(ntokens).arg(s);
                     urlstring = "\"#\"";
                     result = s + "[label=\"" + alt + "\",fontcolor=black,fontname=\"Bitstream Vera Sans\",color=\"#DEFFD7\",shape=box, URL=" + urlstring + "];\n";
                }
                else if ( type.compare("svg", Qt::CaseInsensitive ) == 0 ||
                          type.compare("png", Qt::CaseInsensitive ) == 0) {
                     QVariant myvar = SafetYAWL::getConfFile().getValue("Workflow", "defaultcolor");
                     QString statecolor = myvar.toString();
                     if ( info.length() == 0 ) {
                          QVariant myvar = SafetYAWL::getConfFile().getValue("Workflow", "activecolor");
                          statecolor = myvar.toString();
                     } else if (keys.contains(info)  ) {
                          QVariant myvar = SafetYAWL::getConfFile().getValue("Workflow", "traccolor");
                          statecolor = myvar.toString();
                     } else if ( info.compare( "stats:coloured", Qt::CaseInsensitive ) == 0  ) {
                          statecolor = strcolor;
                     }
                     result = s + "[label=\"" + s + "\",fontcolor=black,color=\""+ "black" + "\",style=filled,fillcolor=\""+statecolor+"\",shape=box];\n";
                     break;
                }
                else {
                     result = s + " [label=\"" + s + "\" ,fontsize=14,style=filled,bgcolor=yellow,fontcolor=white,fontname=\"Bitstream Vera Sans\",color=\""+strcolor+ "\",shape=box, URL=\"" + urlstring + "\"];\n";
                     //qDebug("StrColor result: %s", qPrintable(result));
                }

 		break;
	case 5: //condition
		cond = qobject_cast<SafetCondition*>(node);
		Q_CHECK_PTR(cond);
//		result = s + " [label=\"&#8226;\" fonsize=8,fontname=\"Bitstream Vera Sans\",color=white,shape=circle,shapefile=\""+
//		RESOURCESFILES[ "condition." + cond->type() ] + "\"];\n";
                result = s + " [label=\"&#8226;\",fontsize=16,fontname=\"Bitstream Vera Sans\",color=black,shape=circle" + "];\n";
		break;
	default:;
	}
	return result;
}


QString SafetWorkflow::generateSQLPortKeyFilterExpression(SafetPort* port, int curroption, int npath) {
    Q_CHECK_PTR( port );
   //   qDebug("....curroption: %d ...npath: %d", curroption, npath);
    QString sql = parser.checkBrackets(port->query(curroption)); // Chequear la consulta, quizÃ¡s se debe incluir tambien el patron
    Q_ASSERT_X(parser.error() ==	SafetParser::CORRECT, "generateSQLPortKeyFilterExpression",
               qPrintable(tr("Los parentesis no estÃ¡n balanceados en la expresion: <%1>").arg(port->query(curroption))) );
    parser.setStr(sql);
    parser.parse();
//      qDebug("......generateSQLPortKeyFilterExpression.......(1).....");
    _tablesource = parser.getTablesource();
    QString tokenlink = processTokenlink(port->tokenlink(),true,npath);
//      qDebug("......generateSQLPortKeyFilterExpression.......(2).....");

//    QStringList droplist = UniteWhereset(); // Unificar las clÃ¡usulas "WHERE"
//    qDebug("droplist.count(): %d", droplist.count());
//    qDebug("...sql: %s", qPrintable(sql));
    bool result;
    if ( !SafetYAWL::canTypeConvert(sql, QVariant::Bool) ) {
               Q_ASSERT( parser.getFields().count() > 0 );
               result = delWhereClause(parser.getFields().at(0), true );
//               qDebug("result: %d", result);
    }
//      qDebug("......generateSQLPortKeyFilterExpression.......(3).....");
    if ( parser.error() ==  SafetParser::CORRECT ) {
//       qDebug("......generateSQLPortKeyFilterExpression.......(5).....");
        Q_CHECK_PTR(getToken());
        sql = parser.addField(getToken()->key());
        sql = sql + tokenlink;

        if (lastkeyset.toList().count() > 0 ) {
            if (parser.getWhereclauses().count() == 0  ) {
                sql  += " WHERE " ;
            }
            else {
                sql  += " AND " ;
            }
           QString f = generateKeyFilterString();
            sql += ptoken->key() + " IN " + f;

        }
    }
    else {
//           qDebug("......generateSQLPortKeyFilterExpression.......(4).....");
        QString query = port->query(curroption);
        sql = "SELECT ";
        Q_CHECK_PTR(ptoken);
//           qDebug("......generateSQLPortKeyFilterExpression.......(7).....query:%s",qPrintable(query));
        int option  =  curroption;
        sql = sql + ptoken->key() + "," + query.trimmed() +" FROM " + ptoken->keysource();
        if (port->options().length() == 0 ) {
            port->setOptions(query);
        }

        QStringList optionlist = port->options().split("|");

        Q_ASSERT_X(option < optionlist.count(),"generateSQLPortKeyFilterExpression",
                   qPrintable(tr("La lista de opciones (options) no es correcta")));
        QString wexpression = 	generateWhereExpression(query,optionlist[option], true);

        sql = sql +  wexpression;
        if ( lastkeyset.toList().count() > 0 ) {
            QRegExp rx("\\s+WHERE\\s+(.)+");
            rx.setCaseSensitivity( Qt::CaseInsensitive );
            QString f = generateKeyFilterString();
            if ( sql.contains( rx )   ) {
                sql= sql + " AND " + ptoken->key() + " IN " + f;
            }
            else {
                sql= sql + " WHERE " + ptoken->key() + " IN " + f;
            }
        }
     }

    return sql;
}

QString SafetWorkflow::generateWhereExpression(const QString& f, const QString& o, bool haswhere) {
	QString str = " ";
	QVariant myvar;
	myvar = o;
	QString ope; // Cadena para el operador
        QRegExp rx("\\[([a-zA-Z_0-9\\.:\\-\\s;\\+]+)\\]");
 	int pos = o.indexOf( rx );
 	if ( pos > -1 ) {
 		ope = "=";
// 		qDebug("*.......................olement: %s *", qPrintable(o));
 		QString fields = rx.cap(1);
 //		qDebug("*........................fields: %s *", qPrintable(fields));
 		checkfieldslist = fields.split(";");			
		Q_ASSERT( checkfieldslist.count() > 0 );
 		str +=  (f + " "+ ope + " '" + checkfieldslist.at(checkfieldslist.count()-1) +"'").trimmed();
// 		qDebug("*.......(1).................str list: |%s| ", qPrintable(str));
 		return str;
 	}


	QString onew = SafetYAWL::processOperatorValue(o, ope).trimmed();

	QStringList oroptions = onew.split(",");
	// Colocar For
        if ( (f.compare("true") == 0) || (f.compare("false") == 0) ) {
		if (!haswhere ) {
			str = f;
		}
		else
			str = " WHERE " + f;
		
		return str;
	}	
	else 
	if ( haswhere ) 
		str = " WHERE ";
	foreach(QString oelement, oroptions ) {
		if ( SafetYAWL::canTypeConvert(oelement, QVariant::Bool) || SafetYAWL::canTypeConvert(oelement, QVariant::Int) ) {
			str +=  (f + " "+ ope + " "+ oelement).trimmed();
		}
		else if (SafetYAWL::canTypeConvert(oelement, QVariant::Date)) {
			QDate mydate = QDate::fromString( oelement, Qt::ISODate );
			QString strdate = QString("'%1-%2-%3'").arg(mydate.year()).arg(mydate.month(),2,10, QChar('0')).arg(mydate.day(),2,10, QChar('0'));
			str +=  (f + " "+ ope + " "+ strdate );
		} 
		else  {
                        rx.setPattern(".*\\s+from\\s+.*");

                        if (rx.indexIn( oelement) >= 0 ) {
                               str +=  (f + " "+ ope + " "+ oelement );
                        }
                        else {
                           QRegExp rx("((AND|OR|NOT)\\s+|'(.*)')");
                           rx.setCaseSensitivity( Qt::CaseInsensitive );
                           int pos = rx.indexIn( oelement );
                           if ( pos == -1 ) {
                              str +=  (f + " "+ ope + " '" + oelement +"'").trimmed();
                           }
                           else {
                              str +=  (f + " "+ ope + " " + oelement + " ").trimmed();
                           }
                        }
		}
		str += " OR ";
	}	
	str.chop(4);

	return str;	
}

QString SafetWorkflow::generateJoinString(int numberPath, const QString& nametable) {
	QString sql = "";
	QQueue<QString> tokenlinkqueueInPathCopy = tokenlinkqueueInPath;
	QQueue<QString>        tokenlinkqueueCopy = tokenlinkqueue;
//        SafetYAWL::streamlog
//                << SafetLog::Debug
//                << tr("generateJoinString tokenlinkqueueCopy.count() %1").arg(tokenlinkqueueCopy.count());
//        SafetYAWL::streamlog
//                << SafetLog::Debug
//                << tr("generateJoinString tokenlinkqueueInPathCopy.count() %1").arg(tokenlinkqueueInPathCopy.count());

	while ( !tokenlinkqueueCopy.isEmpty() ) {
		sql += " " + tokenlinkqueueCopy.dequeue();
//                SafetYAWL::streamlog
//                        << SafetLog::Debug
//                        << tr("generateJoinString tokenlink %1").arg(sql);
	}
	QRegExp rx;
	rx.setCaseSensitivity(Qt::CaseInsensitive);
	while ( !tokenlinkqueueInPathCopy.isEmpty() ) {		
		QString allsql = tokenlinkqueueInPathCopy.dequeue();
		bool found = false;
		foreach(QString s, tokenlinkqueue) {
			if (allsql.startsWith(s.left(s.indexOf("ON", 0, Qt::CaseInsensitive)) ) ) {
					found = true;
					break;
				
			}
		}
		if (found) continue;
		QString path = allsql.section('|',1,1);
		allsql.remove("|" + path);
                rx.setPattern("JOIN\\s+([a-zA-Z_0-9\\.\\=']+)");
		int pos = allsql.indexOf(rx, Qt::CaseInsensitive);
		QString mytable;
		if ( pos > - 1 ) mytable = rx.cap(1);		
		
		if ( numberPath == path.toInt() ) {
			if ( nametable.length() == 0 || mytable.compare( nametable,Qt::CaseInsensitive) != 0 ) {
			sql += " " + allsql;
			}
		}
	}
	
	return sql;
}

QString SafetWorkflow::generateKeyFilterString(bool brackets) {
	QString str;
	if (brackets) str+= "(";
	int n = lastkeyset.toList().count();
	if (brackets && n == 0 ) return "(NULL)";
	else if ( !brackets && n == 0 ) return "NULL";	
	for(int i=0; i< n;i++) {
		str  += "'" + lastkeyset.toList().at(i) + "'";
		if (i < n-1) str +=",";	 // no es el Ãºltimo		
	}
	if (brackets) str+=")";
	return str;
}

QString SafetWorkflow::generateKeyFilterExpression(QString& s,SafetNode* node, int curroption, const QQueue<QString>& path , bool isjoin, int ipath) {
        Q_CHECK_PTR(node);
        QString result;
	SafetPort* port = node->port();
	Q_CHECK_PTR(port);
	QSet<QString> newkeyset;
	QString sql = "(";
	SafetSQLParser localparser;
        localparser.setWorkflow(this);
	localparser.setStr(s);	
        localparser.parse();
	QString strsign = localparser.signClause();

        if ( strsign.length() > 0 ) {
		s.remove( strsign );
	}

	if ( strsign.length() == 0 ) {

                if ( localparser.getFields().count() <  2 ) {
                    if (s.trimmed().compare("true",Qt::CaseInsensitive) != 0 ) {
                        SafetYAWL::streamlog
                            << SafetLog::Error
                        << tr("La sentencia SQL \"%1\" debe tener al menos un campo").arg(s);
                    }
                    return result;
                }

		QString expression = localparser.getFields().at(1);
                bool istabletoken = true;
                QString str = generateWhereFilter(expression, node,curroption,ipath);
		whereset.push_back(str);	
		if ( isjoin ) 
			joinWhereset += str;
		istabletoken = false;		
                generateNewKeySet(s, newkeyset, true, node, curroption, path, false, istabletoken,"",ipath);

	} else if ( strsign.length() > 0 ) {
		
                generateNewKeySet(s, newkeyset, true, node, curroption, path, true  /* checkSign */, false /* tabletoken */,
                                  localparser.signers()/* lista de firmantes */, ipath);

        }
        if ( isStartingKeyFilter || lastkeyset.count() == 0) {
		lastkeyset          = newkeyset;
		isStartingKeyFilter = false;
	}
	else {
                lastkeyset &= newkeyset; // Interseccion de viejos con nuevo
	}
	if ( currentfilterkey.length() > 0 ) {
		lastkeyset += dropkeys[ currentfilterkey ];
		delWhereClause(currentfilterkey);
		currentfilterkey = "";
	}
        result = generateKeyFilterString();
	return result;
}

bool SafetWorkflow::delWhereClause(const QString& wc, bool onlynamefield) {
	QList<QString>::iterator i;
//	qDebug("...............wc: %s", qPrintable(wc));
 	for (i = whereset.begin(); i != whereset.end(); ++i) {
	    	QString k = *i;
                k.remove(QRegExp("[\\s']"));
                bool value  = onlynamefield? (k.startsWith(wc)) : (k == wc);
                if ( value ) {
                        whereset.erase(i);
                        if ( onlynamefield ) {
                             for (int j = 0; j < dropkeys.keys().count(); j++) {
                                    if ( dropkeys.keys().at(j).startsWith( wc ) ) {
                                          lastkeyset +=  dropkeys[ dropkeys.keys().at(j) ];
                                          break;
                                    }
                             }
                        }
			break;
		}
	} 
}


void SafetWorkflow::generateNewKeySet(const QString& s, QSet<QString> &set,
                                      bool found, SafetNode* node,
                int curroption, const QQueue<QString>& path, bool checkSign,
                bool checkTableToken, const QString& l, int ipath) {
    Q_CHECK_PTR(node);
    SafetPort* port = node->port();
    Q_CHECK_PTR(port);
    int option = 0;
    option = curroption;// Opcion del camino para el filtro
    QList< QStringList > optionlists; // Para cuando existe un operador (p.e. and, or, xor, etc)
    QStringList querys; // Para cuando existe un operador (p.e. and, or, xor, etc)
    int matchpath = 0;
    // *** Colocar la  opcion ACTUAL como primera
    if ( curroption < port->getConnectionlist().count()  ) {
        matchpath = curroption;
    }
    QStringList optionlist = port->options(matchpath).split("|");
    optionlists.push_back( optionlist );
    querys.push_back( s );
      SafetNode* myparent = qobject_cast<SafetNode*> (port->parent());
      Q_CHECK_PTR( myparent );
    QString pattern = port->pattern();

    if ( port->getConnectionlist().count() == 0) {
         SafetYAWL::streamlog << SafetLog::Error << tr("Número de conexiones (connections) vacías en el nodo: \"%1\"").arg(node->id());
         return;
    }
    if ( port->getConnectionlist().count() > 1 ) {
         if  (pattern.length() == 0 || pattern.compare(tr("none")) == 0 ) {
             SafetYAWL::streamlog
                     << SafetLog::Error
                     <<
          tr("Un puerto que contiene varias conexiones necesita de un "
             "patron/operador diferente a vacio o \"none\","
             "el patron/operador de un puerto de la tarea \"%1\" esta vacio")
                  .arg(myparent->id());
            return;
        }
        for ( int i = 0; i < port->getConnectionlist().count(); i++ ) {
            if ( i != matchpath ) {
                querys.push_back(port->getConnectionlist().at(i)->query());
                QStringList myoptionlist = port->getConnectionlist().at(i)->options().split("|");
                optionlists.push_back(myoptionlist);
//                qDebug("%d....query: %s", i, qPrintable(port->getConnectionlist().at(i)->query()));
  //              qDebug("%d....options: %s", i, qPrintable(port->getConnectionlist().at(i)->options()));
            }
        }
    }
    if ( checkTableToken || checkSign || optionlist.count() == 0) {
        if (curroption > port->getConnectionlist().count() -1 ) curroption = port->getConnectionlist().count() -1;
        checkForSignFilter(s,port->getConnectionlist().at(curroption),l,checkSign, set);
        return;
    }


   Q_ASSERT( querys.count() > 0 );
   proccessQueryFilter(pattern, querys, optionlists, set, found, node, 0 /* curroption */, path, ipath);


}


void SafetWorkflow::checkPatternWithOthers(QString pattern, QStringList& querys,
                                        QList< QStringList >& optionlists, QSet<QString> &set,
                                        bool found, SafetNode* node,
                                        int curroption, const QQueue<QString>& path,int ipath) {


    if (querys.count() == 1 ) {
        return;
    }

    // Hacer facil agregar mas patrones (PATRON)
    if (pattern.compare("or") == 0 ) {
        return;
    }

    QString debugope,debugnew = SafetYAWL::processOperatorValue(
            optionlists.at(0)[0], debugope).trimmed();
    SafetYAWL::streamlog
            << SafetLog::Debug
            << tr("..1..( checkPatternWithOthers) Sentencia SQL: \"%2\" Opcion (#ONEW): \"%1\"").arg(debugnew)
            .arg(querys.at(0));

    QSet<QString> otherspathsset;
    //QString tokenlink = processTokenlink(node->port()->tokenlink(),false,ipath);
    //int option = curroption;
    for( int i=1; i < querys.count();i++ ) {
        otherspathsset.clear();
        QString curquery = querys.at(i);
        parser.setSql(curquery);
        parser.parse();
        curquery = parser.addField(getToken()->key()) /*+ tokenlink*/;
        QSqlQuery query(curquery, SafetYAWL::currentDb); // <-- SafetYAWL::currentDb puntero a db actual

        /*QString news = */
        checkDropFilter(curquery,node,path,ipath); // Chequear si existe filtros drop
        QString ope; // Cadena para el operador a efectuar

        QString onew = SafetYAWL::processOperatorValue(
                optionlists.at(i)[0], ope).trimmed();

                SYD << tr("( checkPatternWithOthers) Sentencia SQL: \"%2\" Opcion (#ONEW): \"%1\"").arg(onew)
                .arg(curquery);

        // ********* Colocar las fichas eliminadas antes de aplicar el filtro

        while (query.next()) {
            //qDebug("ornew: |%s|; ope: |%s| value: |%s|", qPrintable(ornew), qPrintable(ope), qPrintable(query.value(1).toString().trimmed()));
             QString valueforeval = query.value(1).toString().trimmed();

            if ( (found &&  SafetYAWL::evalValues(valueforeval,onew,ope)) || !found) {              

                otherspathsset.insert(query.value(0).toString());

            }

        }
        // *****************************************************************************************
        // *****************************************************************************************
        otherspathsset &= lastkeyset; // Intersectar con el ultimo grupo de claves
        // Aplicando el patron ...
        // ** Importante este paso .... (ACA INCLUIR OPERADOR XOR Y OTROS)

        if (pattern.compare("and") == 0 ) {            
            set &= otherspathsset;
        }
        // *****************************************************************************************
        // *****************************************************************************************
    }

}



void SafetWorkflow::proccessQueryFilter(QString pattern, QStringList& querys,
                                        QList< QStringList >& optionlists, QSet<QString> &set,
                                        bool found, SafetNode* node,
                                        int curroption, const QQueue<QString>& path,int ipath) {
    //  Q_ASSERT( querys.count() == optionlists.count() );
    int option = curroption;
    QSqlQuery query(querys.at(0 /* chequear para varias opciones */), SafetYAWL::currentDb); // <-- SafetYAWL::currentDb puntero a db actual

    /*QString news = */
    checkDropFilter(querys.at(0),node,path,ipath); // Chequear si existe filtros drop
    QString ope; // Cadena para el operador a efectuar
    QString lastk; // Ultima clave a evaluar
    QString keyfilter; // Filtro completo para la clave
    Q_ASSERT(option < optionlists.count() );
 //   foreach (QString s, optionlists.at(0)) qDebug("...........:%s", qPrintable(s));
    QString onew = SafetYAWL::processOperatorValue(
            optionlists.at(option)[0 /*chequear para varias opciones > 0 */], ope).trimmed();

    QStringList oroptions = onew.split(","); // chequear para casos del operador  or
    int poslist = checkInternalOrOptions(onew,optionsset,lastk);

    // ********* Colocar las fichas eliminadas antes de aplicar el filtro
    SafetYAWL::streamlog
            << SafetLog::Debug
            << tr("(proccessQueryFilter) Sentencia SQL: \"%2\" Opcion (onew): \"%1\"").arg(onew)
            .arg(querys.at(0));

    while (query.next()) {
        if ( poslist > -1 )  { // Esto es para admitir el operador "O" a traves ";"
            keysoptions[ query.value(0).toString().trimmed() ].insert (query.value(1).toString().trimmed());
            keyfilter = query.record().fieldName(1)+ope+lastk.trimmed();
        }
        else {
            foreach(QString ornew, oroptions) {
                 //qDebug("ornew: |%s|; ope: |%s| value: |%s|", qPrintable(ornew), qPrintable(ope), qPrintable(query.value(1).toString().trimmed()));
                if ( (found &&  SafetYAWL::evalValues(query.value(1).toString().trimmed(),ornew,ope)) || !found) {                    
                        set.insert(query.value(0).toString());
                }
                else {
                    keyfilter = query.record().fieldName(1)+ope+ornew;
                    SafetPort *outport = node->outport();
                    Q_CHECK_PTR( outport );
                    outport->setFilterkey( keyfilter );
                    dropkeys[ keyfilter ].insert(query.value(0).toString());
                }
            }
        }
    }
    checkPatternWithOthers(pattern,querys,
                           optionlists,set,
                           found,node,
                           curroption,path,ipath);
}


void SafetWorkflow::clearDropkeys(const QString& filters) {
QRegExp rx("^"+filters);

//qDebug("........filters......:%s", qPrintable("^"+filters));
for (int i = 0; i < dropkeys.keys().count(); i++) {
 //   qDebug("....clearDropkeys.........dropkeys.keys().at(i)...:%s", qPrintable(dropkeys.keys().at(i)));
     int index = dropkeys.keys().at(i).indexOf(rx);
//     qDebug("........index........:%d", index);
     if ( index > -1 ) {
         dropkeys[ dropkeys.keys().at(i) ].clear();
     }
 }
}

bool SafetWorkflow::checkPatternWithOthersPath(const QString& value, const QString& pattern, const QStringList& querys, const QList< QStringList >& optionlists) {
        Q_ASSERT( pattern.length() > 0 );
//          qDebug(".....checkPatternWithOthersPath.......value:%s", qPrintable(value));
//          qDebug(".....checkPatternWithOthersPath.....pattern:%s", qPrintable(pattern));
        if ( pattern.compare(tr("none")) == 0 || pattern.compare(tr("or")) == 0) return true;
         if ( (pattern.compare(tr("and")) == 0 && value.compare("8") == 0) ||
              (pattern.compare(tr("and")) == 0 && value.compare("4") == 0) ||
              (pattern.compare(tr("and")) == 0 && value.compare("2") == 0) ||
              (pattern.compare(tr("and")) == 0 && value.compare("9") == 0) ||
    //          (pattern.compare(tr("and")) == 0 && value.compare("6") == 0) ||
              (pattern.compare(tr("and")) == 0 && value.compare("3") == 0) ) {
//              qDebug("....return from 3.or...9.. true....value: %s", qPrintable(value));
             return true;
         }
        return false;
}


int SafetWorkflow::checkInternalOrOptions(const QString& onew, QSet<QString>& optionsset, QString& lastk) {
        QRegExp rx("\\[([a-zA-Z_0-9\\.;\\+]+)\\]");
        int poslist = onew.indexOf( rx );
        optionsset.clear();
        if (poslist > -1 ) {
                QStringList mylist = rx.cap(1).split(";");
                foreach(QString k, mylist) {
                        optionsset.insert( k );
                        lastk = k;
                }
        }
        return poslist;
}

void SafetWorkflow::checkForSignFilter(const QString& s, SafetConnection* conn,
                                       const QString& l, bool checkSign,
                                       QSet<QString> &set) {
    Q_CHECK_PTR( conn ) ;

    QString option = conn->options();
    QSqlQuery query(s, SafetYAWL::currentDb); // <-- SafetYAWL::currentDb puntero a db actual

     bool operatorOption = true;
    if ( conn != NULL ) {

         if ( option.compare(tr("no"),Qt::CaseInsensitive ) == 0 ) {
              operatorOption = false;
         }
    }
    if (checkSign) {
          checkDocuments(query,l,set,operatorOption);
    }
    else {
        while (query.next()) {
            set.insert(query.value(0).toString());
        }
    }

}

QString SafetWorkflow::checkDropFilter(const QString& s,SafetNode* node, const QQueue<QString> &path, int ipath) {
	Q_CHECK_PTR( node );
	QString result = s;
	// 2 Task, 5, Condition
	SafetTask *ptask = NULL;
	SafetCondition *pcond = NULL;
	SafetPort* pport = node->outport();
	Q_CHECK_PTR( pport );
	QString nodename;
	int classtype = SafetYAWL::getClassReference(SafetYAWL::translateNS(node->metaObject()->className()));
	switch( classtype ) {
		case 2: // SafetTask
			ptask = qobject_cast<SafetTask*>(node);
			Q_CHECK_PTR(ptask);
			nodename = ptask->id();
			break;
		case 5:
			pcond = qobject_cast<SafetCondition*>(node);
			Q_CHECK_PTR(pcond);
			nodename = pcond->id();	
		default:;
	}
	if ( pport->drop().length() > 0 ) {
		Q_ASSERT( nodename.length() > 0 );
		int hownodes = pport->drop().toInt();
		// Buscar el node respectivo
		int curr = path.indexOf ( nodename );
		curr = curr - hownodes;
		if ( curr < 0 ) curr = 0;
		QString newnodename = path.at( curr );
		SafetNode* filternode = nodemap[ newnodename ];
		Q_CHECK_PTR ( filternode );
		SafetPort *outport = filternode->outport();
		Q_CHECK_PTR( outport );
		Q_ASSERT( outport->filterkey().length() > 0 );	
		Q_ASSERT( dropkeys.contains( outport->filterkey() )  );	
		currentfilterkey = outport->filterkey() ;
	}

	return result;
}

QString SafetWorkflow::generateWhereFilter(const QString& f, SafetNode* node, int curroption,int ipath) {
	Q_CHECK_PTR(node);
	SafetPort* port = node->port();
	Q_CHECK_PTR(port);
        int option = 0;
        option = curroption;
        QStringList optionlist = port->options(option).split("|");
        if ( option >= optionlist.count() ) option = optionlist.count() -1 ;

  //      qDebug("\t\t....optionlist[option]: %s", qPrintable(optionlist[option]));
	QString str = generateWhereExpression(f,optionlist[option]) ;
	return str;		
}


SafetCondition* SafetWorkflow::getStartCondition() {
	QList<SafetCondition*>::iterator i;
	SafetCondition *curcondition;
	 for (i = conditionlist.begin(); i != conditionlist.end(); ++i) {
		curcondition = *i;
		Q_CHECK_PTR(curcondition);
        if (curcondition->type() == QObject::tr("start") ) {
			return curcondition;
		}
	 }
	 return NULL;
}

QList<SafetTask*>& SafetWorkflow::getTasks() {
	return tasklist;
}

QString SafetWorkflow::listTasks(bool inc, const QString& c) {
     QString result;
     foreach(SafetTask* mytask, tasklist) {
                result += mytask->id();
                result += c;
        }
     if ( inc ) {
     QList<SafetCondition*> conditions = getConditions();
        foreach(SafetCondition* mycondition, conditions) {
                result += "*";
                result += mycondition->id();
                result += c;
        }
     }
     return result;

}


QList<SafetCondition*>& SafetWorkflow::getConditions() {
	return conditionlist;
}

SafetToken* SafetWorkflow::getToken() {
	return ptoken;
}

QMap<QString,SafetNode*>& SafetWorkflow::getNodes() {
	return nodemap;
}



QString SafetWorkflow::getSQLOperator(const QString& o) {
	QString result;
	if ( o.compare("and", Qt::CaseInsensitive) == 0 ) result =" INTERSECT ";
	else if ( o.compare("or", Qt::CaseInsensitive) == 0 )result = " UNION ";
	else if ( o.compare("xor", Qt::CaseInsensitive) == 0 ) result = " EXCEPT ";
	return result;
}

QString SafetWorkflow::addSQLPrefix(const QString& sql, const QString& s) {
	QString newsql;
        QRegExp rx("^\\s*\\(*\\s*SELECT\\s*(.)+\\s+FROM\\s+(.)+");
	rx.setCaseSensitivity( Qt::CaseInsensitive );       
	if ( s.contains( rx )   ) 	return s;

	if ( s.length() > 0 )  {
		newsql = sql + " WHERE " + ptoken->key() + " IN " + s;
	}
	else 
		newsql = sql + " WHERE FALSE";	
        //qDebug("....(3)....f:%s", qPrintable(s));
	return newsql;
}



QString SafetWorkflow::processSQLOperation(const QString& sql, QStack<QString> &stack, const QString& ope) {
        QString result;
	SafetSQLParser localparser;
        localparser.setWorkflow(this);
	localparser.setStr(sql);
	localparser.parse();
	if ( ope.compare("xor", Qt::CaseInsensitive) == 0 ) {
		QStack<QString> mystack;
		while(!stack.isEmpty() ) {
			QString newsql = stack.pop();
			QString mypath = newsql.section('|',1,1);
			newsql.remove("|" + mypath);
			QString tokenlink = generateJoinString( mypath.toInt(),localparser.getTablesource() );
//			qDebug("....(2)....addSQLPrefix...:%s", qPrintable(newsql));
			newsql = addSQLPrefix(sql+" "+tokenlink, newsql);
			mystack.push(newsql);
			result  = result + newsql;							
			if (!stack.isEmpty() ) 	result = result + " " + getSQLOperator(ope) + " ";			
		}
		result = "("+ result + ") UNION (";
		while(!mystack.isEmpty() ) {
			QString newsql = mystack.pop();
			result  = result + newsql;				
			if (!mystack.isEmpty() ) 	result = result + " " + getSQLOperator(ope) + " ";			
		}	
		result = result + ")";
	}
	else {
		while(!stack.isEmpty() ) {
			QString newsql = stack.pop();
			QString mypath = newsql.section('|',1,1);
			newsql.remove("|" + mypath);
			QString tokenlink = generateJoinString( mypath.toInt(), localparser.getTablesource() );
			newsql = addSQLPrefix(sql+" "+tokenlink, newsql);			
			result  = result + newsql;
			if (!stack.isEmpty() ) result = "(" + result + ") " + getSQLOperator(ope) + " ";
		}		
	}
	stack.push(result);
	return result;
}


QString SafetWorkflow::generateGraphKey(const QString& s, char* filetype) {
	
	QString result = generateGraph(filetype, s /* info de la clave */);
	return result;
}



bool SafetWorkflow::checkDocuments(QSqlQuery& query,
                                  const QString& signatories /* lista de firmantes */,
                                  QSet<QString> &set /* Conjunto Salida */,
                                  bool operatorOption /*para negacion de la firma */) {


    // ************* prueba para abrir el archivo de configuracion de digidoc ******
    if ( _libdigidocConfigFile.isEmpty()) {
        _libdigidocConfigFile = SafetYAWL::getConfFile().getValue("Libdigidoc", "libdigidoc.configfilepath").toString();
        _libdigidocConfigFile = QDir::homePath() + "/" + Safet::datadir + "/" + "digidoc.conf";
    }

    QString remoteContainer = SafetYAWL::getConfFile()
                              .getValue("XmlRepository",
                                        "xmlrepository.remote")
                              .toString();

    QString type = SafetYAWL::getConfFile().getValue("XmlRepository",
                                                     "xmlrepository.type").toString();
    if ( type == "dir" && _xmlRepository == NULL )  {
        _xmlRepository = new SafetDirXmlRepository();
    }
#ifdef SAFET_DBXML   // Incluir la biblioteca dbxml para soporte de Base de Datos XML Nativa
    else if ( type == "dbxml" )  {
        xmlRepository = new SafetDbXmlRepository();

    }
#endif // Libreria DbXml
    QString url = SafetYAWL::getConfFile().getValue("XmlRepository",
                                                    "xmlrepository.remote.urlwebservice")
    .toString();

    QString home = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.path").toString();
    QString homefile = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.name").toString();
    _xmlRepository->openContainer(home + "/" + homefile);

    bool isremotecontainer = remoteContainer.compare("on",Qt::CaseInsensitive) == 0;
    QStringList signers;
    int commapos = signatories.indexOf(",");
    int commapointpos = signatories.indexOf(";");
    if ( commapos != -1 && commapointpos != -1 ) {
        SafetYAWL::streamlog
                << SafetLog::Error
                << tr("No es posible incluir los caracteres \",\" y \";\""
                      " en una sentencia de una firma: \"%1\"")
                .arg(signatories);
        return false;
    }
    SafetDocument::SignatureOperator signatureOp = SafetDocument::AND;
//    qDebug("       commapos: %d ....commapointpos:%d", commapos,commapointpos);
//    qDebug("       **SIGNERS: %s", qPrintable(signatories));
    if ( commapos != -1 )    {
        signers = signatories.split(",");

    }
    else if ( commapointpos  != -1 ) {
        signers = signatories.split(";");
        signatureOp = SafetDocument::OR;
    }
    else {
        if (signatories == "#ANYBODY#") {
            signatureOp = SafetDocument::ALL;
        }
        signers.append(signatories);

    }
    SafetDocument doc;

    doc.initializeLibdigidoc();

    doc.initDigidocConfigStore(_libdigidocConfigFile);

    while (query.next()) {
        QString documentid = SafetYAWL::getDocumentID(query,true);

	// ********* prueba de llamada a metodo remoto de servicio web safet **************
	// obtener valores del archivo de configuracion	

        if ( isremotecontainer ) {

#ifdef SAFET_GSOAP // Definido conexion a servicios Web
            struct soap soap;
            bool result;

            SafetYAWL::streamlog <<
                    SafetLog::Action
                    << tr("Se consultado el Servicio Web en: \"%1\"").arg(url);

            if (soap_call_ns__verifyDocumentSignatureWithNames(soap_new(),
                                                               qPrintable(url), NULL,
                                       (char*)qPrintable(documentid),
                                       (char*)qPrintable(signatories),
                                       result) == SOAP_OK)
            {
                SafetYAWL::streamlog
                        << SafetLog::Action << tr("La llamada al Servicio Web  a: \"%2\" para el documento: \"%3\" para verificacion de firma fue exitosa...Valor del resultado devuelto: %1...ok! ").arg(result).arg(url).arg(documentid);
                if ( operatorOption ) {
                    set.insert(query.value(0).toString());
                }

            }
            else
            {
                SafetYAWL::streamlog
                        << SafetLog::Error
                        << tr("Ocurrio un error en la llamada del Servicio Web para verificacion de firma...ok! ");
                soap_print_fault(&soap, stderr);
                return false;
            }
            soap_destroy(&soap);
            soap_end(&soap);
            soap_done(&soap);
            continue;
#else
            SYA << tr("No se encuentra definido un gestor de servicios Web");
            continue;
#endif
	}


        //        SafetYAWL::streamlog <<  SafetLog::Debug << tr("Abriendo contenedor: %1").arg(home + "/" + homefile);

       if (!_xmlRepository->searchDocumentInContainer(documentid)) {
           if (SafetWorkflow::verifyNegation(signers))  {
                set.insert(query.value(0).toString());
            }
            continue;
        }

        QString archivoEscrito = home + "/" + homefile + "/" +documentid;
	
	
	// ************* prueba para leer un contenedor desde un archivo en el sistema de archivos*************

        doc.readOpenXAdESContainerFile(archivoEscrito);

	
        //        SafetYAWL::streamlog <<  SafetLog::Debug << tr("Iniciando verificacion de firmantes electrónico...ok!");
        //        SafetYAWL::streamlog <<  SafetLog::Debug << tr("Firmantes: %1").arg(signers.join(","));
        //        SafetYAWL::streamlog <<  SafetLog::Debug << tr("Documento a verificar los firmantes: \"%1\"").arg(archivoEscrito);
        //qDebug("         ....signatureOp:%d",signatureOp);
        bool isneg;
        bool r = doc.verifySignMadeWithSmartCard(archivoEscrito, signers,isneg,signatureOp);

        //SafetYAWL::streamlog <<  SafetLog::Debug << tr("Resultado de la firma: \"%1\"").arg(r);


        if ( r && operatorOption ) {
            set.insert(query.value(0).toString());
        }

    }

    doc.closeLibdigidoc();
    return true;

}

bool SafetWorkflow::verifyNegation(const QStringList& list) {
    foreach(QString s, list){
        if ( s.startsWith("!")) {
            return true;
        }
    }
    return false;

}

QList<SafetWorkflow::InfoSigner> SafetWorkflow::getSignersDocument(const SafetVariable* v,
                                                                   const QString& key,
                                                                   QString &derror) {
        QList<InfoSigner>  result;
#ifdef SAFET_DIGIDOC // Firma Electronica Digidoc

        QString documentid;
        QString dirid, pathid; // Variables para almacenar el documento

        SafetDocument doc;
        doc.initializeLibdigidoc();


        doc.createOpenXAdESContainer();

        // ************* prueba para abrir el archivo de configuracion de digidoc ******
        QString libdigidocConfigFile = SafetYAWL::getConfFile().getValue("Libdigidoc",
                                                                        "libdigidoc.configfilepath")
                                        .toString();
        libdigidocConfigFile = QDir::homePath() + "/" + Safet::datadir + "/" + "digidoc.conf";
        doc.initDigidocConfigStore(libdigidocConfigFile);


        // Campos a incluir en el archivo de configuracion
        dirid       = "/tmp/"; //Directorio Temporal
        int fieldno = 1; // Numero de campo de la consulta


        qDebug("(1)....key:|%s|",qPrintable(key));
        QString searchkey = key;
        v->getXMLDocument(searchkey.trimmed(), fieldno, documentid );
        qDebug("(2)....key:|%s|",qPrintable(key));

        if (documentid.length() == 0 ) {
//            SafetYAWL::streamlog
//                    << SafetLog::Error
//                    << tr("El documento que tiene la clave: \"")
//                        << searchkey.trimmed()
//                        << tr("\" asociado a la variable \"")
//                        << v->id() << tr("\" no se encuentra en el repositorio de datos")
//                        << endl;
            qDebug("(*)Documento con clave: |%s| no se encuentra (getSignersDocument)",
                   qPrintable(searchkey.trimmed()));
                return result;
        }
        pathid  = dirid + documentid;


        QString home = SafetYAWL::getConfFile().getValue("XmlRepository",
                                                         "xmlrepository.path").toString();
        QString homefile = SafetYAWL::getConfFile().getValue("XmlRepository",
                                                             "xmlrepository.name").toString();

        SafetXmlRepository *xmlRepository = NULL;

        QString remoteContainer = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.remote").toString();
        bool founddoc = false;
        if (remoteContainer.compare("off",Qt::CaseInsensitive) == 0) {

                qDebug("...........remoteContainer.compare....off");
                QString type = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.type").toString();
                if ( type == "dir" )  {
                        xmlRepository = new SafetDirXmlRepository();
                }
                Q_CHECK_PTR( xmlRepository );
                if (QFile::exists( home + "/" + homefile) ) {
                        xmlRepository->openContainer(home + "/" + homefile);
                        founddoc =  xmlRepository->searchDocumentInContainer(documentid);
                        if ( founddoc ) {
                                doc.readOpenXAdESContainerFile(home + "/" + homefile + "/" + documentid);
                                qDebug("readOpen...: %s", qPrintable(home
                                                                     + "/" + homefile
                                                                     + "/" + documentid));
                                    QList<bool> ocspsigns = doc.verifyOCSPSign();
                                 for ( int i = 0; i < doc.numberOfSignaturesOnOpenXAdESContainer(); i++){
                                     InfoSigner mysigner;
                                     mysigner.commonName = doc.getCN(i);
                                     mysigner.issuer = doc.getSignerCertificateIssuerName(i).trimmed();
                                     mysigner.date = doc.getSingingTimeOnlyDate(i);
                                     mysigner.hour = doc.getSingingTimeOnlyHour(i);
                                     mysigner.location = doc.signatureLocations(i).join(",");
                                     mysigner.role = doc.getRole(i);
                                     SignatureInfo* mysignature = doc.SigInfo(i);
                                     if (mysignature != NULL ) {
                                         int len = 0;

                                         if (mysignature->pSigValue->mbufSignatureValue.nLen < 2048) {
                                             len = mysignature->pSigValue->mbufSignatureValue.nLen;
                                         }
                                         for(int j=0; j < len; j++) {
                                            mysigner.hexsign
                                                    .push_back(((char*)mysignature->pSigValue->mbufSignatureValue.pMem)[j]);
                                         }


                                         len = 0;
                                         DataFile *mydatafile = NULL;
                                         if (doc.numberOfDataFileOnOpenXAdESContainer() > 0 ) {
                                             doc.dataFile(0);
                                             if (mydatafile != NULL ) {
                                                 qDebug("...mydatafile->mbufDigest.nLen..:%d",
                                                        mydatafile->mbufDigest.nLen);
                                                 qDebug("...mysignature->pSigInfoRealDigest->mbufDigestValue.nLen:%d",
                                                        mysignature->pSigInfoRealDigest->mbufDigestValue.nLen);
                                                 int nLen = mysignature->pSigInfoRealDigest->mbufDigestValue.nLen;
                                                 if (nLen > 50) {
                                                     nLen = 50;
                                                 }
                                                 if (nLen >0 ) {
                                                     for(int j=0; j < nLen; j++){
                                                         qDebug("...getSignerDocument(1)...");
                                                         mysigner.digest.push_back(((char*) mydatafile->mbufDigest.pMem)[j]);
                                                         qDebug("...getSignerDocument(2)...");
                                                     }
                                                 }

                                             }
                                         }

                                     }


                                     if ( i < ocspsigns.count()) {
                                         mysigner.isvalid = ocspsigns.at(i);
                                     }
                                     else {
                                         mysigner.isvalid = false;

                                     }
                                     result.append(mysigner);
                                 }
                        }
                        else {
                            return result;
                        }
                }
                else {
                            return result;

                }
        }



        doc.closeLibdigidoc();

#endif // DIGIDOC

        return result;

}

QString SafetWorkflow::signDocument(const SafetVariable* v, const QString& key, QString &derror) {

        QString documentid;
#ifdef SAFET_DIGIDOC // Firma Electronica Digidoc
	QString dirid, pathid; // Variables para almacenar el documento
//        DbXml::setLogLevel(DbXml::LEVEL_ALL, true);
//        DbXml::setLogCategory(DbXml::CATEGORY_ALL, true);

	SafetDocument doc;
        doc.initializeLibdigidoc();
	
	
	doc.createOpenXAdESContainer();
	
	// ************* prueba para abrir el archivo de configuracion de digidoc ******
	QString libdigidocConfigFile = SafetYAWL::getConfFile().getValue("Libdigidoc", "libdigidoc.configfilepath").toString();
        libdigidocConfigFile = QDir::homePath() + "/" + Safet::datadir + "/" + "digidoc.conf";
	doc.initDigidocConfigStore(libdigidocConfigFile);

	
	// Campos a incluir en el archivo de configuracion
	dirid       = "/tmp/"; //Directorio Temporal
	int fieldno = 1; // Numero de campo de la consulta	


	QString xml = v->getXMLDocument(key, fieldno, documentid );
 	//qDebug("xml:\n%s", qPrintable(xml));
	SafetYAWL::streamlog << SafetLog::Debug << tr("Archivo XML a Firmar:\n%1").arg(xml);
//	documentid  = v->id().trimmed() + "-" + key.trimmed();
        qDebug("documentid: |%s|", qPrintable(documentid));
	if (documentid.length() == 0 ) {
            SafetYAWL::streamlog
                    << SafetLog::Error
                    << tr("El documento con la clave: \"")
                        << key
                        << tr("\" asociado a la variable \"")
                        << v->id() << tr("\" no se encuentra en el repositorio de datos")
                        << endl;
                return QString("");
	}	
	pathid  = dirid + documentid;
        qDebug("pathid: |%s|", qPrintable(pathid.section(".",0,0) + ".xml"));

//	doc.createXMLFileFromQuery(query, pathid + ".xml");
        QFile file(pathid.section(".",0,0) + ".xml");
	
	bool open = file.open(QIODevice::WriteOnly | QIODevice::Text); 
        if (!open ) {

            SafetYAWL::streamlog
                    << SafetLog::Error
            << qPrintable(QObject::tr("No se pudo crear el archivo XML "
                                      "correspondiente a la sentencia SQL."));

            return QString("");
        }

	
	QTextStream out(&file);
	out << xml;
	out.flush();
	file.close();
        QString home = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.path").toString();
        QString homefile = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.name").toString();

	SafetXmlRepository *xmlRepository = NULL;

        QString remoteContainer = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.remote").toString();
	bool founddoc = false;
	if (remoteContainer.compare("off",Qt::CaseInsensitive) == 0) {

                QString type = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.type").toString();
		if ( type == "dir" )  {
			xmlRepository = new SafetDirXmlRepository();
		}
#ifdef SAFET_DBXML   // Incluir la biblioteca dbxml para soporte de Base de Datos XML Nativa 
		else if ( type == "dbxml" )  {
			xmlRepository = new SafetDbXmlRepository();
	
		}
#endif // Libreria DbXml
		Q_CHECK_PTR( xmlRepository );
		if (QFile::exists( home + "/" + homefile) ) {
//			qDebug("....xmlRepository->openContainer...documentid:%s", qPrintable(documentid));
			xmlRepository->openContainer(home + "/" + homefile);
			founddoc =  xmlRepository->searchDocumentInContainer(documentid);
//			qDebug("...............found : %d", found);
			if ( founddoc ) {
				doc.readOpenXAdESContainerFile(home + "/" + homefile + "/" + documentid);
//				qDebug("readOpen...: %s", qPrintable(home + "/" + homefile + "/" + documentid));
				SafetYAWL::streamlog << SafetLog::Action << tr("Contenedor \"%1\" encontrado").arg(homefile);
				SafetYAWL::streamlog << SafetLog::Action << tr("Documento \"%1\" encontrado y esta firmado previamente").arg(documentid);
			}
			else {
                                doc.addFileToOpenXAdESContainer(doc.signedDoc(), pathid.section(".",0,0) + ".xml",
					SafetDocument::SAFET_CONTENT_EMBEDDED_BASE64, "application/xml");
				SafetYAWL::streamlog << SafetLog::Action << tr("Contenedor \"%1\" encontrado").arg(homefile);
				SafetYAWL::streamlog << SafetLog::Action << tr("Documento \"%1\" creado").arg(documentid);
			}
		}
		else {
			SafetYAWL::streamlog << SafetLog::Action << tr("Creando contenedor \"%1\" ").arg(home + "/" + homefile);
			xmlRepository->createContainer(home + "/" + homefile, "XmlContainer::WholedocContainer");	
                        doc.addFileToOpenXAdESContainer(doc.signedDoc(), pathid.section(".",0,0) + ".xml",
					SafetDocument::SAFET_CONTENT_EMBEDDED_BASE64, "application/xml");
			SafetYAWL::streamlog << SafetLog::Action << tr("Contenedor \"%1\" creado").arg(homefile);
	
	
		}	
	}

	// ************* prueba para agregar un archivo a contenedor digidoc ******
	
	// ************* prueba para agregar atributos a un archivo existente en un contenedor digidoc ******
//	doc.addAttributeToFile(doc.dataFile(), pathid.section(".",0,0) + ".xml", "Fecha", "Martes 01/07/08");
//	doc.addAttributeToFile(doc.dataFile(), pathid.section(".",0,0) + ".xml", "Hora", "18:52pm");
	

	// ************* prueba para cargar driver de la tarjeta inteligente ********
	doc.testSmartCardDriver(0);
	
	SafetYAWL::streamlog << SafetLog::Action << tr("Carga del driver de la tarjeta probada ...OK!");
	// ************* prueba firma con tarjeta inteligente***********************

	SafetYAWL::streamlog << SafetLog::Action << tr("Esperando a ingresar el PIN....OK!");
        bool ok;
      QString pin = (SafetYAWL::evalInput())(SafetYAWL::PIN, tr("Ingrese Pin de la Tarjeta:"), ok);
        if ( !ok ) {
             SafetYAWL::streamlog << SafetLog::Action << tr("PIN No se ingreso...No se aplica la firma...OK!");
             return QString("");
        }
	SafetYAWL::streamlog << SafetLog::Action << tr("PIN Ingresado...esperando chequeo...OK!");

//qDebug("ANTES DE doc.signWithSmartCard");
//        doc.signWithSmartCard(doc.signedDoc(), "T6qB1jbe", "Desarrollador","Merida", "Merida", "5101", "Venezuela");
        int err = doc.signWithSmartCard(doc.signedDoc(), pin, "Desarrollador","Merida", "Merida", "5101", "Venezuela");
        if ( err != ERR_OK ) {
                QString stringerror = SafetDocument::digidocStringError(err);
                if (stringerror.isEmpty() ) {
                    stringerror = getErrorString(err);
                }

               SafetYAWL::streamlog
                       <<  SafetLog::Error
                       << tr("No se pudo realizar la Firma "
                       "Electrónica sobre el documento: error:\"%1\"")
                       .arg(stringerror);
               derror = stringerror;
               return QString("");

        }
        SafetYAWL::streamlog << SafetLog::Action << tr("PIN validado correctamente....OK!");


	SafetYAWL::streamlog << SafetLog::Debug << tr("Documento firmado correctamente ...OK!");

	// ************* prueba segunda firma con tarjeta inteligente***********************
	
//qDebug("DESPUES DE doc.signWithSmartCard");
	
	
	// ********* prueba de llamada a metodo remoto de servicio web safet **************
	
	// obtener valores del archivo de configuracion	
	if (remoteContainer.compare("on",Qt::CaseInsensitive) == 0) {

#ifdef SAFET_GSOAP // Definido conexion a servicios Web
		struct soap soap;
   		bool result;
                // QString stringToReturn = doc.returnFileToString(pathid + ".xml");
                doc.writeOpenXAdESContainerToFile((founddoc)?(pathid.section(".",0,0) + ".ddoc"):"", pathid.section(".",0,0) + ".ddoc");
                QString stringToReturn = doc.returnFileToString(pathid.section(".",0,0) + ".ddoc");

		//qDebug(qPrintable(stringToReturn));
                QString url = SafetYAWL::getConfFile().getValue("XmlRepository", "xmlrepository.remote.urlwebservice").toString();
                //url = "http://192.168.37.148/cgi-bin/safet";
                //qDebug(qPrintable(url));

                SafetYAWL::streamlog << SafetLog::Action << tr("Llamando de llamar a ServicioWeb soap_call_ns__guardarXml a la direccion: \"%1\"").arg(url);
                SafetYAWL::streamlog << SafetLog::Debug << tr("Archivo a firmar remotamente: \"%1\"").arg(stringToReturn);
                SafetYAWL::streamlog << SafetLog::Debug << tr("Identificador (id) del documento a firmar remotamente: \"%1\"").arg(documentid);
		if (soap_call_ns__guardarXml(soap_new(), qPrintable(url), NULL, (char*)qPrintable(stringToReturn), (char*)qPrintable(documentid), result) == SOAP_OK)
   		{
			if (result)
			{
                                //printf("result = verdadero\n");
                                SafetYAWL::streamlog << SafetLog::Action << tr("Llamado a servicio:soap_call_ns__guardarXml exitoso...Ok!");
			}
			else
			{
                                //printf("result = false\n");
                                SafetYAWL::streamlog << SafetLog::Action << tr("Llamado a servicio:soap_call_ns__guardarXml...NO fue exitoso.. no Ok!");
			}
   		}
   		else
   		{
//			printf("\nERROR != SOAP_OK\n");
			soap_print_fault(&soap, stderr);
                        SafetYAWL::streamlog << SafetLog::Error << tr("Ocurrio un error al llamar al servicio soap_call_ns__guardarXml ");
   		}
		soap_destroy(&soap);
  		soap_end(&soap);
	  	soap_done(&soap);
#else // SAFET_GSOAP
                SYA << tr("No se encuentra definido un gestor de servicios WEB (para firmar documentos)");

#endif  // SAFET_GSOAP
	}
	else {
		// ************* prueba para escribir contenedor en sistema de archivos ***************
                doc.writeOpenXAdESContainerToFile((founddoc)?(pathid.section(".",0,0) + ".ddoc"):"", pathid.section(".",0,0) + ".ddoc");
                SafetYAWL::streamlog << SafetLog::Debug
                        << tr("Documento \"%1\" escrito correctamente").arg(pathid.section(".",0,0) + ".ddoc");

                SafetYAWL::streamlog << SafetLog::Debug << tr("addXmlDocumentToContainer ruta del contenedor de archivos XML: %1").arg(pathid.section(".",0,0));
                xmlRepository->addXmlDocumentToContainer(pathid.section(".",0,0) + ".ddoc", documentid);
	}

//	qDebug("xmlRepository->addXmlDocumentToContainer(pathid.section(".",0,0) + \".ddoc\", documentid):%s,%s",
//	qPrintable(pathid.section(".",0,0) + ".ddoc"),qPrintable(documentid));

//	qDebug("...despues...documentid");
	int r = ERR_OK;
	doc.closeLibdigidoc();
#endif // DIGIDOC

	return documentid;

}



SafetVariable*  SafetWorkflow::searchVariable(const QString& n) {
	
	foreach(SafetTask* task, getTasks() ) {
		foreach(SafetVariable* variable, task->getVariables() ) {
                     Q_CHECK_PTR( variable );
            //         qDebug("var: %s", qPrintable(variable->id()) );
			if ( variable->id() == n ) {
				return variable;
			}	
		}
	}
	return NULL;
}






QStringList SafetWorkflow::UniteWhereset() {
        QRegExp rx("([a-zA-Z_0-9\\.][a-zA-Z_0-9\\.\\(\\)]*)\\s*(\\=|>|<|<\\=|>\\=|IS|LIKE)\\s*(['\"a-zA-Z_0-9:\\-\\.]+)");
        rx.setCaseSensitivity(Qt::CaseInsensitive);
        QStringList droplist;
	QList<QString> newwhereset;
	QSet<QString> fields;
	for( int i = whereset.size()-1; i >= 0; i--) {	
		QString w = whereset.at(i);
		int pos = w.indexOf(rx);	
                qDebug(".................w: %s", qPrintable(w));
		if ( SafetYAWL::canTypeConvert(w, QVariant::Bool) ) continue;
		Q_ASSERT( pos > -1 );
		QString field = rx.cap(1);
		if (!fields.contains(field) ) {
			newwhereset.push_back( w );
			fields.insert( field );
		}
		else {	
			w.remove(QRegExp("[\\s']"));
			droplist.push_back( w );			
		}
	}
        whereset = newwhereset;
        return droplist;
}


QString SafetWorkflow::generateWhereClauses(const QString& s, int ipath) {
//        QStringList droplist = UniteWhereset();
//	QString splitresult = addFilterToKeySet(s,droplist,ipath);
        QString splitresult;
//          qDebug("...before..splitresult:%s",qPrintable(splitresult));
          splitresult  = s + ((whereset.count() > 0 ) ? " AND " : " ");
//          qDebug("...after..splitresult:%s",qPrintable(splitresult));
        int i = 0;
	for(; i < whereset.count(); i++) {
		splitresult = splitresult + "(" + whereset.at(i) +" )";		
		splitresult += " AND ";
	}
	if ( i > 0) 
		splitresult.chop(5);

	whereset.clear();
	return splitresult;
}


bool SafetWorkflow::checkFilterNULL(const QString& f) {
    if ( f.trimmed().compare("('')", Qt::CaseInsensitive) == 0 ) 
			return false;

    return true;
}


bool  SafetWorkflow::checkSourcesTask() {
     foreach(SafetTask *curtask, tasklist) {
          Q_CHECK_PTR( curtask);
          for( int i = 0 ; i < curtask->outport()->getConnectionlist().count(); i++) {
               QString source = curtask->outport()->getConnectionlist().at(i)->source();
               if ( ! searchTask( source ) && !searchCondition( source)  ) {
                    SafetYAWL::streamlog << SafetLog::Error
                            << tr("<b>Falla  de la configuración del archivo de Flujo de Trabajo"
                                  "(xml)</b>: El nodo (tarea/condición) con id: \"%1\" no se encuentra en el enlace de la tarea \"%2\"").arg(source).arg(curtask->id());
                    return false;
               }

          }
     }
     return true;
}

QString SafetWorkflow::adjustTimeZone(const QString& d, const QString& formatin,
                                      const QString& formatout) {

    QMap<QString,int> zones;
    zones["America/Caracas"] = -5*3600+3600/2; // Una hora 3600 segundos
    QString result = d;
    QString timezone = SafetYAWL::getConf()["GeneralOptions/timezone"].trimmed();
    qDebug("...timezone: |%s|",
           qPrintable(timezone));

    QDateTime mydate = QDateTime::fromString(d,formatin);
    if (zones.contains(timezone)) {
        qDebug("contains...zone..");
        mydate= mydate.addSecs(zones[timezone]);
    }
    result = mydate.toString(formatout);

    return result;

}


QString SafetWorkflow::humanizeDate(int &pdays, const QString& d, const QString& formatin,
                                    QDateTime now,
                                    HumanizeDateType typetime) {

    qDebug("\n\nentrando..SafetWorkflow::humanizeDate....");
    QString result = "";
    QDateTime mydate = QDateTime::fromString(d,formatin);

    int seconds = now.secsTo(mydate) ;

    int months;
    int weeks;
    int days;
    int hours;
    int minutes;
    int timediff;
    if (seconds < 0) {
        seconds = -1*seconds; // cambiarlo a valor positivo
    }

    // Valor devuelto pdays *****
    pdays = seconds/ ( 60 * 60 * 24 );
    // Valor devuelto pdays *****

    timediff = seconds;
    months = -1;

    QDateTime mymonthdate = now;

    while( mymonthdate > mydate ) {
        months++;
        mymonthdate = mymonthdate.addMonths(-1);
    }

    if ( months > 0 ) {
        mymonthdate = mymonthdate.addMonths(1);
        qDebug("....SafetWorkflow::humanizeDate....mymonthdate:|%s|",qPrintable(mymonthdate.toString()));
        int myseconds = now.secsTo(mymonthdate);
        if (myseconds < 0 ) {
            myseconds = -1 * myseconds;
        }
        qDebug("....SafetWorkflow::humanizeDate....myseconds..:|%d|",myseconds);
        if ( myseconds < timediff ) {
            timediff =  timediff - myseconds;
        }
        else {
            timediff = 0;
        }
        qDebug("....SafetWorkflow::humanizeDate....timediff (2)..:|%d|",timediff);

    }

//    timediff =  timediff - months*( 60 * 60 * 24 * 7 * 4);
    weeks = timediff / ( 60 * 60 * 24 * 7);
    qDebug("....SafetWorkflow::humanizeDate....weeks..:|%d|",weeks);

    timediff =  timediff - weeks*( 60 * 60 * 24 * 7);
    days = timediff / ( 60 * 60 * 24 );
    timediff =  timediff - days*( 60 * 60 * 24);
    hours = timediff / ( 60 * 60 );
    timediff =  timediff - hours*( 60 * 60 );
    minutes = timediff / ( 60 );
    timediff =  timediff - minutes*( 60 );
    seconds = timediff - minutes;


    if (months > 0 ) {
        if (months == 1) {
            result =  result + tr("1 month ");
        }
        else {
            result = result + tr("%1 months").arg(months);
        }
        if (weeks == 1) {
            result = result + tr(" and 1 week");
        }
        else if (weeks > 1) {
            result = result + tr(" and %1 week%2").arg(weeks).arg(weeks>1?"s ":"");
        }

    }
    else {
        if (weeks > 0) {
            if (weeks == 1) {
                result =  result + tr("1 week ");
            }
            else {
                result = result + tr("%1 weeks ").arg(weeks);
            }

            if (days > 1) {
                result = result + tr("and %1 day%2").arg(days).arg(days>1?"s ":"");
            }
        }

        else {
            if (days > 0) {
                if (days == 1) {
                    result = tr("yesterday");
                }
                else if ( days == 2 ) {
                    result =  tr("day before yesterday");
                }
                else {
                    result = result + tr("%1 days").arg(days);
                }
                if (hours > 1) {
                    if (result == tr("yesterday")) {
                        result = tr("a day") + tr(" and %1 hour%2").arg(hours).arg(hours>1?"s ":"");
                    }
                    else if (result == tr("day before yesterday")) {
                        result = tr("2 days") + tr(" and %1 hour%2").arg(hours).arg(hours>1?"s ":"");
                    }
                    else {
                        result = result + tr(" and %1 hour%2").arg(hours).arg(hours>1?"s ":"");
                    }

                }
            }
            else {
                if (hours > 0) {
                    if (hours == 1) {
                        result = result + tr("1 hour ");
                    }
                    else {
                        result = result + tr("%1 hours ").arg(hours);
                    }
                    if (minutes > 1) {
                        result = result + tr("and %1 minute%2").arg(minutes).arg(minutes>1?"s ":"");
                    }
                }
                else {
                    if (minutes > 0 ) {
                        if (minutes == 1) {
                            result = result + tr("1 minute ");
                        }
                        else {
                            result = result + tr("%1 minutes ").arg(minutes);
                        }
                    }
                    else {
                        if (seconds > 1) {
                            result = result + tr("%1 seconds ").arg(seconds);
                        }
                        else {
                            result = result + tr("just a moment");
                        }
                    }
                }
            }
        }
    }
    QString contexttime;
    switch(typetime) {
    case None:
        result.replace(tr("day before yesterday"),tr("2 days"));
        result.replace(tr("yesterday"),tr("one day"));
         contexttime = result;
        break;
    case SinceWhenTime:

        if (result.endsWith("yesterday")) {
            contexttime = tr("%1 come here").arg(result);
        }
        else {
            contexttime = tr(" %1 ago").arg(result);
        }
        break;
    case WaitTime:
        if (result == tr("yesterday")) {
            contexttime = tr("%1 waiting").arg(tr("a day"));
        }
        else if (result == tr("day before yesterday")) {
            contexttime = tr("%1 waiting").arg(tr("2 days"));

        }
        else {
            contexttime = tr("%1 waiting").arg(result);
        }

        break;
    }

    return contexttime;

}
